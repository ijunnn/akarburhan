<!--Scroll Indicator Load-->
<div class='progress-container'>
	<div class='progress-bar' id='progressbar'/>
</div>

<script>
//<![CDATA[
window.addEventListener("scroll", myFunction);
function myFunction() {
	var winScroll = document.body.scrollTop || document.documentElement.scrollTop;
	var height = document.documentElement.scrollHeight - document.documentElement.clientHeight;
	var scrolled = (winScroll / height) * 100;
	document.getElementById("progressbar").style.width = scrolled + "%";
}
//]]>
</script>

<!-- Scroll -->
<script type='text/javascript'>
//<![CDATA[
$(".back-top").each(function(){var $this=$(this);$(window).on("scroll",function(){$(this).scrollTop()>=100?$this.fadeIn(250):$this.fadeOut(250)}),$this.click(function(){$("html, body").animate({scrollTop:0},500)})});
//]]>
</script>
<div class="back-top" title="Back to Top"></div>


<!--Nofollow-->
<script type="text/javascript">
//<![CDATA[
$("a").filter(function() {
	return this.hostname && this.hostname !== location.hostname
}).attr("rel", "dofollow").attr("target", "_blank");
//]]>
</script>