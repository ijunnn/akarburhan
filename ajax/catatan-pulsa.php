<?php
//
//SC	:	KM Panel - SMM & PPOB
//Dev	:	BlackRose / Adi Gunawan, S.Kom., M.Kom.
//By	:	401XD Group Indonesia
//Email	:	mycoding@401xd.com / 401xdssh@gmail.com
//
//▪ http://401xd.com / http://mycoding.net
//▪ http://youtube.com/c/MyCodingXD
//▪ http://instagram.com/MyCodingXD
//▪ http://facebook.com/MyCodingXD
//
//Hak cipta 2017-2021
//Terakhir dikembangkan Februari 2021
//Dilarang menjual/membagikan script KM Panel. Serta mengubah/menghapus copyright ini!
//

session_start();
require_once '../config.php';

if (!isset($_SESSION['user'])) {
	die("Anda Tidak Memiliki Akses!");
}
if (isset($_POST['layanan'])) {
	$post_layanan = $conn->real_escape_string($_POST['layanan']);
	$cek_layanan = $conn->query("SELECT * FROM layanan_pulsa WHERE provider_id = '$post_layanan' AND status = 'Normal'");
	if (mysqli_num_rows($cek_layanan) == 1) {
		$data_layanan = mysqli_fetch_assoc($cek_layanan);
		?>
		 <div class="alert alert-info" role="alert">
            <h4 class="alert-heading"><b>Deskripsi</b><br /></h4>
                <div class="alert-body">
                	• Tujuan harus valid sesuai produk yang dipilih.<br /><br /><br />
                <span><?= nl2br($data_layanan['catatan']); ?></span><hr>

			<b>Nama:</b> <?php echo($data_layanan['layanan']); ?><br />
			<b>Kategori:</b> <?php echo($data_layanan['operator']); ?><br />
			<b>Harga:</b> Rp. <?php echo number_format($data_layanan['harga'],0,',','.'); ?>
                </div>
             </div>

		<?php
	} else {
		?>
		<div class="alert alert-icon alert-danger alert-dismissible fade in" role="alert">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true">&times;</span>
			</button>
			<i class="mdi mdi-block-helper"></i>
			<b>Gagal :</b> Service Tidak Ditemukan
		</div>
		<?php
	}
} else {
	?>
	<div class="alert alert-icon alert-danger alert-dismissible fade in" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			<span aria-hidden="true">&times;</span>
		</button>
		<i class="mdi mdi-block-helper"></i>
		<b>Gagal : </b> Terjadi Kesalahan.
	</div>
	<?php
}