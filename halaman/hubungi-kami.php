<?php
session_start();
require '../config.php';
require '../lib/header.php';
?>

<div class="app-content content ">
    <div class="content-overlay"></div>
    <div class="header-navbar-shadow"></div>
    <div class="content-wrapper">
        <div class="content-header row">
        </div>
        <!--Title-->
        <title>Hubungi <?php echo $data['short_title']; ?></title>
        <meta name="description" content="Jangan ragu menghubungi kami melalui whatsapp 081392814105, halaman kontak, atau via email support@boostnaa.com." />

        <!--OG2-->
        <meta content="Hubungi <?php echo $data['short_title']; ?>" property="og:title" />
        <meta content="Jangan ragu menghubungi kami melalui whatsapp 081392814105, halaman kontak, atau via email support@azfapanel.com." property="og:description" />
        <meta content="<?php echo $data['short_title']; ?> - Hubungi <?php echo $data['short_title']; ?>" property="og:headline" />
        <meta content="<?php echo $config['web']['url']; ?>assets/images/kincaimedia/kincaimedia256.png" property="og:image" />
        <meta content="Hubungi <?php echo $data['short_title']; ?>" property="twitter:title" />
        <meta content="Jangan ragu menghubungi kami melalui whatsapp 081392814105, halaman kontak, atau via email support@azfapanel.com." property="twitter:description" />
        <meta content="<?php echo $config['web']['url']; ?>assets/images/kincaimedia/kincaimedia256.png" property="twitter:image" />

        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-body table-responsive">
                        <center>
                            <h1 class="m-t-0 text-uppercase text-center header-title"><b>Hubungi <?php echo $data['short_title']; ?></b></h1>
                        </center>
                        <div class="box-content text-center">
                            <a href="../halaman/daftar-harga" title="<?php echo $data['short_title']; ?>" alt="<?php echo $data['title']; ?>"> <img src="../assets/images/logo1.png" class="img-responsive" width="400px"></a>
                        </div>
                    </div>

                    <div class="card-body table-responsive">
                        <center>
                            <div class="m-t-0 header-title"><b> KONTAK </b></div>
                            Jangan ragu menghubungi kami melalui whatsapp 081287000274, halaman Kontak
                        </center>
                        <table class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                            <tbody>
                                <tr>
                                    <td align="left" style="width: 50%">
                                        <a href="https://api.whatsapp.com/send?phone=6281287000274&text=Hallo%20Admin" class="btn btn-primary btn-bordred btn-rounded waves-effect waves-light" style="width: 115px" target="_blank" rel="nofollow noopener"><i class="mdi mdi-whatsapp"></i> WHATSAPP</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" style="width: 50%;">
                                        <a href="#" class="btn btn-primary btn-bordred btn-rounded waves-effect waves-light" style="width: 118px" target="_blank" rel="nofollow noopener"><i class="mdi mdi-telegram"></i> TELEGRAM</a>
                                    </td>
                                    <td align="left" style="width: 50%">
                                        <a href="https://youtu.be/9yChorWeoiI" alt="Kerja Sama Periklanan" alt="Kerja Sama Periklanan" class="btn btn-primary btn-bordred btn-rounded waves-effect waves-light" style="width: 118px" target="_blank" rel="dofollow"><i class="mdi mdi-heart"></i> IKLAN</a>
                                    </td>
                                </tr>
                            </tbody>
                        </table>

                        <center>
                            <div class="m-t-0 header-title"><b> SOSIAL MEDIA </b></div>
                        </center>
                        <table class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                            <tbody>
                                <tr>
                                    <td align="right" style="width: 50%;">
                                        <a href="#" class="btn btn-primary btn-bordred btn-rounded waves-effect waves-light" style="width: 118px" target="_blank" rel="nofollow noopener"><i class="mdi mdi-facebook"></i> FACEBOOK</a>
                                    </td>
                                </tr>

                                <tr>
                                    <td align="right" style="width: 50%;">
                                        <a href="#" class="btn btn-primary btn-bordred btn-rounded waves-effect waves-light" style="width: 118px" target="_blank" rel="nofollow noopener"><i class="mdi mdi-whatsapp"></i> JOIN GRUP 1</a>
                                    </td>
                                    <td align="left" style="width: 50%;">
                                        <a href="#" class="btn btn-primary btn-bordred btn-rounded waves-effect waves-light" style="width: 118px" target="_blank" rel="nofollow noopener"><i class="mdi mdi-whatsapp"></i> JOIN GRUP 2</a>
                                    </td>
                                </tr>
                            </tbody>
                        </table>

                        <center>
                            <div class="m-t-0 header-title"><b> MAU PUNYA WEBSITE SEPERTI KAMI ? </b></div>
                            <div><br />
                                <a href="https://api.whatsapp.com/send?phone=6281384248407&text=Hallo%20Admin" class="btn btn-primary btn-bordred btn-rounded waves-effect waves-light" style="width: 200px" target="_blank" rel="nofollow noopener"><i class="mdi mdi-whatsapp"></i> KLIK DISINI</a>
                            </div>
                            <br />



                        </center>

                    </div>

                </div>
            </div>
        </div>

        <?php
        require '../lib/footer.php';
        ?>