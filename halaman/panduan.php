<?php
session_start();
require '../config.php';
require '../lib/header.php';
?>
    <div class="app-content content ">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-md-9 col-12 mb-2">
                    <div class="row breadcrumbs-top">
                        <div class="col-12">
            
                        </div>
                    </div>
                </div>
                <div class="content-header-right text-md-right col-md-3 col-12 d-md-block d-none">
                    <div class="form-group breadcrumb-right">
                        <div class="dropdown">
                           
                    </div>
                </div>
            </div>
            <div class="content-body">
                <!-- search header -->
                <section id="faq-search-filter">
                    <div class="card faq-search" style="background-image: url('/build/app-assets/images/banner/banner.png')">
                        <div class="card-body text-center">
                            <!-- main title -->
                            <h2 class="text-primary">Let's answer some questions</h2>

                            <!-- subtitle -->
                            <p class="card-text mb-2">Berikut adalah pernyataan yang sering ditanyakan pada panel kami.</p>

                            
                        </div>
                    </div>
                </section>
                <!-- /search header -->

                <!-- frequently asked questions tabs pills -->
                <section id="faq-tabs">
                    <!-- vertical tab pill -->
                    <div class="row">
                        <div class="col-lg-3 col-md-4 col-sm-12">
                            <div class="faq-navigation d-flex justify-content-between flex-column mb-2 mb-md-0">
                                <!-- pill tabs navigation -->
                                <ul class="nav nav-pills nav-left flex-column" role="tablist">
                                    <!-- payment -->
                                    <li class="nav-item">
                                        <a class="nav-link active" id="payment" data-toggle="pill" href="#faq-payment" aria-expanded="true" role="tab">
                                            <i data-feather='alert-octagon' class="font-medium-3 mr-1"></i>
                                            <span class="font-weight-bold">Ketentuan Umum</span>
                                        </a>
                                    </li>

                                    <!-- delivery -->
                                    <li class="nav-item">
                                        <a class="nav-link" id="delivery" data-toggle="pill" href="#faq-delivery" aria-expanded="false" role="tab">
                                           <i data-feather='camera-off' class="font-medium-3 mr-1"></i>
                                            <span class="font-weight-bold">Kebijakan Privasi</span>
                                        </a>
                                    </li>

                                    <!-- cancellation and return -->
                                    <li class="nav-item">
                                        <a class="nav-link" id="cancellation-return" data-toggle="pill" href="#faq-cancellation-return" aria-expanded="false" role="tab">
                                            <i data-feather="check-square" class="font-medium-3 mr-1"></i>
                                            <span class="font-weight-bold">reffil and refund</span>
                                        </a>
                                    </li>

                                    <!-- my order -->
                                    <li class="nav-item">
                                        <a class="nav-link" id="my-order" data-toggle="pill" href="#faq-my-order" aria-expanded="false" role="tab">
                                            <i data-feather="credit-card" class="font-medium-3 mr-1"></i>
                                            <span class="font-weight-bold">Cara Deposit</span>
                                        </a>
                                    </li>

                                    <!-- product and services-->
                                    <li class="nav-item">
                                        <a class="nav-link" id="product-services" data-toggle="pill" href="#faq-product-services" aria-expanded="false" role="tab">
                                            <i data-feather="settings" class="font-medium-3 mr-1"></i>
                                            <span class="font-weight-bold">Memulai Transaksi</span>
                                        </a>
                                    </li>
                                </ul>

                                <!-- FAQ image -->
                                <img src="../build/app-assets/images/illustration/faq-illustrations.svg" class="img-fluid d-none d-md-block" alt="demand img" />
                            </div>
                        </div>

                        <div class="col-lg-9 col-md-8 col-sm-12">
                            <!-- pill tabs tab content -->
                            <div class="tab-content">
                                <!-- payment panel -->
                                <div role="tabpanel" class="tab-pane active" id="faq-payment" aria-labelledby="payment" aria-expanded="true">
                                    <!-- icon and header -->
                                    <div class="d-flex align-items-center">
                                        <div class="avatar avatar-tag bg-light-primary mr-1">
                                            <i data-feather='alert-octagon' class="font-medium-4"></i>
                                        </div>
                                        <div>
                                            <h4 class="mb-0">Ketentuan Umum</h4>
                                            <span>Berikut Ketentuan Umum Yang Berlaku di <?php echo $data['short_title']; ?></span>
                                        </div>
                                    </div>

                                    <!-- frequent answer and question  collapse  -->
                                    <div class="collapse-margin collapse-icon mt-2" id="faq-payment-qna">
                                        <div class="card">
                                            <div class="card-header" id="paymentOne" data-toggle="collapse" role="button" data-target="#faq-payment-one" aria-expanded="false" aria-controls="faq-payment-one">
                                                <span class="lead collapse-title">Ketentuan Layanan Pertama</span>
                                            </div>

                                            <div id="faq-payment-one" class="collapse" aria-labelledby="paymentOne" data-parent="#faq-payment-qna">
                                                <div class="card-body">
                                                    Dengan mendaftar dan menggunakan layanan <?php echo $data['short_title']; ?>T, Anda secara otomatis menyetujui semua Ketentuan Layanan kami. Kami berhak mengubah Ketentuan Layanan ini tanpa pemberitahuan terlebih dahulu. Anda diharapkan membaca semua Ketentuan Layanan kami sebelum membuat pesanan.
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card">
                                            <div class="card-header" id="paymentTwo" data-toggle="collapse" role="button" data-target="#faq-payment-two" aria-expanded="true" aria-controls="faq-payment-two">
                                                <span class="lead collapse-title">Ketentuan Layanan Kedua</span>
                                            </div>
                                            <div id="faq-payment-two" class="collapse show" aria-labelledby="paymentTwo" data-parent="#faq-payment-qna">
                                                <div class="card-body">
                                                    <?php echo $data['short_title']; ?> tidak akan bertanggung jawab jika Anda mengalami kerugian dalam bisnis Anda.
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card">
                                            <div class="card-header" id="paymentThree" data-toggle="collapse" role="button" data-target="#faq-payment-three" aria-expanded="false" aria-controls="faq-payment-three">
                                                <span class="lead collapse-title">Ketentuan Layanan Ketiga</span>
                                            </div>
                                            <div id="faq-payment-three" class="collapse" aria-labelledby="paymentThree" data-parent="#faq-payment-qna">
                                                <div class="card-body">
                                                    <?php echo $data['short_title']; ?> tidak bertanggung jawab jika Anda mengalami suspensi akun atau penghapusan kiriman yang dilakukan oleh Instagram, Twitter, Facebook, Youtube, dan lain-lain.
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card">
                                            <div class="card-header" id="paymentFour" data-toggle="collapse" role="button" data-target="#faq-payment-four" aria-expanded="false" aria-controls="faq-payment-four">
                                                <span class="lead collapse-title">Ketentuan Layanan Ke-empat</span>
                                            </div>
                                            <div id="faq-payment-four" class="collapse" aria-labelledby="paymentFour" data-parent="#faq-payment-qna">
                                                <div class="card-body">
                                                    Layanan sosial media <?php echo $data['short_title']; ?> hanya membantu untuk membuat akun sosial anda lebih terlihat menarik, dan tidak ada jaminan bahwa pengikut yang bertambah akan dapat berinteraksi dengan anda.
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card">
                                            <div class="card-header" id="paymentFive" data-toggle="collapse" role="button" data-target="#faq-payment-five" aria-expanded="false" aria-controls="faq-payment-five">
                                                <span class="lead collapse-title">
                                                   Ketentuan Layanan Kelima
                                                </span>
                                            </div>
                                            <div id="faq-payment-five" class="collapse" aria-labelledby="paymentFive" data-parent="#faq-payment-qna">
                                                <div class="card-body">
                                                    Layanan <?php echo $data['short_title']; ?> tidak pernah berafiliasi atau bekerjasama dengan pihak manapun untuk menjual / membeli panel, pendaftaran bersifat gratis dan apabila ada pihak yang mengatasnamakan <?php echo $data['short_title']; ?> untuk penjualan berjenis kerjasama maka itu bukanlah kami dan kami tidak bertanggung jawab jika hal itu terjadi.
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!-- delivery panel -->
                                <div class="tab-pane" id="faq-delivery" role="tabpanel" aria-labelledby="delivery" aria-expanded="false">
                                    <!-- icon and header -->
                                    <div class="d-flex align-items-center">
                                        <div class="avatar avatar-tag bg-light-primary mr-1">
                                            <i data-feather="shopping-bag" class="font-medium-4"></i>
                                        </div>
                                        <div>
                                            <h4 class="mb-0">Delivery</h4>
                                            <span>Which license do I need?</span>
                                        </div>
                                    </div>

                                    <!-- frequent answer and question  collapse  -->
                                    <div class="collapse-margin collapse-icon mt-2" id="faq-delivery-qna">
                                        <div class="card">
                                            <div class="card-header" id="deliveryOne" data-toggle="collapse" role="button" data-target="#faq-delivery-one" aria-expanded="false" aria-controls="faq-delivery-one">
                                                <span class="lead collapse-title">Kebijakan Privasi Pertama</span>
                                            </div>

                                            <div id="faq-delivery-one" class="collapse" aria-labelledby="deliveryOne" data-parent="#faq-delivery-qna">
                                                <div class="card-body">
                                                     Kami menjaga privasi Anda dengan serius dan akan mengambil semua tindakan untuk melindungi informasi pribadi Anda.
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card">
                                            <div class="card-header" id="deliveryTwo" data-toggle="collapse" role="button" data-target="#faq-delivery-two" aria-expanded="false" aria-controls="faq-delivery-two">
                                                <span class="lead collapse-title">
                                                   Kebijakan Privasi Kedua
                                                </span>
                                            </div>
                                            <div id="faq-delivery-two" class="collapse" aria-labelledby="deliveryTwo" data-parent="#faq-delivery-qna">
                                                <div class="card-body">
                                                    Setiap informasi pribadi yang diterima hanya akan digunakan untuk mengisi pesanan Anda. Kami tidak akan menjual atau mendistribusikan ulang informasi Anda kepada siapa pun. Semua informasi dienkripsi dan disimpan di server yang aman.
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card">
                                            <div class="card-header" id="deliveryThree" data-toggle="collapse" role="button" data-target="#faq-delivery-three" aria-expanded="false" aria-controls="faq-delivery-three">
                                                <span class="lead collapse-title">Kebijakan Privasi Ketiga</span>
                                            </div>
                                            <div id="faq-delivery-three" class="collapse" aria-labelledby="deliveryThree" data-parent="#faq-delivery-qna">
                                                <div class="card-body">
                                                    Cookie adalah satu bagian data yang disimpan di komputer pengguna yang berkaitan dengan informasi tentang pengguna tersebut. Kami menggunakan session ID cookie ketika pengguna menutup browser, cookie tersebut dihapus. Cookie juga digunakan untuk manajemen session di website kami. Pengguna harus mengaktifkan cookie untuk menggunakan website kami.
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card">
                                            <div class="card-header" id="deliveryFour" data-toggle="collapse" role="button" data-target="#faq-delivery-four" aria-expanded="false" aria-controls="faq-delivery-four">
                                                <span class="lead collapse-title">
                                                   Kebijakan Privasi Keempat
                                                </span>
                                            </div>
                                            <div id="faq-delivery-four" class="collapse" aria-labelledby="deliveryFour" data-parent="#faq-delivery-qna">
                                                <div class="card-body">
                                                    <?php echo $data['short_title']; ?> menyimpan IP (Internet Protocol) address, atau lokasi komputer Anda di Internet, untuk keperluan administrasi sistem dan troubleshooting. Kami menggunakan IP address secara keseluruhan (agregat) untuk mengetahui lokasi-lokasi yang mengakses website kami.
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card">
                                            <div class="card-header" id="deliveryFive" data-toggle="collapse" role="button" data-target="#faq-delivery-five" aria-expanded="false" aria-controls="faq-delivery-five">
                                                <span class="lead collapse-title">
                                                   Kebijakan Privasi Kelima
                                                </span>
                                            </div>
                                            <div id="faq-delivery-five" class="collapse" aria-labelledby="deliveryFive" data-parent="#faq-delivery-qna">
                                                <div class="card-body">
                                                    Pada data log files, data ini hanya akan digunakan dalam bentuk agregat (keseluruhan) untuk menganalisa aktivitas login dan logout penggunaan situs.
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!-- cancellation return  -->
                                <div class="tab-pane" id="faq-cancellation-return" role="tabpanel" aria-labelledby="cancellation-return" aria-expanded="false">
                                    <!-- icon and header -->
                                    <div class="d-flex align-items-center">
                                        <div class="avatar avatar-tag bg-light-primary mr-1">
                                            <i data-feather="refresh-cw" class="font-medium-4"></i>
                                        </div>
                                        <div>
                                            <h4 class="mb-0">Refill and Refund</h4>
                                            <span>Kebijakan Reffil and Refund di panel kami</span>
                                        </div>
                                    </div>

                                    <!-- frequent answer and question  collapse  -->
                                    <div class="collapse-margin collapse-icon mt-2" id="faq-cancellation-qna">
                                        <div class="card">
                                            <div class="card-header" id="cancellationOne" data-toggle="collapse" role="button" data-target="#faq-cancellation-one" aria-expanded="false" aria-controls="faq-cancellation-one">
                                                <span class="lead collapse-title">
                                                   Kebijakan Pertama
                                                </span>
                                            </div>

                                            <div id="faq-cancellation-one" class="collapse" aria-labelledby="cancellationOne" data-parent="#faq-cancellation-qna">
                                                <div class="card-body">
                                                   Semua proses refill atau isi ulang dilakukan secara otomatis untuk layanan sosial media, ini berlaku jika data pesanan anda adalah layanan refill. Kami tidak memiliki estimasi waktu kapan pesanan akan selesai direfill, semua pesanan yang ada dalam daftar laporan akan di konfirmasi seperti biasa jika pesanan sudah selesai refill.
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card">
                                            <div class="card-header" id="cancellationTwo" data-toggle="collapse" role="button" data-target="#faq-cancellation-two" aria-expanded="false" aria-controls="faq-cancellation-two">
                                                <span class="lead collapse-title">Kebijakan Kedua</span>
                                            </div>
                                            <div id="faq-cancellation-two" class="collapse" aria-labelledby="cancellationTwo" data-parent="#faq-cancellation-qna">
                                                <div class="card-body">
                                                    Kami tidak akan memberikan pengembalian dana pada pesanan Anda, kecuali jika kami memberi informasi pengembalian dana pada layanan tersebut. Semua layanan bersifat otomatis, jika terjadi Error atau Partial maka refund otomatis sesuai jumlah yang belum diproses sistem.
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card">
                                            <div class="card-header" id="cancellationThree" data-toggle="collapse" role="button" data-target="#faq-cancellation-three" aria-expanded="false" aria-controls="faq-cancellation-three">
                                                <span class="lead collapse-title">Kebijakan Ketiga</span>
                                            </div>
                                            <div id="faq-cancellation-three" class="collapse" aria-labelledby="cancellationThree" data-parent="#faq-cancellation-qna">
                                                <div class="card-body">
                                                     Kami tidak melakukan garansi untuk setiap layanan dengan kondisi No Refill seperti yang telah dijelaskan pada note layanan dan deskripsi layanan.
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card">
                                            <div class="card-header" id="cancellationFour" data-toggle="collapse" role="button" data-target="#faq-cancellation-four" aria-expanded="false" aria-controls="faq-cancellation-four">
                                                <span class="lead collapse-title">Kebijakan Keempat</span>
                                            </div>
                                            <div id="faq-cancellation-four" class="collapse" aria-labelledby="cancellationFour" data-parent="#faq-cancellation-qna">
                                                <div class="card-body">
                                                   Kesalahan input oleh pengguna bukan tanggung jawab kami, dengan melakukan order artinya pengguna telah melakukan cek sebelum diinput, berakibat tidak akan refund karena kesalahan input.
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!-- my order -->
                                <div class="tab-pane" id="faq-my-order" role="tabpanel" aria-labelledby="my-order" aria-expanded="false">
                                    <!-- icon and header -->
                                    <div class="d-flex align-items-center">
                                        <div class="avatar avatar-tag bg-light-primary mr-1">
                                            <i data-feather="dollar-sign" class="font-medium-4"></i>
                                        </div>
                                        <div>
                                            <h4 class="mb-0">Cara Deposit</h4>
                                            <span>Panduan Cara Deposit</span>
                                        </div>
                                    </div>

                                    <!-- frequent answer and question  collapse  -->
                                    <div class="collapse-margin collapse-icon mt-2" id="faq-my-order-qna">
                                        <div class="card">
                                            <div class="card-header" id="myOrderOne" data-toggle="collapse" role="button" data-target="#faq-my-order-one" aria-expanded="false" aria-controls="faq-my-order-one">
                                                <span class="lead collapse-title">MEMILIH METODE DEPOSIT</span>
                                            </div>

                                            <div id="faq-my-order-one" class="collapse" aria-labelledby="myOrderOne" data-parent="#faq-my-order-qna">
                                                <div class="card-body">
                                                    Lakukan permintaan deposit pada halaman member dengan memilih menu Deposit Baru. Lalu memilih metode dan nominal deposit. Sistem akan menampilkan detail deposit berupa nominal transfer & tujuan transfer sesuai metode pembayaran yang Anda pilih.
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card">
                                            <div class="card-header" id="myOrderTwo" data-toggle="collapse" role="button" data-target="#faq-my-order-two" aria-expanded="false" aria-controls="faq-my-order-two">
                                                <span class="lead collapse-title">
                                                   TRANSFER PEMBAYARAN
                                                </span>
                                            </div>
                                            <div id="faq-my-order-two" class="collapse" aria-labelledby="myOrderTwo" data-parent="#faq-my-order-qna">
                                                <div class="card-body">
                                                    Anda akan diminta untuk melakukan transfer sejumlah nominal yang tertera pada detail deposit, nominal transfer ditambah dengan <b>3 Angka Unik</b> di belakang agar konfirmasi pembayaran otomatis. Disarankan transfer pembayaran sesuai nominal yang tertera.
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card">
                                            <div class="card-header" id="myOrderThree" data-toggle="collapse" role="button" data-target="#faq-my-order-three" aria-expanded="false" aria-controls="faq-my-order-three">
                                                <span class="lead collapse-title"> KONFIRMASI PEMBAYARAN </span>
                                            </div>
                                            <div id="faq-my-order-three" class="collapse" aria-labelledby="myOrderThree" data-parent="#faq-my-order-qna">
                                                <div class="card-body">
                                                    Konfirmasi pembayaran cukup dengan klik tombol konfirmasi pada halaman invoice setelah transfer. Jika jumlah transfer sesuai dan valid, maka saldo akan bertambah otomatis. Jika ada kesalahan nominal dan gagal, silahkan buat tiket bantuan di halaman Hubungi Customer Support.
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!-- product services -->
                                <div class="tab-pane" id="faq-product-services" role="tabpanel" aria-labelledby="product-services" aria-expanded="false">
                                    <!-- icon and header -->
                                    <div class="d-flex align-items-center">
                                        <div class="avatar avatar-tag bg-light-primary mr-1">
                                            <i data-feather="settings" class="font-medium-4"></i>
                                        </div>
                                        <div>
                                            <h4 class="mb-0">Memulai Transaksi</h4>
                                            <span>Panduan Memulai Transaksi</span>
                                        </div>
                                    </div>

                                    <!-- frequent answer and question  collapse  -->
                                    <div class="collapse-margin collapse-icon mt-2" id="faq-product-qna">
                                        <div class="card">
                                            <div class="card-header" id="productOne" data-toggle="collapse" role="button" data-target="#faq-product-one" aria-expanded="false" aria-controls="faq-product-one">
                                                <span class="lead collapse-title">
                                                    DAFTAR AKUN
                                                </span>
                                            </div>

                                            <div id="faq-product-one" class="collapse" aria-labelledby="productOne" data-parent="#faq-product-qna">
                                                <div class="card-body">
                                                    Segera daftarkan diri Anda menjadi Member/Agen/Reseller di Java Pedia, pendaftaran member tidak akan dikenakan biaya apapun [GRATIS] selamanya.
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card">
                                            <div class="card-header" id="productTwo" data-toggle="collapse" role="button" data-target="#faq-product-two" aria-expanded="false" aria-controls="faq-product-two">
                                                <span class="lead collapse-title">
                                                   DEPOSIT SALDO
                                                </span>
                                            </div>
                                            <div id="faq-product-two" class="collapse" aria-labelledby="productTwo" data-parent="#faq-product-qna">
                                                <div class="card-body">
                                                    Silahkan login akun Anda dan menuju ke halaman Deposit Baru. Kami menyediakan metode deposit melalui Bank, E-Money, Pulsa & Voucher dengan berbagai sistem.
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card">
                                            <div class="card-header" id="productThree" data-toggle="collapse" role="button" data-target="#faq-product-three" aria-expanded="false" aria-controls="faq-product-three">
                                                <span class="lead collapse-title">MULAI TRANSAKSI</span>
                                            </div>
                                            <div id="faq-product-three" class="collapse" aria-labelledby="productThree" data-parent="#faq-product-qna">
                                                <div class="card-body">
                                                   Setelah 2 langkah sebelumnya selesai, Anda dapat melakukan transaksi pembelian semua Layanan Jasa & Produk Digital/Virtual yang tersedia di <?php echo $data['short_title']; ?>
                                                </div>
                                            </div>
                                        </div>
                                    
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!-- / frequently asked questions tabs pills -->

                <!-- contact us -->
                <section class="faq-contact">
                    <div class="row mt-5 pt-75">
                        <div class="col-12 text-center">
                            <h2>Anda masih memiliki pertanyaan?</h2>
                            <p class="mb-3">
                                Jika Anda tidak dapat menemukan pertanyaan di FAQ kami, Anda selalu dapat menghubungi kami. Kami akan segera menjawab Anda!
                            </p>
                        </div>
                        <div class="col-sm-6">
                            <div class="card text-center faq-contact-card shadow-none py-1">
                                <div class="card-body">
                                    <div class="avatar avatar-tag bg-light-primary mb-2 mx-auto">
                                        <i data-feather="phone-call" class="font-medium-3"></i>
                                    </div>
                                    <h4>+62 812 7611 0730</h4>
                                    <span class="text-body">Kami selalu senang membantu!</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="card text-center faq-contact-card shadow-none py-1">
                                <div class="card-body">
                                    <div class="avatar avatar-tag bg-light-primary mb-2 mx-auto">
                                        <i data-feather="mail" class="font-medium-3"></i>
                                    </div>
                                    <h4>support@boostnaa.com</h4>
                                    <span class="text-body">Cara terbaik untuk mendapatkan jawaban lebih cepat!</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!--/ contact us -->

            </div>
        </div>
    </div>
    <!-- END: Content-->
<?php
require '../lib/footer.php';
?>
