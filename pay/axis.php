<?php
session_start();
require("../config.php");
require '../lib/session_user.php';
if (isset($_POST['request'])) {
	require '../lib/session_login.php';

	$post_tipe = $conn->real_escape_string($_POST['tipe']);		
	$post_provider = $conn->real_escape_string($_POST['provider']);
	$post_pembayaran = $conn->real_escape_string($_POST['pembayaran']);
	$post_jumlah = $conn->real_escape_string(trim(filter($_POST['jumlah'])));
	$post_pengirim = $conn->real_escape_string(trim(filter($_POST['pengirim'])));

	$cek_metod = $conn->query("SELECT * FROM metode_depo WHERE id = '$post_provider'");
	$data_metod = $cek_metod->fetch_assoc();
	$cek_metod_rows = mysqli_num_rows($cek_metod);

	$cek_provider = $conn->query("SELECT * FROM provider_pulsa WHERE code = 'DPEDIA'");
	$data_provider = mysqli_fetch_assoc($cek_provider);

	$cek_depo = $conn->query("SELECT * FROM deposit WHERE username = '$sess_username' AND status = 'Pending'");
	$data_depo = $cek_depo->fetch_assoc();
	$count_depo = mysqli_num_rows($cek_depo);

	$kode = acak_nomor(3).acak_nomor(3);


	if (!$post_provider || !$post_pembayaran || !$post_jumlah) {
		$_SESSION['hasil'] = array('alert' => 'danger', 'judul' => 'Permintaan Gagal', 'pesan' => 'Silahkan lengkapi bidang berikut :<br/> - Tipe Pembayaran <br /> - Provider Pembayaran <br /> - Pembayaran <br /> - Pengirim <br /> - Jumlah');

	} else if ($cek_metod_rows == 0) {
		$_SESSION['hasil'] = array('alert' => 'danger', 'judul' => 'Permintaan Gagal', 'pesan' => 'Metode Deposit Tidak Tersedia.');

	} else if ($count_depo >= 1) {
		$_SESSION['hasil'] = array('alert' => 'danger', 'judul' => 'Permintaan Gagal', 'pesan' => 'Masih Terdapat Deposit Yang Berstatus Pending.');
	} else if ($post_jumlah < 10000) {
		$_SESSION['hasil'] = array('alert' => 'danger', 'judul' => 'Permintaan Gagal', 'pesan' => 'Minimal Deposit Saldo 10000.');		

	} else {

		$postdata = "api_key=".$data_provider['key']."&nopengirim=$post_pengirim&quantity=$post_jumlah&provider=".$data_metod['provider']."";

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL,"https://serverh2h.com/order/deposit");
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		$chresult = curl_exec($ch);
//var_dump($chresult);
		curl_close($ch);
		$json_result = json_decode($chresult, true);

		if ($json_result['error'] == TRUE) {
			$_SESSION['hasil'] = array('alert' => 'danger', 'judul' => 'Pemesanan Gagal', 'pesan' => ''.$json_result['error']);
		} else {
			$pesan = $json_result['pesan'];
			$tujuan = $json_result['tujuan'];
			$jumlah_tf = $json_result['jumlah_tf'];
			$provider_oid = $json_result['code'];
			$amount = $json_result['amount'];

			$metodnya = $data_metod['nama'];
			$get_saldo = $post_jumlah * $data_metod['rate'];
			$insert = $conn->query("INSERT INTO deposit VALUES ('','$provider_oid', '$sess_username', '".$data_metod['tipe']."', '".$data_metod['provider']."' ,'$metodnya', '$post_pengirim','$tujuan','$jumlah_tf', '$amount', 'Pending', 'Website', '$date', '$time')");
			if ($insert == TRUE) {
				exit(header("Location: ".$config['web']['url']."invoice")); 


			} else {
				$_SESSION['hasil'] = array('alert' => 'danger', 'judul' => 'Permintaan Gagal', 'pesan' => 'Error System (Insert To Database).');
			}
		}
	}
}
require("../lib/header.php");
?>

<div class="app-content content ">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper">
            <div class="content-header row">
            </div>
            <div class="content-body">
                 <?php
            if (isset($_SESSION['hasil'])) {
                ?>
        <div class="alert alert-<?php echo $_SESSION['hasil']['alert'] ?>" role="alert">
    <h4 class="alert-heading"><strong>Respon : </strong><?php echo $_SESSION['hasil']['judul'] ?><br /> <strong></h4>
        <div class="alert-body">
        Pesan : </strong> <?php echo $_SESSION['hasil']['pesan'] ?>
        </div>
    </div>
    <?php
                unset($_SESSION['hasil']);
            }
            ?>

            <?php
            $time = microtime();
            $time = explode(' ', $time);
            $time = $time[1] + $time[0];
            $start = $time;
            ?>
<!--Title-->
<title>Deposit via Pulsa Axis</title>
<meta name="description" content="Platform Layanan Digital All in One, Berkualitas, Cepat & Aman. Menyediakan Produk & Layanan Pemasaran Sosial Media, Payment Point Online Bank, Layanan Pembayaran Elektronik, Optimalisasi Toko Online, Voucher Game dan Produk Digital."/>

<div class="row">
	<div class="col-md-7">
		<div class="card">
			<div class="card-body">
				<h4 class="m-t-0 text-uppercase text-center header-title"><i class="ti-wallet text-primary"></i> DEPOSIT PULSA AXIS</h4><hr>
				<form class="form-horizontal" role="form" method="POST">
					<input type="hidden" name="csrf_token" value="<?php echo $config['csrf_token'] ?>">
					<div class="form-group">
						<label class="col-md-12 control-label">Provider *</label>
						<div class="col-md-12">
							<select class="form-control" name="provider" id="provider">
								<option value="0">Pilih Salah Satu</option>
								<?php
								$cek_kategori = $conn->query("SELECT * FROM metode_depo WHERE provider = 'XL' AND keterangan = 'ON' ORDER BY nama ASC");
								while ($data_metode = $cek_kategori->fetch_assoc()) {
									?>
									<option value="<?php echo $data_metode['id'];?>">AXIS</option>

								<?php } ?>														
							</select>
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-12 control-label">Pembayaran *</label>
						<div class="col-md-12">
							<select class="form-control" name="pembayaran" id="pembayaran">
								<option value="0">Pilih Provider Pembayaran Terlebih Dahulu</option>
							</select>
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-12 control-label">Nomor Pengirim *<br> <small class="text-danger">Gunakan Awalan 08. Bukan 62 / +62</small></label>
						<div class="col-md-12">
							<input type="text" class="form-control"  placeholder="082377XXXXXX"  name="pengirim">
						</div>
					</div>

					<div class="form-row">
						<div class="form-group col-md-6">
							<label class="col-md-12 col-form-label">Jumlah *</label>
							<div class="col-md-12">
								<input type="number" class="form-control" name="jumlah" placeholder="Jumlah Deposit" id="jumlah">
							</div>
						</div>

						<div class="form-group col-md-6">
							<label class="col-md-12 col-form-label">Saldo yang didapat tampil di invoice setelah klik deposit</label>
							<!--
							<div class="col-md-12">
								<div class="input-group">
									<div class="input-group-prepend">
										<span class="input-group-text">Rp</span>
									</div>
									<input type="text" class="form-control"  name="saldo" placeholder="Saldo Yang Didapat" id="rate" readonly>
								</div>
							</div>
							-->
						</div>
						
					</div>
					<div class="form-group">
						<div class="col-md-offset-2 col-md-12">
							<button type="submit" class="pull-right btn btn-primary btn-block waves-effect w-md waves-light" name="request"><i class="ti-wallet"></i> Deposit</button>
						</div>
					</div>    
				</form>
			</div>
		</div>
	</div>										
	<!-- end col -->

	<!-- INFORMASI ORDER -->
	<div class="col-md-5">
		<div class="card">
			<div class="card-body">

				<center><h4 class="m-t-0 text-uppercase header-title"><i class="fa fa-info-circle"></i><b> Informasi Deposit</h4></b>
					<b>Keterangan</b> : Deposit ON 24 jam.<hr>
				</center>

				<!--CARA-->
				<div class="table-responsive">
					<center><i class="fa fa-check-circle"></i><b> Langkah Melakukan Deposit</b></center>
					<ol class="list-p">
						<li>Pilih salah satu provider & pembayaran.</li>
						<li>Masukkan nomor pengirim.</li>
						<li>Masukkan jumlah deposit.</li>
						<li>Klik <span class="badge badge-primary"><b>Deposit</b></span></li>
					</ol>
				</div>

				<!--KETENTUAN-->
				<div class="table-responsive">
					<center><i class="fa fa-check-circle"></i><b> Syarat & Ketentuan Deposit</b></center>
					<ol class="list-p">
						<li>Minimal deposit Rp.10.000.</li>
						<li>Saldo yang didapat mengikuti rate deposit.</li>
						<li>Detail invoice tampil setelah klik deposit.</li>
					</ol>
				</div>

			</div>
		</div>
	</div>
	<!-- INFORMASI ORDER -->

</div>
</div>	

<script type="text/javascript">
	$(document).ready(function() {
		$("#tipe").change(function() {
			var tipe = $("#tipe").val();
			$.ajax({
				url: '<?php echo $config['web']['url'];?>ajax/provider-deposit.php',
				data: 'tipe=' + tipe,
				type: 'POST',
				dataType: 'html',
				success: function(msg) {
					$("#provider").html(msg);
				}
			});
		});
		$("#provider").change(function() {
			var provider = $("#provider").val();
			$.ajax({
				url: '<?php echo $config['web']['url'];?>ajax/pembayaran-deposit.php',
				data: 'provider=' + provider,
				type: 'POST',
				dataType: 'html',
				success: function(msg) {
					$("#pembayaran").html(msg);
				}
			});
		});
		$("#jumlah").change(function(){
			var pembayaran = $("#pembayaran").val();
			var jumlah = $("#jumlah").val();
			$.ajax({
				url : '<?php echo $config['web']['url'];?>ajax/rate-deposit.php',
				type  : 'POST',
				dataType: 'html',
				data  : 'pembayaran='+pembayaran+'&jumlah='+jumlah,
				success : function(result){
					$("#rate").val(result);
				}
			});
		});  
	});

</script>	
<?php
require ("../lib/footer.php");
?>