<?php
session_start();
require 'config.php';
require 'lib/session_user.php';
require 'lib/session_login.php';

$cek_depo = $conn->query("SELECT * FROM deposit WHERE username = '$sess_username' AND status = 'Pending'");
$data_depo = $cek_depo->fetch_assoc();
$depo = $cek_depo->num_rows;

$get_id = $data_depo['kode_deposit'];
$saldo = $data_depo['get_saldo'];
$username = $data_depo['username'];

$cek_provider = $conn->query("SELECT * FROM provider_pulsa WHERE code = 'DPEDIA'");
$data_provider = mysqli_fetch_assoc($cek_provider);

if ($depo == 0) {  
	$_SESSION['hasil'] = array('alert' => 'danger', 'judul' => 'Deposit Pending Tidak Ditemukan', 'pesan' => 'Pilih Metode Deposit Untuk Mengisi Saldo');
	exit(header("Location: pay"));
} else {


	if ($data_depo['status'] == "Pending") {
		$label = "warning";
	} else if ($data_depo['status'] == "Error") {
		$label = "danger";     
	}
	if ($data_depo['status'] == "Pending") {
		$message = "Menunggu pembayaran";
	} else if ($data_depo['status'] == "Error") {
		$message = "Permintaan dibatalkan";
	} 

	if (isset($_POST['konfirm'])) {
		$postdata = "api_key=".$data_provider['key']."&code=".$data_depo['kode_deposit']."&provider=".$data_depo['provider']."";
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, "https://serverh2h.com/status/deposit");
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		$chresult = curl_exec($ch);

		curl_close($ch);
		$json_result = json_decode($chresult, true);
		$status = $json_result['status'];

		if ($json_result['error'] == true) {
			$_SESSION['hasil'] = array('alert' => 'danger', 'judul' => 'Deposit Belum Ditemukan', 'pesan' => $json_result['error']);
		} else {
			if ($conn->query("UPDATE deposit SET status = '$status' WHERE kode_deposit = '$get_id'") == true){

				$conn->query("UPDATE users set saldo = saldo + $saldo WHERE username = '$username'");
				$conn->query("INSERT INTO history_saldo VALUES ('', '$username', 'Penambahan Saldo', '$saldo', 'Penambahan Saldo Dengan Deposit ID $get_id', '$date', '$time')");

				$_SESSION['hasil'] = array('alert' => 'success', 'judul' => 'Deposit Ditemukan', 'pesan' => 'Saldo Telah Ditambah Ke Akun Anda');
				exit(header("location: riwayat/deposit-saldo"));
			}
		}

	} else if (isset($_POST['cancel'])) {
		$postdata = "api_key=".$data_provider['key']."&code=".$data_depo['kode_deposit']."&provider=".$data_depo['provider']."";
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, "https://serverh2h.com/status/deposit_cancel");
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		$chresult = curl_exec($ch);

		curl_close($ch);
		$json_result = json_decode($chresult, true);


		if ($json_result['error'] == true) {
			$_SESSION['hasil'] = array('alert' => 'danger', 'judul' => 'Deposit Belum Ditemukan', 'pesan' => $json_result['error']);
		} else {
			if ($conn->query("UPDATE deposit SET status = 'Error'  WHERE kode_deposit = '$get_id'") == true){

				$_SESSION['hasil'] = array('alert' => 'success', 'judul' => 'Deposit Dibatalkan', 'pesan' => 'Anda Telah Membatalkan Deposit Pending');
				exit(header("location: pay"));
			}
		}
	}
}

require 'lib/header.php';
?>
     <div class="container-fluid">        
                    <!-- Content Row -->
                    <div class="row">
       <div class="col-xl-12 col-lg-7">
                            <div class="card shadow mb-4">
                            
                                <div
                                    class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                                    <h6 class="m-0 font-weight-bold text-primary">INVOICE PEMBAYARAN #<?php echo $data_depo['kode_deposit']; ?></h6>
                                    
                                </div>
                                <!-- Card Body -->
                                <div class="card-body">
				<div class="invoice-wrap">
					<div class="invoice-box">
						<div class="invoice-header">
							<div class="logo text-center">
								<img src="/image/logo/logo.png" width="35%"alt="">
							</div>
						</div>
						<h5 class="text-center mb-30 weight-500">
						<strong><font face="BatangChe"><?php echo tanggal_indo($data_deposit['date']); ?>, <?php echo $data_deposit['time']; ?></font></strong></h5>
  
       					
					
						
						</div>
          
                              
			        <div class="kt-invoice__actions">
                        <div class="kt-invoice__container">
                            <button type="button" class="btn btn-info btn-sm" style="float:left;" onclick="window.print();"> <i class="fa fa-print fa-lg"></i> Print Invoice</button>
                      
                        <form class="form-horizontal" role="form" method="POST" action="">
                        <input type="hidden" name="csrf_token" value="<?php echo $config['csrf_token'] ?>">
							<button class="btn btn-danger btn-sm" style="float:right;" name="cancel"> BATALKAN </button>
               
                        </div>
                    </div>
                        <?php
                    if (isset($_SESSION['hasil'])) {
                    ?>
                    <div class="alert alert-<?php echo $_SESSION['hasil']['alert'] ?> alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <b class="text-left"><i class="fa fa-spinner fa-lg fa-spin"></i> <?php echo $_SESSION['hasil']['pesan'] ?></b>
                    </div>
                    <?php
                    unset($_SESSION['hasil']);
                    }
                    ?>
<br>
			       
					<br>
     <div class="table-responsive">
                        <table class="table table-bordered table-striped" id="kt_table_1">
                            <thead>
                                <tr >
								<td colspan="2" class="text-center">
					            <strong><font face="BatangChe"><h4><font face="Algerian">DETAIL PEMBAYARAN</font></h4></font></strong>
		</td>				
		</tr>
		
						            <tr>
							            <td>TIPE TRANSFER</td>
							            <td><?php echo $data_depo['payment']; ?></td>
						            </tr>
						            <tr>
							            <td>PENERIMA</td>
							            <td><?php echo $data_depo['tujuan']; ?></td>
						            </tr>
						            <?php if($data_deposit['provider'] == "QRIS" ) { ?>
                                    <tr>
                                      <td>SCAN QR<br><small>(SILAHKAN SCAN <b>QR KODE</b> INI<BR> SUPPORT <b>SEMUA BANK</b> BCA,BRI,BNI,MANDIRI DLL.<BR>
                                      <b>SEMUA E-WALLET</b> DANA-OVO-GOPAY-LINKAJA-SHOPEPAY-DLL)</td>	
                                    <td><img src="<?php echo  $data_deposit['pengirim'];?>" height="300" width="300"><br></td>
                                     </tr>
                                      <?php } ?>

                                      
						            <tr>
							            <td>SALDO YANG DIDAPATKAN</td>
							            <td><b>Rp <?php echo number_format($data_depo['jumlah_transfer'],0,',','.'); ?></b></td>
						            </tr>
						            <tr>
							            <td>JUMLAH PEMBAYARAN</td>
							            <td class="kt-font-danger kt-font-xl kt-font-boldest">Rp <?php echo number_format($data_depo['jumlah_transfer'],0,',','.'); ?></td>
						            </tr>
                                    
                                    <tr>   
                                 <td>STATUS DEPOSIT</td>
                                    <td>
						<span class="badge badge-<?php echo $label; ?>"><?php echo $message; ?></span>
                        
                           </td>
                        <tr>
                     
                        <?php if($data_deposit['status'] !== "Success" AND $data_deposit['status'] !== "Error") { ?>
                      <td colspan="2">
             <span class="badge badge-danger">HARAP DIBACA!</span><small> Jika sudah transfer tapi masih Status 						<span class="badge badge-<?php echo $label; ?>"><?php echo $message; ?></span> melebihi <b>  15 menit </b> silahkan hubungi kontak <a href="https://api.whatsapp.com/send?phone=6281384248407&text=Konfirmasi%20deposit%20ID%20<?php echo $data_depo['kode_deposit']; ?>">WhatsApp</a>  </small>
                              <?php } ?>
                        </td>
                        </tr>
      
                                    </tr>
					            </tbody>
				            </table>
                                   </form>
				        </div>
				    </div>
      
			    </div>
		    </div>
			        </div>

                </div>
            </div>
        </div>

		</div>
        <!-- End Content -->

        <!-- Start Scrolltop -->
		
		<!-- End Scrolltop -->

<?php
require 'lib/footer.php';
?>