<?php
require 'session_login.php';
require 'database.php';
require 'csrf_token.php';
?>

<!DOCTYPE html>
<html Content-Language="ID" lang="id" xml:lang="id">
<head>

<?php
include_once 'SEOSecretIDN-meta-all.php';
?>

    <!-- App css -->
    <link href="/assets/css/scroll.css?<?php echo $tanggal; ?>" rel="stylesheet" type="text/css" async/>
    <link href="/assets/v1/bootstrap-kmp.css?<?php echo $tanggal; ?>" rel="stylesheet" type="text/css" async/>        
    <link href="/assets/v1/app-kmp.css?<?php echo $tanggal; ?>" rel="stylesheet" type="text/css" async/>
    <link href="/assets/css/remixiconcolab.css?<?php echo $tanggal; ?>" rel="stylesheet" type="text/css" async/>
    <link href="/assets/css/remixicondefault.css?<?php echo $tanggal; ?>" rel="stylesheet" type="text/css" async/>
    <link href="/assets/v1/dataTables.bootstrap4.css?<?php echo $tanggal; ?>" rel="stylesheet" type="text/css" async/>
    <link href="/assets/v1/responsive.bootstrap4.css?<?php echo $tanggal; ?>" rel="stylesheet" type="text/css" async/>
    <link href="/assets/v1/buttons.bootstrap4.css?<?php echo $tanggal; ?>" rel="stylesheet" type="text/css" async/>
    <link href="/assets/v1/select.bootstrap4.css?<?php echo $tanggal; ?>" rel="stylesheet" type="text/css" async/>
    <script src='https://www.google.com/recaptcha/api.js'></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
</head>
<body>

    <!-- Begin page -->
    <div id="wrapper">

        <!-- Topbar Start -->
        <div class="navbar-custom">

            <?php
            if (isset($_SESSION['user'])) {
                ?>
                        <li class="menu-title">Menu Utama</li>
                        <li>
                            <a href="<?php echo $config['web']['url'];?>" target="_blank">
                                <i class="fa fa-home"></i>
                                <span> Beranda </span>
                            </a>
                        </li>

                        <li>
                            <a href="<?php echo $config['web']['url'];?>auth/login">
                                <i class="fa fa-user"></i>
                                <span> Masuk </span>
                            </a>
                        </li>

                        <li>
                            <a href="<?php echo $config['web']['url'];?>auth/register">
                                <i class="fa fa-user-plus "></i>
                                <span> Daftar </span>
                            </a>
                        </li>

                        <li>
                            <a href="<?php echo $config['web']['url'];?>halaman/produk-dan-layanan">
                                <i class="fa fa-tags"></i>
                                <span> Layanan </span>
                            </a>
                        </li>
                        <li class="menu-title"> Bantuan</li>
                        <li>
                            <a href="<?php echo $config['web']['url'];?>tiket">
                                <i class="ri-customer-service-fill"></i>
                                <span> Tiket </span>
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo $config['web']['url'];?>halaman/hubungi-kami">
                                <i class="fas fa-comments"></i>
                                <span> Kontak </span>
                            </a>
                        </li>
                        <li>
                            <a href="javascript: void(0);" class="waves-effect">
                                <i class="fa fa-question-circle"></i>
                                <span> Panduan </span>
                                <span class="menu-arrow"></span>
                            </a>
                            <ul class="nav-second-level nav" aria-expanded="false">
                            </li>
                            <li>
                                <a href="<?php echo $config['web']['url'];?>halaman/cara-deposit">Cara Deposit</a>
                            </li>
                            <li>
                                <a href="<?php echo $config['web']['url'];?>halaman/cara-transaksi">Memulai Transaksi</a>
                            </li>                                   
                            </ul>
                        </li>
                        <li class="menu-title"> Informasi</li>    
                        <li>
                            <a href="<?php echo $config['web']['url'];?>halaman/status-order">
                                <i class="fa fa-info"></i>
                                <span> Keterangan </span>
                            </a>
                        </li>
                        <li>
                            <a href="javascript: void(0);" class="waves-effect">
                                <i class="fa fa-fire"></i>
                                <span> API </span>
                                <span class="menu-arrow"></span>
                            </a>
                            <ul class="nav-second-level nav" aria-expanded="false">
                            <li>
                                <a href="<?php echo $config['web']['url'];?>halaman/api-dokumentasi-sosmed">Sosmed & Lainnya</a>
                            </li>
                            <li>
                                <a href="<?php echo $config['web']['url'];?>halaman/api-dokumentasi-pulsa">Pulsa & Lainnya</a>
                            </li>
                            </ul>
                        </li>

                    <?php } ?>

                </ul>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
<div class="content-page">
    <div class="content">
        <div class="container-fluid">
            <br />

            <?php
            if (isset($_SESSION['hasil'])) {
                ?>
                <div class="alert alert-<?php echo $_SESSION['hasil']['alert'] ?> alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <strong>Respon : </strong><?php echo $_SESSION['hasil']['judul'] ?><br /> <strong>Pesan : </strong> <?php echo $_SESSION['hasil']['pesan'] ?>
                </div>
                <?php
                unset($_SESSION['hasil']);
            }
            ?>

            <?php
            $time = microtime();
            $time = explode(' ', $time);
            $time = $time[1] + $time[0];
            $start = $time;
            ?>