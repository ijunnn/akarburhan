<?php
//
//SC	:	Panel - SMM & PPOB
//By	:	IJUNNN PROJECT
//Email	:	faizunaziz53@gmail.com
//
//▪ http://ddnews.my.id
//▪ https://www.youtube.com/channel/UCSfAkJXJC_mP4mYE32uTYHg
//▪ http://instagram.com/officddnews
//
//Hak cipta 2021
//Dilarang mengubah/menghapus copyright ini!
//

date_default_timezone_set('Asia/Jakarta');
error_reporting(E_ALL);

// status
// 1 = Ya
// 0 = Tidak
$maintenance = 0;
if ($maintenance == 1) {
    die("<h1>Sistem dalam Perbaikan.<br/>System Under Maintenance.</h1>");
}

// database
$config['db'] = array(
    'host' => 'localhost',
    'name' => 'u3557425_gabspedia', //Ganti dengan nama database anda
    'username' => 'u3557425_gabspedia', //Ganti dengan username database anda
    'password' => 'u3557425_gabspedia' //Ganti dengan password database anda
);

$conn = mysqli_connect($config['db']['host'], $config['db']['username'], $config['db']['password'], $config['db']['name']);
if (!$conn) {
    die("Koneksi Gagal : " . mysqli_connect_error());
}
$config['web'] = array(
    'url' => 'https://gabspedia.com/' //Ganti dengan domain anda, ex: https://azfapanel.com/ (wajib diakhiri garis miring /)
);

// date & time
$date = date("Y-m-d");
$time = date("H:i:s");

// date & time
$tanggal = date("Y-m-d");
$waktu = date("H:i:s");

require("lib/function.php");
require("lib/setting.php");