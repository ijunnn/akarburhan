<?php
//SC  : Panel - SMM & PPOB
//By  : IJUNNN PROJECT
//Email : ijunproject@gmail.com
//
//▪ https://www.youtube.com/channel/UCSfAkJXJC_mP4mYE32uTYHg
//
//Hak cipta 2021
//Dilarang mengubah/menghapus copyright ini!
//

session_start();
require("../config.php");
require '../lib/session_user.php';
require '../lib/session_login.php';

require("../lib/header.php");
?>
<div class="app-content content ">
  <div class="content-overlay"></div>
  <div class="header-navbar-shadow"></div>
  <div class="content-wrapper">
    <div class="content-header row">
    </div>
    <div class="content-body">
      <!-- Content Row -->

      <div class="row">


        <div class="col-xl-12 col-lg-7">
          <div class="card shadow mb-4">
            
            <div
            class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
            <h6 class="m-0 font-weight-bold text-primary">PRODUK PPOB</h6>
            
          </div>
          <!-- Card Body -->
          <div class="card-body">
            <div class="row">
             <div class="col-3 mt-3">
              <center>
                <a href="/pemesanan/pulsa" >
                  <img src="/assets/images/produk/pulsa.png" class="rounded" width="50" height="50">
                  <div class="text-xs font-weight-bold text-primary text-uppercase mb-1 ">
                  PULSA REGULER</div>
                </a>
              </center>
            </div>
            
            
            
            <div class="col-3 mt-3">
              <center>
                <a href="/pemesanan/paket">
                 <img src="/assets/images/produk/paket-data.png" class="rounded" width="50" height="50">
                 <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                 PAKET INTERNET</div>
               </a>
             </center>
           </div>
           
           <div class="col-3 mt-3">
            <center>
              <a href="/pemesanan/pln">
               <img src="/assets/images/produk/pln.png" class="rounded" width="50" height="50">
               <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
               PLN PRABAYAR</div>
             </a>
           </center>
         </div>
         
         
         <div class="col-3 mt-3">
          <center>
            <a href="/pemesanan/e-money">
             <img src="/assets/images/produk/e-money.png" class="rounded" width="50" height="50">
             <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
             TOPUP EMONEY</div>
           </a>
         </center>
       </div>
       
       <div class="col-3 mt-4">
        <center>
          <a href="/pemesanan/lainnya">
           <img src="/assets/images/produk/produklainnya.png" class="rounded" width="50" height="50">
           <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
           PROVIDER GLOBAL</div>
         </a>
       </center>
     </div>

     
     <div class="col-3 mt-4">
      <center>
        <a href="/pemesanan/telepon-sms">
          <img src="/assets/images/produk/sms.png" class="rounded" width="50" height="50">
          <div class="text-xs font-weight-bold text-primary text-uppercase mb-1"> TELPON & SMS</div>
        </a>
      </center>
    </div>
    
    
  </div>
</div>
</div>
</div>


<!-- Area Chart -->
<div class="col-xl-6 col-lg-7">
  <div class="card shadow mb-3">
    <!-- Card Header - Dropdown -->
    <div
    class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
    <h6 class="m-0 font-weight-bold text-primary">LAYANAN SOSIAL MEDIA</h6>
    
  </div>
  <!-- Card Body -->
  <div class="card-body">
    <div class="row">
      
     <div class="col-3 mt-3">
      <center>
        <a href="/pemesanan/sosial-media">
         <img src="/assets/images/produk/social-media.png" class="rounded" width="70" height="50">
         <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
         SOSIAL MEDIA</div>
       </a>
     </center>
   </div>
   
   
   
   <div class="col-3 mt-3">
    <center>
      <a href="/pemesanan/netflix">
        <img src="/assets/images/produk/netflixpremium.png" class="rounded" width="50" height="50">
        <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
        NETFLIX PREMIUM</div>
      </a>
    </center>
  </div>
  
  <div class="col-3 mt-3">
    <center>
      <a href="/pemesanan/spotify">
        <img src="/assets/images/produk/spotifypremium.png" class="rounded" width="80" height="50">
        <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
        SPOTIFY PREMIUM</div>
      </a>
    </center>
  </div>
  
  <div class="col-3 mt-3">
    <center>
      <a href="/pemesanan/youtube">
        <img src="/assets/images/produk/youtubepremium.png"  class="rounded" width="50" height="50">
        <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
        YOUTUBE PREMIUM</div>
      </a>
    </center>
  </div>
  
  
  
</div>
</div>
</div>
</div>



<div class="col-xl-6 col-lg-7">
  <div class="card shadow mb-4">
    
    <div
    class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
    <h6 class="m-0 font-weight-bold text-primary">VOUCHER</h6>
    
  </div>
  <!-- Card Body -->
  <div class="card-body">
    <div class="row">
      <div class="col-3 mt-3">
        <center>
          <a href="/pemesanan/voucher">
            <img src="/assets/images/produk/provider1.png" width="50px" height="50px" class="rounded">
            <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
            VOUCHER DIGITAL</div>
          </a>
        </center>
      </div>
      <div class="col-3 mt-3">
        <center>
          <a href="/pemesanan/game">
            <img src="/assets/images/produk/game.png" width="50px" height="50px">
            <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
            VOUCHER GAME</div>
          </a>
        </center>
      </div>
      
    
    
  </div>
</div>
</div>
</div>




</div>

</div>
<!-- End of Main Content -->

<?php
require ("../lib/footer.php");
?>