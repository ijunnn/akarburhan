<?php
session_start();
require '../config.php';
require '../lib/session_user.php';
require '../lib/header.php';
?>

<div class="app-content content ">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper">
            <div class="content-header row">
            </div>
<!--Title-->
<title>Dokumentasi API Sosial Media di <?php echo $data['short_title']; ?></title>
<meta name="description" content="Dokumentasi API <?php echo $data['short_title']; ?> dapat Anda gunakan untuk melakukan pembelian sebagai reseller melalui web SMM dan PPOB oper dengan harga yang lebih murah."/>

<!--OG2-->
<meta content="Dokumentasi API Sosial Media <?php echo $data['short_title']; ?>" property="og:title"/>
<meta content="Dokumentasi API <?php echo $data['short_title']; ?> dapat Anda gunakan untuk melakukan pembelian sebagai reseller melalui web SMM dan PPOB oper dengan harga yang lebih murah." property="og:description"/>
<meta content="<?php echo $data['short_title']; ?> - Dokumentasi API Sosial Media <?php echo $data['short_title']; ?>" property="og:headline"/>
<meta content="<?php echo $config['web']['url'];?>assets/images/halaman/tentang-kami.png" property="og:image"/>
<meta content="Dokumentasi API Sosial Media <?php echo $data['short_title']; ?>" property="twitter:title"/>
<meta content="Dokumentasi API <?php echo $data['short_title']; ?> dapat Anda gunakan untuk melakukan pembelian sebagai reseller melalui web SMM dan PPOB oper dengan harga yang lebih murah." property="twitter:description"/>
<meta content="<?php echo $config['web']['url'];?>assets/images/halaman/tentang-kami.png" property="twitter:image"/>

<div class="card">
    <div class="col-md-12">
        <div class="card-body">
            <h1 class="m-t-0 text-uppercase text-center header-title"> <b>Dokumentasi API Sosial Media <?php echo $data['short_title']; ?></b></h1>
            <div class="box-content text-center">
                <a href="../halaman/daftar-harga" title="<?php echo $data['short_title'];?>" alt="<?php echo $data['title'];?>"> <img src="../assets/images/halaman/dokumentasi-api.png" class="img-responsive" style="max-width: 95%; min-width: 270px;"></a>
            </div><br/>
            <div class="table-responsive">
                <table class="table table-bordered table-striped table-hover">
                    <tbody>
                        <tr>
                            <td>METODE HTTP</td>
                            <td>POST</td>
                        </tr>
                        <tr>
                            <td>API URL</td>
                            <td>
                                <input type="text" class="form-control" name="api" value="<?php echo $config['web']['url'];?>api/sosial-media" readonly> 
                            </td>
                        </tr>
                        <tr>
                            <td>API KEY</td>
                            <td>
                                <input type="text" class="form-control" name="api" value="<?php echo $data_user['api_key']; ?>" readonly>
                            </td>
                        </tr>
                        <tr>
                            <td>FORMAT RESPON</td>
                            <td>JSON</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        
        <div class="row">
            <div class="col-md-6">
                <div class="card">
                    <div class="card-body">
                        <h4 class="text-dark header-title m-t-0">Melakukan <font color="#0d8bf2">Pemesanan</font></h4>
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>Parameter</th>
                                        <th>Deskripsi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>API URL</td>
                                        <td>
                                            <input type="text" class="form-control" name="api" value="<?php echo $config['web']['url'];?>api/sosial-media" readonly> 
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>api_key</td>
                                        <td>
                                            <input type="text" class="form-control" name="api" value="<?php echo $data_user['api_key']; ?>" readonly>                                   </td>
                                        </tr>
                                        <tr>
                                            <td>action</td>
                                            <td>pemesanan</td>
                                        </tr>
                                        <tr>
                                            <td>Layanan</td>
                                            <td>Service ID <a href="<?php echo $config['web']['url'];?>halaman/daftar-harga">Daftar Layanan</a></td>
                                        </tr>
                                        <tr>
                                            <td>target</td>
                                            <td>Target / Link</td>
                                        </tr>
                                        <tr>
                                            <td>jumlah</td>
                                            <td>Jumlah Pemesanan</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <b>Contoh Respon Yang Di Dapat</b><br />
                            <div class="alert alert-success-2">
<pre>
Jika Pemesanan Sukses
{
    "data": {
    "id": "401",
    "start_count": "104"
    }
}

Jika Pemesanan Gagal
{
    "status": false,
    "data": {
    "pesan": "Permintaan Tidak Sesuai"
    }
}
</pre>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end col -->
            <div class="col-md-6">
                <div class="card">
                    <div class="card-body">
                        <h4 class="text-dark header-title m-t-0">Melakukan <font color="#0d8bf2">Cek Status Pesanan</font></h4>
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>Parameter</th>
                                        <th>Deskripsi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>API URL</td>
                                        <td>
                                            <input type="text" class="form-control" name="api" value="<?php echo $config['web']['url'];?>api/sosial-media" readonly> 
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>api_key</td>
                                        <td>
                                            <input type="text" class="form-control" name="api" value="<?php echo $data_user['api_key']; ?>" readonly> 
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>action</td>
                                        <td>status</td>
                                    </tr>
                                    <tr>
                                        <td>id</td>
                                        <td>Order ID</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <b>Contoh Respon Yang Didapat</b><br />
                        <div class="alert alert-success-2">
<pre>
Jika Respon Sukses
{
    "data": {
    "id":"40",
    "start_count":"401",
    "status":"Success",
    "remains":"0"
    }
}

Jika Respon Gagal
{
    "status": false,
    "data": {
    "pesan": "Permintaan Tidak Sesuai"
    }
}
</pre>
                </div>
            </div>
        </div>
    </div>
    <!-- end col -->
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <h4 class="text-dark header-title m-t-0">Mendapatkan <font color="#0d8bf2">Layanan</font></h4>
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>Parameter</th>
                                <th>Deskripsi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>API URL</td>
                                <td>
                                    <input type="text" class="form-control" name="api" value="<?php echo $config['web']['url'];?>api/sosial-media" readonly> 
                                </td>
                            </tr>
                            <tr>
                                <td>api_key</td>
                                <td>
                                    <input type="text" class="form-control" name="api" value="<?php echo $data_user['api_key']; ?>" readonly>                                   </td>
                                </tr>
                                <td>action</td>
                                <td>layanan</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <b>Contoh Respon Yang Di Dapat</b><br />
                    <div class="alert alert-success-2">
<pre>
Jika Respon Sukses
{
    "data": {
    "sid": "1"
    "kategori": "Instagram Followers Indonesia"
    "layanan": "Instagram Followers"
    "min": "100"
    "max": "10000"
    "harga": "10000"
    "catatan": "Input username tanpa @ dan pastikan akun publik"
    }
}

Jika Respon Gagal
{
    "status": false,
    "data": {
    "pesan": "Permintaan Tidak Sesuai"
    }
}
</pre>
            </div>
        </div>
    </div>
    <!-- end col -->
</div>
<!-- end row -->


</div>
<!-- end container -->
</div>
<!-- end content -->
<?php
require '../lib/footer.php';
?>