<?php
session_start();
require '../config.php';
require '../lib/header.php';
?>
<!--Title-->
<title>Kemitraan Usaha dan Jaringan Bisnis <?php echo $data['short_title']; ?></title>
<meta name="description" content="Berikut adalah daftar partner dan jaringan kerjasama kami yang telah terdaftar. Jangan ragu menghubungi kami melalui whatsapp 081392814105, halaman kontak, atau via email support."/>

<!--OG2-->
<meta content="Informasi Kemitraan Usaha dan Jaringan Bisnis <?php echo $data['short_title']; ?>" property="og:title"/>
<meta content="Berikut adalah daftar partner dan jaringan kerjasama kami yang telah terdaftar. Jangan ragu menghubungi kami melalui whatsapp 081392814105, halaman kontak, atau via email support." property="og:description"/>
<meta content="<?php echo $data['short_title']; ?> - Informasi Kemitraan Usaha dan Jaringan Bisnis <?php echo $data['short_title']; ?>" property="og:headline"/>
<meta content="<?php echo $config['web']['url'];?>assets/images/halaman/mitra-dan-jaringan.png" property="og:image"/>
<meta content="Informasi Kemitraan Usaha dan Jaringan Bisnis <?php echo $data['short_title']; ?>" property="twitter:title"/>
<meta content="Berikut adalah daftar partner dan jaringan kerjasama kami yang telah terdaftar. Jangan ragu menghubungi kami melalui whatsapp 081392814105, halaman kontak, atau via email support." property="twitter:description"/>
<meta content="<?php echo $config['web']['url'];?>assets/images/halaman/mitra-dan-jaringan.png" property="twitter:image"/>

<div class="row">
	<div class="col-sm-12">
		<div class="card">
			<div class="card-body table-responsive">
				<center><h1 class="m-t-0 text-uppercase text-center header-title"><b>Kemitraan Usaha dan Jaringan Bisnis <?php echo $data['short_title']; ?></b></h1></center>
				<?php 
//Call Mintra dan Jaringan
				$CallPage = Show('halaman', "id = '4'");
				echo "".$CallPage['konten']."";
				?>
			</div>

			<div class="card-body table-responsive">
				<center>
					<h2 class="m-t-0 text-uppercase header-title"><b> Kontak Kami</b></h2>
					Jangan ragu menghubungi kami melalui whatsapp 081392814105, halaman kontak, atau via email support@azfapanel.com.<br/><br/>

					<h3 class="m-t-0 text-uppercase header-title"><b> Mitra dan Jaringan </b></h3>
					<table class="table table-bordered dt-responsive nowrap" style="width: 100%;">
						<tbody>
							<tr>
								<td align="right" style="width: 50%;">
									<a href="https://www.mediabisnis.co.id" class="btn btn-primary btn-bordred waves-effect waves-light" target="_blank" rel="nofollow noopener"> Media Bisnis</a>
								</td>
								<td align="left" style="width: 50%;">
									<a href="https://www.azfapanel.com" class="btn btn-primary btn-bordred waves-effect waves-light" target="_blank" rel="nofollow noopener"> Kincai Seluler</a>
								</td>
							</tr>

							<tr>
								<td align="right" style="width: 50%;">
									<a href="https://www.mycoding.net" class="btn btn-primary btn-bordred waves-effect waves-light" target="_blank" rel="nofollow noopener"> My Coding</a>
								</td>
								<td align="left" style="width: 50%;">
									<a href="https://www.blackexpo.or.id" class="btn btn-primary btn-bordred waves-effect waves-light" target="_blank" rel="nofollow noopener"> Black Expo</a>
								</td>
							</tr>
						</tbody>
					</table>

				</center>
			</div>

		</div>
	</div>
</div>

<?php
require '../lib/footer.php';
?>