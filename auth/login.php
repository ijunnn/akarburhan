<?php
session_start();
require '../config.php';

if (isset($_SESSION['user'])) {
    header("Location: " . $config['web']['url']);
} else {
    if (isset($_POST['login'])) {
        $post_username = $conn->real_escape_string(trim(filter($_POST['username'])));
        $post_password = $conn->real_escape_string(trim(filter($_POST['password'])));

        $check_user = $conn->query("SELECT * FROM users WHERE username = '$post_username'");
        $check_user_rows = mysqli_num_rows($check_user);
        $data_user = mysqli_fetch_assoc($check_user);

        $verif_pass = password_verify($post_password, $data_user['password']);

        if (!$post_username || !$post_password) {
            $_SESSION['hasil'] = array('alert' => 'danger', 'judul' => 'Gagal', 'pesan' => 'Harap Mengisi Input Pada Form <br /> - Username <br /> - Password.');
        } else if ($check_user_rows == 0) {
            $_SESSION['hasil'] = array('alert' => 'danger', 'judul' => 'Gagal', 'pesan' => 'Pengguna Tidak Tersedia.');
        } else if ($data_user['status'] == "Tidak Aktif") {
            $_SESSION['hasil'] = array('alert' => 'danger', 'judul' => 'Gagal', 'pesan' => 'Akun Sudah Tidak Aktif.');
        } else {
            if ($check_user_rows == 1) {
                if ($verif_pass == true) {
                    $conn->query("INSERT INTO log VALUES (null,'$post_username', 'Login', '" . get_client_ip() . "','$date','$time')");
                    $_SESSION['user'] = $data_user;
                    $_SESSION['hasil'] = array('alert' => 'success', 'judul' => 'Berhasil Masuk!', 'pesan' => 'Selamat Datang <b>' . $data_user['username'] . '</b>, Semoga Hari Anda Menyenangkan');
                    exit(header("Location: " . $config['web']['url']));
                } else {
                    $_SESSION['hasil'] = array('alert' => 'danger', 'judul' => 'Gagal', 'pesan' => 'Username atau password salah.');
                }
            }
        }
    }
}

require '../lib/header_login.php';
?>
<!--Title-->
<title>Login <?php echo $data['short_title']; ?></title>
<meta name="description"
    content="Platform Layanan Digital All in One, Berkualitas, Cepat & Aman. Menyediakan Produk & Layanan Pemasaran Sosial Media, Payment Point Online Bank, Layanan Pembayaran Elektronik, Optimalisasi Toko Online, Voucher Game dan Produk Digital." />

<!--OG2-->
<meta content="Login <?php echo $data['short_title']; ?>" property="og:title" />
<meta
    content="Platform Layanan Digital All in One, Berkualitas, Cepat & Aman. Menyediakan Produk & Layanan Pemasaran Sosial Media, Payment Point Online Bank, Layanan Pembayaran Elektronik, Optimalisasi Toko Online, Voucher Game dan Produk Digital."
    property="og:description" />
<meta content="Login <?php echo $data['short_title']; ?>" property="og:headline" />
<meta content="<?php echo $config['web']['url']; ?>assets/images/halaman/tentang-kami.png" property="og:image" />
<meta content="Login <?php echo $data['short_title']; ?>" property="twitter:title" />
<meta
    content="Platform Layanan Digital All in One, Berkualitas, Cepat & Aman. Menyediakan Produk & Layanan Pemasaran Sosial Media, Payment Point Online Bank, Layanan Pembayaran Elektronik, Optimalisasi Toko Online, Voucher Game dan Produk Digital."
    property="twitter:description" />
<meta content="<?php echo $config['web']['url']; ?>assets/images/halaman/tentang-kami.png" property="twitter:image" />

<body class="vertical-layout vertical-menu-modern blank-page navbar-floating footer-static  " data-open="click"
    data-menu="vertical-menu-modern" data-col="blank-page">
    <!-- BEGIN: Content-->
    <div class="app-content content ">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper">
            <div class="content-header row">
            </div>
            <div class="content-body">
                <div class="auth-wrapper auth-v1 px-2">
                    <div class="auth-inner py-2">
                        <!-- Login v1 -->
                        <?php
                        if (isset($_SESSION['hasil'])) {
                        ?>
                        <div class="demo-spacing-0">
                            <div class="alert alert-<?php echo $_SESSION['hasil']['alert'] ?>" role="alert">
                                <h4 class="alert-heading">Pesan</h4>
                                <div class="alert-body">
                                    <?php echo $_SESSION['hasil']['pesan'] ?>
                                </div>
                            </div>
                            <?php
                            unset($_SESSION['hasil']);
                        }
                            ?>
                            <div class="card mb-0">
                                <div class="card-body">
                                    <a href="javascript:void(0);" class="brand-logo">
                                        <svg viewbox="0 0 139 95" version="1.1" xmlns="http://www.w3.org/2000/svg"
                                            xmlns:xlink="http://www.w3.org/1999/xlink" height="28">
                                            <defs>
                                                <lineargradient id="linearGradient-1" x1="100%" y1="10.5120544%"
                                                    x2="50%" y2="89.4879456%">
                                                    <stop stop-color="#000000" offset="0%"></stop>
                                                    <stop stop-color="#FFFFFF" offset="100%"></stop>
                                                </lineargradient>
                                                <lineargradient id="linearGradient-2" x1="64.0437835%" y1="46.3276743%"
                                                    x2="37.373316%" y2="100%">
                                                    <stop stop-color="#EEEEEE" stop-opacity="0" offset="0%"></stop>
                                                    <stop stop-color="#FFFFFF" offset="100%"></stop>
                                                </lineargradient>
                                            </defs>
                                            <g id="Page-1" stroke="none" stroke-width="1" fill="none"
                                                fill-rule="evenodd">
                                                <g id="Artboard" transform="translate(-400.000000, -178.000000)">
                                                    <g id="Group" transform="translate(400.000000, 178.000000)">
                                                        <path class="text-primary" id="Path"
                                                            d="M-5.68434189e-14,2.84217094e-14 L39.1816085,2.84217094e-14 L69.3453773,32.2519224 L101.428699,2.84217094e-14 L138.784583,2.84217094e-14 L138.784199,29.8015838 C137.958931,37.3510206 135.784352,42.5567762 132.260463,45.4188507 C128.736573,48.2809251 112.33867,64.5239941 83.0667527,94.1480575 L56.2750821,94.1480575 L6.71554594,44.4188507 C2.46876683,39.9813776 0.345377275,35.1089553 0.345377275,29.8015838 C0.345377275,24.4942122 0.230251516,14.560351 -5.68434189e-14,2.84217094e-14 Z"
                                                            style="fill: currentColor"></path>
                                                        <path id="Path1"
                                                            d="M69.3453773,32.2519224 L101.428699,1.42108547e-14 L138.784583,1.42108547e-14 L138.784199,29.8015838 C137.958931,37.3510206 135.784352,42.5567762 132.260463,45.4188507 C128.736573,48.2809251 112.33867,64.5239941 83.0667527,94.1480575 L56.2750821,94.1480575 L32.8435758,70.5039241 L69.3453773,32.2519224 Z"
                                                            fill="url(#linearGradient-1)" opacity="0.2"></path>
                                                        <polygon id="Path-2" fill="#000000" opacity="0.049999997"
                                                            points="69.3922914 32.4202615 32.8435758 70.5039241 54.0490008 16.1851325">
                                                        </polygon>
                                                        <polygon id="Path-21" fill="#000000" opacity="0.099999994"
                                                            points="69.3922914 32.4202615 32.8435758 70.5039241 58.3683556 20.7402338">
                                                        </polygon>
                                                        <polygon id="Path-3" fill="url(#linearGradient-2)"
                                                            opacity="0.099999994"
                                                            points="101.428699 0 83.0667527 94.1480575 130.378721 47.0740288">
                                                        </polygon>
                                                    </g>
                                                </g>
                                            </g>
                                        </svg>
                                        <h2 class="brand-text text-primary ml-1"><?php echo $data['short_title']; ?>
                                        </h2>
                                    </a>

                                    <h4 class="card-title mb-1">Welcome to <?php echo $data['short_title']; ?>! 👋</h4>

                                    <form class="auth-login-form mt-2" method="POST">
                                        <input type="hidden" name="csrf_token"
                                            value="<?php echo $config['csrf_token'] ?>">
                                        <div class="form-group">
                                            <label for="login-email" class="form-label">Username</label>
                                            <input type="text" name="username" class="form-control"
                                                placeholder="Username">
                                        </div>

                                        <div class="form-group">
                                            <div class="d-flex justify-content-between">
                                                <label for="login-password">Password</label>
                                                <a href="/auth/lupa-password">
                                                    <small>Forgot Password?</small>
                                                </a>
                                            </div>
                                            <div class="input-group input-group-merge form-password-toggle">
                                                <input id="pass" type="password" name="password" class="form-control"
                                                    placeholder="Password">
                                                <div class="input-group-append">
                                                    <span class="input-group-text cursor-pointer"><i
                                                            data-feather="eye"></i></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="login-password">Ingat saya</label>
                                            <select class="form-control" for="password" name="remember" id="remember">
                                                <option value="Year">Ingat Saya (optional)</option>
                                                <option value="Week">1 Minggu</option>
                                                <option value="Month">1 Bulan</option>
                                                <option value="Year">1 Tahun</option>
                                            </select>
                                        </div>
                                        <button type="submit" class="btn btn-primary btn-block" name="login"
                                            id="login">Sign in</button>
                                    </form>

                                    <p class="text-center mt-2">
                                        <span>Belum punya akun?</span>
                                        <a href="/auth/register">
                                            <span>Daftar sekarang</span>
                                        </a>
                                    </p>

                                </div>
                            </div>
                            <!-- /Login-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END: Content-->

        <script type="text/javascript">
        function change() {
            var x = document.getElementById('pass').type;
            if (x == 'password') {
                document.getElementById('pass').type = 'text';
                document.getElementById('mybtn').innerHTML = '<i class="mdi mdi-eye-off"></i> Hidden';
            } else {
                document.getElementById('pass').type = 'password';
                document.getElementById('mybtn').innerHTML = '<i class="mdi mdi-eye"></i> Show';
            }
        }
        </script>

        <?php
        require '../lib/footer_login.php';
        ?>