<?php
session_start();
require '../config.php';
require '../lib/session_user.php';
if (isset($_POST['pesan'])) {
	require '../lib/session_login.php';
	$post_operator = $conn->real_escape_string(trim(filter($_POST['operator'])));
	$post_layanan = $conn->real_escape_string(trim(filter($_POST['layanan'])));
	$post_target = $conn->real_escape_string(trim(filter($_POST['target'])));
	$post_nometer = $conn->real_escape_string(trim(filter($_POST['nometer'])));

	$cek_layanan = $conn->query("SELECT * FROM layanan_pulsa WHERE provider_id = '$post_layanan' AND status = 'Normal'");
	$data_layanan = mysqli_fetch_assoc($cek_layanan);

	$cek_pesanan = $conn->query("SELECT * FROM pembelian_pulsa WHERE target = '$post_target' AND status = 'Success'");
	$data_pesanan = mysqli_fetch_assoc($cek_pesanan);

	$order_id = acak_nomor(3).acak_nomor(4);
	$provider = $data_layanan['provider'];

	$cek_provider = $conn->query("SELECT * FROM provider_pulsa WHERE code = '$provider'");
	$data_provider = mysqli_fetch_assoc($cek_provider);

	if (!$post_target || !$post_layanan || !$post_operator) {
		$_SESSION['hasil'] = array('alert' => 'danger', 'judul' => 'Order gagal', 'pesan' => 'Silahkan lengkapi bidang berikut :<br/> - Kategori <br /> - Layanan <br /> - Target');

	} else if (mysqli_num_rows($cek_layanan) == 0) {
		$_SESSION['hasil'] = array('alert' => 'danger', 'judul' => 'Order gagal', 'pesan' => 'Layanan tidak tersedia');

	} else if (mysqli_num_rows($cek_provider) == 0) {
		$_SESSION['hasil'] = array('alert' => 'danger', 'judul' => 'Order gagal', 'pesan' => 'Server sedang maintenance');

	} else if ($data_user['saldo'] < $data_layanan['harga']) {
		$_SESSION['hasil'] = array('alert' => 'danger', 'judul' => 'Order gagal', 'pesan' => 'Saldo tidak mencukupi untuk melakukan order');

	} else if (mysqli_num_rows($cek_pesanan) == 1) {
		$_SESSION['hasil'] = array('alert' => 'danger', 'judul' => 'Order gagal', 'pesan' => 'Dilarang order layanan & target yang sama, sebelum order sebelumnya sukses');

	} else {

		if ($provider == "DPEDIA") {
			$postdata = "api_key=".$data_provider['key']."&service=$post_layanan&phone=$post_target";

			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL,"https://serverh2h.com/order/pulsa");
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			$chresult = curl_exec($ch);
//echo $chresult;
			curl_close($ch);
			$json_result = json_decode($chresult, true);

			if ($provider = 'DPEDIA' AND $json_result['error'] == TRUE) {
				$_SESSION['hasil'] = array('alert' => 'danger', 'judul' => 'Order gagal', 'pesan' => ''.$json_result['error']);
			} else {
				if ($provider == "DPEDIA") {
					$provider_oid = $json_result['code_trx'];
				}
				if ($conn->query("INSERT INTO pembelian_pulsa VALUES ('','$order_id', '$provider_oid', '$sess_username', '".$data_layanan['layanan']."', '".$data_layanan['harga']."', '".$data_layanan['profit']."', '$post_target', '$post_nometer', '$pesan', 'Pending', '$date', '$time', 'Website', '$provider', '0')") == true) {

					$conn->query("UPDATE users SET saldo = saldo-".$data_layanan['harga'].", pemakaian_saldo = pemakaian_saldo+".$data_layanan['harga']." WHERE username = '$sess_username'");
					$conn->query("INSERT INTO history_saldo VALUES ('', '$sess_username', 'Pengurangan saldo', '".$data_layanan['harga']."', 'Pemesanan dengan Order ID $order_id', '$date', '$time')");

					$harga = number_format($data_layanan['harga'],0,',','.');
					$_SESSION['hasil'] = array(
						'alert' => 'success',
						'judul' => 'Order berhasil', 
						'pesan' => '<br/> 
						<b>Order ID : </b> '.$order_id.'<br />
						<b>Layanan : </b> '.$data_layanan['layanan'].'<br />
						<b>Tujuan : </b> '.$post_target.'<br />
						<b>Harga : </b> Rp. '.$harga.'');
				} else {
					$_SESSION['hasil'] = array('alert' => 'danger', 'judul' => 'Order gagal', 'pesan' => 'Pemesanan gagal');
				}
			}
		}
	}
}
require("../lib/header.php");
?>


<div class="app-content content ">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper">
            <div class="content-header row">
            </div>
            <div class="content-body">
                 <?php
            if (isset($_SESSION['hasil'])) {
                ?>
        <div class="alert alert-<?php echo $_SESSION['hasil']['alert'] ?>" role="alert">
    <h4 class="alert-heading"><strong>Respon : </strong><?php echo $_SESSION['hasil']['judul'] ?><br /> <strong></h4>
        <div class="alert-body">
        Pesan : </strong> <?php echo $_SESSION['hasil']['pesan'] ?>
        </div>
    </div>
    <?php
                unset($_SESSION['hasil']);
            }
            ?>

            <?php
            $time = microtime();
            $time = explode(' ', $time);
            $time = $time[1] + $time[0];
            $start = $time;
            ?>
<!--Title-->
<title>SPOTIFY PREMIUM</title>
<meta name="description" content="Platform Layanan Digital All in One, Berkualitas, Cepat & Aman. Menyediakan Produk & Layanan Pemasaran Sosial Media, Payment Point Online Bank, Layanan Pembayaran Elektronik, Optimalisasi Toko Online, Voucher Game dan Produk Digital."/>

<div class="row">
	<div class="col-md-7">
		<div class="card">
			<div class="card-body">
				<h4 class="m-t-0 text-uppercase text-center header-title"><i class="mdi mdi-spotify mr1 text-primary"></i><b> SPOTIFY PREMIUM</b></h4><hr>
				<form class="form-horizontal" method="POST">
					<input type="hidden" name="csrf_token" value="<?php echo $config['csrf_token'] ?>">   										    
					<div class="form-group">
						<label class="col-md-12 control-label">Kategori *</label>
						<div class="col-md-12">
							<select class="form-control" name="operator" id="operator">
								<option value="0">Pilih Salah Satu</option>
								<?php
								$cek_kategori = $conn->query("SELECT * FROM kategori_layanan WHERE tipe IN ('SPOTIFY') ORDER BY nama ASC");
								while ($data_kategori = $cek_kategori->fetch_assoc()) {
									?>
									<option value="<?php echo $data_kategori['kode']; ?>"><?php echo $data_kategori['nama']; ?></option>
								<?php } ?>														
							</select>
						</div>
					</div>											
					<div class="form-group">
						<label class="col-md-12 control-label">Layanan *</label>
						<div class="col-md-12">
							<select class="form-control" name="layanan" id="layanan">
								<option value="0">Pilih Kategori</option>
							</select>
						</div>
					</div>

					<div id="catatan">
					</div>
					
					<div class="form-row">
						<div class="form-group col-md-6">
							<label class="col-md-12 col-form-label">Target *</label>
							<div class="col-md-12">
								<input type="text" name="target" class="form-control" placeholder="Masukkan Nama Akun">
							</div>
						</div>

						<div class="form-group col-md-6">
							<label class="col-md-12 col-form-label">Total Harga</label>
							<div class="col-md-12">
								<div class="input-group">
									<div class="input-group-prepend">
										<span class="input-group-text">Rp. </span>
									</div>
									<input type="number" class="form-control" id="harga" readonly>
								</div>
							</div>
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-12">
							<button type="submit" class="pull-right btn btn-block btn--md btn-primary waves-effect w-md waves-light" name="pesan"><i class="mdi mdi-cart"></i>Order</button>

						</div>
					</div>
				</form>
			</div>

		</div>
	</div>	
	<!-- end col -->

	<!-- INFORMASI ORDER -->
	<div class="col-md-5">
		<div class="card">
			<div class="card-body">

				<center><h4 class="m-t-0 text-uppercase header-title"><i class="fa fa-info-circle"></i><b> Informasi Order</h4></b>
					Gunakan koneksi internet yang stabil agar daftar layanan sinkron dengan kategori yang dipilih.<hr>
				</center>

				<!--KETERANGAN-->
				<div class="table-responsive">
					<center><i class="fa fa-check-circle"></i><b> Keterangan Produk</b></center>
					<ol class="list-p">
						<li>Akun berstatus baru.</li>
						<li>Durasi sesuai masa aktif yang dipilih.</li>
						<li>Akun bisa dengan mudah diperpanjang.</li>
						<li>Email & password akun dari kami.</li>
						<li>Akun no ads / tanpa iklan.</li>
						<li>Bisa unlocked semua lagu.</li>
						<li>Bisa unlimited skip.</li>
					</ol>
				</div>

				<!--CARA-->
				<div class="table-responsive">
					<center><i class="fa fa-check-circle"></i><b> Langkah Membuat Order</b></center>
					<ol class="list-p">
						<li>Pilih salah satu kategori & layanan.</li>
						<li>Masukkan nama akun pada target (sesuai permintaan).</li>
						<li>Klik <span class="badge badge-primary"><b>Order</b></span> untuk membuat pesanan.</li>
					</ol>
				</div>

				<!--KETENTUAN-->
				<div class="table-responsive">
					<center><i class="fa fa-check-circle"></i><b> Syarat & Ketentuan Order</b></center>
					<ol class="list-p">
						<li>Mengikuti langkah-langkah diatas.</li>
						<li>Jika ingin <u>order baru</u> untuk layanan & target yang sama, tunggu <u>order sebelumnya</u> <span class="badge badge-success"><b>Success</b></span>.</li>
						<li>Detail status order tampil setelah membuat pesanan.</li>
						<li>Jika kurang jelas, tanyakan ke CS via <span class="badge badge-info"><a href="../tiket" alt="Tiket Bantuan" title="Tiket Bantuan" target="_blank" style="color:#ffffff;"><b>Live Chat</b></a></span>.</li>
					</ol>
				</div>

			</div>
		</div>
	</div>
	<!-- INFORMASI ORDER -->

</div>
</div>
</div>
</div>


<script type="text/javascript">
	$(document).ready(function() {
		$("#operator").change(function() {
			var operator = $("#operator").val();
			$.ajax({
				url: '<?php echo $config['web']['url'];?>ajax/layanan_pulsa.php',
				data: 'operator=' + operator,
				type: 'POST',
				dataType: 'html',
				success: function(msg) {
					$("#layanan").html(msg);
				}
			});
		});
		$("#layanan").change(function() {
			var layanan = $("#layanan").val();
			$.ajax({
				url: '<?php echo $config['web']['url'];?>ajax/harga_pulsa.php',
				data: 'layanan=' + layanan,
				type: 'POST',
				dataType: 'html',
				success: function(msg) {
					$("#harga").val(msg);
				}
			});
		});
		$("#layanan").change(function() {
			var layanan = $("#layanan").val();
			$.ajax({
				url: '<?php echo $config['web']['url']; ?>ajax/catatan-pulsa.php',
				data: 'layanan=' + layanan,
				type: 'POST',
				dataType: 'html',
				success: function(msg) {
					$("#catatan").html(msg);
				}
			});
		});
	});

</script>				
<?php
require ("../lib/footer.php");
?>