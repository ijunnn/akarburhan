<?php
require 'session_login.php';
require 'database.php';
require 'csrf_token.php';
?>

<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->

<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=0,minimal-ui">
    <meta name="description" content="Pusat Distributor pulsa termurah dan smm.">
    <meta name="keywords" content=pusat,distributor,smm,pulsa">
    <meta name="author" content="PIXINVENT">
    <title><?php echo $data['short_title'];?></title>
    <link rel="apple-touch-icon" href="/assets/images/log/praspay.png">
    <link rel="shortcut icon" type="image/png" href="/assets/images/logo/praspay.png">
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,300;0,400;0,500;0,600;1,400;1,500;1,600" rel="stylesheet">

    <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,300;0,400;0,500;0,600;1,400;1,500;1,600" rel="stylesheet">

    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="/build/app-assets/vendors/css/vendors.min.css">
    <!-- END: Vendor CSS-->

    <!-- BEGIN: Theme CSS-->
    <link rel="stylesheet" type="text/css" href="/build/app-assets/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="/build/app-assets/css/bootstrap-extended.css">
    <link rel="stylesheet" type="text/css" href="/build/app-assets/css/colors.css">
    <link rel="stylesheet" type="text/css" href="/build/app-assets/css/components.css">
    <link rel="stylesheet" type="text/css" href="/build/app-assets/css/themes/dark-layout.css">
    <link rel="stylesheet" type="text/css" href="/build/app-assets/css/themes/bordered-layout.css">
    <link rel="stylesheet" type="text/css" href="/build/app-assets/css/themes/semi-dark-layout.css">

    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css" href="/build/app-assets/css/core/menu/menu-types/horizontal-menu.css">
    <link rel="stylesheet" type="text/css" href="/build/app-assets/css/plugins/forms/form-validation.css">
    <link rel="stylesheet" type="text/css" href="/build/app-assets/css/pages/page-auth.css">
    <!-- END: Page CSS-->

    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="/build/assets/css/style.css">
    <!-- END: Custom CSS-->
    
    <script src='https://www.google.com/recaptcha/api.js'></script>
     <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>

</head>
<!-- END: Head-->