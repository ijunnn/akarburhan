-- phpMyAdmin SQL Dump
-- version 4.9.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Sep 13, 2021 at 07:29 AM
-- Server version: 10.3.31-MariaDB-log-cll-lve
-- PHP Version: 7.3.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `izunnetc_borneocell`
--

-- --------------------------------------------------------

--
-- Table structure for table `berita`
--

CREATE TABLE `berita` (
  `id` int(10) NOT NULL,
  `date` date NOT NULL,
  `time` time NOT NULL,
  `tipe` enum('INFORMASI','PERINGATAN','PENTING','LAYANAN','PERBAIKAN') COLLATE utf8_swedish_ci NOT NULL,
  `subjek` text COLLATE utf8_swedish_ci NOT NULL,
  `konten` text COLLATE utf8_swedish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_swedish_ci;

--
-- Dumping data for table `berita`
--

INSERT INTO `berita` (`id`, `date`, `time`, `tipe`, `subjek`, `konten`) VALUES
(29, '2021-09-12', '00:32:21', 'INFORMASI', 'UNTUK TAMU DEMO TERHORMAT', 'MOHON TIDAK MERUBAH DATA APAPUN. SILAHKAN DIPERBOLEHKAN MENGECEK TETAPI TIDAK DIPERKENANKAN MERUBAH DATA SALDO DLL. ');

-- --------------------------------------------------------

--
-- Table structure for table `deposit`
--

CREATE TABLE `deposit` (
  `id` int(10) NOT NULL,
  `kode_deposit` varchar(250) NOT NULL,
  `username` varchar(250) NOT NULL,
  `tipe` varchar(255) NOT NULL,
  `provider` varchar(255) NOT NULL,
  `payment` varchar(250) NOT NULL,
  `nomor_pengirim` varchar(250) NOT NULL,
  `tujuan` varchar(50) NOT NULL,
  `jumlah_transfer` int(255) NOT NULL,
  `get_saldo` varchar(250) NOT NULL,
  `status` enum('Success','Pending','Error','') NOT NULL,
  `place_from` varchar(50) NOT NULL DEFAULT 'WEB',
  `date` date NOT NULL,
  `time` time NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `deposit`
--

INSERT INTO `deposit` (`id`, `kode_deposit`, `username`, `tipe`, `provider`, `payment`, `nomor_pengirim`, `tujuan`, `jumlah_transfer`, `get_saldo`, `status`, `place_from`, `date`, `time`) VALUES
(65, '50236', 'tesajasyah', 'BANK', 'OVO', 'OVO Konfirmasi Otomatis', '-', '082288808400 a.n BAGUS ADETYO N', 50877, '50877', 'Pending', 'Website', '2021-09-07', '20:20:28');

-- --------------------------------------------------------

--
-- Table structure for table `deposit_bank`
--

CREATE TABLE `deposit_bank` (
  `id` int(10) NOT NULL,
  `kode_deposit` varchar(250) NOT NULL,
  `username` varchar(250) NOT NULL,
  `tipe` varchar(255) NOT NULL,
  `provider` varchar(255) NOT NULL,
  `payment` varchar(250) NOT NULL,
  `nomor_pengirim` varchar(250) NOT NULL,
  `tujuan` varchar(50) NOT NULL,
  `jumlah_transfer` int(255) NOT NULL,
  `get_saldo` varchar(250) NOT NULL,
  `status` enum('Success','Pending','Error','') NOT NULL,
  `place_from` varchar(50) NOT NULL DEFAULT 'WEB',
  `date` date NOT NULL,
  `time` time NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `deposit_dana`
--

CREATE TABLE `deposit_dana` (
  `id` int(10) NOT NULL,
  `kode_deposit` varchar(250) NOT NULL,
  `username` varchar(250) NOT NULL,
  `tipe` varchar(255) NOT NULL,
  `provider` varchar(255) NOT NULL,
  `payment` varchar(250) NOT NULL,
  `nomor_pengirim` varchar(250) NOT NULL,
  `tujuan` varchar(50) NOT NULL,
  `jumlah_transfer` int(255) NOT NULL,
  `get_saldo` varchar(250) NOT NULL,
  `status` enum('Success','Pending','Error','') NOT NULL,
  `place_from` varchar(50) NOT NULL DEFAULT 'WEB',
  `date` date NOT NULL,
  `time` time NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `deposit_dana`
--

INSERT INTO `deposit_dana` (`id`, `kode_deposit`, `username`, `tipe`, `provider`, `payment`, `nomor_pengirim`, `tujuan`, `jumlah_transfer`, `get_saldo`, `status`, `place_from`, `date`, `time`) VALUES
(18, '165341', 'pebirahman97', 'EMoney', 'DANA', 'DANA Konfirmasi Manual', '-', '082314758588', 100468, '100468', 'Error', 'Website', '2021-04-09', '21:20:51'),
(19, '564503', 'Yadi23', 'EMoney', 'DANA', 'DANA Konfirmasi Manual', '-', '082314758588', 10089, '10089', 'Pending', 'Website', '2021-04-09', '22:40:46');

-- --------------------------------------------------------

--
-- Table structure for table `deposit_emoney`
--

CREATE TABLE `deposit_emoney` (
  `id` int(10) NOT NULL,
  `kode_deposit` varchar(250) NOT NULL,
  `username` varchar(250) NOT NULL,
  `tipe` varchar(255) NOT NULL,
  `provider` varchar(255) NOT NULL,
  `payment` varchar(250) NOT NULL,
  `nomor_pengirim` varchar(250) NOT NULL,
  `tujuan` varchar(50) NOT NULL,
  `jumlah_transfer` int(255) NOT NULL,
  `get_saldo` varchar(250) NOT NULL,
  `status` enum('Success','Pending','Error','') NOT NULL,
  `place_from` varchar(50) NOT NULL DEFAULT 'WEB',
  `date` date NOT NULL,
  `time` time NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `deposit_emoney`
--

INSERT INTO `deposit_emoney` (`id`, `kode_deposit`, `username`, `tipe`, `provider`, `payment`, `nomor_pengirim`, `tujuan`, `jumlah_transfer`, `get_saldo`, `status`, `place_from`, `date`, `time`) VALUES
(21, '740772', 'demo', 'EMoney', 'DANA', 'DANA Konfirmasi Otomatis', '-', '082288808400', 10901, '10901', 'Error', 'Website', '2021-09-07', '18:34:09'),
(20, '877008', 'demo', 'EMoney', 'GOPAY', 'GOPAY Konfirmasi Manual', '-', '085876550051', 200675, '200675', 'Error', 'Website', '2021-09-05', '13:46:21');

-- --------------------------------------------------------

--
-- Table structure for table `deposit_gopay`
--

CREATE TABLE `deposit_gopay` (
  `id` int(10) NOT NULL,
  `kode_deposit` varchar(250) NOT NULL,
  `username` varchar(250) NOT NULL,
  `tipe` varchar(255) NOT NULL,
  `provider` varchar(255) NOT NULL,
  `payment` varchar(250) NOT NULL,
  `nomor_pengirim` varchar(250) NOT NULL,
  `tujuan` varchar(50) NOT NULL,
  `jumlah_transfer` int(255) NOT NULL,
  `get_saldo` varchar(250) NOT NULL,
  `status` enum('Success','Pending','Error','') NOT NULL,
  `place_from` varchar(50) NOT NULL DEFAULT 'WEB',
  `date` date NOT NULL,
  `time` time NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `deposit_gopay`
--

INSERT INTO `deposit_gopay` (`id`, `kode_deposit`, `username`, `tipe`, `provider`, `payment`, `nomor_pengirim`, `tujuan`, `jumlah_transfer`, `get_saldo`, `status`, `place_from`, `date`, `time`) VALUES
(18, '723695', 'pebirahman97', 'EMoney', 'GOPAY', 'GOPAY MANUAL', '-', '082314758588', 10682, '10682', 'Error', 'Website', '2021-04-09', '20:53:16'),
(19, '120666', 'Kopi94', 'EMoney', 'GOPAY', 'GOPAY MANUAL', '-', '082314758588', 10242, '10242', 'Pending', 'Website', '2021-04-09', '22:36:14'),
(20, '322664', 'pebirahman97', 'EMoney', 'GOPAY', 'GOPAY MANUAL', '-', '082314758588', 10335, '10335', 'Error', 'Website', '2021-04-09', '23:53:44'),
(21, '960447', 'demo', 'EMoney', 'GOPAY', 'Gopay Konfirmasi Otomatis', '-', '085876550051', 10172, '10172', 'Pending', 'Website', '2021-08-10', '07:32:17');

-- --------------------------------------------------------

--
-- Table structure for table `deposit_ovo`
--

CREATE TABLE `deposit_ovo` (
  `id` int(10) NOT NULL,
  `kode_deposit` varchar(250) NOT NULL,
  `username` varchar(250) NOT NULL,
  `tipe` varchar(255) NOT NULL,
  `provider` varchar(255) NOT NULL,
  `payment` varchar(250) NOT NULL,
  `nomor_pengirim` varchar(250) NOT NULL,
  `tujuan` varchar(50) NOT NULL,
  `jumlah_transfer` int(255) NOT NULL,
  `get_saldo` varchar(250) NOT NULL,
  `status` enum('Success','Pending','Error','') NOT NULL,
  `place_from` varchar(50) NOT NULL DEFAULT 'WEB',
  `date` date NOT NULL,
  `time` time NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `deposit_ovo`
--

INSERT INTO `deposit_ovo` (`id`, `kode_deposit`, `username`, `tipe`, `provider`, `payment`, `nomor_pengirim`, `tujuan`, `jumlah_transfer`, `get_saldo`, `status`, `place_from`, `date`, `time`) VALUES
(18, '883572', 'pebirahman97', 'EMoney', 'OVO', 'OVO Konfirmasi Otomatis', '-', '082314758588', 100123, '100123', 'Error', 'Website', '2021-04-09', '21:23:40'),
(19, '435824', 'Parjo94', 'EMoney', 'OVO', 'OVO Konfirmasi Otomatis', '-', '082314758588', 10974, '10974', 'Pending', 'Website', '2021-04-09', '22:34:36');

-- --------------------------------------------------------

--
-- Table structure for table `deposit_voucher`
--

CREATE TABLE `deposit_voucher` (
  `id` int(10) NOT NULL,
  `kode_deposit` varchar(250) NOT NULL,
  `username` varchar(250) NOT NULL,
  `tipe` varchar(255) NOT NULL,
  `provider` varchar(255) NOT NULL,
  `payment` varchar(250) NOT NULL,
  `nomor_pengirim` varchar(250) NOT NULL,
  `tujuan` varchar(50) NOT NULL,
  `jumlah_transfer` int(255) NOT NULL,
  `get_saldo` varchar(250) NOT NULL,
  `status` enum('Success','Pending','Error','') NOT NULL,
  `place_from` varchar(50) NOT NULL DEFAULT 'WEB',
  `date` date NOT NULL,
  `time` time NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `halaman`
--

CREATE TABLE `halaman` (
  `id` int(2) NOT NULL,
  `konten` text NOT NULL,
  `update_terakhir` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `harga_pendaftaran`
--

CREATE TABLE `harga_pendaftaran` (
  `id` int(2) NOT NULL,
  `level` varchar(50) NOT NULL,
  `harga` double NOT NULL,
  `bonus` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `harga_pendaftaran`
--

INSERT INTO `harga_pendaftaran` (`id`, `level`, `harga`, `bonus`) VALUES
(1, 'Member', 0, 0),
(2, 'Agen', 0, 0),
(3, 'Reseller', 0, 0),
(4, 'Admin', 0, 0),
(5, 'Developers', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `history_saldo`
--

CREATE TABLE `history_saldo` (
  `id` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `aksi` enum('Penambahan Saldo','Pengurangan Saldo') NOT NULL,
  `nominal` double NOT NULL,
  `pesan` varchar(255) NOT NULL,
  `date` date NOT NULL,
  `time` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `history_saldo`
--

INSERT INTO `history_saldo` (`id`, `username`, `aksi`, `nominal`, `pesan`, `date`, `time`) VALUES
(41, 'demo', 'Pengurangan Saldo', 1768, 'Pemesanan dengan Order ID 5391665', '2021-09-05', '19:44:48'),
(42, 'demo', 'Pengurangan Saldo', 1836, 'Pemesanan dengan Order ID 4577465', '2021-09-05', '19:45:14'),
(43, 'demo', 'Pengurangan Saldo', 1708, 'Pemesanan dengan Order ID 6912142', '2021-09-05', '19:48:02'),
(44, 'demo', 'Pengurangan Saldo', 2610, 'Pemesanan dengan Order ID 1658877', '2021-09-05', '19:48:42'),
(45, 'demo', 'Pengurangan Saldo', 1520.6, 'Pemesanan dengan Order ID 4234255', '2021-09-05', '19:50:23'),
(46, 'demo', 'Pengurangan Saldo', 35100, 'Pemesanan dengan Order ID 7438583', '2021-09-05', '19:58:00'),
(47, 'demo', 'Pengurangan Saldo', 0, 'Penambahan Pengguna Dengan Username tesajasyah Dengan level Member ', '2021-09-07', '20:17:11'),
(48, 'demo', 'Pengurangan Saldo', 35100, 'Pemesanan dengan Order ID 1931271', '2021-09-08', '18:52:25');

-- --------------------------------------------------------

--
-- Table structure for table `kategori_layanan`
--

CREATE TABLE `kategori_layanan` (
  `id` int(30) NOT NULL,
  `nama` varchar(100) COLLATE utf8_swedish_ci NOT NULL,
  `kode` varchar(50) COLLATE utf8_swedish_ci NOT NULL,
  `tipe` varchar(50) COLLATE utf8_swedish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_swedish_ci;

--
-- Dumping data for table `kategori_layanan`
--

INSERT INTO `kategori_layanan` (`id`, `nama`, `kode`, `tipe`) VALUES
(677, 'NETFLIX', 'NETFLIX', 'NETFLIX'),
(678, 'YT', 'YT', 'YT'),
(679, 'PLN', 'PLN', 'TOKENPLN'),
(680, 'DANA', 'DANA', 'SALGO'),
(681, 'SHOPEE PAY', 'SHOPEE PAY', 'SALGO'),
(682, 'AXIS', 'AXIS', 'PULSA'),
(683, 'XL', 'XL', 'PULSA'),
(684, 'TRI', 'TRI', 'PULSA'),
(685, 'TELKOMSEL', 'TELKOMSEL', 'PULSA'),
(686, 'INDOSAT', 'INDOSAT', 'PULSA'),
(687, 'GOOGLE PLAY INDONESIA', 'GOOGLE PLAY INDONESIA', 'VGAME'),
(688, 'GOOGLE PLAY US REGION', 'GOOGLE PLAY US REGION', 'VGAME'),
(689, 'GEMSCOOL', 'GEMSCOOL', 'VGAME'),
(690, 'MEGAXUS', 'MEGAXUS', 'VGAME'),
(691, 'MOBILE LEGEND', 'MOBILE LEGEND', 'VGAME'),
(692, 'WIFI ID', 'WIFI ID', 'VOUCHER'),
(693, 'POINT BLANK', 'POINT BLANK', 'VGAME'),
(694, 'PUBG', 'PUBG', 'VGAME'),
(695, 'FREE FIRE', 'FREE FIRE', 'VGAME'),
(696, 'ARENA OF VALOR', 'ARENA OF VALOR', 'VGAME'),
(697, 'SMART', 'SMART', 'PULSA'),
(698, 'Rules of Survival Mobile', 'Rules of Survival Mobile', 'VGAME'),
(699, 'Speed Drifters', 'Speed Drifters', 'VGAME'),
(700, 'STEAM SEA', 'STEAM SEA', 'VGAME'),
(701, 'ALFAMART VOUCHER', 'ALFAMART VOUCHER', 'VOUCHER'),
(702, 'INDOMARET', 'INDOMARET', 'VOUCHER'),
(703, 'BUKALAPAK', 'BUKALAPAK', 'VOUCHER'),
(704, 'JUNGLELAND', 'JUNGLELAND', 'VOUCHER'),
(705, 'DIGI', 'DIGI', 'LAINYA'),
(706, 'MAXIS', 'MAXIS', 'LAINYA'),
(707, 'CELCOM', 'CELCOM', 'LAINYA'),
(708, 'UMOBILE', 'UMOBILE', 'LAINYA'),
(709, 'TUNETALK', 'TUNETALK', 'LAINYA'),
(710, 'MANDIRI E-TOLL', 'MANDIRI E-TOLL', 'SALGO'),
(711, 'GO PAY', 'GO PAY', 'SALGO'),
(712, 'PUBG MOBILE', 'PUBG MOBILE', 'VGAME'),
(713, 'OVO', 'OVO', 'SALGO'),
(714, 'BRI BRIZZI', 'BRI BRIZZI', 'SALGO'),
(715, 'GRAB', 'GRAB', 'SALGO'),
(716, 'GRAB DRIVER', 'GRAB DRIVER', 'VOUCHER'),
(717, 'TAPCASH BNI', 'TAPCASH BNI', 'SALGO'),
(718, 'ITUNES US REGION', 'ITUNES US REGION', 'VGAME'),
(719, 'Unipin Voucher', 'Unipin Voucher', 'VGAME'),
(720, 'IMVU', 'IMVU', 'VGAME'),
(721, 'Steam Wallet (USD)', 'Steam Wallet (USD)', 'LAINYA'),
(722, 'AXIS AIGO', 'AXIS AIGO', 'VOUCHER'),
(723, 'TSEL DATA', 'TSEL DATA', 'VOUCHER'),
(724, 'TSEL BULK', 'TSEL BULK', 'VOUCHER'),
(725, 'AIGO MINI', 'AIGO MINI', 'VOUCHER'),
(726, 'AIGO OWSEM', 'AIGO OWSEM', 'VOUCHER'),
(727, 'VOC INDOSAT', 'VOC INDOSAT', 'VOUCHER'),
(728, 'VOC TRI', 'VOC TRI', 'VOUCHER'),
(729, 'VOC TRI A', 'VOC TRI A', 'VOUCHER'),
(730, 'VOC TRI B', 'VOC TRI B', 'VOUCHER'),
(731, 'VOC TRI C', 'VOC TRI C', 'VOUCHER'),
(732, 'XL MINI', 'XL MINI', 'VOUCHER'),
(733, 'XL LITE', 'XL LITE', 'VOUCHER'),
(734, 'TEL TSEL', 'TEL TSEL', 'PKSMS'),
(735, 'TEL TSEL 3', 'TEL TSEL 3', 'PKSMS'),
(736, 'TEL TSEL 1', 'TEL TSEL 1', 'PKSMS'),
(737, 'TEL TSEL 2', 'TEL TSEL 2', 'PKSMS'),
(738, 'SMS TSEL', 'SMS TSEL', 'PKSMS'),
(739, 'TEL IND', 'TEL IND', 'PKSMS'),
(740, 'SMS IND', 'SMS IND', 'PKSMS'),
(741, 'TEL TRI', 'TEL TRI', 'PKSMS'),
(742, 'TEL XL', 'TEL XL', 'PKSMS'),
(743, 'BULK', 'BULK', 'PKIN'),
(744, 'FLASH', 'FLASH', 'PKIN'),
(745, 'MINI', 'MINI', 'PKIN'),
(746, 'MAX', 'MAX', 'PKIN'),
(747, 'GAMESMAX', 'GAMESMAX', 'PKIN'),
(748, 'COMBO', 'COMBO', 'PKIN'),
(749, 'TSEL ALL', 'TSEL ALL', 'PKIN'),
(750, 'MALAM', 'MALAM', 'PKIN'),
(751, 'KECIL', 'KECIL', 'PKIN'),
(752, 'OMG', 'OMG', 'PKIN'),
(753, 'IND DATA', 'IND DATA', 'PKIN'),
(754, 'FREEDOM', 'FREEDOM', 'PKIN'),
(755, 'FREE COMBO', 'FREE COMBO', 'PKIN'),
(756, 'FREE INET', 'FREE INET', 'PKIN'),
(757, 'FREE INET P', 'FREE INET P', 'PKIN'),
(758, 'HAJI', 'HAJI', 'PKIN'),
(759, 'HAJI K', 'HAJI K', 'PKIN'),
(760, 'DATA UNLI', 'DATA UNLI', 'PKIN'),
(761, 'EKSTRA', 'EKSTRA', 'PKIN'),
(762, 'YELLOW', 'YELLOW', 'PKIN'),
(763, 'APPS', 'APPS', 'PKIN'),
(764, 'BRONET', 'BRONET', 'PKIN'),
(765, 'OWSEM', 'OWSEM', 'PKIN'),
(766, 'SMART INT', 'SMART INT', 'PKIN'),
(767, 'SMART MIX', 'SMART MIX', 'PKIN'),
(768, 'SMART EVO', 'SMART EVO', 'PKIN'),
(769, 'TRI DATA', 'TRI DATA', 'PKIN'),
(770, 'TRI MIX', 'TRI MIX', 'PKIN'),
(771, 'TRI LTE', 'TRI LTE', 'PKIN'),
(772, 'TRI AO', 'TRI AO', 'PKIN'),
(773, 'TRI AON', 'TRI AON', 'PKIN'),
(774, 'TRI MINI', 'TRI MINI', 'PKIN'),
(775, 'NASIONAL', 'NASIONAL', 'PKIN'),
(776, 'COMBO LITE', 'COMBO LITE', 'PKIN'),
(777, 'HOTROD', 'HOTROD', 'PKIN'),
(778, 'XTRA COMBO', 'XTRA COMBO', 'PKIN'),
(779, 'COMBO VIP', 'COMBO VIP', 'PKIN'),
(780, 'COMBO BARU', 'COMBO BARU', 'PKIN'),
(781, 'XTRA KUOTA', 'XTRA KUOTA', 'PKIN'),
(782, 'XL MINI KC', 'XL MINI KC', 'PKIN'),
(783, 'PLUS', 'PLUS', 'PKIN'),
(784, 'by.U', 'by.U', 'PULSA'),
(785, 'GOPAY DRIVER', 'GOPAY DRIVER', 'SALGO'),
(786, 'XL DATA', 'XL DATA', 'PKIN'),
(787, 'VOUCHER POINT BLANK', 'VOUCHER POINT BLANK', 'VGAME'),
(788, 'AXISTEL', 'AXISTEL', 'VOUCHER'),
(789, 'HAGO', 'HAGO', 'VGAME'),
(790, 'VOC SMART', 'VOC SMART', 'VOUCHER'),
(791, 'GARENA', 'GARENA', 'VGAME'),
(792, 'Call of Duty MOBILE', 'Call of Duty MOBILE', 'VGAME'),
(793, 'i.saku', 'i.saku', 'SALGO'),
(794, 'Sakuku', 'Sakuku', 'SALGO'),
(795, 'Mitra Shopee', 'Mitra Shopee', 'SALGO'),
(796, 'Indomaret Card E-Money', 'Indomaret Card E-Money', 'SALGO'),
(797, 'LinkAja', 'LinkAja', 'SALGO'),
(798, 'Sausage Man', 'Sausage Man', 'VGAME'),
(799, 'Higgs Domino', 'Higgs Domino', 'VGAME'),
(888, 'Instagram Views', 'Instagram Views', 'Sosial Media'),
(889, 'SoundCloud', 'SoundCloud', 'Sosial Media'),
(890, 'Telegram', 'Telegram', 'Sosial Media'),
(891, 'Google', 'Google', 'Sosial Media'),
(892, 'Instagram Story Views', 'Instagram Story Views', 'Sosial Media'),
(893, 'Instagram Live Video', 'Instagram Live Video', 'Sosial Media'),
(894, 'Instagram Story / Impressions / Saves / Profile Visit', 'Instagram Story / Impressions / Saves / Profile Vi', 'Sosial Media'),
(895, 'Twitter Views & Impressions', 'Twitter Views & Impressions', 'Sosial Media'),
(896, 'Linkedin', 'Linkedin', 'Sosial Media'),
(897, 'Website Traffic', 'Website Traffic', 'Sosial Media'),
(898, 'Instagram TV', 'Instagram TV', 'Sosial Media'),
(899, 'Youtube Likes / Dislikes / Shares / Comment', 'Youtube Likes / Dislikes / Shares / Comment', 'Sosial Media'),
(900, 'Youtube Views', 'Youtube Views', 'Sosial Media'),
(901, 'Spotify', 'Spotify', 'Sosial Media'),
(902, 'Facebook Page / Website - Likes / Stars', 'Facebook Page / Website - Likes / Stars', 'Sosial Media'),
(903, 'Shopee/Tokopedia/Bukalapak', 'Shopee/Tokopedia/Bukalapak', 'Sosial Media'),
(904, 'Pinterest', 'Pinterest', 'Sosial Media'),
(905, 'Instagram Like Indonesia', 'Instagram Like Indonesia', 'Sosial Media'),
(906, 'Instagram Like Komentar [ top koment ]', 'Instagram Like Komentar [ top koment ]', 'Sosial Media'),
(907, 'Instagram Likes', 'Instagram Likes', 'Sosial Media'),
(908, 'TikTok Followers', 'TikTok Followers', 'Sosial Media'),
(909, 'TikTok Likes', 'TikTok Likes', 'Sosial Media'),
(910, 'Instagram Followers Indonesia', 'Instagram Followers Indonesia', 'Sosial Media'),
(911, 'Facebook Followers / Friends', 'Facebook Followers / Friends', 'Sosial Media'),
(912, 'Facebook Post Likes / Comments / Shares', 'Facebook Post Likes / Comments / Shares', 'Sosial Media'),
(913, 'Instagram Followers [guaranteed]', 'Instagram Followers [guaranteed]', 'Sosial Media'),
(914, '- PROMO - ON OFF', '- PROMO - ON OFF', 'Sosial Media'),
(915, 'Instagram Followers [ No Refill ]', 'Instagram Followers [ No Refill ]', 'Sosial Media'),
(916, 'TikTok View/share/comment', 'TikTok View/share/comment', 'Sosial Media'),
(917, 'Youtube Live Stream', 'Youtube Live Stream', 'Sosial Media'),
(918, 'Twitch', 'Twitch', 'Sosial Media'),
(919, 'Likee app', 'Likee app', 'Sosial Media'),
(920, 'Youtube View Jam Tayang', 'Youtube View Jam Tayang', 'Sosial Media'),
(921, 'Twitter Indonesia', 'Twitter Indonesia', 'Sosial Media'),
(922, 'Youtube Subscribers', 'Youtube Subscribers', 'Sosial Media'),
(923, 'TikTok INDONESIA', 'TikTok INDONESIA', 'Sosial Media'),
(924, 'Instagram Followers Indonesia Guaranted/Refill', 'Instagram Followers Indonesia Guaranted/Refill', 'Sosial Media'),
(925, 'Youtube View Target Negara', 'Youtube View Target Negara', 'Sosial Media'),
(926, 'Instagram Comments', 'Instagram Comments', 'Sosial Media'),
(927, 'Twitter Favorites/Like', 'Twitter Favorites/Like', 'Sosial Media'),
(928, 'Clubhouse', 'Clubhouse', 'Sosial Media'),
(929, 'Twitter Followers', 'Twitter Followers', 'Sosial Media'),
(930, 'Instagram Reels', 'Instagram Reels', 'Sosial Media'),
(931, 'Facebook Video Views / Live Stream', 'Facebook Video Views / Live Stream', 'Sosial Media');

-- --------------------------------------------------------

--
-- Table structure for table `layanan_pulsa`
--

CREATE TABLE `layanan_pulsa` (
  `id` int(11) NOT NULL,
  `service_id` int(255) NOT NULL,
  `provider_id` varchar(50) COLLATE utf8_swedish_ci NOT NULL,
  `operator` varchar(50) COLLATE utf8_swedish_ci NOT NULL,
  `layanan` text COLLATE utf8_swedish_ci NOT NULL,
  `harga` double NOT NULL,
  `harga_api` double NOT NULL,
  `profit` double NOT NULL,
  `status` enum('Normal','Gangguan') COLLATE utf8_swedish_ci NOT NULL,
  `provider` varchar(50) COLLATE utf8_swedish_ci NOT NULL,
  `tipe` varchar(50) COLLATE utf8_swedish_ci NOT NULL,
  `catatan` text COLLATE utf8_swedish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_swedish_ci;

--
-- Dumping data for table `layanan_pulsa`
--

INSERT INTO `layanan_pulsa` (`id`, `service_id`, `provider_id`, `operator`, `layanan`, `harga`, `harga_api`, `profit`, `status`, `provider`, `tipe`, `catatan`) VALUES
(2848, 1, 'NP1', 'NETFLIX', 'Netflix Premium 1 Month [Shared] NO VPN', 45800, 45500, 800, 'Normal', 'DPEDIA', 'NETFLIX', 'input menggunakan nomor whatsapp - proses max 1x24 jam - full garansi 30 Hari'),
(2849, 2, 'NP2', 'NETFLIX', 'Netflix Premium 1 Month [Private] NO VPN', 145800, 145500, 800, 'Normal', 'DPEDIA', 'NETFLIX', 'input menggunakan nomor whatsapp - proses max 1x24 jam - full garansi 30 Hari'),
(2850, 3, 'YT1', 'YT', 'Youtube Premium 1 Month - Invite', 5800, 5500, 800, 'Normal', 'DPEDIA', 'YT', 'input menggunakan email - proses max 1x24 jam - full garansi 30 Hari'),
(2851, 4, 'YT2', 'YT', 'Youtube Premium 1 Month - Admin', 25800, 25500, 800, 'Normal', 'DPEDIA', 'YT', 'input menggunakan nomor whatsapp\r\n- proses max 1x24 jam - full garansi 30 Hari'),
(2852, 5, 'YT3', 'YT', 'Youtube Premium 4 Month', 15800, 15500, 800, 'Normal', 'DPEDIA', 'YT', 'input menggunakan nomor whatsapp- proses max 1x24 jam - buy with your own risk'),
(2853, 6, 'DPLN20', 'PLN', 'PLN 20.000', 20868, 20568, 800, 'Normal', 'DPEDIA', 'TOKENPLN', 'PLN 20.000'),
(2854, 7, 'DPLN50', 'PLN', 'PLN 50.000', 50883, 50583, 800, 'Normal', 'DPEDIA', 'TOKENPLN', 'PLN 50.000'),
(2855, 8, 'DPLN100', 'PLN', 'PLN 100.000', 100883, 100583, 800, 'Normal', 'DPEDIA', 'TOKENPLN', 'PLN 100.000'),
(2856, 9, 'DPLN200', 'PLN', 'PLN 200.000', 200883, 200583, 800, 'Normal', 'DPEDIA', 'TOKENPLN', 'PLN 200.000'),
(2857, 10, 'DPLN500', 'PLN', 'PLN 500.000', 500883, 500583, 800, 'Normal', 'DPEDIA', 'TOKENPLN', 'PLN 500.000'),
(2858, 11, 'DPLN1000', 'PLN', 'PLN 1.000.000', 1000883, 1000583, 800, 'Normal', 'DPEDIA', 'TOKENPLN', 'PLN 1.000.000'),
(2859, 12, 'DPDANA10', 'DANA', 'DANA Rp 10.000', 10915, 10615, 800, 'Normal', 'DPEDIA', 'SALGO', '-'),
(2860, 13, 'DPDANA20', 'DANA', 'DANA Rp 20.000', 20915, 20615, 800, 'Normal', 'DPEDIA', 'SALGO', '-'),
(2861, 14, 'DPDANA25', 'DANA', 'DANA Rp 25.000', 26110, 25810, 800, 'Normal', 'DPEDIA', 'SALGO', '-'),
(2862, 15, 'DPDANA50', 'DANA', 'DANA Rp 50.000', 50915, 50615, 800, 'Normal', 'DPEDIA', 'SALGO', '-'),
(2863, 16, 'DPDANA1OO', 'DANA', 'DANA Rp 100.000', 100915, 100615, 800, 'Normal', 'DPEDIA', 'SALGO', '-'),
(2864, 17, 'DPDANA200', 'DANA', 'DANA Rp 200.000', 200915, 200615, 800, 'Normal', 'DPEDIA', 'SALGO', '-'),
(2865, 18, 'DPDANA75', 'DANA', 'DANA 75.000', 75920, 75620, 800, 'Normal', 'DPEDIA', 'SALGO', '-'),
(2866, 19, 'DPDANA150', 'DANA', 'DANA Rp 150.000', 150915, 150615, 800, 'Normal', 'DPEDIA', 'SALGO', '-'),
(2867, 20, 'DPDANA300', 'DANA', 'DANA Rp 300.000', 300915, 300615, 800, 'Normal', 'DPEDIA', 'SALGO', '-'),
(2868, 21, 'DPDANA30', 'DANA', 'DANA Rp 30.000', 30915, 30615, 800, 'Normal', 'DPEDIA', 'SALGO', '-'),
(2869, 22, 'DPDANA40', 'DANA', 'DANA Rp 40.000', 40915, 40615, 800, 'Normal', 'DPEDIA', 'SALGO', '-'),
(2870, 23, 'DPDANA500', 'DANA', 'DANA 500.000', 500890, 500590, 800, 'Normal', 'DPEDIA', 'SALGO', '-'),
(2871, 24, 'DSHOPPE20', 'SHOPEE PAY', 'SHOPEE PAY 20.000', 21125, 20825, 800, 'Normal', 'DPEDIA', 'SALGO', 'SHOPEE PAY 20.000'),
(2872, 25, 'DSHOPPE25', 'SHOPEE PAY', 'SHOPEE PAY 25.000', 26125, 25825, 800, 'Normal', 'DPEDIA', 'SALGO', 'SHOPEE PAY 25.000'),
(2873, 26, 'DSHOPPE30', 'SHOPEE PAY', 'SHOPEE PAY 30.000', 31125, 30825, 800, 'Normal', 'DPEDIA', 'SALGO', 'SHOPEE PAY 30.000'),
(2874, 27, 'DSHOPPE50', 'SHOPEE PAY', 'SHOPEE PAY 50.000', 51120, 50820, 800, 'Normal', 'DPEDIA', 'SALGO', 'SHOPEE PAY 50.000'),
(2875, 28, 'DSHOPPE75', 'SHOPEE PAY', 'SHOPEE PAY 75.000', 76125, 75825, 800, 'Normal', 'DPEDIA', 'SALGO', 'SHOPEE PAY 75.000'),
(2876, 29, 'DSHOPPE100', 'SHOPEE PAY', 'SHOPEE PAY 100.000', 101125, 100825, 800, 'Normal', 'DPEDIA', 'SALGO', 'SHOPEE PAY 100.000'),
(2877, 30, 'DSHOPPE200', 'SHOPEE PAY', 'SHOPEE PAY 200.000', 201125, 200825, 800, 'Normal', 'DPEDIA', 'SALGO', 'SHOPEE PAY 200.000'),
(2878, 31, 'DSHOPPE300', 'SHOPEE PAY', 'SHOPEE PAY 300.000', 301325, 301025, 800, 'Normal', 'DPEDIA', 'SALGO', 'SHOPEE PAY 300.000'),
(2879, 32, 'DAX5', 'AXIS', 'Axis 5.000', 6630, 6330, 800, 'Normal', 'DPEDIA', 'PULSA', 'Pulsa Axis Rp 5.000'),
(2880, 33, 'DAX10', 'AXIS', 'Axis 10.000', 11550, 11250, 800, 'Normal', 'DPEDIA', 'PULSA', 'Pulsa Axis Rp 10.000'),
(2881, 34, 'DAX25', 'AXIS', 'Axis 25.000', 25475, 25175, 800, 'Normal', 'DPEDIA', 'PULSA', 'Pulsa Axis Rp 25.000'),
(2882, 35, 'DAX50', 'AXIS', 'Axis 50.000', 50125, 49825, 800, 'Normal', 'DPEDIA', 'PULSA', 'Pulsa Axis Rp 50.000'),
(2883, 36, 'DAX100', 'AXIS', 'Axis 100.000', 99050, 98750, 800, 'Normal', 'DPEDIA', 'PULSA', 'Pulsa Axis Rp 100.000'),
(2884, 37, 'DAX200', 'AXIS', 'Axis 200.000', 197875, 197575, 800, 'Normal', 'DPEDIA', 'PULSA', 'Pulsa Axis Rp 200.000'),
(2885, 38, 'DAX15', 'AXIS', 'Axis 15.000', 15653, 15353, 800, 'Normal', 'DPEDIA', 'PULSA', 'Pulsa Axis Rp 15.000'),
(2886, 39, 'DXL5', 'XL', 'Xl 5.000', 6645, 6345, 800, 'Normal', 'DPEDIA', 'PULSA', 'Pulsa Xl Rp 5.000'),
(2887, 40, 'DXL10', 'XL', 'Xl 10.000', 11605, 11305, 800, 'Normal', 'DPEDIA', 'PULSA', 'Pulsa Xl Rp 10.000'),
(2888, 41, 'DXL15', 'XL', 'Xl 15.000', 15680, 15380, 800, 'Normal', 'DPEDIA', 'PULSA', 'Pulsa Xl Rp 15.000'),
(2889, 42, 'DXL25', 'XL', 'Xl 25.000', 25500, 25200, 800, 'Normal', 'DPEDIA', 'PULSA', 'Pulsa Xl Rp 25.000'),
(2890, 43, 'DXL50', 'XL', 'Xl 50.000', 50050, 49750, 800, 'Normal', 'DPEDIA', 'PULSA', 'Pulsa Xl Rp 50.000'),
(2891, 44, 'DXL100', 'XL', 'Xl 100.000', 99025, 98725, 800, 'Normal', 'DPEDIA', 'PULSA', 'Pulsa Xl Rp 100.000'),
(2892, 45, 'DXL150', 'XL', 'Xl 150.000', 150075, 149775, 800, 'Normal', 'DPEDIA', 'PULSA', 'Pulsa Xl Rp 150.000'),
(2893, 46, 'DXL200', 'XL', 'Xl 200.000', 198375, 198075, 800, 'Normal', 'DPEDIA', 'PULSA', 'Pulsa Xl Rp 200.000'),
(2894, 47, 'DXL300', 'XL', 'Xl 300.000', 299350, 299050, 800, 'Normal', 'DPEDIA', 'PULSA', 'Pulsa Xl Rp 300.000'),
(2895, 48, 'DXL500', 'XL', 'Xl 500.000', 499375, 499075, 800, 'Normal', 'DPEDIA', 'PULSA', 'Pulsa Xl Rp 500.000'),
(2896, 49, 'DXL1000', 'XL', 'Xl 1.000.000', 995375, 995075, 800, 'Normal', 'DPEDIA', 'PULSA', 'Pulsa Xl Rp 1.000.000'),
(2897, 50, 'DXL30', 'XL', 'XL 30.000', 30500, 30200, 800, 'Normal', 'DPEDIA', 'PULSA', 'Pulsa Xl Rp 30.000'),
(2898, 51, 'DTH1', 'TRI', 'Three 1.000', 2020, 1720, 800, 'Normal', 'DPEDIA', 'PULSA', 'Pulsa Three Rp 1.000'),
(2899, 52, 'DTH2', 'TRI', 'Three 2.000', 2980, 2680, 800, 'Normal', 'DPEDIA', 'PULSA', 'Pulsa Three Rp 2.000'),
(2900, 53, 'DTH3', 'TRI', 'Three 3.000', 3965, 3665, 800, 'Normal', 'DPEDIA', 'PULSA', 'Pulsa Three Rp 3.000'),
(2901, 54, 'DTH5', 'TRI', 'Three 5.000', 6098, 5798, 800, 'Normal', 'DPEDIA', 'PULSA', 'Pulsa Three Rp 5.000'),
(2902, 55, 'DTH10', 'TRI', 'Three 10.000', 10968, 10668, 800, 'Normal', 'DPEDIA', 'PULSA', 'Pulsa Three Rp 10.000'),
(2903, 56, 'DTH15', 'TRI', 'Three 15.000', 15370, 15070, 800, 'Normal', 'DPEDIA', 'PULSA', 'Pulsa Three Rp 15.000'),
(2904, 57, 'DTH20', 'TRI', 'Three 20.000', 20210, 19910, 800, 'Normal', 'DPEDIA', 'PULSA', 'Pulsa Three Rp 20.000'),
(2905, 58, 'DTH25', 'TRI', 'Three 25.000', 25050, 24750, 800, 'Normal', 'DPEDIA', 'PULSA', 'Pulsa Three Rp 25.000'),
(2906, 59, 'DTH30', 'TRI', 'Three 30.000', 29890, 29590, 800, 'Normal', 'DPEDIA', 'PULSA', 'Pulsa Three Rp 30.000'),
(2907, 60, 'DTH50', 'TRI', 'Three 50.000', 49250, 48950, 800, 'Normal', 'DPEDIA', 'PULSA', 'Pulsa Three Rp 50.000'),
(2908, 61, 'DTH100', 'TRI', 'Three 100.000', 97650, 97350, 800, 'Normal', 'DPEDIA', 'PULSA', 'Pulsa Three Rp 100.000'),
(2909, 62, 'DTH150', 'TRI', 'Three 150.000', 148850, 148550, 800, 'Normal', 'DPEDIA', 'PULSA', 'Pulsa Three Rp 150.000'),
(2910, 63, 'DTH300', 'TRI', 'Three 300.000', 295125, 294825, 800, 'Normal', 'DPEDIA', 'PULSA', 'Pulsa Three Rp 300.000'),
(2911, 64, 'DTH500', 'TRI', 'Three 500.000', 485375, 485075, 800, 'Normal', 'DPEDIA', 'PULSA', 'Pulsa Three Rp 500.000'),
(2912, 65, 'DTSEL1', 'TELKOMSEL', 'Telkomsel 1.000', 2405, 2105, 800, 'Normal', 'DPEDIA', 'PULSA', 'Pulsa Telkomsel Rp 1.000'),
(2913, 66, 'DTSEL5', 'TELKOMSEL', 'Telkomsel 5.000', 6140, 5840, 800, 'Normal', 'DPEDIA', 'PULSA', 'Pulsa Telkomsel Rp 5.000'),
(2914, 67, 'DTSEL10', 'TELKOMSEL', 'Telkomsel 10.000', 11050, 10750, 800, 'Normal', 'DPEDIA', 'PULSA', 'Pulsa Telkomsel Rp 10.000'),
(2915, 68, 'DTSEL20', 'TELKOMSEL', 'Telkomsel 20.000', 20760, 20460, 800, 'Normal', 'DPEDIA', 'PULSA', 'Pulsa Telkomsel Rp 20.000'),
(2916, 69, 'DTSEL25', 'TELKOMSEL', 'Telkomsel 25.000', 25725, 25425, 800, 'Normal', 'DPEDIA', 'PULSA', 'Pulsa Telkomsel Rp 25.000'),
(2917, 70, 'DTSEL40', 'TELKOMSEL', 'Telkomsel 40.000', 39900, 39600, 800, 'Normal', 'DPEDIA', 'PULSA', 'Pulsa Telkomsel Rp 40.000'),
(2918, 71, 'DTSEL50', 'TELKOMSEL', 'Telkomsel 50.000', 50550, 50250, 800, 'Normal', 'DPEDIA', 'PULSA', 'Pulsa Telkomsel Rp 50.000'),
(2919, 72, 'DTSEL100', 'TELKOMSEL', 'Telkomsel 100.000', 97800, 97500, 800, 'Normal', 'DPEDIA', 'PULSA', 'Pulsa Telkomsel Rp 100.000'),
(2920, 73, 'DTSEL150', 'TELKOMSEL', 'Telkomsel 150.000', 149550, 149250, 800, 'Normal', 'DPEDIA', 'PULSA', 'Pulsa Telkomsel Rp 150.000'),
(2921, 74, 'DTSEL200', 'TELKOMSEL', 'Telkomsel 200.000', 198845, 198545, 800, 'Normal', 'DPEDIA', 'PULSA', 'Pulsa Telkomsel Rp 200.000'),
(2922, 75, 'DTSEL300', 'TELKOMSEL', 'Telkomsel 300.000', 292875, 292575, 800, 'Normal', 'DPEDIA', 'PULSA', 'Pulsa Telkomsel Rp 300.000'),
(2923, 76, 'DTSEL500', 'TELKOMSEL', 'Telkomsel 500.000', 483975, 483675, 800, 'Normal', 'DPEDIA', 'PULSA', 'Pulsa Telkomsel Rp 500.000'),
(2924, 77, 'DTSEL1000', 'TELKOMSEL', 'Telkomsel 1.000.000', 969450, 969150, 800, 'Normal', 'DPEDIA', 'PULSA', 'Pulsa Telkomsel Rp 1.000.000'),
(2925, 78, 'DTSEL65', 'TELKOMSEL', 'Telkomsel 65.000', 65775, 65475, 800, 'Normal', 'DPEDIA', 'PULSA', 'Pulsa Telkomsel Rp 65.000'),
(2926, 79, 'DTSEL75', 'TELKOMSEL', 'Telkomsel 75.000', 74125, 73825, 800, 'Normal', 'DPEDIA', 'PULSA', 'Pulsa Telkomsel Rp 75.000'),
(2927, 80, 'DTSEL80', 'TELKOMSEL', 'Telkomsel 80.000', 80625, 80325, 800, 'Normal', 'DPEDIA', 'PULSA', 'Pulsa Telkomsel Rp 80.000'),
(2928, 81, 'DTSEL85', 'TELKOMSEL', 'Telkomsel 85.000', 85775, 85475, 800, 'Normal', 'DPEDIA', 'PULSA', 'Pulsa Telkomsel Rp 85.000'),
(2929, 82, 'DTSEL30', 'TELKOMSEL', 'Telkomsel 30.000', 30500, 30200, 800, 'Normal', 'DPEDIA', 'PULSA', 'Pulsa Telkomsel Rp 30.000'),
(2930, 83, 'DTSEL35', 'TELKOMSEL', 'Telkomsel 35.000', 35850, 35550, 800, 'Normal', 'DPEDIA', 'PULSA', 'Pulsa Telkomsel Rp 35.000'),
(2931, 84, 'DTSEL45', 'TELKOMSEL', 'Telkomsel 45.000', 46100, 45800, 800, 'Normal', 'DPEDIA', 'PULSA', 'Pulsa Telkomsel Rp 45.000'),
(2932, 85, 'DTSEL70', 'TELKOMSEL', 'Telkomsel 70.000', 70600, 70300, 800, 'Normal', 'DPEDIA', 'PULSA', 'Pulsa Telkomsel Rp 70.000'),
(2933, 86, 'DTSEL55', 'TELKOMSEL', 'Telkomsel 55.000', 55849, 55549, 800, 'Normal', 'DPEDIA', 'PULSA', 'Pulsa Telkomsel Rp 55.000'),
(2934, 87, 'DTSEL60', 'TELKOMSEL', 'Telkomsel 60.000', 60600, 60300, 800, 'Normal', 'DPEDIA', 'PULSA', 'Pulsa Telkomsel Rp 60.000'),
(2935, 88, 'DTSEL90', 'TELKOMSEL', 'Telkomsel 90.000', 90550, 90250, 800, 'Normal', 'DPEDIA', 'PULSA', 'Pulsa Telkomsel Rp 90.000'),
(2936, 89, 'DTSEL95', 'TELKOMSEL', 'Telkomsel 95.000', 95395, 95095, 800, 'Normal', 'DPEDIA', 'PULSA', 'Pulsa Telkomsel Rp 95.000'),
(2937, 90, 'DIND5', 'INDOSAT', 'Indosat 5.000', 6585, 6285, 800, 'Normal', 'DPEDIA', 'PULSA', 'Pulsa Indosat Rp 5.000'),
(2938, 91, 'DIND10', 'INDOSAT', 'Indosat 10.000', 11585, 11285, 800, 'Normal', 'DPEDIA', 'PULSA', 'Pulsa Indosat Rp 10.000'),
(2939, 92, 'DIND12', 'INDOSAT', 'Indosat 12.000', 12845, 12545, 800, 'Normal', 'DPEDIA', 'PULSA', 'Pulsa Indosat Rp 12.000'),
(2940, 93, 'DIND20', 'INDOSAT', 'Indosat 20.000', 20710, 20410, 800, 'Normal', 'DPEDIA', 'PULSA', 'Pulsa Indosat Rp 20.000'),
(2941, 94, 'DIND25', 'INDOSAT', 'Indosat 25.000', 25745, 25445, 800, 'Normal', 'DPEDIA', 'PULSA', 'Pulsa Indosat Rp 25.000'),
(2942, 95, 'DIND30', 'INDOSAT', 'Indosat 30.000', 30325, 30025, 800, 'Normal', 'DPEDIA', 'PULSA', 'Pulsa Indosat Rp 30.000'),
(2943, 96, 'DIND50', 'INDOSAT', 'Indosat 50.000', 49675, 49375, 800, 'Normal', 'DPEDIA', 'PULSA', 'Pulsa Indosat Rp 50.000'),
(2944, 97, 'DIND100', 'INDOSAT', 'Indosat 100.000', 97420, 97120, 800, 'Normal', 'DPEDIA', 'PULSA', 'Pulsa Indosat Rp 100.000'),
(2945, 98, 'DIND150', 'INDOSAT', 'Indosat 150.000', 140850, 140550, 800, 'Normal', 'DPEDIA', 'PULSA', 'Pulsa Indosat Rp 150.000'),
(2946, 99, 'DIND200', 'INDOSAT', 'Indosat 200.000', 186375, 186075, 800, 'Normal', 'DPEDIA', 'PULSA', 'Pulsa Indosat Rp 200.000'),
(2947, 100, 'DIND250', 'INDOSAT', 'Indosat 250.000', 234685, 234385, 800, 'Gangguan', 'DPEDIA', 'PULSA', 'Pulsa Indosat Rp 250.000'),
(2948, 101, 'DIND500', 'INDOSAT', 'Indosat 500.000', 463550, 463250, 800, 'Normal', 'DPEDIA', 'PULSA', 'Pulsa Indosat Rp 500.000'),
(2949, 102, 'DIND1000', 'INDOSAT', 'Indosat 1.000.000', 926175, 925875, 800, 'Normal', 'DPEDIA', 'PULSA', 'Pulsa Indosat Rp 1.000.000'),
(2950, 103, 'DIND15', 'INDOSAT', 'INDOSAT 15.000', 15835, 15535, 800, 'Normal', 'DPEDIA', 'PULSA', 'Pulsa Indosat Rp 15.000'),
(2951, 104, 'DIND60', 'INDOSAT', 'Indosat 60.000', 59425, 59125, 800, 'Normal', 'DPEDIA', 'PULSA', 'Pulsa Indosat Rp 60.000'),
(2952, 105, 'DIND80', 'INDOSAT', 'Indosat 80.000', 78520, 78220, 800, 'Normal', 'DPEDIA', 'PULSA', 'Pulsa Indosat Rp 80.000'),
(2953, 106, 'DIND90', 'INDOSAT', 'Indosat 90.000', 87770, 87470, 800, 'Normal', 'DPEDIA', 'PULSA', 'Pulsa Indosat Rp 90.000'),
(2954, 107, 'DIND125', 'INDOSAT', 'Indosat 125.000', 120375, 120075, 800, 'Normal', 'DPEDIA', 'PULSA', 'Pulsa Indosat Rp 125.000'),
(2955, 108, 'DIND175', 'INDOSAT', 'Indosat 175.000', 168375, 168075, 800, 'Normal', 'DPEDIA', 'PULSA', 'Pulsa Indosat Rp. 175.000'),
(2956, 109, 'DGP100', 'GOOGLE PLAY INDONESIA', 'Google Play Rp. 100.000 INDONESIA REGION', 100375, 100075, 800, 'Normal', 'DPEDIA', 'VGAME', 'Google Play Gift Card Indonesia Rp. 100.000'),
(2957, 110, 'DGP150', 'GOOGLE PLAY INDONESIA', 'Google Play Rp. 150.000 INDONESIA REGION', 150125, 149825, 800, 'Normal', 'DPEDIA', 'VGAME', 'Google Play Gift Card Indonesia Rp. 150.000'),
(2958, 111, 'DGP20', 'GOOGLE PLAY INDONESIA', 'Google Play Rp. 20.000 INDONESIA REGION', 19850, 19550, 800, 'Normal', 'DPEDIA', 'VGAME', 'Google Play Gift Card Indonesia Rp. 20.000'),
(2959, 112, 'DGP300', 'GOOGLE PLAY INDONESIA', 'Google Play Rp. 300.000 INDONESIA REGION', 301850, 301550, 800, 'Normal', 'DPEDIA', 'VGAME', 'Google Play Gift Card Indonesia Rp. 300.000'),
(2960, 113, 'DGP50', 'GOOGLE PLAY INDONESIA', 'Google Play Rp. 50.000 INDONESIA REGION', 49350, 49050, 800, 'Normal', 'DPEDIA', 'VGAME', 'Google Play Gift Card Indonesia Rp. 50.000'),
(2961, 114, 'DGP500', 'GOOGLE PLAY INDONESIA', 'Google Play Rp. 500.000 INDONESIA REGION', 498375, 498075, 800, 'Normal', 'DPEDIA', 'VGAME', 'Google Play Gift Card Indonesia Rp. 500.000'),
(2962, 115, 'DGPU15', 'GOOGLE PLAY US REGION', 'Google Play 15$ USA REGION', 240850, 240550, 800, 'Gangguan', 'DPEDIA', 'VGAME', '-'),
(2963, 116, 'DGPU25', 'GOOGLE PLAY US REGION', 'Google Play 25$ USA REGION', 415850, 415550, 800, 'Normal', 'DPEDIA', 'VGAME', '-'),
(2964, 117, 'DGPU50', 'GOOGLE PLAY US REGION', 'Google Play 50$ USA REGION', 740875, 740575, 800, 'Normal', 'DPEDIA', 'VGAME', '-'),
(2965, 118, 'DGS1', 'GEMSCOOL', 'Gemscool 1,000 G-Cash', 11520, 11220, 800, 'Normal', 'DPEDIA', 'VGAME', '-'),
(2966, 119, 'DGS2', 'GEMSCOOL', 'Gemscool 2,000 G-Cash', 22850, 22550, 800, 'Normal', 'DPEDIA', 'VGAME', '-'),
(2967, 120, 'DGS3', 'GEMSCOOL', 'Gemscool 3,000 G-Cash', 31890, 31590, 800, 'Normal', 'DPEDIA', 'VGAME', '-'),
(2968, 121, 'DGS5', 'GEMSCOOL', 'Gemscool 5,000 G-Cash', 52260, 51960, 800, 'Normal', 'DPEDIA', 'VGAME', '-'),
(2969, 122, 'DGS10', 'GEMSCOOL', 'Gemscool 10,000 G-Cash', 103670, 103370, 800, 'Normal', 'DPEDIA', 'VGAME', '-'),
(2970, 123, 'DGS20', 'GEMSCOOL', 'Gemscool 20,000 G-Cash', 186050, 185750, 800, 'Normal', 'DPEDIA', 'VGAME', '-'),
(2971, 124, 'DGS30', 'GEMSCOOL', 'Gemscool 30,000 G-Cash', 306400, 306100, 800, 'Normal', 'DPEDIA', 'VGAME', '-'),
(2972, 125, 'DMG10', 'MEGAXUS', 'Megaxus 10.000 MI-Cash', 10910, 10610, 800, 'Normal', 'DPEDIA', 'VGAME', '-'),
(2973, 126, 'DMG20', 'MEGAXUS', 'Megaxus 20.000 MI-Cash', 20960, 20660, 800, 'Normal', 'DPEDIA', 'VGAME', '-'),
(2974, 127, 'DMG50', 'MEGAXUS', 'Megaxus 50.000 MI-Cash', 51125, 50825, 800, 'Normal', 'DPEDIA', 'VGAME', '-'),
(2975, 128, 'DMG100', 'MEGAXUS', 'Megaxus 100.000 MI-Cash', 101375, 101075, 800, 'Normal', 'DPEDIA', 'VGAME', '-'),
(2976, 129, 'DMG210', 'MEGAXUS', 'Megaxus 210.000 MI-Cash', 201865, 201565, 800, 'Normal', 'DPEDIA', 'VGAME', '-'),
(2977, 130, 'DMG550', 'MEGAXUS', 'Megaxus 550.000 MI-Cash', 503355, 503055, 800, 'Normal', 'DPEDIA', 'VGAME', '-'),
(2978, 131, 'DML2010', 'MOBILE LEGEND', 'MOBILELEGEND - 2010 Diamond', 537125, 536825, 800, 'Gangguan', 'DPEDIA', 'VGAME', 'no pelanggan = gabungan antara user_id dan zone_id'),
(2979, 132, 'DML36', 'MOBILE LEGEND', 'MOBILELEGEND - 36 Diamond', 11580, 11280, 800, 'Normal', 'DPEDIA', 'VGAME', 'no pelanggan = gabungan antara user_id dan zone_id'),
(2980, 133, 'DML74', 'MOBILE LEGEND', 'MOBILELEGEND - 74 Diamond', 19860, 19560, 800, 'Normal', 'DPEDIA', 'VGAME', 'no pelanggan = gabungan antara user_id dan zone_id'),
(2981, 134, 'DML568', 'MOBILE LEGEND', 'MOBILELEGEND - 568 Diamond', 161750, 161450, 800, 'Gangguan', 'DPEDIA', 'VGAME', 'no pelanggan = gabungan antara user_id dan zone_id'),
(2982, 135, 'DML1159', 'MOBILE LEGEND', 'MOBILELEGEND - 1159 Diamond', 324250, 323950, 800, 'Normal', 'DPEDIA', 'VGAME', 'no pelanggan = gabungan antara user_id dan zone_id'),
(2983, 136, 'DML12', 'MOBILE LEGEND', 'MOBILELEGEND - 12 Diamond', 4390, 4090, 800, 'Normal', 'DPEDIA', 'VGAME', 'no pelanggan = gabungan antara user_id dan zone_id'),
(2984, 137, 'DML28', 'MOBILE LEGEND', 'MOBILELEGEND - 28 Diamond', 8930, 8630, 800, 'Normal', 'DPEDIA', 'VGAME', 'no pelanggan = gabungan antara user_id dan zone_id'),
(2985, 138, 'DML85', 'MOBILE LEGEND', 'MOBILELEGEND - 85 Diamond', 23160, 22860, 800, 'Normal', 'DPEDIA', 'VGAME', 'input id game + server\r\nsilahkan coba menggunakan opsi gabung dan koma'),
(2986, 139, 'DML875', 'MOBILE LEGEND', 'MOBILELEGEND - 875 Diamond', 247550, 247250, 800, 'Gangguan', 'DPEDIA', 'VGAME', 'no pelanggan = gabungan antara user_id dan zone_id'),
(2987, 140, 'DML59', 'MOBILE LEGEND', 'MOBILELEGEND - 59 Diamond', 16850, 16550, 800, 'Normal', 'DPEDIA', 'VGAME', 'no pelanggan = gabungan antara user_id dan zone_id'),
(2988, 141, 'DML170', 'MOBILE LEGEND', 'MOBILELEGEND - 170 Diamond', 36875, 36575, 800, 'Normal', 'DPEDIA', 'VGAME', 'no pelanggan = gabungan antara user_id dan zone_id'),
(2989, 142, 'DML185', 'MOBILE LEGEND', 'MOBILELEGEND - 185 Diamond', 46875, 46575, 800, 'Normal', 'DPEDIA', 'VGAME', 'no pelanggan = gabungan antara user_id dan zone_id'),
(2990, 143, 'DML222', 'MOBILE LEGEND', 'MOBILELEGEND - 222 Diamond', 60375, 60075, 800, 'Normal', 'DPEDIA', 'VGAME', 'no pelanggan = gabungan antara user_id dan zone_id'),
(2991, 144, 'DML296', 'MOBILE LEGEND', 'MOBILELEGEND - 296 Diamond', 77850, 77550, 800, 'Normal', 'DPEDIA', 'VGAME', 'no pelanggan = gabungan antara user_id dan zone_id'),
(2992, 145, 'DML370', 'MOBILE LEGEND', 'MOBILELEGEND - 370 Diamond', 108125, 107825, 800, 'Gangguan', 'DPEDIA', 'VGAME', 'no pelanggan = gabungan antara user_id dan zone_id'),
(2993, 146, 'DML4830', 'MOBILE LEGEND', 'MOBILELEGEND - 4830 Diamond', 1146425, 1146125, 800, 'Normal', 'DPEDIA', 'VGAME', 'no pelanggan = gabungan antara user_id dan zone_id'),
(2994, 147, 'DML6050', 'MOBILE LEGEND', 'MOBILELEGEND - 6050 Diamond', 1400875, 1400575, 800, 'Normal', 'DPEDIA', 'VGAME', 'no pelanggan = gabungan antara user_id dan zone_id'),
(2995, 148, 'DML4', 'MOBILE LEGEND', 'MOBILELEGEND - 4 Diamond + Starlight Member', 143400, 143100, 800, 'Normal', 'DPEDIA', 'VGAME', 'input id game + server silahkan coba menggunakan opsi gabung dan koma'),
(2996, 149, 'DML193', 'MOBILE LEGEND', 'MOBILELEGEND - 193 Diamond + Starlight Member', 190900, 190600, 800, 'Normal', 'DPEDIA', 'VGAME', 'input id game + server silahkan coba menggunakan opsi gabung dan koma'),
(2997, 150, 'DMLL568', 'MOBILE LEGEND', 'MOBILELEGEND - 586 Diamond + Starlight Member', 285900, 285600, 800, 'Normal', 'DPEDIA', 'VGAME', 'input id game + server silahkan coba menggunakan opsi gabung dan koma'),
(2998, 151, 'DML1411', 'MOBILE LEGEND', 'MOBILELEGEND - 1411 Diamond + Starlight Member', 475900, 475600, 800, 'Normal', 'DPEDIA', 'VGAME', 'input id game + server silahkan coba menggunakan opsi gabung dan koma'),
(2999, 152, 'DML5408', 'MOBILE LEGEND', 'MOBILELEGEND - 5408 Diamond + Starlight Member', 1425900, 1425600, 800, 'Normal', 'DPEDIA', 'VGAME', 'input id game + server silahkan coba menggunakan opsi gabung dan koma'),
(3000, 153, 'DWIFI1', 'WIFI ID', 'Akses wifi.id 1 hari', 4555, 4255, 800, 'Normal', 'DPEDIA', 'VOUCHER', '6 Jam untuk Sumatera, Jawa, Balinusa. 4 jam untuk Kalimantan dan Sulawesi. 2 Jam untuk Maluku dan Papua. NB : Pelanggan akan terima notif sms berupa USER dan PASS dari Telkom'),
(3001, 154, 'DWIFI3', 'WIFI ID', 'Akses wifi.id 30 hari', 37375, 37075, 800, 'Normal', 'DPEDIA', 'VOUCHER', 'NB : Pelanggan akan terima notif sms berupa USER dan PASS dari Telkom'),
(3002, 155, 'DWIFI2', 'WIFI ID', 'Akses wifi.id 7 hari', 15160, 14860, 800, 'Normal', 'DPEDIA', 'VOUCHER', 'NB : Pelanggan akan terima notif sms berupa USER dan PASS dari Telkom'),
(3003, 156, 'DPB1', 'POINT BLANK', '1200 PB Cash', 10550, 10250, 800, 'Normal', 'DPEDIA', 'VGAME', '1200 Point Blank Cash'),
(3004, 157, 'DPB2', 'POINT BLANK', '2400 PB Cash', 20250, 19950, 800, 'Normal', 'DPEDIA', 'VGAME', '2400 PB Cash'),
(3005, 158, 'DPB3', 'POINT BLANK', '6000 PB Cash', 49350, 49050, 800, 'Normal', 'DPEDIA', 'VGAME', '6000 PB Cash'),
(3006, 159, 'DPB4', 'POINT BLANK', '12000 PB Cash', 97850, 97550, 800, 'Normal', 'DPEDIA', 'VGAME', '12000 PB Cash'),
(3007, 160, 'DPUBG1', 'PUBG', 'PUBG Early Bird Key', 35350, 35050, 800, 'Normal', 'DPEDIA', 'VGAME', '-'),
(3008, 161, 'DPUBG2', 'PUBG', 'PUBG Steam Game Key', 215850, 215550, 800, 'Normal', 'DPEDIA', 'VGAME', '-'),
(3009, 162, 'DFF70', 'FREE FIRE', 'Free Fire 70 Diamond', 9865, 9565, 800, 'Normal', 'DPEDIA', 'VGAME', 'Free Fire 70 Diamond'),
(3010, 163, 'DFF140', 'FREE FIRE', 'Free Fire 140 Diamond', 18869, 18569, 800, 'Normal', 'DPEDIA', 'VGAME', 'Free Fire 140 Diamond'),
(3011, 164, 'DFF355', 'FREE FIRE', 'Free Fire 355 Diamond', 45895, 45595, 800, 'Normal', 'DPEDIA', 'VGAME', 'Free Fire 355 Diamond'),
(3012, 165, 'DFF720', 'FREE FIRE', 'Free Fire 720 Diamond', 91020, 90720, 800, 'Normal', 'DPEDIA', 'VGAME', 'Free Fire 720 Diamond'),
(3013, 166, 'DFF5', 'FREE FIRE', 'Free Fire 5 Diamond', 1692, 1392, 800, 'Normal', 'DPEDIA', 'VGAME', 'Free Fire 5 Diamond'),
(3014, 167, 'DFF12', 'FREE FIRE', 'Free Fire 12 Diamond', 2700, 2400, 800, 'Normal', 'DPEDIA', 'VGAME', 'Free Fire 12 Diamond'),
(3015, 168, 'DFF50', 'FREE FIRE', 'Free Fire 50 Diamond', 7445, 7145, 800, 'Normal', 'DPEDIA', 'VGAME', 'Free Fire 50 Diamond'),
(3016, 169, 'DFF1450', 'FREE FIRE', 'Free Fire 1450 Diamond', 184900, 184600, 800, 'Normal', 'DPEDIA', 'VGAME', 'Free Fire 1450 Diamond'),
(3017, 170, 'DFF2180', 'FREE FIRE', 'Free Fire 2180 Diamond', 272275, 271975, 800, 'Normal', 'DPEDIA', 'VGAME', 'Free Fire 2180 Diamond'),
(3018, 171, 'DFF3640', 'FREE FIRE', 'Free Fire 3640 Diamond', 462049, 461749, 800, 'Normal', 'DPEDIA', 'VGAME', 'Free Fire 3640 Diamond'),
(3019, 172, 'DFF7290', 'FREE FIRE', 'Free Fire 7290 Diamond', 938850, 938550, 800, 'Normal', 'DPEDIA', 'VGAME', 'Free Fire 7290 Diamond'),
(3020, 173, 'DFF36500', 'FREE FIRE', 'Free Fire 36500 Diamond', 4754085, 4753785, 800, 'Normal', 'DPEDIA', 'VGAME', 'Free Fire 36500 Diamond'),
(3021, 174, 'DFF73100', 'FREE FIRE', 'Free Fire 73100 Diamond', 9500850, 9500550, 800, 'Normal', 'DPEDIA', 'VGAME', 'Free Fire 73100 Diamond'),
(3022, 175, 'DAO40', 'ARENA OF VALOR', 'AOV 40 Vouchers', 10350, 10050, 800, 'Normal', 'DPEDIA', 'VGAME', 'AOV 40 Vouchers'),
(3023, 176, 'DAO90', 'ARENA OF VALOR', 'AOV 90 Vouchers', 19850, 19550, 800, 'Normal', 'DPEDIA', 'VGAME', 'AOV 90 Vouchers'),
(3024, 177, 'DAO230', 'ARENA OF VALOR', 'AOV 230 Vouchers', 46850, 46550, 800, 'Normal', 'DPEDIA', 'VGAME', 'AOV 230 Vouchers'),
(3025, 178, 'DAO470', 'ARENA OF VALOR', 'AOV 470 Vouchers', 96050, 95750, 800, 'Normal', 'DPEDIA', 'VGAME', 'AOV 470 Vouchers'),
(3026, 179, 'DAO950', 'ARENA OF VALOR', 'AOV 950 Vouchers', 192850, 192550, 800, 'Normal', 'DPEDIA', 'VGAME', 'AOV 950 Vouchers'),
(3027, 180, 'DAO7', 'ARENA OF VALOR', 'AOV 7 Vouchers', 2790, 2490, 800, 'Normal', 'DPEDIA', 'VGAME', 'AOV 7 Vouchers'),
(3028, 181, 'DAO18', 'ARENA OF VALOR', 'AOV 18 Vouchers', 5700, 5400, 800, 'Normal', 'DPEDIA', 'VGAME', 'AOV 18 Vouchers'),
(3029, 182, 'DAO1430', 'ARENA OF VALOR', 'AOV 1430 Vouchers', 284850, 284550, 800, 'Normal', 'DPEDIA', 'VGAME', 'AOV 1430 Vouchers'),
(3030, 183, 'DAO2390', 'ARENA OF VALOR', 'AOV 2390 Vouchers', 485850, 485550, 800, 'Normal', 'DPEDIA', 'VGAME', 'AOV 2390 Vouchers'),
(3031, 184, 'DAO4800', 'ARENA OF VALOR', 'AOV 4800 Vouchers', 970850, 970550, 800, 'Normal', 'DPEDIA', 'VGAME', 'AOV 4800 Vouchers'),
(3032, 185, 'DAO24050', 'ARENA OF VALOR', 'AOV 24050 Vouchers', 4750900, 4750600, 800, 'Normal', 'DPEDIA', 'VGAME', ''),
(3033, 186, 'DAO48200', 'ARENA OF VALOR', 'AOV 48200 Vouchers', 9500900, 9500600, 800, 'Normal', 'DPEDIA', 'VGAME', ''),
(3034, 187, 'DSM5', 'SMART', 'Smart 5.000', 5775, 5475, 800, 'Normal', 'DPEDIA', 'PULSA', 'Pulsa Smart Rp 5.000'),
(3035, 188, 'DSM10', 'SMART', 'Smart 10.000', 10780, 10480, 800, 'Normal', 'DPEDIA', 'PULSA', 'Pulsa Smart Rp 10.000'),
(3036, 189, 'DSM20', 'SMART', 'Smart 20.000', 20535, 20235, 800, 'Normal', 'DPEDIA', 'PULSA', 'Pulsa Smart Rp 20.000'),
(3037, 190, 'DSM25', 'SMART', 'Smart 25.000', 25210, 24910, 800, 'Normal', 'DPEDIA', 'PULSA', 'Pulsa Smart Rp 25.000'),
(3038, 191, 'DSM50', 'SMART', 'Smart 50.000', 49400, 49100, 800, 'Normal', 'DPEDIA', 'PULSA', 'Pulsa Smart Rp 50.000'),
(3039, 192, 'DSM60', 'SMART', 'Smart 60.000', 59320, 59020, 800, 'Normal', 'DPEDIA', 'PULSA', 'Pulsa Smart Rp 60.000'),
(3040, 193, 'DSM100', 'SMART', 'Smart 100.000', 97125, 96825, 800, 'Normal', 'DPEDIA', 'PULSA', 'Pulsa Smart Rp 100.000'),
(3041, 194, 'DSM150', 'SMART', 'Smart 150.000', 149850, 149550, 800, 'Normal', 'DPEDIA', 'PULSA', 'Pulsa Smart Rp 150.000'),
(3042, 195, 'DSM200', 'SMART', 'Smart 200.000', 198900, 198600, 800, 'Normal', 'DPEDIA', 'PULSA', 'Pulsa Smart Rp 200.000'),
(3043, 196, 'DSM300', 'SMART', 'Smart 300.000', 298350, 298050, 800, 'Normal', 'DPEDIA', 'PULSA', 'Pulsa Smart Rp 300.000'),
(3044, 197, 'DSM500', 'SMART', 'Smart 500.000', 496850, 496550, 800, 'Normal', 'DPEDIA', 'PULSA', 'Pulsa Smart Rp 500.000'),
(3045, 198, 'DSM1000', 'SMART', 'Smart 1.000.000', 992375, 992075, 800, 'Normal', 'DPEDIA', 'PULSA', 'Pulsa Smart Rp 1.000.000'),
(3046, 199, 'DR60', 'Rules of Survival Mobile', '60 Diamonds', 15150, 14850, 800, 'Normal', 'DPEDIA', 'VGAME', '60 Diamonds'),
(3047, 200, 'DR120', 'Rules of Survival Mobile', '120 Diamonds', 29400, 29100, 800, 'Normal', 'DPEDIA', 'VGAME', '120 Diamonds'),
(3048, 201, 'DR180', 'Rules of Survival Mobile', '180 Diamonds', 43650, 43350, 800, 'Normal', 'DPEDIA', 'VGAME', '180 Diamonds'),
(3049, 202, 'DR300', 'Rules of Survival Mobile', '300 Diamonds', 72150, 71850, 800, 'Normal', 'DPEDIA', 'VGAME', '300 Diamonds'),
(3050, 203, 'DR600', 'Rules of Survival Mobile', '600 Diamonds', 143400, 143100, 800, 'Normal', 'DPEDIA', 'VGAME', '600 Diamonds'),
(3051, 204, 'DR1200', 'Rules of Survival Mobile', '1200 Diamonds', 285900, 285600, 800, 'Normal', 'DPEDIA', 'VGAME', '1200 Diamonds'),
(3052, 205, 'DR3000', 'Rules of Survival Mobile', '3000 Diamonds', 713400, 713100, 800, 'Normal', 'DPEDIA', 'VGAME', '3000 Diamonds'),
(3053, 206, 'DR6000', 'Rules of Survival Mobile', '6000 Diamonds', 1425900, 1425600, 800, 'Normal', 'DPEDIA', 'VGAME', '6000 Diamonds'),
(3054, 207, 'DS10', 'Speed Drifters', '10 Diamonds', 2800, 2500, 800, 'Normal', 'DPEDIA', 'VGAME', '10 Diamonds'),
(3055, 208, 'DS25', 'Speed Drifters', '25 Diamonds', 5650, 5350, 800, 'Normal', 'DPEDIA', 'VGAME', '25 Diamonds'),
(3056, 209, 'DS56', 'Speed Drifters', '56 Diamonds', 10400, 10100, 800, 'Normal', 'DPEDIA', 'VGAME', '56 Diamonds'),
(3057, 210, 'DS112', 'Speed Drifters', '112 Diamonds', 20250, 19950, 800, 'Normal', 'DPEDIA', 'VGAME', '112 Diamonds'),
(3058, 211, 'DS579', 'Speed Drifters', '579 Diamonds', 95900, 95600, 800, 'Normal', 'DPEDIA', 'VGAME', '579 Diamonds'),
(3059, 212, 'DS1230', 'Speed Drifters', '1230 Diamonds', 194850, 194550, 800, 'Normal', 'DPEDIA', 'VGAME', '1230 Diamonds'),
(3060, 213, 'DS1845', 'Speed Drifters', '1845 Diamonds', 291850, 291550, 800, 'Normal', 'DPEDIA', 'VGAME', '1845 Diamonds'),
(3061, 214, 'DS3134', 'Speed Drifters', '3134 Diamonds', 485850, 485550, 800, 'Normal', 'DPEDIA', 'VGAME', '3134 Diamonds'),
(3062, 215, 'DS6279', 'Speed Drifters', '6279 Diamonds', 970850, 970550, 800, 'Normal', 'DPEDIA', 'VGAME', '6279 Diamonds'),
(3063, 216, 'DS31450', 'Speed Drifters', '31450 Diamonds', 4850850, 4850550, 800, 'Normal', 'DPEDIA', 'VGAME', '31450 Diamonds'),
(3064, 217, 'DS63000', 'Speed Drifters', '63000 Diamonds', 9700850, 9700550, 800, 'Normal', 'DPEDIA', 'VGAME', '63000 Diamonds'),
(3065, 218, 'DS282', 'Speed Drifters', '282 Diamonds', 49350, 49050, 800, 'Normal', 'DPEDIA', 'VGAME', '282 Diamonds'),
(3066, 219, 'DSW12', 'STEAM SEA', 'Steam Wallet Code Rp 12.000', 14800, 14500, 800, 'Normal', 'DPEDIA', 'VGAME', '-'),
(3067, 220, 'DSW45', 'STEAM SEA', 'Steam Wallet Code Rp 45.000', 46775, 46475, 800, 'Normal', 'DPEDIA', 'VGAME', '-'),
(3068, 221, 'DSW60', 'STEAM SEA', 'Steam Wallet Code Rp 60.000', 79350, 79050, 800, 'Normal', 'DPEDIA', 'VGAME', '-'),
(3069, 222, 'DSW90', 'STEAM SEA', 'Steam Wallet Code Rp 90.000', 100850, 100550, 800, 'Normal', 'DPEDIA', 'VGAME', '-'),
(3070, 223, 'DSW120', 'STEAM SEA', 'Steam Wallet Code Rp 120.000', 123285, 122985, 800, 'Normal', 'DPEDIA', 'VGAME', '-'),
(3071, 224, 'DSW250', 'STEAM SEA', 'Steam Wallet Code Rp 250.000', 331050, 330750, 800, 'Normal', 'DPEDIA', 'VGAME', '-'),
(3072, 225, 'DSW400', 'STEAM SEA', 'Steam Wallet Code Rp 400.000', 528050, 527750, 800, 'Normal', 'DPEDIA', 'VGAME', '-'),
(3073, 226, 'DSW600', 'STEAM SEA', 'Steam Wallet Code Rp 600.000', 790050, 789750, 800, 'Normal', 'DPEDIA', 'VGAME', '-'),
(3074, 227, 'DALFA100', 'ALFAMART VOUCHER', 'Voucher Alfamart Rp 100.000', 100600, 100300, 800, 'Normal', 'DPEDIA', 'VOUCHER', '-'),
(3075, 228, 'DALFA10', 'ALFAMART VOUCHER', 'Voucher Alfamart Rp 10.000', 10950, 10650, 800, 'Normal', 'DPEDIA', 'VOUCHER', '-'),
(3076, 229, 'DALFA25', 'ALFAMART VOUCHER', 'Voucher Alfamart Rp 25.000', 25788, 25488, 800, 'Normal', 'DPEDIA', 'VOUCHER', '-'),
(3077, 230, 'DALFA5', 'ALFAMART VOUCHER', 'Voucher Alfamart Rp 5.000', 5955, 5655, 800, 'Normal', 'DPEDIA', 'VOUCHER', '-'),
(3078, 231, 'DALFA50', 'ALFAMART VOUCHER', 'Voucher Alfamart Rp 50.000', 50725, 50425, 800, 'Normal', 'DPEDIA', 'VOUCHER', '-'),
(3079, 232, 'DINDO100', 'INDOMARET', 'Voucher Indomaret Rp 100.000', 100600, 100300, 800, 'Normal', 'DPEDIA', 'VOUCHER', '-'),
(3080, 233, 'DINDO10', 'INDOMARET', 'Voucher Indomaret Rp 10.000', 10825, 10525, 800, 'Normal', 'DPEDIA', 'VOUCHER', '-'),
(3081, 234, 'DINDO25', 'INDOMARET', 'Voucher Indomaret Rp 25.000', 25788, 25488, 800, 'Normal', 'DPEDIA', 'VOUCHER', '-'),
(3082, 235, 'DINDO5', 'INDOMARET', 'Voucher Indomaret Rp 5.000', 5838, 5538, 800, 'Normal', 'DPEDIA', 'VOUCHER', '-'),
(3083, 236, 'DINDO50', 'INDOMARET', 'Voucher Indomaret Rp 50.000', 50725, 50425, 800, 'Normal', 'DPEDIA', 'VOUCHER', '-'),
(3084, 237, 'DBKL50', 'BUKALAPAK', 'Voucher Bukalapak Rp 50.000', 50850, 50550, 800, 'Normal', 'DPEDIA', 'VOUCHER', ''),
(3085, 238, 'DETJ2', 'JUNGLELAND', 'E-Ticket JungleLand Sentul WeekEnd', 166350, 166050, 800, 'Normal', 'DPEDIA', 'VOUCHER', '-'),
(3086, 239, 'DETJ1', 'JUNGLELAND', 'E-Ticket JungleLand Sentul - High Season', 192200, 191900, 800, 'Normal', 'DPEDIA', 'VOUCHER', '-'),
(3087, 240, 'DDIGI5', 'DIGI', 'Digi 5', 20725, 20425, 800, 'Normal', 'DPEDIA', 'LAINYA', 'Digi 5'),
(3088, 241, 'DDIGI10', 'DIGI', 'Digi 10', 40600, 40300, 800, 'Normal', 'DPEDIA', 'LAINYA', 'Digi 10'),
(3089, 242, 'DDIGI25', 'DIGI', 'Digi 25', 100225, 99925, 800, 'Normal', 'DPEDIA', 'LAINYA', 'Digi 25'),
(3090, 243, 'DDIGI30', 'DIGI', 'Digi 30', 120100, 119800, 800, 'Normal', 'DPEDIA', 'LAINYA', 'Digi 30'),
(3091, 244, 'DDIGI50', 'DIGI', 'Digi 50', 199600, 199300, 800, 'Normal', 'DPEDIA', 'LAINYA', 'Digi 50'),
(3092, 245, 'DDIGI100', 'DIGI', 'Digi 100', 398350, 398050, 800, 'Normal', 'DPEDIA', 'LAINYA', 'Digi 100'),
(3093, 246, 'DMX5', 'MAXIS', 'Maxis 5', 20725, 20425, 800, 'Normal', 'DPEDIA', 'LAINYA', 'Maxis 5'),
(3094, 247, 'DMX10', 'MAXIS', 'Maxis 10', 40600, 40300, 800, 'Normal', 'DPEDIA', 'LAINYA', 'Maxis 10'),
(3095, 248, 'DMX15', 'MAXIS', 'Maxis 15', 60475, 60175, 800, 'Normal', 'DPEDIA', 'LAINYA', ''),
(3096, 249, 'DMX20', 'MAXIS', 'Maxis 20', 80350, 80050, 800, 'Normal', 'DPEDIA', 'LAINYA', 'Maxis 20'),
(3097, 250, 'DMX30', 'MAXIS', 'Maxis 30', 120100, 119800, 800, 'Normal', 'DPEDIA', 'LAINYA', 'Maxis 30'),
(3098, 251, 'DMX50', 'MAXIS', 'Maxis 50', 199600, 199300, 800, 'Normal', 'DPEDIA', 'LAINYA', 'Maxis 50'),
(3099, 252, 'DMX100', 'MAXIS', 'Maxis 100', 398350, 398050, 800, 'Normal', 'DPEDIA', 'LAINYA', 'Maxis 100'),
(3100, 253, 'DCL5', 'CELCOM', 'Celcom 5', 20725, 20425, 800, 'Normal', 'DPEDIA', 'LAINYA', 'Celcom 5'),
(3101, 254, 'DCL10', 'CELCOM', 'Celcom 10', 40600, 40300, 800, 'Normal', 'DPEDIA', 'LAINYA', 'Celcom 10'),
(3102, 255, 'DCL15', 'CELCOM', 'Celcom 15', 60475, 60175, 800, 'Normal', 'DPEDIA', 'LAINYA', 'Celcom 15'),
(3103, 256, 'DCL20', 'CELCOM', 'Celcom 20', 80350, 80050, 800, 'Normal', 'DPEDIA', 'LAINYA', 'Celcom 20'),
(3104, 257, 'DCL25', 'CELCOM', 'Celcom 25', 100225, 99925, 800, 'Normal', 'DPEDIA', 'LAINYA', 'Celcom 25'),
(3105, 258, 'DCL30', 'CELCOM', 'Celcom 30', 120100, 119800, 800, 'Normal', 'DPEDIA', 'LAINYA', 'Celcom 30'),
(3106, 259, 'DCL50', 'CELCOM', 'Celcom 50', 199600, 199300, 800, 'Normal', 'DPEDIA', 'LAINYA', 'Celcom 50'),
(3107, 260, 'DCL100', 'CELCOM', 'Celcom 100', 398350, 398050, 800, 'Normal', 'DPEDIA', 'LAINYA', 'Celcom 100'),
(3108, 261, 'DUM5', 'UMOBILE', 'Umobile 5', 20725, 20425, 800, 'Normal', 'DPEDIA', 'LAINYA', 'Umobile 5'),
(3109, 262, 'DUM10', 'UMOBILE', 'Umobile 10', 40600, 40300, 800, 'Normal', 'DPEDIA', 'LAINYA', 'Umobile 10'),
(3110, 263, 'DUM15', 'UMOBILE', 'Umobile 15', 60475, 60175, 800, 'Normal', 'DPEDIA', 'LAINYA', 'Umobile 15'),
(3111, 264, 'DUM20', 'UMOBILE', 'Umobile 20', 80350, 80050, 800, 'Normal', 'DPEDIA', 'LAINYA', 'Umobile 20'),
(3112, 265, 'DUM25', 'UMOBILE', 'Umobile 25', 100225, 99925, 800, 'Normal', 'DPEDIA', 'LAINYA', 'Umobile 25'),
(3113, 266, 'DUM30', 'UMOBILE', 'Umobile 30', 120100, 119800, 800, 'Normal', 'DPEDIA', 'LAINYA', 'Umobile 30'),
(3114, 267, 'DUM50', 'UMOBILE', 'Umobile 50', 199600, 199300, 800, 'Normal', 'DPEDIA', 'LAINYA', 'Umobile 50'),
(3115, 268, 'DUM100', 'UMOBILE', 'Umobile 100', 398350, 398050, 800, 'Normal', 'DPEDIA', 'LAINYA', 'Umobile 100'),
(3116, 269, 'DTN5', 'TUNETALK', 'Tunetalk 5', 20725, 20425, 800, 'Normal', 'DPEDIA', 'LAINYA', 'Tunetalk 5'),
(3117, 270, 'DTN10', 'TUNETALK', 'Tunetalk 10', 40600, 40300, 800, 'Normal', 'DPEDIA', 'LAINYA', 'Tunetalk 10'),
(3118, 271, 'DTN15', 'TUNETALK', 'Tunetalk 15', 60475, 60175, 800, 'Normal', 'DPEDIA', 'LAINYA', 'Tunetalk 15'),
(3119, 272, 'DTN20', 'TUNETALK', 'Tunetalk 20', 80350, 80050, 800, 'Normal', 'DPEDIA', 'LAINYA', 'Tunetalk 20'),
(3120, 273, 'DTN25', 'TUNETALK', 'Tunetalk 25', 100225, 99925, 800, 'Normal', 'DPEDIA', 'LAINYA', 'Tunetalk 25'),
(3121, 274, 'DTN30', 'TUNETALK', 'Tunetalk 30', 120100, 119800, 800, 'Normal', 'DPEDIA', 'LAINYA', 'Tunetalk 30'),
(3122, 275, 'DTN50', 'TUNETALK', 'Tunetalk 50', 199600, 199300, 800, 'Normal', 'DPEDIA', 'LAINYA', 'Tunetalk 50'),
(3123, 276, 'DTN100', 'TUNETALK', 'Tunetalk 100', 398350, 398050, 800, 'Normal', 'DPEDIA', 'LAINYA', 'Tunetalk 100'),
(3124, 277, 'DMTOL100', 'MANDIRI E-TOLL', 'Mandiri E-Toll 100.000', 101800, 101500, 800, 'Normal', 'DPEDIA', 'SALGO', '-'),
(3125, 278, 'DMTOL200', 'MANDIRI E-TOLL', 'Mandiri E-Toll 200.000', 201825, 201525, 800, 'Normal', 'DPEDIA', 'SALGO', '-'),
(3126, 279, 'DMTOL300', 'MANDIRI E-TOLL', 'Mandiri E-Toll 300.000', 301825, 301525, 800, 'Normal', 'DPEDIA', 'SALGO', '-'),
(3127, 280, 'DMTOL10', 'MANDIRI E-TOLL', 'Mandiri E-Toll 10.000', 11830, 11530, 800, 'Normal', 'DPEDIA', 'SALGO', '-'),
(3128, 281, 'DMTOL20', 'MANDIRI E-TOLL', 'Mandiri E-Toll 20.000', 21800, 21500, 800, 'Normal', 'DPEDIA', 'SALGO', '-'),
(3129, 282, 'DMTOL25', 'MANDIRI E-TOLL', 'Mandiri E-Toll 25.000', 26800, 26500, 800, 'Normal', 'DPEDIA', 'SALGO', '-'),
(3130, 283, 'DMTOL50', 'MANDIRI E-TOLL', 'Mandiri E-Toll 50.000', 51800, 51500, 800, 'Normal', 'DPEDIA', 'SALGO', '-'),
(3131, 284, 'DMTOL75', 'MANDIRI E-TOLL', 'Mandiri E-Toll 75.000', 76625, 76325, 800, 'Normal', 'DPEDIA', 'SALGO', ''),
(3132, 285, 'DMTOL400', 'MANDIRI E-TOLL', 'Mandiri E-Toll 400.000', 401825, 401525, 800, 'Normal', 'DPEDIA', 'SALGO', '-'),
(3133, 286, 'DMTOL500', 'MANDIRI E-TOLL', 'Mandiri E-Toll 500.000', 501825, 501525, 800, 'Normal', 'DPEDIA', 'SALGO', '-'),
(3134, 287, 'DMTOL30', 'MANDIRI E-TOLL', 'Mandiri E-Toll 30.000', 30850, 30550, 800, 'Normal', 'DPEDIA', 'SALGO', '-'),
(3135, 288, 'DMTOL40', 'MANDIRI E-TOLL', 'Mandiri E-Toll 40.000', 40850, 40550, 800, 'Normal', 'DPEDIA', 'SALGO', '-'),
(3136, 289, 'DMTOL150', 'MANDIRI E-TOLL', 'Mandiri E-Toll 150.000', 151825, 151525, 800, 'Normal', 'DPEDIA', 'SALGO', '-'),
(3137, 290, 'DGOPAY10', 'GO PAY', 'Go Pay 10.000', 11440, 11140, 800, 'Normal', 'DPEDIA', 'SALGO', 'Masukan no HP'),
(3138, 291, 'DGOPAY100', 'GO PAY', 'Go Pay 100.000', 101440, 101140, 800, 'Normal', 'DPEDIA', 'SALGO', 'Masukan no HP'),
(3139, 292, 'DGOPAY150', 'GO PAY', 'Go Pay 150.000', 151575, 151275, 800, 'Normal', 'DPEDIA', 'SALGO', 'Masukan no HP'),
(3140, 293, 'DGOPAY20', 'GO PAY', 'Go Pay 20.000', 21440, 21140, 800, 'Normal', 'DPEDIA', 'SALGO', 'Masukan no HP'),
(3141, 294, 'DGOPAY25', 'GO PAY', 'Go Pay 25.000', 26440, 26140, 800, 'Normal', 'DPEDIA', 'SALGO', 'Masukan no HP'),
(3142, 295, 'DGOPAY50', 'GO PAY', 'Go Pay 50.000', 51440, 51140, 800, 'Normal', 'DPEDIA', 'SALGO', 'Masukan no HP'),
(3143, 296, 'DGOPAY30', 'GO PAY', 'Go Pay 30.000', 31575, 31275, 800, 'Normal', 'DPEDIA', 'SALGO', 'Masukan no HP'),
(3144, 297, 'DGOPAY40', 'GO PAY', 'Go Pay 40.000', 41440, 41140, 800, 'Normal', 'DPEDIA', 'SALGO', 'Masukan no HP'),
(3145, 298, 'DGOPAY1000', 'GO PAY', 'Go Pay 1.000.000', 1001450, 1001150, 800, 'Normal', 'DPEDIA', 'SALGO', 'Masukan no HP'),
(3146, 299, 'DGOPAY250', 'GO PAY', 'Go Pay 250.000', 251450, 251150, 800, 'Normal', 'DPEDIA', 'SALGO', 'Masukan no HP'),
(3147, 300, 'DGOPAY500', 'GO PAY', 'Go Pay 500.000', 501545, 501245, 800, 'Normal', 'DPEDIA', 'SALGO', 'Masukan no HP'),
(3148, 301, 'DGOPAY75', 'GO PAY', 'Go Pay 75.000', 76440, 76140, 800, 'Normal', 'DPEDIA', 'SALGO', 'Masukan no HP'),
(3149, 302, 'DGOPAY200', 'GO PAY', 'Go Pay 200.000', 201440, 201140, 800, 'Normal', 'DPEDIA', 'SALGO', 'Masukan no HP'),
(3150, 303, 'DSHOPPE10', 'SHOPEE PAY', 'SHOPEE PAY 10.000', 11100, 10800, 800, 'Normal', 'DPEDIA', 'SALGO', 'SHOPEE PAY 10.000'),
(3151, 304, 'DSHOPPE40', 'SHOPEE PAY', 'SHOPEE PAY 40.000', 41125, 40825, 800, 'Normal', 'DPEDIA', 'SALGO', 'SHOPEE PAY 40.000'),
(3152, 305, 'DSHOPPE150', 'SHOPEE PAY', 'SHOPEE PAY 150.000', 151120, 150820, 800, 'Normal', 'DPEDIA', 'SALGO', 'SHOPEE PAY 150.000'),
(3153, 306, 'DSHOPPE250', 'SHOPEE PAY', 'SHOPEE PAY 250.000', 251350, 251050, 800, 'Normal', 'DPEDIA', 'SALGO', 'SHOPEE PAY 250.000'),
(3154, 307, 'DSHOPPP400', 'SHOPEE PAY', 'SHOPEE PAY 400.000', 401275, 400975, 800, 'Normal', 'DPEDIA', 'SALGO', 'SHOPEE PAY 400.000'),
(3155, 308, 'DSHOPPE1000', 'SHOPEE PAY', 'SHOPEE PAY 1.000.000', 1001125, 1000825, 800, 'Normal', 'DPEDIA', 'SALGO', 'SHOPEE PAY 1.000.000'),
(3156, 309, 'DSHOPPE60', 'SHOPEE PAY', 'SHOPEE PAY 60.000', 61125, 60825, 800, 'Normal', 'DPEDIA', 'SALGO', 'SHOPEE PAY 60.000'),
(3157, 310, 'DSHOPPE70', 'SHOPEE PAY', 'SHOPEE PAY 70.000', 71120, 70820, 800, 'Normal', 'DPEDIA', 'SALGO', 'SHOPEE PAY 70.000'),
(3158, 311, 'DSHOPPE500', 'SHOPEE PAY', 'SHOPEE PAY 500.000', 501325, 501025, 800, 'Normal', 'DPEDIA', 'SALGO', 'SHOPEE PAY 500.000'),
(3159, 312, 'DSHOPPE600', 'SHOPEE PAY', 'SHOPEE PAY 600.000', 601125, 600825, 800, 'Normal', 'DPEDIA', 'SALGO', 'SHOPEE PAY 600.000'),
(3160, 313, 'DSHOPPE700', 'SHOPEE PAY', 'SHOPEE PAY 700.000', 701125, 700825, 800, 'Normal', 'DPEDIA', 'SALGO', 'SHOPEE PAY 700.000'),
(3161, 314, 'DSHOPPE80', 'SHOPEE PAY', 'SHOPEE PAY 80.000', 81125, 80825, 800, 'Normal', 'DPEDIA', 'SALGO', 'SHOPEE PAY 80.000'),
(3162, 315, 'DPRP', 'PUBG MOBILE', 'Pubg Royale Pass', 151650, 151350, 800, 'Normal', 'DPEDIA', 'VGAME', 'Kartu Upgrade Royal Pass + Bonus'),
(3163, 316, 'DPEPP', 'PUBG MOBILE', 'Pubg Elite Pass Plus', 361675, 361375, 800, 'Normal', 'DPEDIA', 'VGAME', 'Kartu Upgrade Elite Pass Plus + Bonus'),
(3164, 317, 'DPOVO20', 'OVO', 'OVO 20.000', 20110, 19810, 800, 'Normal', 'DPEDIA', 'SALGO', 'OVO 20.000'),
(3165, 318, 'DPOVO25', 'OVO', 'OVO 25.000', 25110, 24810, 800, 'Normal', 'DPEDIA', 'SALGO', 'OVO 25.000'),
(3166, 319, 'DPOVO30', 'OVO', 'OVO 30.000', 30525, 30225, 800, 'Normal', 'DPEDIA', 'SALGO', 'OVO 30.000'),
(3167, 320, 'DOVO40', 'OVO', 'OVO 40.000', 41400, 41100, 800, 'Normal', 'DPEDIA', 'SALGO', 'OVO 40.000'),
(3168, 321, 'DPOVO50', 'OVO', 'OVO 50.000', 50525, 50225, 800, 'Normal', 'DPEDIA', 'SALGO', 'OVO 50.000'),
(3169, 322, 'DPOVO75', 'OVO', 'OVO 75.000', 75545, 75245, 800, 'Normal', 'DPEDIA', 'SALGO', 'OVO 75.000'),
(3170, 323, 'DPOVO100', 'OVO', 'OVO 100.000', 100125, 99825, 800, 'Normal', 'DPEDIA', 'SALGO', 'OVO 100.000'),
(3171, 324, 'DPOVO150', 'OVO', 'OVO 150.000', 150125, 149825, 800, 'Normal', 'DPEDIA', 'SALGO', 'OVO 150.000'),
(3172, 325, 'DPOVO200', 'OVO', 'OVO 200.000', 200545, 200245, 800, 'Normal', 'DPEDIA', 'SALGO', 'OVO 200.000'),
(3173, 326, 'DPOVO250', 'OVO', 'OVO 250.000', 250125, 249825, 800, 'Normal', 'DPEDIA', 'SALGO', 'OVO 250.000'),
(3174, 327, 'DPOVO300', 'OVO', 'OVO 300.000', 300475, 300175, 800, 'Normal', 'DPEDIA', 'SALGO', 'OVO 300.000'),
(3175, 328, 'DOVO400', 'OVO', 'OVO 400.000', 400545, 400245, 800, 'Normal', 'DPEDIA', 'SALGO', 'OVO 400.000'),
(3176, 329, 'DPOVO500', 'OVO', 'OVO 500.000', 500125, 499825, 800, 'Normal', 'DPEDIA', 'SALGO', 'OVO 500.000'),
(3177, 330, 'DPOVO60', 'OVO', 'OVO 60.000', 60125, 59825, 800, 'Normal', 'DPEDIA', 'SALGO', 'OVO 60.000'),
(3178, 331, 'DPOVO70', 'OVO', 'OVO 70.000', 71400, 71100, 800, 'Normal', 'DPEDIA', 'SALGO', 'OVO 70.000'),
(3179, 332, 'DPOVO80', 'OVO', 'OVO 80.000', 80545, 80245, 800, 'Normal', 'DPEDIA', 'SALGO', 'OVO 80.000'),
(3180, 333, 'DPOVO90', 'OVO', 'OVO 90.000', 91400, 91100, 800, 'Normal', 'DPEDIA', 'SALGO', 'OVO 90.000'),
(3181, 334, 'DPOVO1000', 'OVO', 'OVO 1.000.000', 1000525, 1000225, 800, 'Normal', 'DPEDIA', 'SALGO', 'OVO 1.000.000'),
(3182, 335, 'DPOVO600', 'OVO', 'OVO 600.000', 600575, 600275, 800, 'Normal', 'DPEDIA', 'SALGO', 'OVO 600.000'),
(3183, 336, 'DPOVO700', 'OVO', 'OVO 700.000', 700575, 700275, 800, 'Normal', 'DPEDIA', 'SALGO', 'OVO 700.000'),
(3184, 337, 'DPOVO800', 'OVO', 'OVO 800.000', 800575, 800275, 800, 'Normal', 'DPEDIA', 'SALGO', 'OVO 800.000'),
(3185, 338, 'DPOVO900', 'OVO', 'OVO 900.000', 900525, 900225, 800, 'Normal', 'DPEDIA', 'SALGO', 'OVO 900.000'),
(3186, 339, 'DPDANA600', 'DANA', 'DANA Rp 600.000', 601125, 600825, 800, 'Normal', 'DPEDIA', 'SALGO', '-'),
(3187, 340, 'DPDANA700', 'DANA', 'DANA Rp 700.000', 701125, 700825, 800, 'Normal', 'DPEDIA', 'SALGO', '-'),
(3188, 341, 'DPDANA800', 'DANA', 'DANA Rp 800.000', 801125, 800825, 800, 'Normal', 'DPEDIA', 'SALGO', '-'),
(3189, 342, 'DPDANA900', 'DANA', 'DANA Rp 900.000', 901125, 900825, 800, 'Normal', 'DPEDIA', 'SALGO', '-'),
(3190, 343, 'DPDANA1000', 'DANA', 'DANA Rp 1.000.000', 1000915, 1000615, 800, 'Normal', 'DPEDIA', 'SALGO', '-'),
(3191, 344, 'DSHOPPE90', 'SHOPEE PAY', 'SHOPEE PAY 90.000', 91125, 90825, 800, 'Normal', 'DPEDIA', 'SALGO', 'SHOPEE PAY 90.000'),
(3192, 345, 'DSHOPPE800', 'SHOPEE PAY', 'SHOPEE PAY 800.000', 801125, 800825, 800, 'Normal', 'DPEDIA', 'SALGO', 'SHOPEE PAY 800.000'),
(3193, 346, 'DSHOPPE900', 'SHOPEE PAY', 'SHOPEE PAY 900.000', 901125, 900825, 800, 'Normal', 'DPEDIA', 'SALGO', 'SHOPEE PAY 900.000'),
(3194, 347, 'DBRI20', 'BRI BRIZZI', 'BRIZZI 20.000', 21450, 21150, 800, 'Normal', 'DPEDIA', 'SALGO', 'Saldo BRIZZI 20.000'),
(3195, 348, 'DBRI50', 'BRI BRIZZI', 'BRIZZI 50.000', 51450, 51150, 800, 'Normal', 'DPEDIA', 'SALGO', 'Saldo BRIZZI 50.000'),
(3196, 349, 'DBRI100', 'BRI BRIZZI', 'BRIZZI 100.000', 101450, 101150, 800, 'Normal', 'DPEDIA', 'SALGO', 'Saldo BRIZZI 100.000'),
(3197, 350, 'DBRI200', 'BRI BRIZZI', 'BRIZZI 200.000', 201875, 201575, 800, 'Normal', 'DPEDIA', 'SALGO', 'Saldo BRIZZI 200.000'),
(3198, 351, 'DBRI300', 'BRI BRIZZI', 'BRIZZI 300.000', 301450, 301150, 800, 'Normal', 'DPEDIA', 'SALGO', 'Saldo BRIZZI 300.000'),
(3199, 352, 'DBRI400', 'BRI BRIZZI', 'BRIZZI 400.000', 401450, 401150, 800, 'Normal', 'DPEDIA', 'SALGO', 'Saldo BRIZZI 400.000'),
(3200, 353, 'DBRI500', 'BRI BRIZZI', 'BRIZZI 500.000', 501450, 501150, 800, 'Normal', 'DPEDIA', 'SALGO', 'Saldo BRIZZI 500.000'),
(3201, 354, 'DGRAB20', 'GRAB', 'Grab penumpang 20.000', 21645, 21345, 800, 'Normal', 'DPEDIA', 'SALGO', '-'),
(3202, 355, 'DGRAB25', 'GRAB', 'Grab penumpang 25.000', 26645, 26345, 800, 'Normal', 'DPEDIA', 'SALGO', '-'),
(3203, 356, 'DGRAB40', 'GRAB', 'Grab penumpang 40.000', 41675, 41375, 800, 'Normal', 'DPEDIA', 'SALGO', '-'),
(3204, 357, 'DGRAB50', 'GRAB', 'Grab penumpang 50.000', 51645, 51345, 800, 'Normal', 'DPEDIA', 'SALGO', '-'),
(3205, 358, 'DGRAB100', 'GRAB', 'Grab penumpang 100.000', 101645, 101345, 800, 'Normal', 'DPEDIA', 'SALGO', '-'),
(3206, 359, 'DGRAB150', 'GRAB', 'Grab penumpang 150.000', 151645, 151345, 800, 'Normal', 'DPEDIA', 'SALGO', '-'),
(3207, 360, 'DGRAB200', 'GRAB', 'Grab penumpang 200.000', 201645, 201345, 800, 'Normal', 'DPEDIA', 'SALGO', '-'),
(3208, 361, 'DGRAB300', 'GRAB', 'Grab penumpang 300.000', 301645, 301345, 800, 'Normal', 'DPEDIA', 'SALGO', '-'),
(3209, 362, 'DGRAB500', 'GRAB', 'Grab penumpang 500.000', 501645, 501345, 800, 'Normal', 'DPEDIA', 'SALGO', '-'),
(3210, 363, 'DGD150', 'GRAB DRIVER', 'Voucher Grab driver 150.000', 150550, 150250, 800, 'Normal', 'DPEDIA', 'VOUCHER', '-'),
(3211, 364, 'DGD200', 'GRAB DRIVER', 'Voucher Grab driver 200.000', 200550, 200250, 800, 'Normal', 'DPEDIA', 'VOUCHER', '-'),
(3212, 365, 'DGD250', 'GRAB DRIVER', 'Voucher Grab driver 250.000', 250550, 250250, 800, 'Normal', 'DPEDIA', 'VOUCHER', '-'),
(3213, 366, 'DBNI10', 'TAPCASH BNI', 'Tapcash BNI 10.000', 12575, 12275, 800, 'Normal', 'DPEDIA', 'SALGO', 'Tapcash BNI 10.000'),
(3214, 367, 'DBNI20', 'TAPCASH BNI', 'Tapcash BNI 20.000', 22575, 22275, 800, 'Normal', 'DPEDIA', 'SALGO', 'Tapcash BNI 20.000'),
(3215, 368, 'DBNI50', 'TAPCASH BNI', 'Tapcash BNI 50.000', 52575, 52275, 800, 'Normal', 'DPEDIA', 'SALGO', 'Tapcash BNI 50.000'),
(3216, 369, 'DBNI100', 'TAPCASH BNI', 'Tapcash BNI 100.000', 102575, 102275, 800, 'Normal', 'DPEDIA', 'SALGO', 'Tapcash BNI 100.000'),
(3217, 370, 'DITUNES10', 'ITUNES US REGION', 'Voucher iTunes 10$', 166450, 166150, 800, 'Normal', 'DPEDIA', 'VGAME', '-'),
(3218, 371, 'DITUNES15', 'ITUNES US REGION', 'Voucher iTunes $15', 231850, 231550, 800, 'Normal', 'DPEDIA', 'VGAME', '-'),
(3219, 372, 'DITUNES25', 'ITUNES US REGION', 'Voucher iTunes $25', 406650, 406350, 800, 'Normal', 'DPEDIA', 'VGAME', '-'),
(3220, 373, 'DITUNES50', 'ITUNES US REGION', 'Voucher iTunes $50', 780850, 780550, 800, 'Normal', 'DPEDIA', 'VGAME', '-'),
(3221, 374, 'DUNIPIN10', 'Unipin Voucher', 'Voucher Unipin 10.000', 10550, 10250, 800, 'Normal', 'DPEDIA', 'VGAME', 'Voucher Unipin 10.000'),
(3222, 375, 'DUNIPIN20', 'Unipin Voucher', 'Voucher Unipin 20.000', 20250, 19950, 800, 'Normal', 'DPEDIA', 'VGAME', 'Voucher Unipin 20.000'),
(3223, 376, 'DUNIPIN50', 'Unipin Voucher', 'Voucher Unipin 50.000', 49350, 49050, 800, 'Normal', 'DPEDIA', 'VGAME', 'Voucher Unipin 50.000'),
(3224, 377, 'DUNIPIN100', 'Unipin Voucher', 'Voucher Unipin 100.000', 97850, 97550, 800, 'Normal', 'DPEDIA', 'VGAME', 'Voucher Unipin 100.000'),
(3225, 378, 'DUNIPIN300', 'Unipin Voucher', 'Voucher Unipin 300.000', 291850, 291550, 800, 'Normal', 'DPEDIA', 'VGAME', 'Voucher Unipin 300.000'),
(3226, 379, 'DUNIPIN500', 'Unipin Voucher', 'Voucher Unipin 500.000', 485850, 485550, 800, 'Normal', 'DPEDIA', 'VGAME', 'Voucher Unipin 500.000'),
(3227, 380, 'DIMVU10', 'IMVU', 'Voucher IMVU $10', 173510, 173210, 800, 'Normal', 'DPEDIA', 'VGAME', 'Voucher IMVU $10'),
(3228, 381, 'DIMVU25', 'IMVU', 'Voucher IMVU $25', 427650, 427350, 800, 'Normal', 'DPEDIA', 'VGAME', 'Voucher IMVU $25'),
(3229, 382, 'DIMVU50', 'IMVU', 'Voucher IMVU $50', 854450, 854150, 800, 'Normal', 'DPEDIA', 'VGAME', 'Voucher IMVU $50'),
(3230, 383, 'DPDANA250', 'DANA', 'DANA Rp 250.000', 250915, 250615, 800, 'Normal', 'DPEDIA', 'SALGO', '-'),
(3231, 384, 'DUC50', 'PUBG MOBILE', 'PUBG MOBILE 50 UC', 10760, 10460, 800, 'Normal', 'DPEDIA', 'VGAME', 'PUBG MOBILE 50 UC'),
(3232, 385, 'DUC150', 'PUBG MOBILE', 'PUBG MOBILE 150 UC', 32370, 32070, 800, 'Normal', 'DPEDIA', 'VGAME', 'PUBG MOBILE 150 UC'),
(3233, 386, 'DUC250', 'PUBG MOBILE', 'PUBG MOBILE 250 UC', 47700, 47400, 800, 'Normal', 'DPEDIA', 'VGAME', 'PUBG MOBILE 250 UC'),
(3234, 387, 'DUC500', 'PUBG MOBILE', 'PUBG MOBILE 500 UC', 93750, 93450, 800, 'Normal', 'DPEDIA', 'VGAME', 'PUBG MOBILE 500 UC'),
(3235, 388, 'DUC1250', 'PUBG MOBILE', 'PUBG MOBILE 1250 UC', 262420, 262120, 800, 'Normal', 'DPEDIA', 'VGAME', 'PUBG MOBILE 1250 UC'),
(3236, 389, 'DUC2500', 'PUBG MOBILE', 'PUBG MOBILE 2500 UC', 478050, 477750, 800, 'Normal', 'DPEDIA', 'VGAME', 'PUBG MOBILE 2500 UC'),
(3237, 390, 'DUC125', 'PUBG MOBILE', 'PUBG MOBILE 125 UC', 24905, 24605, 800, 'Normal', 'DPEDIA', 'VGAME', 'PUBG MOBILE 125 UC'),
(3238, 391, 'DPB5', 'POINT BLANK', '36000 PB Cash', 291850, 291550, 800, 'Normal', 'DPEDIA', 'VGAME', '-'),
(3239, 392, 'DPB6', 'POINT BLANK', '60000 PB Cash', 485850, 485550, 800, 'Normal', 'DPEDIA', 'VGAME', '-'),
(3240, 393, 'DUS50', 'Steam Wallet (USD)', 'Steam Wallet Code $50', 883550, 883250, 800, 'Normal', 'DPEDIA', 'LAINYA', 'Steam Wallet Code $50'),
(3241, 394, 'DAIGO6', 'AXIS AIGO', 'AIGO 25GB 24 JAM 60HR (Voucher)', 87850, 87550, 800, 'Normal', 'DPEDIA', 'VOUCHER', '25 GB All Jaringan Berlaku 24 Jam Masa Aktif 60 Hari. Setelah mendapat SN, ketik *838*kodevoucher# untuk mengaktifkan paket'),
(3242, 395, 'DAIGO1', 'AXIS AIGO', 'AIGO 1GB 24 JAM 30HR (Voucher)', 13655, 13355, 800, 'Normal', 'DPEDIA', 'VOUCHER', '1 GB All Jaringan Berlaku 24 Jam Masa Aktif 30 Hari');
INSERT INTO `layanan_pulsa` (`id`, `service_id`, `provider_id`, `operator`, `layanan`, `harga`, `harga_api`, `profit`, `status`, `provider`, `tipe`, `catatan`) VALUES
(3243, 396, 'DAIGO2', 'AXIS AIGO', 'AIGO 2GB 24 JAM 30HR (Voucher)', 22855, 22555, 800, 'Normal', 'DPEDIA', 'VOUCHER', '2 GB All Jaringan Berlaku 24 Jam Masa Aktif 30 Hari. Setelah mendapat SN, ketik *838*kodevoucher# untuk mengaktifkan paket'),
(3244, 397, 'DAIGO3', 'AXIS AIGO', 'AIGO 3GB 24 JAM 30HR (Voucher)', 28670, 28370, 800, 'Normal', 'DPEDIA', 'VOUCHER', '3 GB All Jaringan Berlaku 24 Jam Masa Aktif 30 Hari. Setelah mendapat SN, ketik *838*kodevoucher# untuk mengaktifkan paket'),
(3245, 398, 'DAIGO4', 'AXIS AIGO', 'Voucher AIGO 5 GB / 30 Hari', 42670, 42370, 800, 'Normal', 'DPEDIA', 'VOUCHER', '5 GB All Jaringan Berlaku 24 Jam Masa Aktif 30 Hari. Setelah mendapat SN, ketik *838*kodevoucher# untuk mengaktifkan paket'),
(3246, 399, 'DAIGO5', 'AXIS AIGO', 'AIGO 8GB 24 JAM 30HR (Voucher)', 56920, 56620, 800, 'Normal', 'DPEDIA', 'VOUCHER', '8 GB All Jaringan Berlaku 24 Jam Masa Aktif 30 Hari. Setelah mendapat SN, ketik *838*kodevoucher# untuk mengaktifkan paket'),
(3247, 400, 'DTDD3', 'TSEL DATA', 'Voucher Telkomsel Data 3.5GB // 7 Hari', 24849, 24549, 800, 'Normal', 'DPEDIA', 'VOUCHER', 'VOUCHER DATA TELKOMSEL KHUSUS UNTUK AREA JABODETABEK: 3.5GB FLASH, 7 HARI. CARA INPUT KUOTA: *133*SN#'),
(3248, 401, 'DTDD2', 'TSEL DATA', 'Voucher Telkomsel Data 2.5GB // 5 Hari', 17849, 17549, 800, 'Normal', 'DPEDIA', 'VOUCHER', 'VOUCHER DATA TELKOMSEL KHUSUS UNTUK AREA JABODETABEK: 2.5GB FLASH, 5 HARI. CARA INPUT KUOTA: *133*SN#'),
(3249, 402, 'DTDD1', 'TSEL DATA', 'Voucher Telkomsel Data 1 GB / 3 Hari', 13849, 13549, 800, 'Normal', 'DPEDIA', 'VOUCHER', '1 GB Flash, 3 Hari'),
(3250, 403, 'DTDB1', 'TSEL BULK', 'Voucher Telkomsel Data 1 GB / 5 Hari', 16849, 16549, 800, 'Normal', 'DPEDIA', 'VOUCHER', '1GB, 5 HARI'),
(3251, 404, 'DTDB2', 'TSEL BULK', 'Voucher Telkomsel Data 1.5 GB / 7 Hari', 24849, 24549, 800, 'Normal', 'DPEDIA', 'VOUCHER', '1,5GB, 7 HARI'),
(3252, 405, 'DTDB3', 'TSEL BULK', 'Telkomsel Data Bulk 2 GB', 39850, 39550, 800, 'Normal', 'DPEDIA', 'VOUCHER', '2GB + 2GB OMG, 30 hari'),
(3253, 406, 'DTDB4', 'TSEL BULK', 'Voucher Telkomsel Data Bulk 4 GB / 30 Hari', 36849, 36549, 800, 'Normal', 'DPEDIA', 'VOUCHER', '-'),
(3254, 407, 'DTDB6', 'TSEL BULK', 'Voucher Telkomsel Data Bulk 6 GB / 30 Hari', 61849, 61549, 800, 'Normal', 'DPEDIA', 'VOUCHER', '6GB + 2GB OMG, 30 HARI'),
(3255, 408, 'DTDB9', 'TSEL BULK', 'Voucher Telkomsel Data Bulk 9 GB / 30 Hari', 75849, 75549, 800, 'Normal', 'DPEDIA', 'VOUCHER', '9GB + 2GB OMG, 30 HARI'),
(3256, 409, 'DTDB10', 'TSEL BULK', 'Telkomsel Data Bulk 10 GB', 65975, 65675, 800, 'Normal', 'DPEDIA', 'VOUCHER', '10GB + 2GB OMG, 30 HARI'),
(3257, 410, 'DAM1', 'AIGO MINI', 'AIGO MINI 1GB 24 JAM 5HR (Voucher)', 8525, 8225, 800, 'Normal', 'DPEDIA', 'VOUCHER', 'Setelah mendapat SN, ketik *838*kodevoucher# untuk mengaktifkan paket'),
(3258, 411, 'DAM2', 'AIGO MINI', 'AIGO MINI 2GB 24 JAM 7HR (Voucher)', 15205, 14905, 800, 'Normal', 'DPEDIA', 'VOUCHER', 'Setelah mendapat SN, ketik *838*kodevoucher# untuk mengaktifkan paket'),
(3259, 412, 'DAM3', 'AIGO MINI', 'AIGO MINI 3GB 24 JAM 15HR (Voucher)', 19755, 19455, 800, 'Normal', 'DPEDIA', 'VOUCHER', 'Setelah mendapat SN, ketik *838*kodevoucher# untuk mengaktifkan paket'),
(3260, 413, 'DAM4', 'AIGO MINI', 'AIGO MINI 5GB 24 JAM 15HR (Voucher)', 31070, 30770, 800, 'Normal', 'DPEDIA', 'VOUCHER', 'Setelah mendapat SN, ketik *838*kodevoucher# untuk mengaktifkan paket'),
(3261, 414, 'DOW1', 'AIGO OWSEM', 'Voucher OWSEM 1GB + 1GB 4G', 17405, 17105, 800, 'Normal', 'DPEDIA', 'VOUCHER', 'Voucher BRONET 4G OWSEM 1GB+1GB(4G)+2GB Games, 24 jam 30HR'),
(3262, 415, 'DOW3', 'AIGO OWSEM', 'Voucher OWSEM 2GB + 6GB 4G', 45570, 45270, 800, 'Normal', 'DPEDIA', 'VOUCHER', 'Voucher BRONET 4G OWSEM 2GB+6GB(4G)+8GB Games, 24 jam 30HR'),
(3263, 416, 'DOW2', 'AIGO OWSEM', 'Voucher OWSEM 1GB + 3GB 4G', 28970, 28670, 800, 'Normal', 'DPEDIA', 'VOUCHER', 'Voucher BRONET 4G OWSEM 1GB+3GB(4G)+4GB Games, 24 jam 30HR'),
(3264, 417, 'DOW4', 'AIGO OWSEM', 'Voucher OWSEM 3GB + 9GB 4G', 60170, 59870, 800, 'Normal', 'DPEDIA', 'VOUCHER', 'Voucher BRONET 4G OWSEM 3GB+9GB(4G)+12GB Games, 24 jam 30HR'),
(3265, 418, 'DVF2', 'VOC INDOSAT', 'Voucher Freedom Internet 10GB 30Hr', 49850, 49550, 800, 'Normal', 'DPEDIA', 'VOUCHER', 'INDOSAT DATA FREEDOM 10GB FULL 24 JAM NASIONAL, 30HARI'),
(3266, 419, 'DVF1', 'VOC INDOSAT', 'Voucher Freedom Internet 18GB 30Hr', 74350, 74050, 800, 'Normal', 'DPEDIA', 'VOUCHER', 'INDOSAT DATA FREEDOM 18GB FULL 24 JAM NASIONAL, 30HARI'),
(3267, 420, 'DVU1', 'VOC INDOSAT', 'Indosat Internet Unlimited 3GB 30Hari', 25850, 25550, 800, 'Normal', 'DPEDIA', 'VOUCHER', '*556*kode voucher# untuk aktivasi'),
(3268, 421, 'DVTR1', 'VOC TRI', 'Voucher Tri LTE 33GB', 68850, 68550, 800, 'Normal', 'DPEDIA', 'VOUCHER', '24 Jam, 30 Hari'),
(3269, 422, 'DVTR2', 'VOC TRI', 'Voucher Tri LTE 66GB', 110850, 110550, 800, 'Normal', 'DPEDIA', 'VOUCHER', '24 Jam, 30 Hari'),
(3270, 423, 'DVTR3', 'VOC TRI', 'Voucher Tri 1 GB + 500 MB All Net / 30 Hari', 14349, 14049, 800, 'Normal', 'DPEDIA', 'VOUCHER', 'Claim Voucher dengan cara *111*SN#'),
(3271, 424, 'DVTR4', 'VOC TRI', 'Voucher Tri 1,5GB + 500MB All Net 30 Hari', 34000, 33700, 800, 'Normal', 'DPEDIA', 'VOUCHER', 'Claim Voucher dengan cara *111*SN#'),
(3272, 425, 'DVTR5', 'VOC TRI', 'Voucher Tri 2GB + 1GB All Net 30 Hari', 24950, 24650, 800, 'Normal', 'DPEDIA', 'VOUCHER', 'Claim Voucher dengan cara *111*SN#'),
(3273, 426, 'DVTR6', 'VOC TRI', 'Voucher Tri 5GB + 1GB All Net 30 Hari', 55850, 55550, 800, 'Normal', 'DPEDIA', 'VOUCHER', 'Claim Voucher dengan cara *111*SN#'),
(3274, 427, 'DVTR7', 'VOC TRI', 'Voucher Tri 6GB + Unlimited 30 Hari', 59845, 59545, 800, 'Normal', 'DPEDIA', 'VOUCHER', 'Unlimited pada jam 01.00 - 17.00, Claim Voucher dengan cara *111*SN#'),
(3275, 428, 'DVTR8', 'VOC TRI', 'Voucher Tri 8GB + 14GB All Net + Unlimited Youtube 15 Hari', 100100, 99800, 800, 'Normal', 'DPEDIA', 'VOUCHER', 'Claim Voucher dengan cara *111*SN#'),
(3276, 429, 'DVTR9', 'VOC TRI', 'Voucher Tri 10GB + 20GB All Net + Unlimited Youtube 15 Hari', 118800, 118500, 800, 'Normal', 'DPEDIA', 'VOUCHER', 'Claim Voucher dengan cara *111*SN#'),
(3277, 430, 'DVTR10', 'VOC TRI', 'Voucher Tri AMI 12 GB', 75850, 75550, 800, 'Normal', 'DPEDIA', 'VOUCHER', 'Voucher Three 12GB 30 Hari + Unlimited Youtube 30 Hari'),
(3278, 431, 'DVTR11', 'VOC TRI', 'Voucher Tri AMI 4 GB', 32500, 32200, 800, 'Normal', 'DPEDIA', 'VOUCHER', 'Voucher Three 4GB 30 Hari + Unlimited Youtube 7 Hari'),
(3279, 432, 'DVAO1', 'VOC TRI A', 'Voucher AlwaysOn 1.5 GB', 15850, 15550, 800, 'Normal', 'DPEDIA', 'VOUCHER', '1 GB Nasional, 500 MB lokal, Mengikuti Masa Aktif Kartu'),
(3280, 433, 'DVAO2', 'VOC TRI A', 'Voucher AlwaysOn 2 GB', 19850, 19550, 800, 'Normal', 'DPEDIA', 'VOUCHER', '1.5 GB Nasional,500 MB lokal, Mengikuti Masa Aktif Kartu'),
(3281, 434, 'DVAO3', 'VOC TRI A', 'Voucher AlwaysOn 3 GB', 27850, 27550, 800, 'Normal', 'DPEDIA', 'VOUCHER', '2 GB Nasional, 1 GB lokal, Mengikuti Masa Aktif Kartu'),
(3282, 435, 'DVAO4', 'VOC TRI A', 'Voucher AlwaysOn 6 GB', 34525, 34225, 800, 'Normal', 'DPEDIA', 'VOUCHER', '5 GB Nasional, 1 GB lokal, Mengikuti Masa Aktif Kartu'),
(3283, 436, 'DVAO5', 'VOC TRI A', 'Voucher AlwaysOn 6 GB + Unlimited', 64100, 63800, 800, 'Normal', 'DPEDIA', 'VOUCHER', '6 GB Nasional, Mengikuti Masa Aktif Kartu, Unlimited 30 hari lokal'),
(3284, 437, 'DVAO6', 'VOC TRI A', 'Voucher AlwaysOn 10 GB + Unlimited', 80850, 80550, 800, 'Normal', 'DPEDIA', 'VOUCHER', '10 GB Nasional, Mengikuti Masa Aktif Kartu, Unlimited 30 hari lokal'),
(3285, 438, 'DTRA1', 'VOC TRI B', 'Tri Data AON 1GB + 1GB 30Hr', 21670, 21370, 800, 'Normal', 'DPEDIA', 'VOUCHER', 'Jangan lupa input *111*SN# agar customer mendapatkan paket data sesuai'),
(3286, 439, 'DTRA2', 'VOC TRI B', 'Tri Data AON 2GB + 3GB 30Hr', 25250, 24950, 800, 'Normal', 'DPEDIA', 'VOUCHER', 'Jangan lupa input *111*SN# agar customer mendapatkan paket data sesuai'),
(3287, 440, 'DTRA3', 'VOC TRI B', 'Voucher Tri AON 3 GB + 6 GB / 30 Hari', 66850, 66550, 800, 'Normal', 'DPEDIA', 'VOUCHER', 'Jangan lupa input *111*SN# agar customer mendapatkan paket data sesuai'),
(3288, 441, 'DTRA4', 'VOC TRI B', 'Tri Data AON 5GB + 8GB 30Hr', 74150, 73850, 800, 'Normal', 'DPEDIA', 'VOUCHER', 'Jangan lupa input *111*SN# agar customer mendapatkan paket data sesuai'),
(3289, 442, 'DTRA5', 'VOC TRI B', 'Tri Data AON 6GB + 12GB 30Hr', 87350, 87050, 800, 'Normal', 'DPEDIA', 'VOUCHER', 'Jangan lupa input *111*SN# agar customer mendapatkan paket data sesuai'),
(3290, 443, 'DVMN1', 'VOC TRI C', 'Voucher Mini 1 GB 5 Hari', 9349, 9049, 800, 'Normal', 'DPEDIA', 'VOUCHER', '1 GB Lokal, 5 Hari'),
(3291, 444, 'DVMN2', 'VOC TRI C', 'Voucher Mini 1.5 GB 5 Hari', 14260, 13960, 800, 'Normal', 'DPEDIA', 'VOUCHER', '1.5 GB Lokal, 5 Hari'),
(3292, 445, 'DMN1', 'XL MINI', 'Voucher XL Data Mini 1GB', 10450, 10150, 800, 'Normal', 'DPEDIA', 'VOUCHER', '1GB NASIONAL SEMUA JARINGAN, 7 HARI'),
(3293, 446, 'DCMB1', 'XL LITE', 'Voucher Combo LITE 3.5 GB', 27849, 27549, 800, 'Normal', 'DPEDIA', 'VOUCHER', '2,5GB + 1GB YOUTUBE'),
(3294, 447, 'DCMB2', 'XL LITE', 'Voucher Combo LITE 6 GB', 38849, 38549, 800, 'Normal', 'DPEDIA', 'VOUCHER', '5GB + 1GB YOUTUBE'),
(3295, 448, 'DCMB3', 'XL LITE', 'Voucher Combo LITE 11 GB', 61849, 61549, 800, 'Normal', 'DPEDIA', 'VOUCHER', '10GB + 1GB YOUTUBE'),
(3296, 449, 'DCMB4', 'XL LITE', 'Voucher Combo LITE 21 GB', 97849, 97549, 800, 'Normal', 'DPEDIA', 'VOUCHER', '20GB + 1GB YOUTUBE'),
(3297, 450, 'DCMB5', 'XL LITE', 'Voucher Combo LITE 31 GB', 120849, 120549, 800, 'Normal', 'DPEDIA', 'VOUCHER', '30GB + 1GB YOUTUBE'),
(3298, 451, 'DTEL1', 'TEL TSEL', 'Telkomsel Telepon 5.000', 6850, 6550, 800, 'Normal', 'DPEDIA', 'PKSMS', 'Telepon 80 menit sesama, 20 menit semua op 1 Hari (sesuai zona)'),
(3299, 452, 'DTEL2', 'TEL TSEL', 'Telkomsel Telepon 10.000', 12350, 12050, 800, 'Normal', 'DPEDIA', 'PKSMS', 'Telepon 185 menit sesama, 15 menit semua operator 1 Hari (sesuai zona)'),
(3300, 453, 'DTEL3', 'TEL TSEL', 'Telkomsel Telepon 20.000', 15860, 15560, 800, 'Normal', 'DPEDIA', 'PKSMS', 'Telepon 370 menit sesama, 30 menit semua op 3 Hari (sesuai zona)'),
(3301, 454, 'DTEL4', 'TEL TSEL', 'Telkomsel Telepon 25.000', 25350, 25050, 800, 'Normal', 'DPEDIA', 'PKSMS', 'Telepon 550 menit sesama, 50 menit semua op 7 Hari (sesuai zona)'),
(3302, 455, 'DTEL5', 'TEL TSEL', 'Telkomsel Telepon 50.000', 47875, 47575, 800, 'Normal', 'DPEDIA', 'PKSMS', 'Telepon 1200 menit sesama, 100 menit semua op 15 Hari (sesuai zona)'),
(3303, 456, 'DTEL6', 'TEL TSEL', 'Telkomsel Telepon 80.000', 75450, 75150, 800, 'Normal', 'DPEDIA', 'PKSMS', 'Telepon 2000 menit sesama, 100 menit semua op 30 Hari (sesuai zona)'),
(3304, 457, 'DTEL7', 'TEL TSEL', 'Telkomsel Telepon 130.000', 108850, 108550, 800, 'Normal', 'DPEDIA', 'PKSMS', 'Telepon 6250 menit sesama, 250 menit semua op 30 Hari (sesuai zona)'),
(3305, 458, 'DTEL8', 'TEL TSEL 3', 'Telkomsel Telepon 170 menit sesama, 30 menit semua op 3 Hari', 11600, 11300, 800, 'Normal', 'DPEDIA', 'PKSMS', 'Telepon 170 menit sesama, 30 menit semua op 3 Hari (sesuai zona)'),
(3306, 459, 'DTEL9', 'TEL TSEL 3', 'Telkomsel Telepon 350 menit sesama, 50 menit semua op 7 Hari', 21460, 21160, 800, 'Normal', 'DPEDIA', 'PKSMS', 'Telepon 350 menit sesama, 50 menit semua op 7 Hari (sesuai zona)'),
(3307, 460, 'DTEL10', 'TEL TSEL 3', 'Telepon 1000 menit sesama, 100 menit semua op (30 Hari)', 56375, 56075, 800, 'Normal', 'DPEDIA', 'PKSMS', 'Telepon 1000 menit sesama, 100 menit semua op (30 Hari) (manfaat sesuai zona)'),
(3308, 461, 'DTEL11', 'TEL TSEL 3', 'Telkomsel Telepon 1700 menit On Net / 7 Hari', 40375, 40075, 800, 'Normal', 'DPEDIA', 'PKSMS', 'manfaat sesuai zona'),
(3309, 462, 'DTEL12', 'TEL TSEL 3', 'Telkomsel Telepon 9000 menit On Net / 30 Hari', 107850, 107550, 800, 'Normal', 'DPEDIA', 'PKSMS', 'Manfaat sesuai zona'),
(3310, 463, 'DTSS1', 'TEL TSEL 1', 'Telepon Sesama Tsel (200 menit, 30Hari)', 51475, 51175, 800, 'Normal', 'DPEDIA', 'PKSMS', '-'),
(3311, 464, 'DTSS2', 'TEL TSEL 1', 'Telepon Sesama Tsel (300 menit + 1000SMS, 30Hari)', 101475, 101175, 800, 'Normal', 'DPEDIA', 'PKSMS', '-'),
(3312, 465, 'DTSS3', 'TEL TSEL 1', 'Telepon Sesama Tsel (50 menit + 200SMS,7Hari)', 26575, 26275, 800, 'Normal', 'DPEDIA', 'PKSMS', 'manfaat sesuai zona'),
(3313, 466, 'DTSS4', 'TEL TSEL 1', 'Telepon Sesama Tsel (50 menit, 3Hari)', 12850, 12550, 800, 'Normal', 'DPEDIA', 'PKSMS', 'Telepon Sesama Tsel (50 menit, 3Hari) (sesuai zona)'),
(3314, 467, 'DTSS5', 'TEL TSEL 1', 'Telkomsel Telepon 50 Menit Sesama / 7 Hari', 22850, 22550, 800, 'Normal', 'DPEDIA', 'PKSMS', 'Telepon Sesama Tsel (50 menit, 7Hari) (sesuai zona)'),
(3315, 468, 'DTES3', 'TEL TSEL 2', 'Telepon Semua Op (20 menit, 1Hari)', 6555, 6255, 800, 'Normal', 'DPEDIA', 'PKSMS', 'Paket TELEPON - Telepon Semua Op (20 menit, 1Hari)'),
(3316, 469, 'DSMS2', 'SMS TSEL', '250 SMS ke semua op 1 hari', 6605, 6305, 800, 'Normal', 'DPEDIA', 'PKSMS', '250 SMS ke semua op 1 hari'),
(3317, 470, 'DSMS4', 'SMS TSEL', 'Telkomsel 1500 SMS Semua Op / 30 Hari', 21125, 20825, 800, 'Normal', 'DPEDIA', 'PKSMS', '1500 SMS ke semua op 30 hari'),
(3318, 471, 'DSMS1', 'SMS TSEL', 'Telkomsel 200 SMS Semua Op / 1 Hari', 2850, 2550, 800, 'Gangguan', 'DPEDIA', 'PKSMS', 'Telkomsel 200 SMS ke semua op 1 hari'),
(3319, 472, 'DSMS3', 'SMS TSEL', '500 SMS ke semua op 7 hari', 11360, 11060, 800, 'Normal', 'DPEDIA', 'PKSMS', '500 SMS ke semua op 7 hari'),
(3320, 473, 'DSMS5', 'SMS TSEL', 'Telkomsel 1000 SMS Semua Op / 5 Hari', 6480, 6180, 800, 'Normal', 'DPEDIA', 'PKSMS', 'Telkomsel 1000 SMS ke semua op 5 hari'),
(3321, 474, 'DSMS6', 'SMS TSEL', 'Telkomsel 1000 SMS Semua Op / 15 Hari', 16450, 16150, 800, 'Normal', 'DPEDIA', 'PKSMS', 'Telkomsel 1000 SMS ke semua op 15 hari'),
(3322, 475, 'DSMS7', 'SMS TSEL', 'Telkomsel 2000 SMS ke semua op 30 hari', 26710, 26410, 800, 'Normal', 'DPEDIA', 'PKSMS', 'Telkomsel 2000 SMS ke semua op 30 hari'),
(3323, 476, 'DTELN3', 'TEL IND', 'Indosat telepon unlimited sesama 30 Hari', 11350, 11050, 800, 'Normal', 'DPEDIA', 'PKSMS', 'Indosat telepon unlimited sesama 30 Hari'),
(3324, 477, 'DTELN1', 'TEL IND', 'Indosat Telepon 1000 Menit Sesama / 1 Hari', 3120, 2820, 800, 'Normal', 'DPEDIA', 'PKSMS', '-'),
(3325, 478, 'DTELN2', 'TEL IND', 'Indosat telepon unlimited sesama Masa Aktif Paket 10 Hari', 15900, 15600, 800, 'Normal', 'DPEDIA', 'PKSMS', '-'),
(3326, 479, 'DTELN4', 'TEL IND', 'Indosat Telepon Unlimited Sesama + 30 Menit Semua Op / 7 Hari', 17100, 16800, 800, 'Normal', 'DPEDIA', 'PKSMS', 'Indosat telepon unlimited sesama + 30 Menit All Operator Masa Aktif Paket 7 Hari'),
(3327, 480, 'DTELN5', 'TEL IND', 'Indosat Telepon Unlimited Sesama + 50 Menit Semua Op / 7 Hari', 15550, 15250, 800, 'Normal', 'DPEDIA', 'PKSMS', '-'),
(3328, 481, 'DTELN6', 'TEL IND', 'Indosat telepon unlimited sesama + 60 Menit All Operator Masa Aktif Paket 30 Hari', 26850, 26550, 800, 'Normal', 'DPEDIA', 'PKSMS', 'Indosat telepon unlimited sesama + 60 Menit All Operator Masa Aktif Paket 30 Hari'),
(3329, 482, 'DTELN7', 'TEL IND', 'Indosat telepon unlimited sesama + 250 Menit All Operator  Masa Aktif Paket 30 Hari', 51100, 50800, 800, 'Normal', 'DPEDIA', 'PKSMS', '-'),
(3330, 483, 'DSMSN1', 'SMS IND', '300 SMS sesama Indosat + 100 SMS operator lain', 6975, 6675, 800, 'Normal', 'DPEDIA', 'PKSMS', '300 SMS sesama Isat + 100 SMS operator lain'),
(3331, 484, 'DSMSN2', 'SMS IND', '600 SMS sesama Isat + 200 SMS operator lain', 12475, 12175, 800, 'Normal', 'DPEDIA', 'PKSMS', '600 SMS sesama Isat + 200 SMS operator lain'),
(3332, 485, 'DSMSN3', 'SMS IND', '1250 SMS sesama Isat + 250 SMS operator lain', 10975, 10675, 800, 'Normal', 'DPEDIA', 'PKSMS', '1250 SMS sesama Isat + 250 SMS operator lain 30 hari'),
(3333, 486, 'DSMSN4', 'SMS IND', '2000 SMS sesama Isat + 500 SMS operator lain', 28475, 28175, 800, 'Normal', 'DPEDIA', 'PKSMS', '2000 SMS sesama Isat + 500 SMS operator lain'),
(3334, 487, 'DTT1', 'TEL TRI', 'Tri Telepon 20 Menit Ke Semua Op 7 Hari', 5945, 5645, 800, 'Normal', 'DPEDIA', 'PKSMS', '-'),
(3335, 488, 'DTT2', 'TEL TRI', 'Tri Telepon 60 Menit Ke Semua Op 7 Hari', 15970, 15670, 800, 'Normal', 'DPEDIA', 'PKSMS', '-'),
(3336, 489, 'DTT3', 'TEL TRI', 'Tri Telepon 150 Menit Ke Semua Op 30 Hari', 30425, 30125, 800, 'Normal', 'DPEDIA', 'PKSMS', '-'),
(3337, 490, 'DXLTEL1', 'TEL XL', 'XL Nelpon Semua Operator 12.000', 12560, 12260, 800, 'Normal', 'DPEDIA', 'PKSMS', 'Bebas telepon ke semua operator selamat 400 Menit (370 Menit ke XL + 30 Menit ke Operator Lain). Masa aktif 7 Hari.'),
(3338, 491, 'DXLTEL2', 'TEL XL', 'XL Nelpon Semua Operator 40.000', 39100, 38800, 800, 'Normal', 'DPEDIA', 'PKSMS', 'Bebas telepon ke semua operator selama 150 Menit. Masa aktif 30 Hari.'),
(3339, 492, 'DXLTEL3', 'TEL XL', 'AnyNet 250 Menit 30 Hari', 35850, 35550, 800, 'Normal', 'DPEDIA', 'PKSMS', 'Bebas telepon ke semua operator selamat 250 Menit. Masa aktif 30 Hari.'),
(3340, 493, 'DXLTEL4', 'TEL XL', 'AnyNet 325 Menit 30 Hari', 72400, 72100, 800, 'Normal', 'DPEDIA', 'PKSMS', 'Bebas telepon ke semua operator selamat 325 Menit. Masa aktif 30 Hari.'),
(3341, 494, 'DXLTEL5', 'TEL XL', 'AnyNet 500 Menit 30 Hari', 120850, 120550, 800, 'Normal', 'DPEDIA', 'PKSMS', 'Bebas telepon ke semua operator selamat 500 Menit. Masa aktif 30 Hari.'),
(3342, 495, 'DXLTEL6', 'TEL XL', 'AnyNet 500 Menit 90 Hari', 143100, 142800, 800, 'Normal', 'DPEDIA', 'PKSMS', 'Bebas telepon ke semua operator selamat 500 Menit. Masa aktif 90 Hari.'),
(3343, 496, 'DXLTEL7', 'TEL XL', 'AnyNet 300 Menit 90 Hari', 96100, 95800, 800, 'Normal', 'DPEDIA', 'PKSMS', 'Bebas telepon ke semua operator selamat 300 Menit. Masa aktif 90 Hari.'),
(3344, 497, 'DBULK1', 'BULK', 'Telkomsel Data Bulk 3 GB', 57500, 57200, 800, 'Normal', 'DPEDIA', 'PKIN', 'Telkomsel 3 GB + 1GB OMG 30 Hari'),
(3345, 498, 'DBULK2', 'BULK', 'Telkomsel Data Bulk 4.5 GB', 69200, 68900, 800, 'Normal', 'DPEDIA', 'PKIN', 'Telkomsel 4.5 GB + 2 GB OMG + 100 menit telepon + 60 sms (30 Hari)'),
(3346, 499, 'DBULK3', 'BULK', 'Telkomsel Data Bulk 5 GB', 87850, 87550, 800, 'Normal', 'DPEDIA', 'PKIN', 'Telkomsel 5 GB + 2GB OMG 30 Hari'),
(3347, 500, 'DBULK4', 'BULK', 'Telkomsel Data Bulk 8 GB / 30 Hari', 75850, 75550, 800, 'Normal', 'DPEDIA', 'PKIN', 'Telkomsel 8 GB + 2GB OMG 30 Hari'),
(3348, 501, 'DBULK5', 'BULK', 'Telkomsel Data Bulk 12 GB', 120850, 120550, 800, 'Normal', 'DPEDIA', 'PKIN', 'Telkomsel 12 GB + 2 GB OMG 30 Hari'),
(3349, 502, 'DBULK6', 'BULK', 'Telkomsel Data Bulk 17 GB', 150350, 150050, 800, 'Normal', 'DPEDIA', 'PKIN', 'Telkomsel 17 GB + 2 GB OMG + 300 menit telepon + 100 sms (30 Hari)'),
(3350, 503, 'DBULK7', 'BULK', 'Telkomsel Data Bulk 25 GB', 165350, 165050, 800, 'Normal', 'DPEDIA', 'PKIN', 'Telkomsel 25 GB + 2 GB OMG 30 Hari'),
(3351, 504, 'DBULK8', 'BULK', 'Telkomsel Data Bulk 28 GB', 175350, 175050, 800, 'Normal', 'DPEDIA', 'PKIN', 'Telkomsel 28 GB + 2 GB OMG + 600 menit telepon + 200 sms (30 Hari)'),
(3352, 505, 'DBULK9', 'BULK', 'Telkomsel Data Bulk 50 GB', 198450, 198150, 800, 'Normal', 'DPEDIA', 'PKIN', 'Telkomsel 50 GB + 2 GB OMG 30 Hari'),
(3353, 506, 'DTFL1', 'FLASH', 'Telkomsel Data Flash 5.000', 5350, 5050, 800, 'Normal', 'DPEDIA', 'PKIN', '20MB - 40MB sesuai zona / 7 Hari (sesuai zona)'),
(3354, 507, 'DTFL2', 'FLASH', 'Telkomsel Data Flash 10.000', 8350, 8050, 800, 'Normal', 'DPEDIA', 'PKIN', '50MB - 110MB sesuai zona, 7 Hari (sesuai zona)'),
(3355, 508, 'DTFL3', 'FLASH', 'Telkomsel Data Flash 20.000', 13850, 13550, 800, 'Normal', 'DPEDIA', 'PKIN', 'Telkomsel 200MB - 420MB sesuai zona / 7 Hari (sesuai zona)'),
(3356, 509, 'DTFL4', 'FLASH', 'Telkomsel Data Flash 25.000', 20850, 20550, 800, 'Normal', 'DPEDIA', 'PKIN', '360MB - 800MB sesuai zona / 30 Hari'),
(3357, 510, 'DTFL5', 'FLASH', 'Telkomsel Data Flash 50.000', 44850, 44550, 800, 'Normal', 'DPEDIA', 'PKIN', '800MB - 1.5GB sesuai zona / 30 Hari (sesuai zona)'),
(3358, 511, 'DTFL6', 'FLASH', 'Telkomsel Data Flash 68.000', 69350, 69050, 800, 'Normal', 'DPEDIA', 'PKIN', '4.5 GB, 24Jam 30Hari (sesuai zona)'),
(3359, 512, 'DTFL7', 'FLASH', 'Telkomsel Data Flash 85.000', 89350, 89050, 800, 'Normal', 'DPEDIA', 'PKIN', '8 GB, 24Jam 30Hari (sesuai zona)'),
(3360, 513, 'DTFL8', 'FLASH', 'Telkomsel Data Flash 100.000', 90850, 90550, 800, 'Normal', 'DPEDIA', 'PKIN', '12 GB / 30 Hari (sesuai zona)'),
(3361, 514, 'DTFL9', 'FLASH', 'Telkomsel Data Flash 200.000', 199850, 199550, 800, 'Normal', 'DPEDIA', 'PKIN', '50 GB / 30 Hari (sesuai zona)'),
(3362, 515, 'DTFL10', 'FLASH', 'Telkomsel Data Flash 100 MB / 7 Hari', 2450, 2150, 800, 'Normal', 'DPEDIA', 'PKIN', '24 jam 7 hari'),
(3363, 516, 'DTFL11', 'FLASH', 'Telkomsel Data Flash 50 MB / 7 Hari', 2200, 1900, 800, 'Normal', 'DPEDIA', 'PKIN', '24 jam 7 hari'),
(3364, 517, 'DTFL12', 'FLASH', 'Telkomsel Data Flash 250 MB / 7 Hari', 3900, 3600, 800, 'Normal', 'DPEDIA', 'PKIN', '24 jam 7 hari'),
(3365, 518, 'DTFL13', 'FLASH', 'Telkomsel Data Flash 500 MB / 30 Hari', 7450, 7150, 800, 'Normal', 'DPEDIA', 'PKIN', '24 jam 30 hari'),
(3366, 519, 'DTFL14', 'FLASH', 'Telkomsel Data Flash 750 MB / 30 Hari', 13225, 12925, 800, 'Normal', 'DPEDIA', 'PKIN', '24 jam 30 hari'),
(3367, 520, 'DTFL15', 'FLASH', 'Telkomsel Data Flash 1 GB / 30 Hari', 12800, 12500, 800, 'Normal', 'DPEDIA', 'PKIN', '24Jam 30Hari'),
(3368, 521, 'DTFL16', 'FLASH', 'Telkomsel Data Flash 2 GB / 30 Hari', 24800, 24500, 800, 'Normal', 'DPEDIA', 'PKIN', '24Jam 30Hari'),
(3369, 522, 'DTFL18', 'FLASH', 'Telkomsel Data Flash 4 GB / 30 Hari', 49850, 49550, 800, 'Normal', 'DPEDIA', 'PKIN', '24Jam 30Hari'),
(3370, 523, 'DTFL17', 'FLASH', 'Telkomsel Data Flash 3 GB / 30 Hari', 37300, 37000, 800, 'Normal', 'DPEDIA', 'PKIN', '24Jam 30Hari'),
(3371, 524, 'DTFL19', 'FLASH', 'Telkomsel Data Flash 8 GB', 89050, 88750, 800, 'Normal', 'DPEDIA', 'PKIN', '24 jam 30 Hari'),
(3372, 525, 'DTFL20', 'FLASH', 'Telkomsel Data Flash 12 GB / 30 Hari', 104550, 104250, 800, 'Normal', 'DPEDIA', 'PKIN', '24 jam 30 hari'),
(3373, 526, 'DMNN1', 'MINI', 'Telkomsel Data Mini 1 Gb 3 Hari', 13650, 13350, 800, 'Normal', 'DPEDIA', 'PKIN', '24 jam'),
(3374, 527, 'DMNN2', 'MINI', 'Telkomsel Data Mini 2 Gb 3 Hari', 16650, 16350, 800, 'Normal', 'DPEDIA', 'PKIN', '24 jam'),
(3375, 528, 'DMNN3', 'MINI', 'Telkomsel Data Mini 10 Gb 3 Hari', 38550, 38250, 800, 'Normal', 'DPEDIA', 'PKIN', '24 jam'),
(3376, 529, 'DMNN4', 'MINI', 'Telkomsel Data Mini 5 GB / 7 Hari', 39150, 38850, 800, 'Normal', 'DPEDIA', 'PKIN', '24 jam'),
(3377, 530, 'DMSX4', 'MAX', 'Telkomsel MAX-STREAM GALA 100.000', 100350, 100050, 800, 'Normal', 'DPEDIA', 'PKIN', '15 GB Maxstream Gala (HBO Go, Viu Premium, Vidio Gold, HOOQ), Berlaku 30 Hari'),
(3378, 531, 'DMXS3', 'MAX', 'Telkomsel MAX-STREAM GALA 150.000', 149550, 149250, 800, 'Normal', 'DPEDIA', 'PKIN', '30 GB Maxstream Gala (HBO Go, Viu Premium, Vidio Gold, HOOQ), Berlaku 30 Hari'),
(3379, 532, 'DMXS1', 'MAX', 'Telkomsel Maxstream 20.000', 20950, 20650, 800, 'Normal', 'DPEDIA', 'PKIN', '5 GB Maxstream 30 Hari'),
(3380, 533, 'DMSX2', 'MAX', 'Telkomsel Maxstream 50.000', 99450, 99150, 800, 'Normal', 'DPEDIA', 'PKIN', '12 GB Maxstream, 30 Hari'),
(3381, 534, 'DGAME4', 'GAMESMAX', 'Telkomsel Data 1GB + Youtube + Game + Voucher Spesial Cube', 25360, 25060, 800, 'Normal', 'DPEDIA', 'PKIN', 'Telkomsel Data 1GB + 500MB Youtube + 30GB Games + Voucher Spesial Cube, 30 Hari'),
(3382, 535, 'DGAME3', 'GAMESMAX', 'Telkomsel Data 1GB + Youtube + Game + Voucher AOV', 25360, 25060, 800, 'Normal', 'DPEDIA', 'PKIN', 'Telkomsel Data 1GB + 500MB Youtube + 30GB Games + Voucher AOV 40 Diamond, 30 Hari'),
(3383, 536, 'DGAME2', 'GAMESMAX', 'Telkomsel Data 1GB + Youtube + Game + Voucher Google Play', 25360, 25060, 800, 'Normal', 'DPEDIA', 'PKIN', 'Telkomsel Data 1GB + 500MB Youtube + 30GB Games + Voucher Google Play 5.000, 30 Hari'),
(3384, 537, 'DGAME6', 'GAMESMAX', 'Telkomsel Data 1GB + Youtube + Game + Voucher Mobile legends', 25360, 25060, 800, 'Normal', 'DPEDIA', 'PKIN', 'Telkomsel Data 1GB + 500MB Youtube + 30GB Games + Voucher Mobile legends 56 Diamond, 30 Hari'),
(3385, 538, 'DGAME5', 'GAMESMAX', 'Telkomsel Data 1GB + Youtube + Game + Voucher Freefire', 25360, 25060, 800, 'Normal', 'DPEDIA', 'PKIN', 'Telkomsel Data 1GB + 500MB Youtube + 30GB Games + Voucher Freefire 50 Diamond, 30 Hari'),
(3386, 539, 'DGAME7', 'GAMESMAX', 'Telkomsel GamesMAX 500MB Internet + 2.5GB Internet Games Kuota', 25450, 25150, 800, 'Normal', 'DPEDIA', 'PKIN', 'GamesMAX 500MB Internet + 2.5GB Internet Games Kuota'),
(3387, 540, 'DGAME1', 'GAMESMAX', 'Telkomsel GamesMax 25.000', 24850, 24550, 800, 'Normal', 'DPEDIA', 'PKIN', 'GamesMax 1 GB Internet + 2 GB Internet Games, berlaku 30 Hari'),
(3388, 541, 'DCMBO1', 'COMBO', 'Telkomsel COMBO 10 GB / 30 Hari', 98375, 98075, 800, 'Normal', 'DPEDIA', 'PKIN', '10GB + 100 Menit Sesama + 100 sms Sesama (on net), berlaku 30 Hari'),
(3389, 542, 'DALLL4', 'TSEL ALL', '500 MB semua jaringan + bebas zona', 14850, 14550, 800, 'Normal', 'DPEDIA', 'PKIN', '-'),
(3390, 543, 'DALLL5', 'TSEL ALL', '750 MB semua jaringan + bebas zona', 19450, 19150, 800, 'Normal', 'DPEDIA', 'PKIN', '-'),
(3391, 544, 'DALLL6', 'TSEL ALL', '1 GB semua jaringan + bebas zona', 20775, 20475, 800, 'Normal', 'DPEDIA', 'PKIN', '-'),
(3392, 545, 'DALLL1', 'TSEL ALL', 'Telkomsel Data Flash 50 MB / 30 Hari', 3855, 3555, 800, 'Normal', 'DPEDIA', 'PKIN', '24 jam'),
(3393, 546, 'DALLL2', 'TSEL ALL', 'Telkomsel Data Flash 100 MB / 30 Hari', 3475, 3175, 800, 'Normal', 'DPEDIA', 'PKIN', '24 jam'),
(3394, 547, 'DALLL3', 'TSEL ALL', 'Telkomsel Data Flash 250 MB / 30 Hari', 4050, 3750, 800, 'Normal', 'DPEDIA', 'PKIN', '24 jam'),
(3395, 548, 'DTMLM1', 'MALAM', 'Telkomsel Data Malam 1 GB / 1 Hari', 4950, 4650, 800, 'Normal', 'DPEDIA', 'PKIN', 'Aktif pukul 00-07'),
(3396, 549, 'DTMLM2', 'MALAM', 'Telkomsel Data Malam 5 GB / 1 Hari', 7950, 7650, 800, 'Normal', 'DPEDIA', 'PKIN', 'Aktif pukul 00-07'),
(3397, 550, 'DTMLM4', 'MALAM', 'Telkomsel Data Malam 5 GB / 30 Hari', 19450, 19150, 800, 'Normal', 'DPEDIA', 'PKIN', 'Aktif pukul 00-07'),
(3398, 551, 'DTMLM5', 'MALAM', 'Telkomsel Data Malam 15 GB / 30 Hari', 26450, 26150, 800, 'Normal', 'DPEDIA', 'PKIN', 'Aktif pukul 00-07'),
(3399, 552, 'DKCL1', 'KECIL', 'Telkomsel Data 1GB 1 Hari', 11650, 11350, 800, 'Normal', 'DPEDIA', 'PKIN', '-'),
(3400, 553, 'DKCL2', 'KECIL', 'Telkomsel Data 1GB 7 Hari', 20750, 20450, 800, 'Normal', 'DPEDIA', 'PKIN', '-'),
(3401, 554, 'DKCL3', 'KECIL', 'Telkomsel Data Mini 5 GB / 3 Hari', 29325, 29025, 800, 'Normal', 'DPEDIA', 'PKIN', '-'),
(3402, 555, 'DTFL21', 'FLASH', 'Flash 1GB + 2GB Game (30 hari)', 24900, 24600, 800, 'Normal', 'DPEDIA', 'PKIN', 'Flash 1GB + 2GB Game (30 hari)'),
(3403, 556, 'DOMG1', 'OMG', 'Telkomsel Paket Internet Facebook 1GB / 1 Hari', 8350, 8050, 800, 'Normal', 'DPEDIA', 'PKIN', '-'),
(3404, 557, 'DOMG2', 'OMG', 'Telkomsel Paket Internet Facebook 1GB / 3 Hari', 11350, 11050, 800, 'Normal', 'DPEDIA', 'PKIN', '-'),
(3405, 558, 'DOMG3', 'OMG', 'Telkomsel Paket Internet Facebook 2GB / 3 Hari', 15350, 15050, 800, 'Normal', 'DPEDIA', 'PKIN', '-'),
(3406, 559, 'DOMG4', 'OMG', 'Telkomsel Paket Internet Facebook 1GB / 7 Hari', 18350, 18050, 800, 'Normal', 'DPEDIA', 'PKIN', '-'),
(3407, 560, 'DOMG5', 'OMG', 'Telkomsel Paket Internet Facebook 2GB / 7 Hari', 22350, 22050, 800, 'Normal', 'DPEDIA', 'PKIN', '-'),
(3408, 561, 'DOMG6', 'OMG', 'Telkomsel Paket Internet Facebook 3GB / 7 Hari', 26350, 26050, 800, 'Normal', 'DPEDIA', 'PKIN', '-'),
(3409, 562, 'DOMG7', 'OMG', 'Telkomsel Paket Internet Instagram 1 GB / 1 Hari', 6850, 6550, 800, 'Normal', 'DPEDIA', 'PKIN', '-'),
(3410, 563, 'DOMG8', 'OMG', 'Telkomsel Paket Internet Instagram 1GB / 3 Hari', 11350, 11050, 800, 'Normal', 'DPEDIA', 'PKIN', '-'),
(3411, 564, 'DOMG9', 'OMG', 'Telkomsel Paket Internet Instagram 2GB / 3 Hari', 15350, 15050, 800, 'Normal', 'DPEDIA', 'PKIN', '-'),
(3412, 565, 'DOMG10', 'OMG', 'Telkomsel Paket Internet Instagram 1GB / 7 Hari', 18350, 18050, 800, 'Normal', 'DPEDIA', 'PKIN', '-'),
(3413, 566, 'DOMG11', 'OMG', 'Telkomsel Paket Internet Instagram 2GB / 7 Hari', 22350, 22050, 800, 'Normal', 'DPEDIA', 'PKIN', '-'),
(3414, 567, 'DOMG12', 'OMG', 'Telkomsel Paket Internet Instagram 3GB / 7 Hari', 26350, 26050, 800, 'Normal', 'DPEDIA', 'PKIN', '-'),
(3415, 568, 'DOMG13', 'OMG', 'Telkomsel Paket Internet Youtube 1GB / 1 Hari', 8350, 8050, 800, 'Normal', 'DPEDIA', 'PKIN', '-'),
(3416, 569, 'DOMG14', 'OMG', 'Telkomsel Paket Internet Youtube 1GB / 3 Hari', 11350, 11050, 800, 'Normal', 'DPEDIA', 'PKIN', '-'),
(3417, 570, 'DOMG15', 'OMG', 'Telkomsel Paket Internet Youtube 2GB / 3 Hari', 15950, 15650, 800, 'Normal', 'DPEDIA', 'PKIN', '-'),
(3418, 571, 'DOMG16', 'OMG', 'Telkomsel Paket Internet Youtube 1GB / 7 Hari', 18350, 18050, 800, 'Normal', 'DPEDIA', 'PKIN', '-'),
(3419, 572, 'DOMG17', 'OMG', 'Telkomsel Paket Internet Youtube 2GB / 7 Hari', 22350, 22050, 800, 'Normal', 'DPEDIA', 'PKIN', '-'),
(3420, 573, 'DOMG18', 'OMG', 'Telkomsel Paket Internet Youtube 3GB / 7 Hari', 26350, 26050, 800, 'Normal', 'DPEDIA', 'PKIN', '-'),
(3421, 574, 'DINDD1', 'IND DATA', 'Indosat 300 MB / 30 Hari', 3525, 3225, 800, 'Normal', 'DPEDIA', 'PKIN', 'Indosat 300MB 30 Hari'),
(3422, 575, 'DINDD2', 'IND DATA', 'Indosat 500 MB / 30 Hari', 5225, 4925, 800, 'Normal', 'DPEDIA', 'PKIN', 'Indosat 500MB 30 Hari'),
(3423, 576, 'DINDD3', 'IND DATA', 'Indosat 6 GB / 30 Hari', 52550, 52250, 800, 'Normal', 'DPEDIA', 'PKIN', 'Kuota 6GB 30 hari'),
(3424, 577, 'DINDD4', 'IND DATA', 'Indosat 7 GB / 30 Hari', 61050, 60750, 800, 'Normal', 'DPEDIA', 'PKIN', 'Kuota 7GB 30 hari'),
(3425, 578, 'DINDD5', 'IND DATA', 'Indosat 8 GB / 30 Hari', 69550, 69250, 800, 'Normal', 'DPEDIA', 'PKIN', 'Kuota 8GB 30 hari'),
(3426, 579, 'DINDD6', 'IND DATA', 'Indosat 9 GB / 30 Hari', 78050, 77750, 800, 'Normal', 'DPEDIA', 'PKIN', 'Kuota 9GB 30 hari'),
(3427, 580, 'DINDD7', 'IND DATA', 'Indosat 10 GB / 30 Hari', 86550, 86250, 800, 'Normal', 'DPEDIA', 'PKIN', 'Kuota 10GB 30 hari'),
(3428, 581, 'DFRF1', 'FREEDOM', 'Indosat Freedom L', 101375, 101075, 800, 'Normal', 'DPEDIA', 'PKIN', 'Kuota Dasar 4GB (2G/3G/4G) + Bonus 8GB (4G) + Kuota Aplikasi 2GB + 10GB kuota malam, Unlimited nelpon ke sesama Operator Indosat. Masa aktif 30 hari.'),
(3429, 582, 'DFRF2', 'FREEDOM', 'Indosat Freedom M', 65900, 65600, 800, 'Normal', 'DPEDIA', 'PKIN', 'Kuota Dasar 2GB (2G/3G/4G) + Bonus 3GB (4G) + 5GB Kuota malam + 2GB stream on, Kuota Aplikasi 2GB, Unlimited nelpon ke sesama Operator Indosat. Masa aktif 30 hari.'),
(3430, 583, 'DFRF3', 'FREEDOM', 'Indosat Freedom XL', 149675, 149375, 800, 'Normal', 'DPEDIA', 'PKIN', 'Kuota Dasar 8GB (2G/3G/4G) + Bonus 12GB (4G) + Kuota Aplikasi 6GB + 15GB kuota malam, Unlimited nelpon ke sesama Operator Indosat. Masa aktif 30 hari.'),
(3431, 584, 'DFRF4', 'FREEDOM', 'Indosat Freedom XXL', 199175, 198875, 800, 'Normal', 'DPEDIA', 'PKIN', 'Kuota Dasar 12GB (2G/3G/4G) + Bonus 25GB (4G) + Kuota Aplikasi 8GB + 20GB kuota malam, Unlimited nelpon ke sesama Operator Indosat. Masa aktif 30 hari.'),
(3432, 585, 'DFFC1', 'FREE COMBO', 'Indosat Freedom Combo 4 GB / 30 Hari', 24955, 24655, 800, 'Normal', 'DPEDIA', 'PKIN', '4GB (2GB + 1GB Lokal, 1 GB Malam,Telepon 5 Mnt All 30hr) Paket hanya berlaku di area Jakarta, Botabek, West Java, Central Java, East Java, Kalimantan.'),
(3433, 586, 'DFFC2', 'FREE COMBO', 'Freedom Combo 8GB 30 Hari', 33125, 32825, 800, 'Normal', 'DPEDIA', 'PKIN', '8GB (4GB + 2GB Lokal, 2 GB Malam, Telepon 20 Mnt All 30hr) Paket hanya berlaku di area Jakarta, Botabek, West Java, Central Java, East Java, Kalimantan.'),
(3434, 587, 'DFFC3', 'FREE COMBO', 'Indosat Freedom Combo 14 GB / 30 Hari', 47175, 46875, 800, 'Normal', 'DPEDIA', 'PKIN', '14GB (7.5GB + 3.5GB Lokal, 3GB Mlm, Telepon 30 Mnt ALL 30hr) Paket hanya berlaku di area Jakarta, Botabek, West Java, Central Java, East Java, Kalimantan.'),
(3435, 588, 'DFFC4', 'FREE COMBO', 'Freedom Combo 20GB 30 Hari', 65075, 64775, 800, 'Normal', 'DPEDIA', 'PKIN', '20GB (11GB + 5GB Lokal, 4GB Mlm,Telepon 40 Mnt All 30hr) Paket hanya berlaku di area Jakarta, Botabek, West Java, Central Java, East Java, Kalimantan.'),
(3436, 589, 'DFFC5', 'FREE COMBO', 'Freedom Combo 30GB 30 Hari', 93625, 93325, 800, 'Normal', 'DPEDIA', 'PKIN', '30GB (16GB + 8GB Lokal, 6 GB Mlm, Telepon 60 Mnt All 30hr) Paket hanya berlaku di area Jakarta, Botabek, West Java, Central Java, East Java, Kalimantan.'),
(3437, 590, 'DFFI1', 'FREE INET', 'Freedom Internet 3GB 30Hr', 25550, 25250, 800, 'Normal', 'DPEDIA', 'PKIN', 'INDOSAT DATA FREEDOM 3GB FULL 24 JAM NASIONAL, 30HARI'),
(3438, 591, 'DFFI2', 'FREE INET', 'Freedom Internet 10GB 30Hr', 48225, 47925, 800, 'Normal', 'DPEDIA', 'PKIN', 'INDOSAT DATA FREEDOM 10GB FULL 24 JAM NASIONAL, 30HARI'),
(3439, 592, 'DFFI3', 'FREE INET', 'Freedom Internet 18GB 30Hr', 69875, 69575, 800, 'Normal', 'DPEDIA', 'PKIN', 'INDOSAT DATA FREEDOM 18GB FULL 24 JAM NASIONAL, 30HARI'),
(3440, 593, 'DFFI4', 'FREE INET', 'Freedom Internet 25GB 30Hr', 93525, 93225, 800, 'Normal', 'DPEDIA', 'PKIN', 'INDOSAT DATA FREEDOM 25GB FULL 24 JAM NASIONAL, 30HARI'),
(3441, 594, 'DFFRP1', 'FREE INET P', 'Indosat Paket Freedom Internet Plus 1 GB', 29650, 29350, 800, 'Normal', 'DPEDIA', 'PKIN', 'Kuota Utama (2G/3G/4G) 1 GB, Kuota Bonus (4G) 1 GB'),
(3442, 595, 'DFFRP2', 'FREE INET P', 'Indosat Paket Freedom Internet Plus 2 GB', 44050, 43750, 800, 'Normal', 'DPEDIA', 'PKIN', 'Kuota Utama (2G/3G/4G) 2 GB, Kuota Bonus (4G) 2 GB'),
(3443, 596, 'DFFRP3', 'FREE INET P', 'Indosat Paket Freedom Internet Plus 4 GB', 62125, 61825, 800, 'Normal', 'DPEDIA', 'PKIN', 'Kuota Utama (2G/3G/4G) 4 GB, Kuota Bonus (4G) 4 GB'),
(3444, 597, 'DFFRP4', 'FREE INET P', 'Indosat Paket Freedom Internet Plus 5 GB', 107125, 106825, 800, 'Normal', 'DPEDIA', 'PKIN', 'Kuota Utama (2G/3G/4G) 5 GB, Kuota Bonus (4G) 5 GB'),
(3445, 598, 'DHJ1', 'HAJI', 'Indosat Haji Data Unlimited 20 hari.', 333950, 333650, 800, 'Normal', 'DPEDIA', 'PKIN', 'Paket Haji Data Unlimited Berlaku untuk 20 hari.'),
(3446, 599, 'DHJ2', 'HAJI', 'Indosat Haji Data Unlimited 40 hari.', 540420, 540120, 800, 'Normal', 'DPEDIA', 'PKIN', 'Paket Haji Data Unlimited Berlaku untuk 40 hari.'),
(3447, 600, 'DHJ3', 'HAJI K', 'Indosat Haji Komplit Unlimited 20 hari', 423814, 423514, 800, 'Normal', 'DPEDIA', 'PKIN', 'Paket Haji Komplit Unlimited. Bonus telepon 60 min to Indonesia/to local/incoming , 60 SMS. Berlaku untuk 20 hari.'),
(3448, 601, 'DHJ4', 'HAJI K', 'Indosat Haji Komplit Unlimited 40 hari', 629920, 629620, 800, 'Normal', 'DPEDIA', 'PKIN', 'Paket Haji Komplit Unlimited. Bonus telepon 120 min to Indonesia/to local/incoming , 120 SMS. Berlaku untuk 40 hari.'),
(3449, 602, 'DFF15', 'FREE INET', 'Freedom Internet 2GB 15Hr', 15510, 15210, 800, 'Normal', 'DPEDIA', 'PKIN', 'INDOSAT DATA FREEDOM 2GB FULL 24 JAM NASIONAL, 15HARI'),
(3450, 603, 'DIIU1', 'DATA UNLI', 'Indosat Freedom U 1 GB + 4.5 GB Apps / 30 Hari', 24730, 24430, 800, 'Normal', 'DPEDIA', 'PKIN', '1 GB Kuota Utama + 4.5 GB Aplikasi + Unlimited Aplikasi sehari-hari + Unlimited Streaming + Unlimited Apps Belajat, Berlaku 30 Hari'),
(3451, 604, 'DIIU2', 'DATA UNLI', 'Indosat Internet Unlimited + 2GB, 30 hari', 37735, 37435, 800, 'Normal', 'DPEDIA', 'PKIN', '2 GB Kuota Utama + 7.5 GB Aplikasi + Unlimited Aplikasi sehari-hari + Unlimited Streaming + Unlimited Apps Belajat, Berlaku 30 Hari'),
(3452, 605, 'DIIU3', 'DATA UNLI', 'Indosat Freedom U 3 GB + 15 GB Apps / 30 Hari', 56425, 56125, 800, 'Normal', 'DPEDIA', 'PKIN', '3 GB Kuota Utama + 15 GB Aplikasi + Unlimited Aplikasi sehari-hari + Unlimited Streaming + Unlimited Apps Belajat + Unlimited SMS ke sesama IM3, Berlaku 30 Hari'),
(3453, 606, 'DIIU4', 'DATA UNLI', 'Indosat Freedom U 7 GB + 20 GB Apps / 30 Hari', 74220, 73920, 800, 'Normal', 'DPEDIA', 'PKIN', '7 GB Kuota Utama + 20 GB Aplikasi + Unlimited Aplikasi sehari-hari + Unlimited Streaming + Unlimited Apps Belajat + Unlimited SMS ke sesama IM3, Berlaku 30 Hari'),
(3454, 607, 'DIIU5', 'DATA UNLI', 'Indosat Internet Unlimited + 10GB, 30 hari', 93575, 93275, 800, 'Normal', 'DPEDIA', 'PKIN', '10 GB Kuota Utama + 25 GB Aplikasi + Unlimited Aplikasi sehari-hari + Unlimited Streaming + Unlimited Apps Belajat + Unlimited Telepon dan SMS ke sesama IM3, Berlaku 30 Hari'),
(3455, 608, 'DIIU6', 'DATA UNLI', 'Indosat Freedom U 15 GB + 25 GB Apps / 30 Hari', 126850, 126550, 800, 'Normal', 'DPEDIA', 'PKIN', 'cek di https://www.indosatooredoo.com/portal/id/psfreedomu'),
(3456, 609, 'DIIU7', 'DATA UNLI', 'Indosat Internet Unlimited JUMBO, 30 hari', 144845, 144545, 800, 'Normal', 'DPEDIA', 'PKIN', 'Kuota Utama JUMBO + Unlimited Aplikasi sehari-hari + Unlimited Streaming Spotify dan iFlix + 100 SMS ke Semua Operator + Unlimited Nelpon + SMS ke Indosat + Unlimited Youtube'),
(3457, 610, 'DEKS1', 'EKSTRA', 'Indosat Paket Ekstra 2GB', 38375, 38075, 800, 'Normal', 'DPEDIA', 'PKIN', 'Paket Ekstra 2GB, Tanpa batas waktu (24 jam). Hanya berlaku bagi pelanggan paket Super Internet Kuota, Super Internet Paket Khusus, SP Mentari 3GB, Super Internet Unlimited dan Freedom Combo.'),
(3458, 611, 'DEKS2', 'EKSTRA', 'Indosat Paket Ekstra 4GB', 55125, 54825, 800, 'Normal', 'DPEDIA', 'PKIN', 'Paket Ekstra 4GB, Tanpa batas waktu (24 jam). Hanya berlaku bagi pelanggan paket Super Internet Kuota, Super Internet Paket Khusus, SP Mentari 3GB, Super Internet Unlimited dan Freedom Combo.'),
(3459, 612, 'DEKS3', 'EKSTRA', 'Indosat Paket Ekstra 6GB', 73125, 72825, 800, 'Normal', 'DPEDIA', 'PKIN', 'Paket Ekstra 6GB, Tanpa batas waktu (24 jam). Hanya berlaku bagi pelanggan paket Super Internet Kuota, Super Internet Paket Khusus, SP Mentari 3GB, Super Internet Unlimited dan Freedom Combo.'),
(3460, 613, 'DYLW1', 'YELLOW', 'Indosat Yellow 1 GB / 1 Hari', 4350, 4050, 800, 'Normal', 'DPEDIA', 'PKIN', '-'),
(3461, 614, 'DYLW2', 'YELLOW', 'Indosat Yellow 1 GB / 3 Hari', 5850, 5550, 800, 'Normal', 'DPEDIA', 'PKIN', '-'),
(3462, 615, 'DYLW3', 'YELLOW', 'Indosat Yellow 1 GB / 7 Hari', 10400, 10100, 800, 'Normal', 'DPEDIA', 'PKIN', '-'),
(3463, 616, 'DYLW4', 'YELLOW', 'Indosat Yellow 1 GB / 15 Hari', 13825, 13525, 800, 'Normal', 'DPEDIA', 'PKIN', '-'),
(3464, 617, 'DIIU8', 'DATA UNLI', 'Indosat Freedom U 5 GB / 30 Hari', 61850, 61550, 800, 'Normal', 'DPEDIA', 'PKIN', 'Kuota Utama 5GB + Unlimited Aplikasi sehari-hari + Unlimited Streaming Spotify dan iFlix . Masa Aktif 30 hari'),
(3465, 618, 'DIIU9', 'DATA UNLI', 'Indosat Freedom U 1 GB + 2 GB Apps / 7 Hari', 15680, 15380, 800, 'Normal', 'DPEDIA', 'PKIN', '1 GB Kuota Utama + 2 GB Aplikasi + Unlimited Aplikasi sehari-hari + Unlimited Streaming + Unlimited Apps Belajat, Berlaku 7 Hari'),
(3466, 619, 'DIIU10', 'DATA UNLI', 'Indosat Internet Unlimited + 500MB, 2 hari', 5900, 5600, 800, 'Normal', 'DPEDIA', 'PKIN', 'Kuota Utama 500MB + Unlimited Aplikasi sehari-hari + Unlimited Streaming Spotify dan iFlix + Unlimited SMS ke IM3 Ooredoo + Unlimited Nelpon ke IM3 Ooredoo + Unlimited Youtube (via MyIM3)'),
(3467, 620, 'DAPPS1', 'APPS', 'Apps Kuota 5GB 30hr', 26645, 26345, 800, 'Normal', 'DPEDIA', 'PKIN', '-'),
(3468, 621, 'DAPPS2', 'APPS', 'Apps Kuota 10GB 30hr', 37175, 36875, 800, 'Normal', 'DPEDIA', 'PKIN', '-'),
(3469, 622, 'DAPPS3', 'APPS', 'Apps Kuota 15GB 30hr', 51275, 50975, 800, 'Normal', 'DPEDIA', 'PKIN', '-'),
(3470, 623, 'DAPPS4', 'APPS', 'Apps Kuota 20GB 30hr', 65375, 65075, 800, 'Normal', 'DPEDIA', 'PKIN', '-'),
(3471, 624, 'DBRO1', 'BRONET', 'BRONET 500 MB', 8850, 8550, 800, 'Normal', 'DPEDIA', 'PKIN', 'Kuota 500 MB, Masa aktif 1 Hari, 24 Jam'),
(3472, 625, 'DBRO2', 'BRONET', 'BRONET 1GB', 13775, 13475, 800, 'Normal', 'DPEDIA', 'PKIN', '24 jam'),
(3473, 626, 'DBRO3', 'BRONET', 'BRONET 2GB', 22975, 22675, 800, 'Normal', 'DPEDIA', 'PKIN', '24 jam'),
(3474, 627, 'DBRO5', 'BRONET', 'BRONET 3GB', 28775, 28475, 800, 'Normal', 'DPEDIA', 'PKIN', 'Kuota 3GB. 24 Jam'),
(3475, 628, 'DBRO6', 'BRONET', 'BRONET 5GB', 42525, 42225, 800, 'Normal', 'DPEDIA', 'PKIN', 'Kuota 5GB. 24 Jam'),
(3476, 629, 'DBRO7', 'BRONET', 'BRONET 8 GB', 57075, 56775, 800, 'Normal', 'DPEDIA', 'PKIN', 'Kuota 8 GB, Masa Aktif 30 Hari, 24 Jam'),
(3477, 630, 'DBRO8', 'BRONET', 'BRONET 2GB 60 hari', 24350, 24050, 800, 'Normal', 'DPEDIA', 'PKIN', '2GB Kuota utama bisa digunakan selama 24 jam, berlaku 60hari.'),
(3478, 631, 'DBRO9', 'BRONET', 'BRONET 3 GB / 60 hari', 34446, 34146, 800, 'Normal', 'DPEDIA', 'PKIN', '3GB Kuota utama bisa digunakan selama 24 jam, berlaku 60hari.'),
(3479, 632, 'DBRO10', 'BRONET', 'BRONET 5GB 60 hari', 43850, 43550, 800, 'Normal', 'DPEDIA', 'PKIN', '5GB Kuota utama bisa digunakan selama 24 jam, berlaku 60hari.'),
(3480, 633, 'DBRO11', 'BRONET', 'BRONET 8GB 60 hari', 58350, 58050, 800, 'Normal', 'DPEDIA', 'PKIN', '8GB Kuota utama bisa digunakan selama 24 jam, berlaku 60hari.'),
(3481, 634, 'DBRO12', 'BRONET', 'BRONET 10 GB / 60 hari', 78610, 78310, 800, 'Normal', 'DPEDIA', 'PKIN', '10GB Kuota utama bisa digunakan selama 24 jam, berlaku 60hari.'),
(3482, 635, 'DBRO13', 'BRONET', 'BRONET 12GB 60 hari', 80875, 80575, 800, 'Normal', 'DPEDIA', 'PKIN', '12GB Kuota utama bisa digunakan selama 24 jam, berlaku 60hari.'),
(3483, 636, 'DOWS1', 'OWSEM', 'Axis OWSEM 16 GB + Unlimited Games / 30 Hari', 40125, 39825, 800, 'Normal', 'DPEDIA', 'PKIN', 'Kuota Internet 24jam 2Gb + Kuota Sosmed 4Gb (FB, Twitter, IG, Tiktok) + Kuota Musik 4Gb (Joox, Smule) + 6Gb Kuota Malam 00-06 + Kuota Games Unlimited 1Gb /day (FreeFire, Mobile Legends, AOV, ASPHALT Legends, Vain Glory, Modern Combat 5)'),
(3484, 637, 'DOWS2', 'OWSEM', 'Axis OWSEM 24GB + Unlimited Games 30hr', 42875, 42575, 800, 'Normal', 'DPEDIA', 'PKIN', 'Kuota Internet 24jam 3Gb + Kuota Sosmed 6Gb (FB, Twitter, IG, Tiktok) + Kuota Musik 6Gb (Joox, Smule) +  9Gb Kuota Malam 00-06 + Kuota Games Unlimited 1Gb /day (FreeFire, Mobile Legends, AOV, ASPHALT Legends, Vain Glory, Modern Combat 5)'),
(3485, 638, 'DOWS3', 'OWSEM', 'Axis OWSEM 32GB + Unlimited Games 30hr', 49875, 49575, 800, 'Normal', 'DPEDIA', 'PKIN', 'Kuota Internet 24jam 4Gb + Kuota Sosmed 8Gb (FB, Twitter, IG, Tiktok) + Kuota Musik 8Gb (Joox, Smule) + 12Gb Kuota Malam 00-06 + Kuota Games Unlimited 1Gb /day (FreeFire, Mobile Legends, AOV, ASPHALT Legends, Vain Glory, Modern Combat 5)'),
(3486, 639, 'DOWS4', 'OWSEM', 'Axis OWSEM 48GB + Unlimited Games 30hr', 62350, 62050, 800, 'Normal', 'DPEDIA', 'PKIN', 'Kuota Internet 24jam 6Gb + Kuota Sosmed 12Gb (FB, Twitter, IG, Tiktok) + Kuota Musik 12Gb (Joox, Smule) + 18Gb Kuota Malam 00-06 + Kuota Games Unlimited 1Gb /day (FreeFire, Mobile Legends, AOV, ASPHALT Legends, Vain Glory, Modern Combat 5)'),
(3487, 640, 'DOWS5', 'OWSEM', 'Axis OWSEM 80GB + Unlimited Games 30hr', 95420, 95120, 800, 'Normal', 'DPEDIA', 'PKIN', 'Kuota Internet 24jam 10Gb + Kuota Sosmed 20Gb (FB, Twitter, IG, Tiktok) + Kuota Musik 20Gb (Joox, Smule) + 30Gb Kuota Malam 00-06 + Kuota Games Unlimited 1Gb /day (FreeFire, Mobile Legends, AOV, ASPHALT Legends, Vain Glory, Modern Combat 5)'),
(3488, 641, 'DOWS6', 'OWSEM', 'OWSEM 1GB + 1GB 4G', 17675, 17375, 800, 'Normal', 'DPEDIA', 'PKIN', 'BRONET 4G OWSEM 1GB+1GB(4G)+2GB Aplikasi, 24 jam 30HR'),
(3489, 642, 'DOWS7', 'OWSEM', 'OWSEM 1GB + 3GB 4G', 29675, 29375, 800, 'Normal', 'DPEDIA', 'PKIN', 'BRONET 4G OWSEM 1GB+3GB(4G)+4GB Aplikasi, 24 jam 30HR'),
(3490, 643, 'DOWS8', 'OWSEM', 'OWSEM 2GB + 6GB 4G', 46075, 45775, 800, 'Normal', 'DPEDIA', 'PKIN', 'BRONET 4G OWSEM 2GB+6GB(4G)+8GB Aplikasi, 24 jam 30HR'),
(3491, 644, 'DOWS9', 'OWSEM', 'OWSEM 3GB + 9GB 4G', 60375, 60075, 800, 'Normal', 'DPEDIA', 'PKIN', 'BRONET 4G OWSEM 3GB+9GB(4G)+12GB Aplikasi, 24 jam 30HR'),
(3492, 645, 'DSMR1', 'SMART INT', 'Smart Internet 10.000', 10375, 10075, 800, 'Normal', 'DPEDIA', 'PKIN', 'Kuota Internet sebesar 10rb selama 7 hari'),
(3493, 646, 'DSMR2', 'SMART INT', 'Smart Internet 20.000', 19400, 19100, 800, 'Normal', 'DPEDIA', 'PKIN', 'Kuota Internet sebesar 20rb selama 14 hari'),
(3494, 647, 'DSMR3', 'SMART INT', 'Smart Internet 30.000', 28050, 27750, 800, 'Normal', 'DPEDIA', 'PKIN', 'Kuota Internet sebesar 30rb selama 14 hari'),
(3495, 648, 'DSMR4', 'SMART INT', 'Smart Internet 60.000', 54920, 54620, 800, 'Normal', 'DPEDIA', 'PKIN', 'Kuota Internet sebesar 60rb selama 30 hari'),
(3496, 649, 'DSMR5', 'SMART INT', 'Smart Internet 100.000', 93545, 93245, 800, 'Normal', 'DPEDIA', 'PKIN', 'Kuota Internet sebesar 100rb selama 30 hari'),
(3497, 650, 'DSMR6', 'SMART INT', 'Smart Internet 150.000', 148350, 148050, 800, 'Normal', 'DPEDIA', 'PKIN', 'Kuota Internet sebesar 150rb selama 30 hari'),
(3498, 651, 'DSMR7', 'SMART INT', 'Smart Internet 200.000', 178520, 178220, 800, 'Normal', 'DPEDIA', 'PKIN', 'Kuota Internet sebesar 200.000 selama 30 hari'),
(3499, 652, 'DSMIX1', 'SMART MIX', 'Smart 3GB 7Hari ( 1.5GB Regular + 1.5GB Malam)', 10525, 10225, 800, 'Normal', 'DPEDIA', 'PKIN', '1.5GB Regular + 1.5GB Malam, 7 Hari'),
(3500, 653, 'DSMIX2', 'SMART MIX', 'Smart 4GB 14Hari ( 2GB Regular + 2GB Malam)', 20710, 20410, 800, 'Normal', 'DPEDIA', 'PKIN', '2GB Regular + 2GB Malam'),
(3501, 654, 'DSMIX3', 'SMART MIX', 'Smart 8GB 14Hari ( 4GB Regular + 4GB Malam)', 28575, 28275, 800, 'Normal', 'DPEDIA', 'PKIN', '-'),
(3502, 655, 'DSMIX4', 'SMART MIX', 'Smart 16GB 30Hari ( 8GB Regular + 8GB Malam)', 49550, 49250, 800, 'Normal', 'DPEDIA', 'PKIN', '8GB Regular + 8GB Malam'),
(3503, 656, 'DSMIX5', 'SMART MIX', 'Smart 30GB 30Hari ( 15GB Regular + 15GB Malam)', 64350, 64050, 800, 'Normal', 'DPEDIA', 'PKIN', '15GB Regular + 15GB Malam'),
(3504, 657, 'DSMIX6', 'SMART MIX', 'Smart 12 GB Kuota Malam / 30 Hari', 20850, 20550, 800, 'Normal', 'DPEDIA', 'PKIN', 'Hanya kuota malam'),
(3505, 658, 'DEVO1', 'SMART EVO', 'Smart Data Evo 1.25GB+1.25GB(Mid) 7Hari', 10875, 10575, 800, 'Normal', 'DPEDIA', 'PKIN', '-'),
(3506, 659, 'DEVO2', 'SMART EVO', 'Smart Data Evo 2GB+2GB(Mid) 14Hari', 20650, 20350, 800, 'Normal', 'DPEDIA', 'PKIN', '-'),
(3507, 660, 'DEVO3', 'SMART EVO', 'Smart Data Evo 4GB+4GB(Mid) 14Hari', 30325, 30025, 800, 'Normal', 'DPEDIA', 'PKIN', '-'),
(3508, 661, 'DEVO4', 'SMART EVO', 'Smart Data Evo 8GB+8GB(Mid) 30Hari', 61850, 61550, 800, 'Normal', 'DPEDIA', 'PKIN', '-'),
(3509, 662, 'DEVO5', 'SMART EVO', 'Smart Data Evo 15GB+15GB(Mid) 30Hari', 98950, 98650, 800, 'Normal', 'DPEDIA', 'PKIN', '-'),
(3510, 663, 'DTRD1', 'TRI DATA', 'Tri data 1GB 7 Hari', 15650, 15350, 800, 'Normal', 'DPEDIA', 'PKIN', '-'),
(3511, 664, 'DTRD2', 'TRI DATA', 'Tri Data 1 GB / 14 Hari', 20585, 20285, 800, 'Normal', 'DPEDIA', 'PKIN', '-'),
(3512, 665, 'DTRD3', 'TRI DATA', 'Tri Data 2.75GB ( 750MB + 2GB 4G) 7Hari', 15975, 15675, 800, 'Normal', 'DPEDIA', 'PKIN', '-'),
(3513, 666, 'DTRM1', 'TRI MIX', 'Tri Mix Combo 2 GB + 20 Menit', 35225, 34925, 800, 'Normal', 'DPEDIA', 'PKIN', 'Paket Kombo Tri 2GB + 20 Menit telpon ke semua operator. Masa aktif 30 hari.'),
(3514, 667, 'DTRM2', 'TRI MIX', 'Tri Mix Combo 32 GB + 30 menit', 59745, 59445, 800, 'Normal', 'DPEDIA', 'PKIN', '2 GB (semua jaringan) + 30 GB (1 GB per hari di jaringan 4G) + 30 menit ke semua operator, Masa Aktif 30 Hari'),
(3515, 668, 'DTRM3', 'TRI MIX', 'Tri Mix Combo 38 GB + 30 menit', 123825, 123525, 800, 'Normal', 'DPEDIA', 'PKIN', '8 GB (semua jaringan) + 30 GB (1 GB per hari di jaringan 4G) + 30 menit ke semua operator, Masa Aktif 30 Hari'),
(3516, 669, 'DTRM4', 'TRI MIX', 'Tri Mix Kuota++ 2.25GB', 51800, 51500, 800, 'Normal', 'DPEDIA', 'PKIN', '2.25GB mengikuti masa aktif sim'),
(3517, 670, 'DTRM5', 'TRI MIX', 'Tri Mix Small 2.75GB (750MB + 2GB 4G 3Hari)', 10150, 9850, 800, 'Normal', 'DPEDIA', 'PKIN', '0.75 GB (semua jaringan) + 2 GB (jaringan 4G) , Masa Aktif 3 Hari'),
(3518, 671, 'DTRM6', 'TRI MIX', 'Tri Mix Small 3.75 GB 14 Hari', 25425, 25125, 800, 'Normal', 'DPEDIA', 'PKIN', '1.75 GB (semua jaringan) + 2 GB (jaringan 4G), Masa Aktif 14 Hari'),
(3519, 672, 'DTRM7', 'TRI MIX', 'Tri Mix Small 3 GB 3 Hari', 20505, 20205, 800, 'Normal', 'DPEDIA', 'PKIN', '3 GB semua jaringan 24 jam, Masa Aktif 3 Hari'),
(3520, 673, 'DTRM8', 'TRI MIX', 'Tri Mix Small 5GB', 5755, 5455, 800, 'Normal', 'DPEDIA', 'PKIN', '1 GB (24 jam) + 4 GB (12 malam ? 9 pagi) semua jaringan, Masa Aktif 1 Hari'),
(3521, 674, 'DTRM9', 'TRI MIX', 'Tri Mix Super 10 GB 30 Hari', 49825, 49525, 800, 'Normal', 'DPEDIA', 'PKIN', '2 GB (semua jaringan) + 8 GB (jaringan 4G), Masa Aktif 30 Hari'),
(3522, 675, 'DTRM10', 'TRI MIX', 'Tri Mix Super 24 GB 30 Hari', 88849, 88549, 800, 'Normal', 'DPEDIA', 'PKIN', '24GB (4GB + 20GB 4G )'),
(3523, 676, 'DTRM11', 'TRI MIX', 'Tri Mix Super 30 GB 30 Hari', 98825, 98525, 800, 'Normal', 'DPEDIA', 'PKIN', '30GB (8GB + 22GB 4G)'),
(3524, 677, 'DLTE1', 'TRI LTE', 'LTE 12 GB', 60850, 60550, 800, 'Normal', 'DPEDIA', 'PKIN', '12GB 4G + UNL YOUTUBE 30HR 24JAM'),
(3525, 678, 'DLTE2', 'TRI LTE', 'LTE 33 GB', 115875, 115575, 800, 'Normal', 'DPEDIA', 'PKIN', '3 GB semua jaringan, 30 GB 4G, 30 Hari'),
(3526, 679, 'DLTE3', 'TRI LTE', 'LTE 66 GB', 145875, 145575, 800, 'Normal', 'DPEDIA', 'PKIN', '6 GB semua jaringan, 60 GB 4G, 60 Hari'),
(3527, 680, 'DAO1', 'TRI AO', 'AlwaysOn 1.5 GB', 14600, 14300, 800, 'Normal', 'DPEDIA', 'PKIN', '1.5 GB Nasional, Mengikuti Masa Aktif Kartu'),
(3528, 681, 'DAO2', 'TRI AO', 'AlwaysOn 2 GB', 18600, 18300, 800, 'Normal', 'DPEDIA', 'PKIN', '2 GB Nasional, Mengikuti Masa Aktif Kartu'),
(3529, 682, 'DAO3', 'TRI AO', 'AlwaysOn 3 GB', 24725, 24425, 800, 'Normal', 'DPEDIA', 'PKIN', '3 GB Nasional, Mengikuti Masa Aktif Kartu'),
(3530, 683, 'DAO4', 'TRI AO', 'AlwaysOn 6 GB', 33700, 33400, 800, 'Normal', 'DPEDIA', 'PKIN', '6 GB Nasional, Mengikuti Masa Aktif Kartu'),
(3531, 684, 'DAO5', 'TRI AO', 'AlwaysOn 6 GB + Unlimited', 59825, 59525, 800, 'Normal', 'DPEDIA', 'PKIN', '6 GB Nasional, Mengikuti Masa Aktif Kartu. Unlimited Aplikasi 30 Hari Pukul 01.00 - 17.00 WIB, batas pemakaian wajar 1,5GB per hari.'),
(3532, 685, 'DAO6', 'TRI AO', 'AlwaysOn 10 GB + Unlimited', 81825, 81525, 800, 'Normal', 'DPEDIA', 'PKIN', '10 GB Nasional, Mengikuti Masa Aktif Kartu. Unlimited Aplikasi 30 Hari Pukul 01.00 - 17.00 WIB, batas pemakaian wajar 1,5GB per hari.'),
(3533, 686, 'DAO8', 'TRI AO', 'AlwaysOn 18 GB + Unlimited', 103825, 103525, 800, 'Normal', 'DPEDIA', 'PKIN', '16 GB Nasional, Mengikuti Masa Aktif Kartu, Unlimited Aplikasi 30 Hari Pukul 01.00 - 17.00 WIB, batas pemakaian wajar 1,5GB per hari.'),
(3534, 687, 'DTAON1', 'TRI AON', 'Tri data AON 1GB + Bonus 1GB', 22350, 22050, 800, 'Normal', 'DPEDIA', 'PKIN', '-'),
(3535, 688, 'DTAON2', 'TRI AON', 'Tri data AON 2GB + Bonus 4GB', 38850, 38550, 800, 'Normal', 'DPEDIA', 'PKIN', '-'),
(3536, 689, 'DTAON3', 'TRI AON', 'Tri data AON 3GB + Bonus 6GB', 69875, 69575, 800, 'Normal', 'DPEDIA', 'PKIN', '-'),
(3537, 690, 'DTAON4', 'TRI AON', 'Tri data 2GB + 20 Menit Telepon 30 Hari', 35600, 35300, 800, 'Normal', 'DPEDIA', 'PKIN', '-');
INSERT INTO `layanan_pulsa` (`id`, `service_id`, `provider_id`, `operator`, `layanan`, `harga`, `harga_api`, `profit`, `status`, `provider`, `tipe`, `catatan`) VALUES
(3538, 691, 'DTAON5', 'TRI AON', 'Tri data AON 4GB + Bonus 6GB', 85875, 85575, 800, 'Normal', 'DPEDIA', 'PKIN', '-'),
(3539, 692, 'DTAON6', 'TRI AON', 'Tri data AON 6GB + Bonus 12GB', 73375, 73075, 800, 'Normal', 'DPEDIA', 'PKIN', '-'),
(3540, 693, 'DTAON7', 'TRI AON', 'Tri data AON 8GB + Bonus 16GB', 90850, 90550, 800, 'Normal', 'DPEDIA', 'PKIN', '-'),
(3541, 694, 'DTAON8', 'TRI AON', 'Tri data AON 10GB + Bonus 20GB', 139875, 139575, 800, 'Normal', 'DPEDIA', 'PKIN', '-'),
(3542, 695, 'DTMNI1', 'TRI MINI', 'Tri Data Mini 1 GB / 5 Hari', 8820, 8520, 800, 'Normal', 'DPEDIA', 'PKIN', '1 GB Nasional, Pulsa 2K, 5 Hari'),
(3543, 696, 'DTMNI3', 'TRI MINI', 'Tri Data Mini 5 GB / 7 Hari', 15825, 15525, 800, 'Normal', 'DPEDIA', 'PKIN', '2.5 GB Nasional, 2.5 GB (01.00 - 09.00), 7 Hari'),
(3544, 697, 'DTHR1', 'NASIONAL', '1GB Nasional + 4GB (01.00 - 09.00) 1 Hari', 5849, 5549, 800, 'Normal', 'DPEDIA', 'PKIN', '-'),
(3545, 698, 'DTHR2', 'NASIONAL', 'Kuota++ 80MB Semua Jaringan 1 Hari', 5825, 5525, 800, 'Normal', 'DPEDIA', 'PKIN', '-'),
(3546, 699, 'DTHR3', 'NASIONAL', '300 MB (semua jaringan) Mengikuti Masa Aktif Kartu', 10825, 10525, 800, 'Normal', 'DPEDIA', 'PKIN', '-'),
(3547, 700, 'DTHR4', 'NASIONAL', '500 MB (semua jaringan) + 1 GB (Youtube + Netflix) 3Hari', 10850, 10550, 800, 'Normal', 'DPEDIA', 'PKIN', '-'),
(3548, 701, 'DTHR5', 'NASIONAL', '5GB + 4GB (01 Pagi - 12 Siang) + 10 Menit Telepon All Op 7 Hari', 35600, 35300, 800, 'Normal', 'DPEDIA', 'PKIN', '-'),
(3549, 702, 'DTHR6', 'NASIONAL', '5 GB Semua Jaringan + 20 Menit Telepon + SMS Semua Op + TRIXOGO + VIU 7 Hari', 30025, 29725, 800, 'Normal', 'DPEDIA', 'PKIN', '-'),
(3550, 703, 'DTHR7', 'NASIONAL', '10 GB( Semua Jaringan) + 10GB ( Youtube + Netflix) 30 Hari', 69550, 69250, 800, 'Normal', 'DPEDIA', 'PKIN', '-'),
(3551, 704, 'DTHR8', 'NASIONAL', '8,5 GB +100 Menit Telepon + SMS Semua Op + TRIXOGO + VIU 30 Hari', 59550, 59250, 800, 'Normal', 'DPEDIA', 'PKIN', '-'),
(3552, 705, 'DCLE1', 'COMBO LITE', 'Combo LITE 2 GB (Data)', 23560, 23260, 800, 'Normal', 'DPEDIA', 'PKIN', '3 GB Semua Jaringan + 1 GB (422 Kota) + 2 GB (273 Kota) + 3 GB (86 Kota) + 5 GB (28 Kota) + 5 Menit Nelpon Semua Operator, masa aktif 30 Hari'),
(3553, 706, 'DCLE2', 'COMBO LITE', 'XL Combo LITE 4.5 GB / 30 Hari', 27660, 27360, 800, 'Normal', 'DPEDIA', 'PKIN', '4.5 GB Semua Jaringan + 1.5 GB (422 Kota) + 2 GB (273 Kota) + 4 GB (86 Kota) + 7 GB (28 Kota) + 5 Menit Nelpon Semua Operator, masa aktif 30 Hari'),
(3554, 707, 'DCLE3', 'COMBO LITE', 'XL Combo LITE 7 GB / 30 Hari', 38675, 38375, 800, 'Normal', 'DPEDIA', 'PKIN', '7 GB Semua Jaringan + 3 GB (422 Kota) + 3 GB (273 Kota) + 6 GB (86 Kota) + 11 GB (28 Kota) + 5 Menit Nelpon Semua Operator, masa aktif 30 Hari'),
(3555, 708, 'DCLE4', 'COMBO LITE', 'XL Combo Lite XL', 61225, 60925, 800, 'Normal', 'DPEDIA', 'PKIN', 'deskripsi produk cek di link https://www.xl.co.id/id/bantuan/produk/xtra-combo-lite'),
(3556, 709, 'DCLE5', 'COMBO LITE', 'XL Combo Lite XXL', 96075, 95775, 800, 'Normal', 'DPEDIA', 'PKIN', 'deskripsi produk cek di link https://www.xl.co.id/id/bantuan/produk/xtra-combo-lite'),
(3557, 710, 'DCLE6', 'COMBO LITE', 'XL Combo Lite XXXL', 119075, 118775, 800, 'Normal', 'DPEDIA', 'PKIN', 'deskripsi produk cek di link https://www.xl.co.id/id/bantuan/produk/xtra-combo-lite'),
(3558, 711, 'DXLH1', 'HOTROD', 'XL HOTROD 800MB', 30125, 29825, 800, 'Normal', 'DPEDIA', 'PKIN', '-'),
(3559, 712, 'DXLH2', 'HOTROD', 'XL HOTROD 1.5 GB', 47475, 47175, 800, 'Normal', 'DPEDIA', 'PKIN', 'Kuota 24 Jam 1.5GB. Masa aktif 30 hari.'),
(3560, 713, 'DXLH3', 'HOTROD', 'XL HOTROD 3GB', 56725, 56425, 800, 'Normal', 'DPEDIA', 'PKIN', '-'),
(3561, 714, 'DXLH4', 'HOTROD', 'XL HOTROD 6GB', 93125, 92825, 800, 'Normal', 'DPEDIA', 'PKIN', '-'),
(3562, 715, 'DXLH5', 'HOTROD', 'XL HOTROD 8GB', 116725, 116425, 800, 'Normal', 'DPEDIA', 'PKIN', '-'),
(3563, 716, 'DXLH6', 'HOTROD', 'XL HOTROD 12GB', 167775, 167475, 800, 'Normal', 'DPEDIA', 'PKIN', '-'),
(3564, 717, 'DXLH7', 'HOTROD', 'XL HOTROD 16GB', 204725, 204425, 800, 'Normal', 'DPEDIA', 'PKIN', '-'),
(3565, 718, 'DXTRAC1', 'XTRA COMBO', 'XL Xtra Combo 5 GB + 10 GB / 30 Hari', 55470, 55170, 800, 'Normal', 'DPEDIA', 'PKIN', '5GB (2G/3G/4G) + Nelp Anynet 20 mins. Youtube 24 Jam 10GB. Youtube 01:00-06:00 Tanpa kuota Unlimited.'),
(3566, 719, 'DXTRAC2', 'XTRA COMBO', 'XL Xtra Combo 10 GB + 20 GB / 30 Hari', 82470, 82170, 800, 'Normal', 'DPEDIA', 'PKIN', '10GB (2G/3G/4G) + Nelp Anynet 30 mins. Youtube 24 Jam 20GB. Youtube 01:00-06:00 Tanpa kuota Unlimited.'),
(3567, 720, 'DXTRAC3', 'XTRA COMBO', 'XL Xtra Combo 15 GB + 30 GB / 30 Hari', 114475, 114175, 800, 'Normal', 'DPEDIA', 'PKIN', '15GB (2G/3G/4G) + Nelp Anynet 40 mins. Youtube 24 Jam 30GB. Youtube 01:00-06:00 Tanpa kuota Unlimited.'),
(3568, 721, 'DXTRAC4', 'XTRA COMBO', 'XL Xtra Combo 20 GB + 40 GB / 30 Hari', 161375, 161075, 800, 'Normal', 'DPEDIA', 'PKIN', '20GB (2G/3G/4G) + Nelp Anynet 60 mins. Youtube 24 Jam 40GB. Youtube 01:00-06:00 Tanpa kuota Unlimited.'),
(3569, 722, 'DXTRAC5', 'XTRA COMBO', 'XL Xtra Combo 35 GB + 70 GB / 30 Hari', 219258, 218958, 800, 'Normal', 'DPEDIA', 'PKIN', '35GB (2G/3G/4G) + Nelp Anynet 90 mins. Youtube 24 Jam 70GB. Youtube 01:00-06:00 Tanpa kuota Unlimited.'),
(3570, 723, 'DXTRCV2', 'COMBO VIP', 'XL Xtra Combo VIP 10 GB + 20 GB / 30 Hari', 90308, 90008, 800, 'Normal', 'DPEDIA', 'PKIN', 'Kuota Utama (2G/3G/4G) 10GB + Kuota YouTube 20GB + YouTube Tanpa Kuota Tanpa Batas 01:00-06:00 WIB + Nelpon ke Semua Operator 30Menit'),
(3571, 724, 'DXTRCV3', 'COMBO VIP', 'XL Xtra Combo VIP 15 GB + 30 GB / 30 Hari', 119625, 119325, 800, 'Normal', 'DPEDIA', 'PKIN', 'Kuota Utama (2G/3G/4G) 15GB + Kuota YouTube 30GB + YouTube Tanpa Kuota Tanpa Batas 01:00-06:00 WIB + Nelpon ke Semua Operator 40Menit'),
(3572, 725, 'DXTRCV5', 'COMBO VIP', 'XL Xtra Combo VIP 35 GB + 70 GB / 30 Hari', 232875, 232575, 800, 'Normal', 'DPEDIA', 'PKIN', 'Kuota Utama (2G/3G/4G) 35GB + Kuota YouTube 70GB + YouTube Tanpa Kuota Tanpa Batas 01:00-06:00 WIB + Nelpon ke Semua Operator 90Menit'),
(3573, 726, 'DXBRU1', 'COMBO BARU', 'XTRA Combo BARU 5GB (30Hari)', 55775, 55475, 800, 'Normal', 'DPEDIA', 'PKIN', 'Kuota Utama (2G/3G/4G) 5GB + Kuota YouTube 5GB + YouTube Tanpa Kuota 01:00-06:00 WIB + Nelpon ke Semua Operator 20Menit'),
(3574, 727, 'DXBRU2', 'COMBO BARU', 'XTRA Combo BARU 10GB (30Hari)', 82300, 82000, 800, 'Normal', 'DPEDIA', 'PKIN', 'Kuota Utama (2G/3G/4G) 10GB + Kuota YouTube 10GB + YouTube Tanpa Kuota 01:00-06:00 WIB + Nelpon ke Semua Operator 30Menit'),
(3575, 728, 'DXBR3', 'COMBO BARU', 'XTRA Combo BARU 15GB (30Hari)', 113975, 113675, 800, 'Normal', 'DPEDIA', 'PKIN', 'Kuota Utama (2G/3G/4G) 15GB + Kuota YouTube\n15GB + YouTube Tanpa Kuota 01:00-06:00 WIB + Nelpon ke Semua Operator 40Menit'),
(3576, 729, 'DXBR4', 'COMBO BARU', 'XTRA Combo BARU 20GB (30Hari)', 159725, 159425, 800, 'Normal', 'DPEDIA', 'PKIN', 'Kuota Utama (2G/3G/4G) 20GB + Kuota YouTube 20GB + YouTube Tanpa Kuota 00:01 - 06:00 WIB + Nelpon ke Semua Operator 60Menit'),
(3577, 730, 'DXBR5', 'COMBO BARU', 'XTRA Combo BARU 35GB', 217050, 216750, 800, 'Normal', 'DPEDIA', 'PKIN', 'Kuota Utama (2G/3G/4G) 35GB + Kuota YouTube 35GB + YouTube Tanpa Kuota 01:00 - 06:00 WIB + Nelpon ke Semua Operator 90Menit'),
(3578, 731, 'DXLTRA2', 'XTRA KUOTA', 'XL XTRA KUOTA 2 GB Joox', 10975, 10675, 800, 'Normal', 'DPEDIA', 'PKIN', '2 GB Joox 30 Hari'),
(3579, 732, 'DXLTRA4', 'XTRA KUOTA', 'XL XTRA KUOTA 2 GB Instagram', 11450, 11150, 800, 'Normal', 'DPEDIA', 'PKIN', '2 GB Instagram 30 Hari'),
(3580, 733, 'DXLTRA5', 'XTRA KUOTA', 'XL XTRA KUOTA 2 GB Facebook', 10975, 10675, 800, 'Normal', 'DPEDIA', 'PKIN', '2 GB Facebook 30 Hari'),
(3581, 734, 'DXLTRA6', 'XTRA KUOTA', 'XL XTRA KUOTA 2 GB Youtube', 11450, 11150, 800, 'Normal', 'DPEDIA', 'PKIN', '2 GB Youtube 30 Hari'),
(3582, 735, 'DXLTRA7', 'XTRA KUOTA', 'XL XTRA KUOTA 10 GB Mobile Legends', 10975, 10675, 800, 'Normal', 'DPEDIA', 'PKIN', '10 GB Mobile Legends 30 Hari'),
(3583, 736, 'DXLTRA8', 'XTRA KUOTA', 'XL XTRA KUOTA 5 GB Midnight', 10975, 10675, 800, 'Normal', 'DPEDIA', 'PKIN', '5 GB Midnight 30 Hari'),
(3584, 737, 'DXDATA1', 'XL MINI KC', 'XL Data 100MB 30 Hari', 3805, 3505, 800, 'Normal', 'DPEDIA', 'PKIN', '100MB 30 Hari'),
(3585, 738, 'DXDATA2', 'XL MINI KC', 'XL Data 500MB 30 Hari', 8655, 8355, 800, 'Normal', 'DPEDIA', 'PKIN', 'XL Data 500MB 30 Hari'),
(3586, 739, 'DXDATA3', 'XL MINI KC', 'XL Data 800MB 30 Hari', 11565, 11265, 800, 'Normal', 'DPEDIA', 'PKIN', 'XL Data 800MB 30 Hari'),
(3587, 740, 'DXDATA4', 'XL MINI KC', 'XL Data 1 GB / 30 Hari', 13050, 12750, 800, 'Normal', 'DPEDIA', 'PKIN', 'XL Data 1GB 30 Hari'),
(3588, 741, 'DPLUS3', 'PLUS', 'Hotrod xtra plus 5GB+1GB+1GB 30Hari', 32750, 32450, 800, 'Normal', 'DPEDIA', 'PKIN', '-'),
(3589, 742, 'DPLUS2', 'PLUS', 'Hotrod xtra plus 20GB+5GB+5GB 30Hari', 70750, 70450, 800, 'Normal', 'DPEDIA', 'PKIN', '-'),
(3590, 743, 'DPLUS1', 'PLUS', 'Hotrod xtra plus 2.5GB+500MB+500MB 30Hari', 19750, 19450, 800, 'Normal', 'DPEDIA', 'PKIN', '-'),
(3591, 744, 'DBYU12', 'by.U', 'by.U 25.000', 26225, 25925, 800, 'Normal', 'DPEDIA', 'PULSA', 'pulsa by.U Rp 25.000'),
(3592, 745, 'DBYU5', 'by.U', 'by.U 5.000', 6195, 5895, 800, 'Normal', 'DPEDIA', 'PULSA', 'pulsa by.U Rp 5.000'),
(3593, 746, 'DBYU10', 'by.U', 'by.U 10.000', 11140, 10840, 800, 'Normal', 'DPEDIA', 'PULSA', 'pulsa by.U Rp 10.000'),
(3594, 747, 'DBYU11', 'by.U', 'by.U 20.000', 21360, 21060, 800, 'Normal', 'DPEDIA', 'PULSA', 'pulsa by.U Rp 20.000'),
(3595, 748, 'DBYU13', 'by.U', 'by.U 50.000', 51225, 50925, 800, 'Normal', 'DPEDIA', 'PULSA', 'pulsa by.U Rp 50.000'),
(3596, 749, 'DBYU14', 'by.U', 'by.U 100.000', 100875, 100575, 800, 'Normal', 'DPEDIA', 'PULSA', 'pulsa by.U Rp 100.000'),
(3597, 750, 'DBYU1', 'by.U', 'by.U 1.000', 2005, 1705, 800, 'Normal', 'DPEDIA', 'PULSA', '-'),
(3598, 751, 'DBYU2', 'by.U', 'by.U 2.000', 3210, 2910, 800, 'Normal', 'DPEDIA', 'PULSA', '-'),
(3599, 752, 'DBYU3', 'by.U', 'by.U 3.000', 4005, 3705, 800, 'Normal', 'DPEDIA', 'PULSA', '-'),
(3600, 753, 'DBYU4', 'by.U', 'by.U 4.000', 5005, 4705, 800, 'Normal', 'DPEDIA', 'PULSA', '-'),
(3601, 754, 'DBYU6', 'by.U', 'by.U 6.000', 7175, 6875, 800, 'Normal', 'DPEDIA', 'PULSA', '-'),
(3602, 755, 'DBYU7', 'by.U', 'by.U 7.000', 8175, 7875, 800, 'Normal', 'DPEDIA', 'PULSA', '-'),
(3603, 756, 'DBYU9', 'by.U', 'by.U 9.000', 10555, 10255, 800, 'Normal', 'DPEDIA', 'PULSA', '-'),
(3604, 757, 'DBYU8', 'by.U', 'by.U 8.000', 9555, 9255, 800, 'Normal', 'DPEDIA', 'PULSA', '-'),
(3605, 758, 'DGDV1', 'GOPAY DRIVER', 'Go Pay Driver 10.000', 10950, 10650, 800, 'Normal', 'DPEDIA', 'SALGO', 'KHUSUS DRIVER - Masukan no HP'),
(3606, 759, 'DGDV3', 'GOPAY DRIVER', 'Go Pay Driver 25.000', 25950, 25650, 800, 'Normal', 'DPEDIA', 'SALGO', 'KHUSUS DRIVER - Masukan no HP'),
(3607, 760, 'DGDV2', 'GOPAY DRIVER', 'Go Pay Driver 20.000', 20950, 20650, 800, 'Normal', 'DPEDIA', 'SALGO', 'KHUSUS DRIVER - Masukan no HP'),
(3608, 761, 'DGDV4', 'GOPAY DRIVER', 'Go Pay Driver 50.000', 50950, 50650, 800, 'Normal', 'DPEDIA', 'SALGO', 'KHUSUS DRIVER - Masukan no HP'),
(3609, 762, 'DGDV5', 'GOPAY DRIVER', 'Go Pay Driver 75.000', 75950, 75650, 800, 'Normal', 'DPEDIA', 'SALGO', 'KHUSUS DRIVER - Masukan no HP'),
(3610, 763, 'DGDV6', 'GOPAY DRIVER', 'Go Pay Driver 100.000', 100950, 100650, 800, 'Normal', 'DPEDIA', 'SALGO', 'KHUSUS DRIVER - Masukan no HP'),
(3611, 764, 'DGDV7', 'GOPAY DRIVER', 'Go Pay Driver 150.000', 151000, 150700, 800, 'Normal', 'DPEDIA', 'SALGO', 'KHUSUS DRIVER - Masukan no HP'),
(3612, 765, 'DGDV8', 'GOPAY DRIVER', 'Go Pay Driver 200.000', 201000, 200700, 800, 'Normal', 'DPEDIA', 'SALGO', 'KHUSUS DRIVER - Masukan no HP'),
(3613, 766, 'DGDV9', 'GOPAY DRIVER', 'Go Pay Driver 250.000', 251000, 250700, 800, 'Normal', 'DPEDIA', 'SALGO', 'KHUSUS DRIVER - Masukan no HP'),
(3614, 767, 'DGDV10', 'GOPAY DRIVER', 'Go Pay Driver 300.000', 301000, 300700, 800, 'Normal', 'DPEDIA', 'SALGO', 'KHUSUS DRIVER - Masukan no HP'),
(3615, 768, 'DGDV11', 'GOPAY DRIVER', 'Go Pay Driver 500.000', 501000, 500700, 800, 'Normal', 'DPEDIA', 'SALGO', 'KHUSUS DRIVER - Masukan no HP'),
(3616, 769, 'DXLD1', 'XL DATA', 'XL Data 2 GB / 30 Hari', 24430, 24130, 800, 'Normal', 'DPEDIA', 'PKIN', 'XL Data 2GB 30 Hari'),
(3617, 770, 'DXLD2', 'XL DATA', 'XL Data 3GB 30 Hari', 39120, 38820, 800, 'Normal', 'DPEDIA', 'PKIN', 'XL Data 3GB 30 Hari'),
(3618, 771, 'DXLD3', 'XL DATA', 'XL Data 4GB 30 Hari', 51220, 50920, 800, 'Normal', 'DPEDIA', 'PKIN', 'XL Data 4GB 30 Hari'),
(3619, 772, 'DXLD5', 'XL DATA', 'XL Data 6GB 30 Hari', 88750, 88450, 800, 'Normal', 'DPEDIA', 'PKIN', 'XL Data 6GB 30 Hari'),
(3620, 773, 'DXLD4', 'XL DATA', 'XL Data 5GB 30 Hari', 61620, 61320, 800, 'Normal', 'DPEDIA', 'PKIN', 'XL Data 5GB 30 Hari'),
(3621, 774, 'DXLD6', 'XL DATA', 'XL Data 8GB 30 Hari', 88849, 88549, 800, 'Normal', 'DPEDIA', 'PKIN', 'XL Data 8GB 30 Hari'),
(3622, 775, 'DIIU36', 'DATA UNLI', 'Indosat Internet Unlimited + 36GB, 12 bulan', 441868, 441568, 800, 'Normal', 'DPEDIA', 'PKIN', 'Kuota Utama 3GB/bulan(3G/4G) 24 Jam + Unlimited Aplikasi sehari-hari + Unlimited Streaming Youtube, Spotify, dan iFlix + Unlimited SMS ke IM3 Ooredoo Masa Aktif 12 bulan'),
(3623, 776, 'DVPB5', 'VOUCHER POINT BLANK', 'Voucher 60000 PB Cash', 501450, 501150, 800, 'Normal', 'DPEDIA', 'VGAME', 'Voucher 60000 PB Cash'),
(3624, 777, 'DVPB4', 'VOUCHER POINT BLANK', 'Voucher 12000 PB Cash', 91775, 91475, 800, 'Normal', 'DPEDIA', 'VGAME', 'Voucher 12000 PB Cash'),
(3625, 778, 'DVPB3', 'VOUCHER POINT BLANK', 'Voucher 6000 PB Cash', 46325, 46025, 800, 'Normal', 'DPEDIA', 'VGAME', 'Voucher 6000 PB Cash'),
(3626, 779, 'DVPB2', 'VOUCHER POINT BLANK', 'Voucher 2400 PB Cash', 19040, 18740, 800, 'Normal', 'DPEDIA', 'VGAME', 'Voucher 2400 PB Cash'),
(3627, 780, 'DVPB1', 'VOUCHER POINT BLANK', 'Voucher 1200 PB Cash', 9945, 9645, 800, 'Normal', 'DPEDIA', 'VGAME', 'Voucher 1200 PB Cash'),
(3628, 781, 'DVPB6', 'VOUCHER POINT BLANK', 'Voucher 36000 PB Cash', 301450, 301150, 800, 'Normal', 'DPEDIA', 'VGAME', 'Voucher 36000 PB Cash'),
(3629, 782, 'DTH1000', 'TRI', 'Three 1.000.000', 994650, 994350, 800, 'Normal', 'DPEDIA', 'PULSA', 'Pulsa Three Rp 1.000.000'),
(3630, 783, 'DFF20', 'FREE FIRE', 'Free Fire 20 Diamond', 3845, 3545, 800, 'Normal', 'DPEDIA', 'VGAME', 'Free Fire 20 Diamond'),
(3631, 784, 'DFF55', 'FREE FIRE', 'Free Fire 55 Diamond', 8915, 8615, 800, 'Normal', 'DPEDIA', 'VGAME', 'Free Fire 55 Diamond'),
(3632, 785, 'DFF95', 'FREE FIRE', 'Free Fire 95 Diamond', 13975, 13675, 800, 'Normal', 'DPEDIA', 'VGAME', 'Free Fire 95 Diamond'),
(3633, 786, 'DFF210', 'FREE FIRE', 'Free Fire 210 Diamond', 28060, 27760, 800, 'Normal', 'DPEDIA', 'VGAME', 'Free Fire 210 Diamond'),
(3634, 787, 'DFF925', 'FREE FIRE', 'Free Fire 925 Diamond', 130875, 130575, 800, 'Normal', 'DPEDIA', 'VGAME', 'Free Fire 925 Diamond'),
(3635, 788, 'DFF510', 'FREE FIRE', 'Free Fire 510 Diamond', 66775, 66475, 800, 'Normal', 'DPEDIA', 'VGAME', 'Free Fire 510 Diamond'),
(3636, 789, 'DFF1875', 'FREE FIRE', 'Free Fire 1875 Diamond', 259875, 259575, 800, 'Normal', 'DPEDIA', 'VGAME', 'Free Fire 1875 Diamond'),
(3637, 790, 'DFF1975', 'FREE FIRE', 'Free Fire 1975 Diamond', 260955, 260655, 800, 'Normal', 'DPEDIA', 'VGAME', 'Free Fire 1975 Diamond'),
(3638, 791, 'DFF2000', 'FREE FIRE', 'Free Fire 2000 Diamond', 246575, 246275, 800, 'Normal', 'DPEDIA', 'VGAME', 'Free Fire 2000 Diamond'),
(3639, 792, 'DFF3310', 'FREE FIRE', 'Free Fire 3310 Diamond', 450875, 450575, 800, 'Normal', 'DPEDIA', 'VGAME', 'Free Fire 3310 Diamond'),
(3640, 793, 'DFM30', 'FREE FIRE', 'Free Fire Member[60 Diamonds X 30Hr]', 110450, 110150, 800, 'Normal', 'DPEDIA', 'VGAME', '60 diamond perhari Setiap login selama 30 hari (Wajib Login untuk mendapatkan diamond)'),
(3641, 794, 'DFM7', 'FREE FIRE', 'Free Fire Member[60 Diamonds X 7Hr]', 28275, 27975, 800, 'Normal', 'DPEDIA', 'VGAME', '60 diamond perhari Setiap login selama 7 hari (Wajib Login untuk mendapatkan diamond)'),
(3642, 795, 'DSMPLUS', 'MOBILE LEGEND', 'Startlight Member Plus', 280850, 280550, 800, 'Normal', 'DPEDIA', 'VGAME', 'Startlight Member Plus'),
(3643, 796, 'DTWPLUS', 'MOBILE LEGEND', 'Twilight Pass', 143975, 143675, 800, 'Normal', 'DPEDIA', 'VGAME', 'Twilight Pass - Skin Miya Suzuhime'),
(3644, 797, 'DMSML5408', 'MOBILE LEGEND', 'MOBILELEGEND - 5408 Diamond + Starlight Member', 1427670, 1427370, 800, 'Normal', 'DPEDIA', 'VGAME', 'MOBILELEGEND - 5408 Diamond + Starlight Member'),
(3645, 798, 'DMSML1411', 'MOBILE LEGEND', 'MOBILELEGEND - 1411 Diamond + Starlight Member', 502414, 502114, 800, 'Normal', 'DPEDIA', 'VGAME', 'MOBILELEGEND - 1411 Diamond + Starlight Member'),
(3646, 799, 'DMSML586', 'MOBILE LEGEND', 'MOBILELEGEND - 586 Diamond + Starlight Member', 302350, 302050, 800, 'Normal', 'DPEDIA', 'VGAME', 'MOBILELEGEND - 586 Diamond + Starlight Member'),
(3647, 800, 'DMSML193', 'MOBILE LEGEND', 'MOBILELEGEND - 193 Diamond + Starlight Member', 214420, 214120, 800, 'Normal', 'DPEDIA', 'VGAME', 'MOBILELEGEND - 193 Diamond + Starlight Member'),
(3648, 801, 'DMSML4', 'MOBILE LEGEND', 'MOBILELEGEND - 4 Diamond + Starlight Member', 151652, 151352, 800, 'Normal', 'DPEDIA', 'VGAME', 'MOBILELEGEND - 4 Diamond + Starlight Member'),
(3649, 802, 'DML3', 'MOBILE LEGEND', 'MOBILELEGEND - 3 Diamond', 2385, 2085, 800, 'Normal', 'DPEDIA', 'VGAME', 'no pelanggan = gabungan antara user_id dan zone_id'),
(3650, 803, 'DML67', 'MOBILE LEGEND', 'MOBILELEGEND - 67 Diamond', 21975, 21675, 800, 'Normal', 'DPEDIA', 'VGAME', 'no pelanggan = gabungan antara user_id dan zone_id'),
(3651, 804, 'DVTEL', 'AXISTEL', 'Voucher Telepon Axis Sepuasnya 14 Hari', 1900, 1600, 800, 'Normal', 'DPEDIA', 'VOUCHER', '-'),
(3652, 805, 'DAIGO7', 'AXIS AIGO', 'AIGO 50GB 24 JAM 60HR (Voucher)', 136420, 136120, 800, 'Normal', 'DPEDIA', 'VOUCHER', '50 GB All Jaringan Berlaku 24 Jam Masa Aktif 60 Hari. Setelah mendapat SN, ketik *838*kodevoucher# untuk mengaktifkan paket'),
(3653, 806, 'DVYEL1', 'INDOSAT', 'Voucher Indosat Yellow 1GB 15Hr', 10905, 10605, 800, 'Normal', 'DPEDIA', 'VOUCHER', 'Voucher Indosat Yellow Kuota utama 1GB aktif 7 hari'),
(3654, 807, 'DGD50', 'GRAB DRIVER', 'Voucher Grab driver 50.000', 50625, 50325, 800, 'Normal', 'DPEDIA', 'VOUCHER', '-'),
(3655, 808, 'DGD100', 'GRAB DRIVER', 'Voucher Grab driver 100.000', 100550, 100250, 800, 'Normal', 'DPEDIA', 'VOUCHER', '-'),
(3656, 809, 'DVYEL2', 'INDOSAT', 'Voucher Indosat Yellow 1GB 15Hr', 15080, 14780, 800, 'Normal', 'DPEDIA', 'VOUCHER', 'Voucher Indosat Yellow Kuota utama 1GB aktif 15 hari'),
(3657, 810, 'DHH225', 'HAGO', 'Hago 225 Diamonds', 75875, 75575, 800, 'Normal', 'DPEDIA', 'VGAME', 'Masukkan Play ID Hago Anda'),
(3658, 811, 'DHH375', 'HAGO', 'Hago 375 Diamonds', 125875, 125575, 800, 'Normal', 'DPEDIA', 'VGAME', 'Masukkan Play ID Hago Anda'),
(3659, 812, 'DHH900', 'HAGO', 'Hago 900 Diamonds', 300875, 300575, 800, 'Normal', 'DPEDIA', 'VGAME', 'Masukkan Play ID Hago Anda'),
(3660, 813, 'DHH1650', 'HAGO', 'Hago 1650 Diamonds', 550875, 550575, 800, 'Normal', 'DPEDIA', 'VGAME', 'Masukkan Play ID Hago Anda'),
(3661, 814, 'DHH3300', 'HAGO', 'Hago 3300 Diamonds', 1100875, 1100575, 800, 'Normal', 'DPEDIA', 'VGAME', 'Masukkan Play ID Hago Anda'),
(3662, 815, 'DHH90', 'HAGO', 'Hago 90 Diamonds', 30875, 30575, 800, 'Normal', 'DPEDIA', 'VGAME', 'Masukkan Play ID Hago Anda'),
(3663, 816, 'DHH6', 'HAGO', 'Hago 6 Diamonds', 3355, 3055, 800, 'Normal', 'DPEDIA', 'VGAME', 'Masukkan Play ID Hago Anda'),
(3664, 817, 'DHH45', 'HAGO', 'Hago 45 Diamonds', 15860, 15560, 800, 'Normal', 'DPEDIA', 'VGAME', 'Masukkan Play ID Hago Anda'),
(3665, 818, 'DTSEL15', 'TELKOMSEL', 'Telkomsel 15.000', 15705, 15405, 800, 'Normal', 'DPEDIA', 'PULSA', 'Pulsa Telkomsel Rp 15.000'),
(3666, 819, 'DSMART1', 'VOC SMART', 'Voucher Smartfren Berbasis Volume 10.000', 10649, 10349, 800, 'Normal', 'DPEDIA', 'VOUCHER', '1,25GB 24 jam + 1,75GB malam + 1GB chat + gratis nelpon sesama, 7 Hari'),
(3667, 820, 'DSMART2', 'VOC SMART', 'Smartfren Voucher Data 5GB UTAMA + 5GB MALAM, 30 HARI', 32375, 32075, 800, 'Normal', 'DPEDIA', 'VOUCHER', 'Voucher 5 GB 24 jam, 5 GB malam, 30 Hari'),
(3668, 821, 'DSMART3', 'VOC SMART', 'Smartfren Voucher Data (8GB UTAMA + 8GB MALAM, 30 HARI)', 49875, 49575, 800, 'Normal', 'DPEDIA', 'VOUCHER', '-'),
(3669, 822, 'DSMART4', 'VOC SMART', 'Smartfren Voucher Data (15GB UTAMA + 15GB MALAM, 30 HARI)', 67975, 67675, 800, 'Normal', 'DPEDIA', 'VOUCHER', '-'),
(3670, 823, 'DSMART5', 'VOC SMART', 'Smartfren Voucher Data (30GB UTAMA + 30GB MALAM, 30 HARI)', 99375, 99075, 800, 'Normal', 'DPEDIA', 'VOUCHER', '-'),
(3671, 824, 'DSMART6', 'VOC SMART', 'Voucher Smartfren Unlimited 80.000', 78025, 77725, 800, 'Normal', 'DPEDIA', 'VOUCHER', 'Voucher Batas pemakaian wajar 1GB/hari, Unlimited 24 Jam, Gratis Nelpon ke sesama smartfren, Berlaku 28 hari'),
(3672, 825, 'DSMART7', 'VOC SMART', 'Voucher Smartfren Unlimited 55.000', 59350, 59050, 800, 'Normal', 'DPEDIA', 'VOUCHER', 'Voucher Batas pemakaian wajar 500MB/hari, Unlimited 24 Jam, Gratis Nelpon ke sesama smartfren, Berlaku 28 hari'),
(3673, 826, 'DSMART8', 'VOC SMART', 'Voucher Smartfren Unlimited 20.000', 20160, 19860, 800, 'Normal', 'DPEDIA', 'VOUCHER', 'Voucher Batas pemakaian wajar 1GB/hari, Unlimited 24 Jam, Gratis Nelpon ke sesama smartfren, Berlaku 7 hari'),
(3674, 827, 'DFF100', 'FREE FIRE', 'Free Fire 100 Diamond', 14125, 13825, 800, 'Normal', 'DPEDIA', 'VGAME', 'Free Fire 100 Diamond'),
(3675, 828, 'DGR4', 'GARENA', 'Voucher Garena 330 Shell', 92050, 91750, 800, 'Normal', 'DPEDIA', 'VGAME', '-'),
(3676, 829, 'DGR1', 'GARENA', 'Voucher Garena 33 Shell', 9949, 9649, 800, 'Normal', 'DPEDIA', 'VGAME', '-'),
(3677, 830, 'DGR2', 'GARENA', 'Voucher Garena 66 Shell', 19049, 18749, 800, 'Normal', 'DPEDIA', 'VGAME', '-'),
(3678, 831, 'DGR3', 'GARENA', 'Voucher Garena 165 Shell', 46299, 45999, 800, 'Normal', 'DPEDIA', 'VGAME', '-'),
(3679, 832, 'DTSEL2', 'TELKOMSEL', 'Telkomsel 2.000', 3435, 3135, 800, 'Normal', 'DPEDIA', 'PULSA', 'Pulsa Telkomsel Rp 2.000'),
(3680, 833, 'DTSEL3', 'TELKOMSEL', 'Telkomsel 3.000', 4435, 4135, 800, 'Normal', 'DPEDIA', 'PULSA', 'Pulsa Telkomsel Rp 3.000'),
(3681, 834, 'DTSEL4', 'TELKOMSEL', 'Telkomsel 4.000', 5550, 5250, 800, 'Normal', 'DPEDIA', 'PULSA', 'Pulsa Telkomsel Rp 4.000'),
(3682, 835, 'DXTP1', 'AXISTEL', 'Axis Telepon 30 Menit Semua Op / 7 Hari', 9450, 9150, 800, 'Normal', 'DPEDIA', 'PKSMS', 'Axis Telepon 30 Menit ke Semua Operator / 7 Hari'),
(3683, 836, 'DXTP2', 'AXISTEL', 'Axis Telepon 100 Menit Semua Op / 30 Hari', 25600, 25300, 800, 'Normal', 'DPEDIA', 'PKSMS', 'Axis Telepon 100 Menit ke Semua Operator / 30 Hari'),
(3684, 837, 'DXTP3', 'AXISTEL', 'Axis Telepon Sepuasnya Sesama / 14 Hari', 3875, 3575, 800, 'Normal', 'DPEDIA', 'PKSMS', 'Axis Telepon Sepuasnya ke Sesama / 14 Hari'),
(3685, 838, 'DAIGO15', 'AXIS AIGO', 'Voucher AIGO 15 GB / 30 Hari', 78437, 78137, 800, 'Normal', 'DPEDIA', 'VOUCHER', '15 GB All Jaringan Berlaku 24 Jam Masa Aktif 30 Hari. Setelah mendapat SN, ketik *838*kodevoucher# untuk mengaktifkan paket'),
(3686, 839, 'DAIGO12', 'AXIS AIGO', 'Voucher AIGO 12 GB / 30 Hari', 68495, 68195, 800, 'Normal', 'DPEDIA', 'VOUCHER', '12 GB All Jaringan Berlaku 24 Jam Masa Aktif 30 Hari. Setelah mendapat SN, ketik *838*kodevoucher# untuk mengaktifkan paket'),
(3687, 840, 'DCO26', 'Call of Duty MOBILE', 'COD 26', 5700, 5400, 800, 'Normal', 'DPEDIA', 'VGAME', 'Call of Duty Mobile 26CP'),
(3688, 841, 'DCO53', 'Call of Duty MOBILE', 'COD 53', 10500, 10200, 800, 'Normal', 'DPEDIA', 'VGAME', 'Call of Duty Mobile 53CP'),
(3689, 842, 'DCO106', 'Call of Duty MOBILE', 'COD 106', 20050, 19750, 800, 'Normal', 'DPEDIA', 'VGAME', 'Call of Duty Mobile 106CP'),
(3690, 843, 'DCO264', 'Call of Duty MOBILE', 'COD 264', 48575, 48275, 800, 'Normal', 'DPEDIA', 'VGAME', 'Call of Duty Mobile 264CP'),
(3691, 844, 'DCO528', 'Call of Duty MOBILE', 'COD 528', 96100, 95800, 800, 'Normal', 'DPEDIA', 'VGAME', 'Call of Duty Mobile 528CP'),
(3692, 845, 'DCO1056', 'Call of Duty MOBILE', 'COD 1056', 194850, 194550, 800, 'Normal', 'DPEDIA', 'VGAME', 'Call of Duty Mobile 1056CP'),
(3693, 846, 'DCO1584', 'Call of Duty MOBILE', 'COD 1584', 291850, 291550, 800, 'Normal', 'DPEDIA', 'VGAME', 'Call of Duty Mobile 1584'),
(3694, 847, 'DCO2640', 'Call of Duty MOBILE', 'COD 2640', 485850, 485550, 800, 'Normal', 'DPEDIA', 'VGAME', 'Call of Duty Mobile 2640CP'),
(3695, 848, 'DCO5280', 'Call of Duty MOBILE', 'COD 5280', 952450, 952150, 800, 'Normal', 'DPEDIA', 'VGAME', 'Call of Duty Mobile 5280CP'),
(3696, 849, 'DCO10560', 'Call of Duty MOBILE', 'COD 10560', 1940850, 1940550, 800, 'Normal', 'DPEDIA', 'VGAME', 'Call of Duty Mobile 10560CP'),
(3697, 850, 'DCO26400', 'Call of Duty MOBILE', 'COD 26400', 4850850, 4850550, 800, 'Normal', 'DPEDIA', 'VGAME', 'Call of Duty Mobile 26400CP'),
(3698, 851, 'DCO52800', 'Call of Duty MOBILE', 'COD 52800', 9700850, 9700550, 800, 'Normal', 'DPEDIA', 'VGAME', 'Call of Duty Mobile 52800CP'),
(3699, 852, 'DVTSEL', 'TSEL DATA', 'Voucher Telkomsel Data 1.5 GB 3 Hari', 9355, 9055, 800, 'Normal', 'DPEDIA', 'VOUCHER', '1,5 GB, 3 Hari. perhatikan zona dan regional.'),
(3700, 853, 'DFFC6', 'FREE COMBO', 'Indosat Freedom Combo 50 GB / 30 Hari', 143875, 143575, 800, 'Normal', 'DPEDIA', 'PKIN', '50GB (25GB + 15GB Lokal, 10GB Mlm, Telepon 60 Mnt All 30hr) Paket hanya berlaku di area Jakarta, Botabek, West Java, Central Java, East Java, Kalimantan.'),
(3701, 854, 'DXLTRA1', 'XTRA COMBO', 'XL XTRA KUOTA 5 GB IFLIX', 10855, 10555, 800, 'Normal', 'DPEDIA', 'PKIN', '5 GB IFLIX 30 Hari'),
(3702, 855, 'DXLTRA9', 'XTRA COMBO', 'XL XTRA KUOTA 30 GB - 30 HARI', 12750, 12450, 800, 'Normal', 'DPEDIA', 'PKIN', '1GB -> Youtube, Instagram, Whatsapp, Facebook, Line, BBM | 14GB IFlix | 15GB -> Internet malam (01.00 - 06.00)'),
(3703, 856, 'DON2', 'XTRA COMBO', 'XL XTRA ON 1GB', 13810, 13510, 800, 'Normal', 'DPEDIA', 'PKIN', 'Paket anti hangus, berlaku akumulasi, masa aktif paket unlimited. Bisa menambah masa aktif kartu 14 hari. Semua jaringan.'),
(3704, 857, 'DON1', 'XTRA COMBO', 'XL XTRA ON 2GB', 22610, 22310, 800, 'Normal', 'DPEDIA', 'PKIN', 'Paket anti hangus, berlaku akumulasi, masa aktif paket unlimited. Bisa menambah masa aktif kartu 14 hari. Semua jaringan.'),
(3705, 858, 'DBRO14', 'AXIS', 'BRONET 16 GB / 60 hari', 111315, 111015, 800, 'Normal', 'DPEDIA', 'PKIN', '16GB Kuota utama bisa digunakan selama 24 jam, berlaku 60hari.'),
(3706, 859, 'DTMNI2', 'TRI', 'Tri Data Mini 1.5 GB / 5 Hari', 12007, 11707, 800, 'Normal', 'DPEDIA', 'PKIN', '1.5 GB Nasional, Pulsa 2K, 5 Hari'),
(3707, 860, 'DXTRCV1', 'XL', 'XL Xtra Combo VIP 5 GB + 10 GB / 30 Hari', 62825, 62525, 800, 'Normal', 'DPEDIA', 'PKIN', 'Kuota Utama (2G/3G/4G) 5GB + Kuota YouTube 10GB + YouTube Tanpa Kuota Tanpa Batas 01:00-06:00 WIB + Nelpon ke Semua Operator 20Menit'),
(3708, 861, 'DXTRCV4', 'XL', 'XL Xtra Combo VIP 20 GB + 40 GB / 30 Hari', 166625, 166325, 800, 'Normal', 'DPEDIA', 'PKIN', 'Kuota Utama (2G/3G/4G) 20GB + Kuota YouTube 40GB + YouTube Tanpa Kuota Tanpa Batas 01:00-06:00 WIB + Nelpon ke Semua Operator 60Menit'),
(3709, 862, 'DISK1', 'i.saku', 'i.saku 10.000', 12200, 11900, 800, 'Normal', 'DPEDIA', 'SALGO', 'saldo i.saku 10.000'),
(3710, 863, 'DISK2', 'i.saku', 'i.saku 11.000', 12435, 12135, 800, 'Normal', 'DPEDIA', 'SALGO', 'saldo i.saku 11.000'),
(3711, 864, 'DISK3', 'i.saku', 'i.saku 12.000', 13435, 13135, 800, 'Normal', 'DPEDIA', 'SALGO', 'saldo i.saku 12.000'),
(3712, 865, 'DISK4', 'i.saku', 'i.saku 13.000', 14435, 14135, 800, 'Normal', 'DPEDIA', 'SALGO', 'saldo i.saku 13.000'),
(3713, 866, 'DISK5', 'i.saku', 'i.saku 14.000', 15435, 15135, 800, 'Normal', 'DPEDIA', 'SALGO', 'saldo i.saku 14.000'),
(3714, 867, 'DISK6', 'i.saku', 'i.saku 15.000', 16435, 16135, 800, 'Normal', 'DPEDIA', 'SALGO', 'saldo i.saku 15.000'),
(3715, 868, 'DISK7', 'i.saku', 'i.saku 20.000', 22200, 21900, 800, 'Normal', 'DPEDIA', 'SALGO', 'saldo i.saku 20.000'),
(3716, 869, 'DISK9', 'i.saku', 'i.saku 30.000', 32200, 31900, 800, 'Normal', 'DPEDIA', 'SALGO', 'saldo i.saku 30.000'),
(3717, 870, 'DISK8', 'i.saku', 'i.saku 25.000', 26450, 26150, 800, 'Normal', 'DPEDIA', 'SALGO', 'saldo i.saku 25.000'),
(3718, 871, 'DISK10', 'i.saku', 'i.saku 35.000', 37150, 36850, 800, 'Normal', 'DPEDIA', 'SALGO', 'saldo i.saku 35.000'),
(3719, 872, 'DISK11', 'i.saku', 'i.saku 40.000', 42200, 41900, 800, 'Normal', 'DPEDIA', 'SALGO', 'saldo i.saku 40.000'),
(3720, 873, 'DISK14', 'i.saku', 'i.saku 55.000', 57150, 56850, 800, 'Normal', 'DPEDIA', 'SALGO', 'saldo i.saku 55.000'),
(3721, 874, 'DISK13', 'i.saku', 'i.saku 50.000', 52200, 51900, 800, 'Normal', 'DPEDIA', 'SALGO', 'saldo i.saku 50.000'),
(3722, 875, 'DISK15', 'i.saku', 'i.saku 60.000', 62200, 61900, 800, 'Normal', 'DPEDIA', 'SALGO', 'saldo i.saku 60.000'),
(3723, 876, 'DISK16', 'i.saku', 'i.saku 65.000', 67150, 66850, 800, 'Normal', 'DPEDIA', 'SALGO', 'i.saku 65.000'),
(3724, 877, 'DISK17', 'i.saku', 'i.saku 70.000', 72200, 71900, 800, 'Normal', 'DPEDIA', 'SALGO', 'saldo i.saku 70.000'),
(3725, 878, 'DISK19', 'i.saku', 'i.saku 80.000', 82200, 81900, 800, 'Normal', 'DPEDIA', 'SALGO', 'saldo i.saku 80.000'),
(3726, 879, 'DISK18', 'i.saku', 'i.saku 75.000', 77150, 76850, 800, 'Normal', 'DPEDIA', 'SALGO', 'saldo i.saku 75.000'),
(3727, 880, 'DISK20', 'i.saku', 'i.saku 85.000', 86980, 86680, 800, 'Normal', 'DPEDIA', 'SALGO', 'saldo i.saku 85.000'),
(3728, 881, 'DISK21', 'i.saku', 'i.saku 90.000', 92150, 91850, 800, 'Normal', 'DPEDIA', 'SALGO', 'saldo i.saku 90.000'),
(3729, 882, 'DISK22', 'i.saku', 'i.saku 95.000', 97150, 96850, 800, 'Normal', 'DPEDIA', 'SALGO', 'saldo i.saku 95.000'),
(3730, 883, 'DISK23', 'i.saku', 'i.saku 100.000', 102200, 101900, 800, 'Normal', 'DPEDIA', 'SALGO', 'saldo i.saku 100.000'),
(3731, 884, 'DISK25', 'i.saku', 'i.saku 200.000', 202150, 201850, 800, 'Normal', 'DPEDIA', 'SALGO', 'saldo i.saku 200.000'),
(3732, 885, 'DISK24', 'i.saku', 'i.saku 150.000', 152150, 151850, 800, 'Normal', 'DPEDIA', 'SALGO', 'saldo i.saku 150.000'),
(3733, 886, 'DISK26', 'i.saku', 'i.saku 250.000', 252150, 251850, 800, 'Normal', 'DPEDIA', 'SALGO', 'saldo i.saku 250.000'),
(3734, 887, 'DISK27', 'i.saku', 'i.saku 300.000', 302150, 301850, 800, 'Normal', 'DPEDIA', 'SALGO', 'saldo i.saku 300.000'),
(3735, 888, 'DISK28', 'i.saku', 'i.saku 350.000', 352150, 351850, 800, 'Normal', 'DPEDIA', 'SALGO', 'saldo i.saku 350.000'),
(3736, 889, 'DISK29', 'i.saku', 'i.saku 400.000', 402150, 401850, 800, 'Normal', 'DPEDIA', 'SALGO', 'saldo i.saku 400.000'),
(3737, 890, 'DISK30', 'i.saku', 'i.saku 450.000', 452150, 451850, 800, 'Normal', 'DPEDIA', 'SALGO', 'saldo i.saku 450.000'),
(3738, 891, 'DISK31', 'i.saku', 'i.saku 500.000', 502150, 501850, 800, 'Normal', 'DPEDIA', 'SALGO', 'saldo i.saku 500.000'),
(3739, 892, 'DSK1', 'Sakuku', 'Sakuku 10.000', 11125, 10825, 800, 'Normal', 'DPEDIA', 'SALGO', 'masukkan nomor hp terdaftar pada Sakuku'),
(3740, 893, 'DSK2', 'Sakuku', 'Sakuku 15.000', 16160, 15860, 800, 'Normal', 'DPEDIA', 'SALGO', 'masukkan nomor hp terdaftar pada Sakuku'),
(3741, 894, 'DSK3', 'Sakuku', 'Sakuku 20.000', 21125, 20825, 800, 'Normal', 'DPEDIA', 'SALGO', 'masukkan nomor hp terdaftar pada Sakuku'),
(3742, 895, 'DSK4', 'Sakuku', 'Sakuku 25.000', 26160, 25860, 800, 'Normal', 'DPEDIA', 'SALGO', 'masukkan nomor hp terdaftar pada Sakuku'),
(3743, 896, 'DSK5', 'Sakuku', 'Sakuku 30.000', 31125, 30825, 800, 'Normal', 'DPEDIA', 'SALGO', 'masukkan nomor hp terdaftar pada Sakuku'),
(3744, 897, 'DSK6', 'Sakuku', 'Sakuku 35.000', 36175, 35875, 800, 'Normal', 'DPEDIA', 'SALGO', 'masukkan nomor hp terdaftar pada Sakuku'),
(3745, 898, 'DSK7', 'Sakuku', 'Sakuku 45.000', 46175, 45875, 800, 'Normal', 'DPEDIA', 'SALGO', 'masukkan nomor hp terdaftar pada Sakuku'),
(3746, 899, 'DSK8', 'Sakuku', 'Sakuku 50.000', 51125, 50825, 800, 'Normal', 'DPEDIA', 'SALGO', 'masukkan nomor hp terdaftar pada Sakuku'),
(3747, 900, 'DSK9', 'Sakuku', 'Sakuku 55.000', 56275, 55975, 800, 'Normal', 'DPEDIA', 'SALGO', 'masukkan nomor hp terdaftar pada Sakuku'),
(3748, 901, 'DSK10', 'Sakuku', 'Sakuku 60.000', 61125, 60825, 800, 'Normal', 'DPEDIA', 'SALGO', 'masukkan nomor hp terdaftar pada Sakuku'),
(3749, 902, 'DSK11', 'Sakuku', 'Sakuku 65.000', 66275, 65975, 800, 'Normal', 'DPEDIA', 'SALGO', 'masukkan nomor hp terdaftar pada Sakuku'),
(3750, 903, 'DSK12', 'Sakuku', 'Sakuku 70.000', 71125, 70825, 800, 'Normal', 'DPEDIA', 'SALGO', 'masukkan nomor hp terdaftar pada Sakuku'),
(3751, 904, 'DSK13', 'Sakuku', 'Sakuku 75.000', 76275, 75975, 800, 'Normal', 'DPEDIA', 'SALGO', 'masukkan nomor hp terdaftar pada Sakuku'),
(3752, 905, 'DSK14', 'Sakuku', 'Sakuku 80.000', 81125, 80825, 800, 'Normal', 'DPEDIA', 'SALGO', 'masukkan nomor hp terdaftar pada Sakuku'),
(3753, 906, 'DSK15', 'Sakuku', 'Sakuku 85.000', 86275, 85975, 800, 'Normal', 'DPEDIA', 'SALGO', 'masukkan nomor hp terdaftar pada Sakuku'),
(3754, 907, 'DSK17', 'Sakuku', 'Sakuku 95.000', 96275, 95975, 800, 'Normal', 'DPEDIA', 'SALGO', 'masukkan nomor hp terdaftar pada Sakuku'),
(3755, 908, 'DSK16', 'Sakuku', 'Sakuku 90.000', 91125, 90825, 800, 'Normal', 'DPEDIA', 'SALGO', 'masukkan nomor hp terdaftar pada Sakuku'),
(3756, 909, 'DSK18', 'Sakuku', 'Sakuku 100.000', 101125, 100825, 800, 'Normal', 'DPEDIA', 'SALGO', 'masukkan nomor hp terdaftar pada Sakuku'),
(3757, 910, 'DSK19', 'Sakuku', 'Sakuku 150.000', 151375, 151075, 800, 'Normal', 'DPEDIA', 'SALGO', 'masukkan nomor hp terdaftar pada Sakuku'),
(3758, 911, 'DSK20', 'Sakuku', 'Sakuku 200.000', 201375, 201075, 800, 'Normal', 'DPEDIA', 'SALGO', 'masukkan nomor hp terdaftar pada Sakuku'),
(3759, 912, 'DSK21', 'Sakuku', 'Sakuku 300.000', 301375, 301075, 800, 'Normal', 'DPEDIA', 'SALGO', 'masukkan nomor hp terdaftar pada Sakuku'),
(3760, 913, 'DSK22', 'Sakuku', 'Sakuku 400.000', 401575, 401275, 800, 'Normal', 'DPEDIA', 'SALGO', 'masukkan nomor hp terdaftar pada Sakuku'),
(3761, 914, 'DSK23', 'Sakuku', 'Sakuku 500.000', 501575, 501275, 800, 'Normal', 'DPEDIA', 'SALGO', 'masukkan nomor hp terdaftar pada Sakuku'),
(3762, 915, 'DMS1', 'Mitra Shopee', 'Mitra Shopee 50.000', 51100, 50800, 800, 'Normal', 'DPEDIA', 'SALGO', 'masukkan nomor hp terdaftar pada Mitra Shopee'),
(3763, 916, 'DMS2', 'Mitra Shopee', 'Mitra Shopee 75.000', 76100, 75800, 800, 'Normal', 'DPEDIA', 'SALGO', 'masukkan nomor hp terdaftar pada Mitra Shopee'),
(3764, 917, 'DMS3', 'Mitra Shopee', 'Mitra Shopee 100.000', 101100, 100800, 800, 'Normal', 'DPEDIA', 'SALGO', 'masukkan nomor hp terdaftar pada Mitra Shopee'),
(3765, 918, 'DMS4', 'Mitra Shopee', 'Mitra Shopee 150.000', 151100, 150800, 800, 'Normal', 'DPEDIA', 'SALGO', 'masukkan nomor hp terdaftar pada Mitra Shopee'),
(3766, 919, 'DMS5', 'Mitra Shopee', 'Mitra Shopee 200.000', 201100, 200800, 800, 'Normal', 'DPEDIA', 'SALGO', 'masukkan nomor hp terdaftar pada Mitra Shopee'),
(3767, 920, 'DIDM1', 'Indomaret Card E-Money', 'Indomaret Card E-Money 100000', 101675, 101375, 800, 'Normal', 'DPEDIA', 'SALGO', '-'),
(3768, 921, 'DIDM2', 'Indomaret Card E-Money', 'Indomaret Card E-Money 200000', 201675, 201375, 800, 'Normal', 'DPEDIA', 'SALGO', '-'),
(3769, 922, 'DIDM3', 'Indomaret Card E-Money', 'Indomaret Card E-Money 300000', 301675, 301375, 800, 'Normal', 'DPEDIA', 'SALGO', '-'),
(3770, 923, 'DLA1', 'LinkAja', 'LinkAja Rp 10.000', 11000, 10700, 800, 'Normal', 'DPEDIA', 'SALGO', '-'),
(3771, 924, 'DLA3', 'LinkAja', 'LinkAja Rp 20.000', 21000, 20700, 800, 'Normal', 'DPEDIA', 'SALGO', '-'),
(3772, 925, 'DLA4', 'LinkAja', 'LinkAja Rp 25.000', 26000, 25700, 800, 'Normal', 'DPEDIA', 'SALGO', '-'),
(3773, 926, 'DLA5', 'LinkAja', 'LinkAja Rp 30.000', 31025, 30725, 800, 'Normal', 'DPEDIA', 'SALGO', '-'),
(3774, 927, 'DLA9', 'LinkAja', 'LinkAja Rp 50.000', 51025, 50725, 800, 'Normal', 'DPEDIA', 'SALGO', '-'),
(3775, 928, 'DLA7', 'LinkAja', 'LinkAja Rp 40.000', 41025, 40725, 800, 'Normal', 'DPEDIA', 'SALGO', '-'),
(3776, 929, 'DLA11', 'LinkAja', 'LinkAja Rp 60.000', 61025, 60725, 800, 'Normal', 'DPEDIA', 'SALGO', '-'),
(3777, 930, 'DLA13', 'LinkAja', 'LinkAja Rp 70.000', 71025, 70725, 800, 'Normal', 'DPEDIA', 'SALGO', '-'),
(3778, 931, 'DLA15', 'LinkAja', 'LinkAja Rp 80.000', 81025, 80725, 800, 'Normal', 'DPEDIA', 'SALGO', '-'),
(3779, 932, 'DLA17', 'LinkAja', 'LinkAja Rp 90.000', 91025, 90725, 800, 'Normal', 'DPEDIA', 'SALGO', '-'),
(3780, 933, 'DLA19', 'LinkAja', 'LinkAja Rp 100.000', 101025, 100725, 800, 'Normal', 'DPEDIA', 'SALGO', '-'),
(3781, 934, 'DLA150', 'LinkAja', 'LinkAja Rp 150.000', 151275, 150975, 800, 'Normal', 'DPEDIA', 'SALGO', '-'),
(3782, 935, 'DLA200', 'LinkAja', 'LinkAja Rp 200.000', 201075, 200775, 800, 'Normal', 'DPEDIA', 'SALGO', '-'),
(3783, 936, 'DLA250', 'LinkAja', 'LinkAja Rp 250.000', 251075, 250775, 800, 'Normal', 'DPEDIA', 'SALGO', '-'),
(3784, 937, 'DLA2', 'LinkAja', 'LinkAja Rp 15.000', 15885, 15585, 800, 'Normal', 'DPEDIA', 'SALGO', '-'),
(3785, 938, 'DLA20', 'LinkAja', 'LinkAja Rp 125.000', 126200, 125900, 800, 'Normal', 'DPEDIA', 'SALGO', '-'),
(3786, 939, 'DLA18', 'LinkAja', 'LinkAja Rp 95.000', 96025, 95725, 800, 'Normal', 'DPEDIA', 'SALGO', '-'),
(3787, 940, 'DLA16', 'LinkAja', 'LinkAja Rp 85.000', 86025, 85725, 800, 'Normal', 'DPEDIA', 'SALGO', '-'),
(3788, 941, 'DLA14', 'LinkAja', 'LinkAja Rp 75.000', 76025, 75725, 800, 'Normal', 'DPEDIA', 'SALGO', '-'),
(3789, 942, 'DLA12', 'LinkAja', 'LinkAja Rp 65.000', 66025, 65725, 800, 'Normal', 'DPEDIA', 'SALGO', '-'),
(3790, 943, 'DLA10', 'LinkAja', 'LinkAja Rp 55.000', 56025, 55725, 800, 'Normal', 'DPEDIA', 'SALGO', '-'),
(3791, 944, 'DLA8', 'LinkAja', 'LinkAja Rp 45.000', 46025, 45725, 800, 'Normal', 'DPEDIA', 'SALGO', '-'),
(3792, 945, 'DLA6', 'LinkAja', 'LinkAja Rp 35.000', 36025, 35725, 800, 'Normal', 'DPEDIA', 'SALGO', '-'),
(3793, 946, 'DLA21', 'LinkAja', 'LinkAja Rp 150.000', 151625, 151325, 800, 'Normal', 'DPEDIA', 'SALGO', '-'),
(3794, 947, 'DLA22', 'LinkAja', 'LinkAja Rp 200.000', 201575, 201275, 800, 'Normal', 'DPEDIA', 'SALGO', '-'),
(3795, 948, 'DLA23', 'LinkAja', 'LinkAja Rp 250.000', 251625, 251325, 800, 'Normal', 'DPEDIA', 'SALGO', '-'),
(3796, 949, 'DLA24', 'LinkAja', 'LinkAja Rp 300.000', 301575, 301275, 800, 'Normal', 'DPEDIA', 'SALGO', '-'),
(3797, 950, 'DLA25', 'LinkAja', 'LinkAja Rp 350.000', 351575, 351275, 800, 'Normal', 'DPEDIA', 'SALGO', '-'),
(3798, 951, 'DLA26', 'LinkAja', 'LinkAja Rp 400.000', 401575, 401275, 800, 'Normal', 'DPEDIA', 'SALGO', '-'),
(3799, 952, 'DLA27', 'LinkAja', 'LinkAja Rp 450.000', 451575, 451275, 800, 'Normal', 'DPEDIA', 'SALGO', '-'),
(3800, 953, 'DLA28', 'LinkAja', 'LinkAja Rp 500.000', 501150, 500850, 800, 'Normal', 'DPEDIA', 'SALGO', '-'),
(3801, 954, 'DLA29', 'LinkAja', 'LinkAja Rp 600.000', 600885, 600585, 800, 'Normal', 'DPEDIA', 'SALGO', '-'),
(3802, 955, 'DLA30', 'LinkAja', 'LinkAja Rp 700.000', 701350, 701050, 800, 'Normal', 'DPEDIA', 'SALGO', '-'),
(3803, 956, 'DLA31', 'LinkAja', 'LinkAja Rp 800.000', 801075, 800775, 800, 'Normal', 'DPEDIA', 'SALGO', '-'),
(3804, 957, 'DLA32', 'LinkAja', 'LinkAja Rp 900.000', 901075, 900775, 800, 'Normal', 'DPEDIA', 'SALGO', '-'),
(3805, 958, 'DLA33', 'LinkAja', 'LinkAja Rp 1.000.000', 1001175, 1000875, 800, 'Normal', 'DPEDIA', 'SALGO', '-'),
(3806, 959, 'DISK0', 'i.saku', 'i.saku 45.000', 47150, 46850, 800, 'Normal', 'DPEDIA', 'SALGO', 'saldo i.saku 45.000'),
(3807, 960, 'DTRFTSEL1', 'TELKOMSEL', 'Telkomsel Pulsa Transfer 100.000', 88645, 88345, 800, 'Normal', 'DPEDIA', 'PULSA', 'Pulsa Telkomsel Rp 100.000 ( Tidak menambah masa aktif)'),
(3808, 961, 'DSMC60', 'Sausage Man', 'Sausage Man 60 Candy', 14350, 14050, 800, 'Normal', 'DPEDIA', 'VGAME', 'Sausage Man 60 Candy'),
(3809, 962, 'DSMC180', 'Sausage Man', 'Sausage Man 180 Candy', 39850, 39550, 800, 'Normal', 'DPEDIA', 'VGAME', 'Sausage Man 180 Candy'),
(3810, 963, 'DSMC300', 'Sausage Man', 'Sausage Man 300 Candy', 68850, 68550, 800, 'Normal', 'DPEDIA', 'VGAME', 'Sausage Man 300 Candy'),
(3811, 964, 'DSMC1280', 'Sausage Man', 'Sausage Man 1280 Candy', 270850, 270550, 800, 'Normal', 'DPEDIA', 'VGAME', 'Sausage Man 1280 Candy'),
(3812, 965, 'DSMC680', 'Sausage Man', 'Sausage Man 680 Candy', 135850, 135550, 800, 'Normal', 'DPEDIA', 'VGAME', 'Sausage Man 680 Candy'),
(3813, 966, 'DSMC1980', 'Sausage Man', 'Sausage Man 1980 Candy', 404850, 404550, 800, 'Normal', 'DPEDIA', 'VGAME', 'Sausage Man 1980 Candy'),
(3814, 967, 'DSMC3280', 'Sausage Man', 'Sausage Man 3280 Candy', 699850, 699550, 800, 'Normal', 'DPEDIA', 'VGAME', 'Sausage Man 3280 Candy'),
(3815, 968, 'DSMC6480', 'Sausage Man', 'Sausage Man 6480 Candy', 1350850, 1350550, 800, 'Normal', 'DPEDIA', 'VGAME', 'Sausage Man 6480 Candy'),
(3816, 969, 'DCHIP1', 'Higgs Domino', 'Higgs Domino 1M Koin Emas-D', 1079, 779, 800, 'Normal', 'DPEDIA', 'VGAME', '-'),
(3817, 970, 'DCHIP2', 'Higgs Domino', 'Higgs Domino 10M Koin Emas-D', 2949, 2649, 800, 'Normal', 'DPEDIA', 'VGAME', '-'),
(3818, 971, 'DCHIP3', 'Higgs Domino', 'Higgs Domino 15M Koin Emas-D', 3550, 3250, 800, 'Normal', 'DPEDIA', 'VGAME', '-'),
(3819, 972, 'DCHIP4', 'Higgs Domino', 'Higgs Domino 30M Koin Emas-D', 5350, 5050, 800, 'Normal', 'DPEDIA', 'VGAME', '-'),
(3820, 973, 'DCHIP5', 'Higgs Domino', 'Higgs Domino 60M Koin Emas-D', 6849, 6549, 800, 'Normal', 'DPEDIA', 'VGAME', '-'),
(3821, 974, 'DCHIP7', 'Higgs Domino', 'Higgs Domino 120M Koin Emas-D', 12849, 12549, 800, 'Normal', 'DPEDIA', 'VGAME', '-'),
(3822, 975, 'DCHIP8', 'Higgs Domino', 'Higgs Domino 200M Koin Emas-D', 18999, 18699, 800, 'Normal', 'DPEDIA', 'VGAME', '-'),
(3823, 976, 'DCHIP6', 'Higgs Domino', 'Higgs Domino 100M Koin Emas-D', 14860, 14560, 800, 'Normal', 'DPEDIA', 'VGAME', '-'),
(3824, 977, 'DCHIP9', 'Higgs Domino', 'Higgs Domino 400M Koin Emas-D', 36049, 35749, 800, 'Normal', 'DPEDIA', 'VGAME', '-'),
(3825, 978, 'DCHIP10', 'Higgs Domino', 'Higgs Domino 600M Koin Emas-D', 53850, 53550, 800, 'Normal', 'DPEDIA', 'VGAME', '-'),
(3826, 979, 'DCHIP11', 'Higgs Domino', 'Higgs Domino 800M Koin Emas-D', 69750, 69450, 800, 'Normal', 'DPEDIA', 'VGAME', '-'),
(3827, 980, 'DCHIP13', 'Higgs Domino', 'Higgs Domino 2B Koin Emas-D', 132850, 132550, 800, 'Normal', 'DPEDIA', 'VGAME', '-'),
(3828, 981, 'DCHIP14', 'Higgs Domino', 'Higgs Domino 3B Koin Emas-D', 206350, 206050, 800, 'Normal', 'DPEDIA', 'VGAME', '-'),
(3829, 982, 'DCHIP15', 'Higgs Domino', 'Higgs Domino 4B Koin Emas-D', 265850, 265550, 800, 'Normal', 'DPEDIA', 'VGAME', '-'),
(3830, 983, 'DCHIP12', 'Higgs Domino', 'Higgs Domino 1B Koin Emas-D', 67799, 67499, 800, 'Normal', 'DPEDIA', 'VGAME', '-'),
(3831, 984, 'DGPU10', 'GOOGLE PLAY US REGION', 'Google Play 10$ USA REGION', 174975, 174675, 800, 'Normal', 'DPEDIA', 'VGAME', '-'),
(3832, 985, 'DGPU100', 'GOOGLE PLAY US REGION', 'Google Play 100$ USA REGION', 1350850, 1350550, 800, 'Normal', 'DPEDIA', 'VGAME', '-');

-- --------------------------------------------------------

--
-- Table structure for table `layanan_sosmed`
--

CREATE TABLE `layanan_sosmed` (
  `id` int(10) NOT NULL,
  `service_id` int(10) NOT NULL,
  `kategori` varchar(50) COLLATE utf8_swedish_ci NOT NULL,
  `layanan` text COLLATE utf8_swedish_ci NOT NULL,
  `catatan` text COLLATE utf8_swedish_ci NOT NULL,
  `min` int(10) NOT NULL,
  `max` int(10) NOT NULL,
  `harga` double NOT NULL,
  `harga_api` double NOT NULL,
  `profit` double NOT NULL,
  `status` enum('Aktif','Tidak Aktif') COLLATE utf8_swedish_ci NOT NULL,
  `provider_id` int(10) NOT NULL,
  `provider` varchar(50) COLLATE utf8_swedish_ci NOT NULL,
  `tipe` varchar(50) COLLATE utf8_swedish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_swedish_ci;

--
-- Dumping data for table `layanan_sosmed`
--

INSERT INTO `layanan_sosmed` (`id`, `service_id`, `kategori`, `layanan`, `catatan`, `min`, `max`, `harga`, `harga_api`, `profit`, `status`, `provider_id`, `provider`, `tipe`) VALUES
(980, 541, 'TES', 'TWS API', 'tes ', 1000, 1000, 10, 10, 10, 'Aktif', 541, 'FP', ''),
(2413, 1, 'Instagram Views', 'instagram view Auto 1 [INSTANT]', 'INSTANT', 1000, 3000000, 15500, 15500, 15000, 'Aktif', 29, 'MEDANPEDIA', 'Sosial Media'),
(2414, 2, 'Instagram Views', 'instagram view Versi 1 [ Unlimited ] [ Superfast ]', '[ Unlimited ] [ Superfast ]', 200, 10000000, 16500, 16500, 15000, 'Aktif', 32, 'MEDANPEDIA', 'Sosial Media'),
(2415, 3, 'SoundCloud', 'SoundCloud Plays [1.5M]', '  Real\r\n0-1 Hour Start!\r\n50K - 100K/Day\r\nMultiple of 100\r\nMinimum 100', 100, 1500000, 16000, 16000, 15000, 'Aktif', 101, 'MEDANPEDIA', 'Sosial Media'),
(2416, 4, 'SoundCloud', 'SoundCloud Plays [10M]', 'Real\r\n0-1 Hour Start!\r\n10K - 100K/Day\r\nMinimum 20', 20, 10000000, 16500, 16500, 15000, 'Aktif', 102, 'MEDANPEDIA', 'Sosial Media'),
(2417, 5, 'SoundCloud', 'Soundcloud - Likes ( S1 ) [ HQ ] ( INSTANT )', ' HQ Users, Non Drop. Order Will Be Start Instant.', 20, 40000, 35000, 35000, 15000, 'Aktif', 105, 'MEDANPEDIA', 'Sosial Media'),
(2418, 6, 'Telegram', 'Telegram Post Views [10K] [Last 5]', 'Views Will Be Added To Your Last 5 Posts\r\nReal\r\n0-1 Hour Start!\r\n24 Hours Delivery\r\nMinimum 100', 100, 10000, 30000, 30000, 15000, 'Aktif', 108, 'MEDANPEDIA', 'Sosial Media'),
(2419, 7, 'Google', 'Google Plus - Followers | Circles ( Max 5000)', 'Real\r\n0-1 Hour Start!\r\n6-24 Hours Finish!\r\nMinimum 100', 100, 5000, 135000, 135000, 15000, 'Aktif', 113, 'MEDANPEDIA', 'Sosial Media'),
(2420, 8, 'Google', 'Google Post +1 [ 2K ]', 'Real\r\n0-1 Hour Start!\r\n1-24 Hours Finish!\r\nMinimum 20', 20, 2000, 135000, 135000, 15000, 'Aktif', 114, 'MEDANPEDIA', 'Sosial Media'),
(2421, 9, 'Instagram Story Views', 'Instagram Story Views [20K] [LAST STORY ONLY]', 'Views On The Last Story Posted ONLY !\r\nUsername Only\r\n0-1 Hour Start!\r\nUltra Fast!\r\nMinimum 20', 20, 20000, 17800, 17800, 15000, 'Aktif', 115, 'MEDANPEDIA', 'Sosial Media'),
(2422, 10, 'Instagram Story Views', 'Instagram - Story Views S1', '[ Username Only ] INSTANT', 100, 1000000, 21000, 21000, 15000, 'Aktif', 116, 'MEDANPEDIA', 'Sosial Media'),
(2423, 11, 'Instagram Live Video', 'Instagram Live Video Likes ', 'Username Only\r\nNo Refill / No Refund\r\nLikes On Live Video\r\nFast Delivery\r\nMinimum 200', 200, 10000, 21000, 21000, 15000, 'Aktif', 117, 'MEDANPEDIA', 'Sosial Media'),
(2424, 12, 'Instagram Live Video', 'Instagram - Live Video Views', ' [ Username Only ] INSTANT', 25, 100000, 90000, 90000, 15000, 'Aktif', 118, 'MEDANPEDIA', 'Sosial Media'),
(2425, 13, 'Instagram Story / Impressions / Saves / Profile Vi', 'Instagram Impressions [10M] [EXPLORE - HOME - LOCATION - PROFILE]', 'Impressions showing from ALL in the statistics (Explore, Home, Location ,Etc..)!\r\nInstant Start!\r\nFast Delivery!\r\nMinimum 100\r\nMaximum 10M', 100, 20000000, 16900, 16900, 15000, 'Aktif', 120, 'MEDANPEDIA', 'Sosial Media'),
(2426, 14, 'Instagram Story / Impressions / Saves / Profile Vi', 'Instagram Impressions [1M]', 'Real\r\nInstant Delivery!\r\nMinimum 100', 100, 1000000, 16500, 16500, 15000, 'Aktif', 121, 'MEDANPEDIA', 'Sosial Media'),
(2427, 15, 'Instagram Story / Impressions / Saves / Profile Vi', 'Instagram Saves ', 'No Refill / No Refund\r\n0-1 Hour Start!\r\n15K/Day\r\nMinimum 10', 10, 15000, 19000, 19000, 15000, 'Aktif', 123, 'MEDANPEDIA', 'Sosial Media'),
(2428, 16, 'Twitter Views & Impressions', 'Twitter Views Server 1 [1M]', 'Refill (30 Days Maximum) \r\n0-1 Hour Start! \r\n10K - 100K/Day \r\nMinimum 100', 100, 1000000, 25000, 25000, 15000, 'Aktif', 138, 'MEDANPEDIA', 'Sosial Media'),
(2429, 17, 'Twitter Views & Impressions', 'Twitter Impressions Server 1 [1M]', 'Refill (30 Days Maximum) \r\n0-1 Hour Start! \r\n10K - 100K/Day \r\nMinimum 100', 100, 1000000, 40000, 40000, 15000, 'Aktif', 139, 'MEDANPEDIA', 'Sosial Media'),
(2430, 18, 'Linkedin', 'Linkedin - Followers AUTO 1', 'instan', 100, 1000000, 157000, 157000, 15000, 'Aktif', 141, 'MEDANPEDIA', 'Sosial Media'),
(2431, 19, 'Website Traffic', 'Website Traffic Server 2 [10M]', 'Super Cepat', 100, 10000000, 19000, 19000, 15000, 'Aktif', 143, 'MEDANPEDIA', 'Sosial Media'),
(2432, 20, 'Instagram TV', 'Instagram TV Views Server 1', 'Instagram TV Views ! \r\nFull TV Video Link Needed ! \r\nINSTANT Start ! ', 50, 100000000, 15900, 15900, 15000, 'Aktif', 158, 'MEDANPEDIA', 'Sosial Media'),
(2433, 21, 'Youtube Likes / Dislikes / Shares / Comment', 'Youtube Like Versi 1', 'Likes can be over-delivered!\r\nReal\r\nNo Refill Guarantee\r\n25,000-100,000 Per day\r\nkami tidak ada garansi jika like langsung turun, no kompline\r\norder = berani tanggung resiko', 10, 400000, 37000, 37000, 15000, 'Aktif', 248, 'MEDANPEDIA', 'Sosial Media'),
(2434, 22, 'Telegram', 'Telegram - Channnel Members [ Max 3K]', 'Channel Only\r\n5k/day\r\nNo Refill\r\n1-12hrs start\r\nMin 100, Max 100k', 100, 3000, 28500, 28500, 15000, 'Aktif', 469, 'MEDANPEDIA', 'Sosial Media'),
(2435, 23, 'Youtube Views', 'Youtube Views server 1 [ No Garansi ][ Fast ] ', 'Instan\r\nkecepatan 5k -20k/hari\r\nGK ADA GARANSI APAPUN! JIKA VIEW TURUN\r\nBELI? berani ambil resiko', 1000, 100000, 22240, 22240, 15000, 'Aktif', 572, 'MEDANPEDIA', 'Sosial Media'),
(2436, 24, 'Spotify', 'Spotify Followers S1 [1M] min 1000', 'Start Time: Instant - 6 hours\r\nSpeed: 20K/ day \r\nRefill: no', 1000, 1000000, 42500, 42500, 15000, 'Aktif', 725, 'MEDANPEDIA', 'Sosial Media'),
(2437, 25, 'Spotify', 'Spotify Followers S2 [1M] min 20', 'Start Time: Instant - 6 hours\r\nSpeed: 20K/ day \r\nRefill: no', 20, 1000000, 59000, 59000, 15000, 'Aktif', 726, 'MEDANPEDIA', 'Sosial Media'),
(2438, 26, 'Spotify', 'Spotify Followers S3 [Super Fast] min 20', '100% High-Quality Account\r\nNo Drop - Life Time Guarantee\r\nInstant ( Avg 0-3 hrs ) \r\n500 to 5000 per 24 hour', 20, 1000000, 42000, 42000, 15000, 'Aktif', 727, 'MEDANPEDIA', 'Sosial Media'),
(2439, 27, 'Spotify', 'Spotify Plays S1', 'Spotify Plays S1', 1000, 1000000, 33500, 33500, 15000, 'Aktif', 728, 'MEDANPEDIA', 'Sosial Media'),
(2440, 28, 'Spotify', 'Spotify Playlists S1', 'Correct format: \r\nhttps://open.spotify.com/album/2beOdusX0eDgXQ7KdX8IVf\r\nhttps://open.spotify.com/playlist/4jHJBBSbRZp2SNFeHoJMfA', 50, 100000, 105000, 105000, 15000, 'Aktif', 729, 'MEDANPEDIA', 'Sosial Media'),
(2441, 29, 'Spotify', 'Spotify Playlists S2', 'Correct format: \r\nhttps://open.spotify.com/album/2beOdusX0eDgXQ7KdX8IVf\r\nhttps://open.spotify.com/playlist/4jHJBBSbRZp2SNFeHoJMfA', 5000, 1000000, 37000, 37000, 15000, 'Aktif', 730, 'MEDANPEDIA', 'Sosial Media'),
(2442, 30, 'Youtube Views', 'Youtube Views server 2 [ 50k-100k/day ] [ Lifetime Guarantee ] cheap', 'Start time: 0-3 hours\r\nJika status selesai tetapi view tidak ter update\r\nsilahkan klik like', 100, 10000000, 39500, 39500, 15000, 'Aktif', 768, 'MEDANPEDIA', 'Sosial Media'),
(2443, 31, 'Facebook Page / Website - Likes / Stars', 'Facebook Page Likes [ S 8 ] [20K] [R30]', 'Start Time: Instant - 1 hour\r\nSpeed: 5K/ day \r\nRefill: 30 days\r\nSpecs: Fast', 50, 20000, 135000, 135000, 15000, 'Aktif', 771, 'MEDANPEDIA', 'Sosial Media'),
(2444, 32, 'Shopee/Tokopedia/Bukalapak', 'Shopee Likes Indonesia [2500] {produk} BONUS 10%', 'Fast Process\r\nuntuk produk', 100, 2500, 20000, 20000, 15000, 'Aktif', 894, 'MEDANPEDIA', 'Sosial Media'),
(2445, 33, 'SoundCloud', 'SoundCloud Plays [10M] [S2]', 'Start Time: Instant - 12 hours\r\nSpeed: 3 to 5mil/ day \r\nSpecs: Full Link !', 50000, 10000000, 15300, 15300, 15000, 'Aktif', 912, 'MEDANPEDIA', 'Sosial Media'),
(2446, 34, 'Pinterest', 'Pinterest Followers', 'Pinterest Account Followers', 25, 100000, 53000, 53000, 15000, 'Aktif', 914, 'MEDANPEDIA', 'Sosial Media'),
(2447, 35, 'Pinterest', 'Pinterest Board Followers ', 'Pinterest Board Followers', 10, 250000, 53000, 53000, 15000, 'Aktif', 915, 'MEDANPEDIA', 'Sosial Media'),
(2448, 36, 'Pinterest', 'Pinterest Likes ', 'Pinterest Likes ', 22, 250000, 53000, 53000, 15000, 'Aktif', 916, 'MEDANPEDIA', 'Sosial Media'),
(2449, 37, 'Instagram Story Views', 'Instagram Story Views [9K] [1H - Ultra Fast! ]', 'All Stories\r\nUsername Only\r\n0-1 Hour Start! \r\nUltra Fast! \r\nMinimum 100', 250, 3000, 17000, 17000, 15000, 'Aktif', 917, 'MEDANPEDIA', 'Sosial Media'),
(2450, 38, 'Instagram Live Video', 'Instagram Live Video Comments Random', 'Username Only \r\nNo Refill / No Refund \r\nRandom Comments On Live Video \r\nFast Delivery \r\nMinimum 50', 100, 2000, 115000, 115000, 15000, 'Aktif', 922, 'MEDANPEDIA', 'Sosial Media'),
(2451, 39, 'Website Traffic', 'Indonesia Traffic from Google', 'Start Time: Instant - 12 hours\r\nSpeed: Up to 10K/ day \r\nSpecs:\r\n100% Real & Unique Traffic\r\nDesktop Traffic\r\nGoogle Analytics Supported\r\nLow bounce rates 15-20%\r\nYou can use bit.ly to track results\r\nNo Adult, Drugs or other harmful websites allowed\r\nLink Format: Full Website URL\r\n', 500, 50000, 23000, 23000, 15000, 'Aktif', 925, 'MEDANPEDIA', 'Sosial Media'),
(2452, 40, 'Instagram Like Indonesia', 'Instagram likes Indonesia CHEAP', 'no kompline\r\nproses 1-3 hari', 100, 2000, 17000, 17000, 15000, 'Aktif', 928, 'MEDANPEDIA', 'Sosial Media'),
(2453, 41, 'Youtube Views', 'Youtube  Views server 3 [ High Retention ] No refill', 'High Retention Youtube Views\r\nRetention : 0-1 Minutes ( Average 30-50 Second )\r\nSource: Google Search\r\nStart Instan\r\nNo refill', 100, 10000000, 23250, 23250, 15000, 'Aktif', 969, 'MEDANPEDIA', 'Sosial Media'),
(2454, 42, 'Shopee/Tokopedia/Bukalapak', 'Bukalapak Pelanggan Indonesia [5000] ', 'Perempuan / Nama Asli / Foto Asli / BOT\r\nIndonesia\r\n\r\nGUnakan TARGET CUMA USERNAME !!!!!', 50, 5000, 37000, 37000, 15000, 'Aktif', 1035, 'MEDANPEDIA', 'Sosial Media'),
(2455, 43, 'Shopee/Tokopedia/Bukalapak', 'Tokopedia Followers INDO FAST  [ 4k ]', 'High Quality\r\nIndonesia\r\nGunakan Link https://www.tokopedia.com/username\r\nJangan https://m.tokopedia.com/username Atau pun Yang Lain Bakal Kaga bisa\r\nFORMAT WAJIB https://www.tokopedia.com/username   (username ganti dengan usernam mu)\r\nDilarang double order', 100, 4000, 47000, 47000, 15000, 'Aktif', 1036, 'MEDANPEDIA', 'Sosial Media'),
(2456, 44, 'Shopee/Tokopedia/Bukalapak', 'Tokopedia Views Produk', 'Gunakan Link feed produk', 50, 10000, 17000, 17000, 15000, 'Aktif', 1037, 'MEDANPEDIA', 'Sosial Media'),
(2457, 45, 'Instagram Story / Impressions / Saves / Profile Vi', 'Instagram kunjungan profil / Profile Visit', 'Profile Visit', 100, 100000, 22500, 22500, 15000, 'Aktif', 1043, 'MEDANPEDIA', 'Sosial Media'),
(2458, 46, 'Shopee/Tokopedia/Bukalapak', 'Tokopedia wishlist/ Favorite [ max 2k ]', 'gunakan link Produk', 50, 2000, 23000, 23000, 15000, 'Aktif', 1047, 'MEDANPEDIA', 'Sosial Media'),
(2459, 47, 'Shopee/Tokopedia/Bukalapak', 'Shopee Followers [5K] MURAH [BONUS 5%', 'username ya bro\r\ninstan\r\norder 100 dapat 105', 50, 5000, 33500, 33500, 15000, 'Aktif', 1048, 'MEDANPEDIA', 'Sosial Media'),
(2460, 48, 'Instagram TV', 'Instagram TV Views Server 2  [ SUPER FAST ]', 'no partial\r\nsuperfast\r\n1M/day', 50, 100000000, 15600, 15600, 15000, 'Aktif', 1059, 'MEDANPEDIA', 'Sosial Media'),
(2461, 49, 'Instagram TV', 'Instagram TV Views Server 3 [ INSTANT ] ', 'IG TV Views \r\nReal Views \r\nSuper Fast\r\nInstant', 200, 5000000, 15700, 15700, 15000, 'Aktif', 1060, 'MEDANPEDIA', 'Sosial Media'),
(2462, 50, 'Instagram Like Indonesia', 'Instagram Likes + Reels Indonesia S22 [max 4000] REALL', 'Waktu mulai proses\r\n1-2 hari', 100, 4000, 22800, 22800, 15000, 'Aktif', 1065, 'MEDANPEDIA', 'Sosial Media'),
(2463, 51, 'Instagram Like Komentar [ top koment ]', 'Instagram Like Komentar FAST Server 2', 'Fast', 20, 10000, 37000, 37000, 15000, 'Aktif', 1087, 'MEDANPEDIA', 'Sosial Media'),
(2464, 52, 'Shopee/Tokopedia/Bukalapak', 'Tokopedia Followers  [2K]', 'Gunakan username wajib!', 50, 2000, 48000, 48000, 15000, 'Aktif', 1088, 'MEDANPEDIA', 'Sosial Media'),
(2465, 53, 'Instagram Like Indonesia', 'Instagram Likes Indonesia SRV 16 max 4500 BONUS 10%', 'Instan \r\nsesuai antrian\r\nno refill/no garansi', 100, 4500, 19000, 19000, 15000, 'Aktif', 1094, 'MEDANPEDIA', 'Sosial Media'),
(2466, 54, 'Shopee/Tokopedia/Bukalapak', 'Shopee Followers Server NEW  [100] ', 'bot female\r\ngunakan username', 100, 100, 42000, 42000, 15000, 'Aktif', 1101, 'MEDANPEDIA', 'Sosial Media'),
(2467, 55, 'Youtube Views', 'Youtube Ranking Views V10 [ Recommended ][ 0 - 1 Mint Retention]', ' [ Lifetime Guarantee Views ]\r\n- Cheapest In Market\r\n- Start times : 0 - 1h ( Instant )\r\n- Non drop - Lifetime Guarantee Views\r\n- Speed 20k - 30k+ ( Some times will be Faster )\r\n- Retention : 0-1 Minutes +', 500, 10000000, 39500, 39500, 15000, 'Aktif', 1124, 'MEDANPEDIA', 'Sosial Media'),
(2468, 56, 'Instagram Like Indonesia', 'Instagram Likes Indonesia Server 1 max 1000 [ on off ] âš¡ï¸âš¡ï¸âš¡ï¸', 'Proses sesuai antian\r\ngud service\r\n', 100, 1000, 43000, 43000, 15000, 'Aktif', 1161, 'MEDANPEDIA', 'Sosial Media'),
(2469, 57, 'Instagram Likes', 'Instagram Likes Server 3 [ Kualitas bagus ] [ Superfast ] ', 'fast', 10, 25000, 31000, 31000, 15000, 'Aktif', 1169, 'MEDANPEDIA', 'Sosial Media'),
(2470, 58, 'Shopee/Tokopedia/Bukalapak', 'Shopee Likes Indonesia Server new [1000] {produk} ', 'bot\r\ngunakan link\r\nContoh : https://shopee.co.id/Rok-Midi-A-Line-Lipit-Bahan-Tulle-Warna-Polos-untuk-Wanita-i.29961905.1125996814\r\njangan pakai smttt\r\nCONTOH LINK SALAH :\r\n1. https://shopee.co.id/product/xxxxxx/xxxxxxxx?smtt=0.0.9\r\n2. https://shopee.co.id/product/xxxxxx/xxxxxxxx/?smtt=0.0.9\r\nTolong gunakan link yang benar !', 50, 1000, 23000, 23000, 15000, 'Aktif', 1173, 'MEDANPEDIA', 'Sosial Media'),
(2471, 59, 'Instagram TV', 'Instagram TV Views Server 5 [1M/day] [ Cheapest in market]', '[1M/day] [ Cheapest in market]', 500, 10000000, 15800, 15800, 15000, 'Aktif', 1179, 'MEDANPEDIA', 'Sosial Media'),
(2472, 60, 'Instagram Story Views', 'Instagram - Story Views S2 [30K] [INSTANT - 30K/Day]', 'ALL STORIES !\r\nUSERNAME ONLY !\r\nINSTANT START !\r\nFAST DELIVERY !', 20, 30000, 16200, 16200, 15000, 'Aktif', 1198, 'MEDANPEDIA', 'Sosial Media'),
(2473, 61, 'Spotify', 'Spotify Plays [ 1M ] Speed : 500 - 3500/D', '- Start Time: 1 - 12 Hours\r\n- Speed : 500 - 3500/D\r\n- Refill : Non Drop - LifeTime Guarantee\r\n- Best Service in the Market\r\n- Followers from TIER 1 countries only! USA/CA/EU/AU/NZ/UK.\r\n- Quality: HQ\r\n- Min/Max: 1000/1M', 1000, 1000000, 24000, 24000, 15000, 'Aktif', 1225, 'MEDANPEDIA', 'Sosial Media'),
(2474, 62, 'TikTok Followers', 'TIK TOK FOLLOWERS S4 [ 30 days refill - Full URL ]', 'Complete URL \r\n30 days refill\r\nSpeed 2-5k/Day', 10, 15000, 130000, 130000, 15000, 'Aktif', 1240, 'MEDANPEDIA', 'Sosial Media'),
(2475, 63, 'TikTok Likes', 'TIK TOK Likes S5 [ 30 days refill - Full URL ] ', 'Complete URL \r\n30 days refill\r\nSpeed 2-5k/Day', 10, 15000, 130000, 130000, 15000, 'Aktif', 1241, 'MEDANPEDIA', 'Sosial Media'),
(2476, 64, 'Instagram Like Indonesia', 'Instagram Likes Indonesia Server 4 [ 50K ] ', 'Bisa up ke 50k\r\nper order 10.000 max', 100, 10000, 36000, 36000, 15000, 'Aktif', 1244, 'MEDANPEDIA', 'Sosial Media'),
(2477, 65, 'Youtube Views', 'Youtube Views Server 4 ( No refill )  [Speed: 50K/Day] CHEAP', '- Start : 0 - 6 jam', 100, 10000000, 22100, 22100, 15000, 'Aktif', 1268, 'MEDANPEDIA', 'Sosial Media'),
(2478, 66, 'Instagram Story Views', 'Instagram - Story Views S3 All Story Views Fast ', 'instan', 100, 40000, 20000, 20000, 15000, 'Aktif', 1271, 'MEDANPEDIA', 'Sosial Media'),
(2479, 67, 'Instagram Like Indonesia', 'Instagram Likes Indonesia Server 10 max 500[ MURAH ] âš¡ï¸', 'Proses 2x24 jam\r\nMax hanya 500 jangn order lagi\r\ntetapi paling lama jika pesanan banyak 1- 3 hari\r\nLimit pesanan perhari 300 jadi jika tidak bisa order berarti udah limit harian\r\nsilahkan di pesan esok hari\r\nReal indo\r\ntidak bisa untuk igtv!\r\nJangan di private saat proses pesanan berlangsung!', 100, 500, 20200, 20200, 15000, 'Aktif', 1297, 'MEDANPEDIA', 'Sosial Media'),
(2480, 68, 'Shopee/Tokopedia/Bukalapak', 'Tokopedia Feeds Comment', '-', 5, 2000, 365000, 365000, 15000, 'Aktif', 1303, 'MEDANPEDIA', 'Sosial Media'),
(2481, 69, 'Facebook Page / Website - Likes / Stars', 'Facebook Page Likes [ S 11 ] [Refill 30] [Instant Start] Real', ' Refill 30 Days\r\nInstant Start\r\nSpeed : 5K / Day\r\nNon Drop Likes', 100, 10000, 185000, 185000, 15000, 'Aktif', 1316, 'MEDANPEDIA', 'Sosial Media'),
(2482, 70, 'Instagram Followers Indonesia', 'Instagram Followers Indonesia Server 5 [max 7000] REAL AKTIFâš¡', 'Proses normal 1x 24 jam\r\nproses bisa lebih lama jika delay\r\nmax 3 hari bisa kompline untuk canceled\r\ntergantung pada antrian juga\r\nReal indo, fresh db\r\nkemungkinan besar real pasif hanya 10%', 20, 7000, 66000, 66000, 15000, 'Aktif', 1325, 'MEDANPEDIA', 'Sosial Media'),
(2483, 71, 'Facebook Page / Website - Likes / Stars', 'Facebook Page Likes [ S12 ] [30 Days Auto Refill]', ' Facebook Fan Page Likes\r\n( Min 500 And Max 50k )\r\nAuto-Refill if Likes Drop\r\n( Drop Ratio: 10% but we added Auto-Refill in Backend System )\r\n( Speed 5-10k Per Day )\r\nHigh Quality and Real Likes\r\nRefill : 30 Days Auto Refill', 100, 50000, 75000, 75000, 15000, 'Aktif', 1333, 'MEDANPEDIA', 'Sosial Media'),
(2484, 72, 'TikTok Followers', 'TIK TOK FOLLOWERS S5 [ 30 days refill - Full URL ] ', '- Speed 5000 per day\r\n- Avatars Followers and Likes\r\n- 30 days warranty\r\n- instant start to 5 minute start Time\r\n( Contoh Target yang kamu masukin https://www.tiktok.com/@username )', 10, 30000, 95000, 95000, 15000, 'Aktif', 1342, 'MEDANPEDIA', 'Sosial Media'),
(2485, 73, 'TikTok Likes', 'TIK TOK Likes S6 [ 30 days refill - Full URL ] ', '- Speed 5000 per day\r\n- Avatars Followers and Likes\r\n- 30 days warranty\r\n- instant start to 5 minute start Time', 9, 30000, 100000, 100000, 15000, 'Aktif', 1343, 'MEDANPEDIA', 'Sosial Media'),
(2486, 74, 'Instagram Like Indonesia', 'Instagram Likes + Reels Indonesia Server 11 max 4000 REKOMENDEDâš¡ï¸â­', 'Dapat Bonus like jika beruntung\r\nProses instan\r\nreal indo', 20, 4000, 28000, 28000, 15000, 'Aktif', 1347, 'MEDANPEDIA', 'Sosial Media'),
(2487, 75, 'Shopee/Tokopedia/Bukalapak', 'PAKET shopee produk terjual 1000  ( BACA DESKRIPSI )', 'FORMAT username | password | Link Produk\r\n\r\nProduk terjual shopee \r\nuntuk meningkatkan toko baru atau starseller yg ingin launching produk barunya, karenaÂ  kalau berjualan di shopee tidak ada yang beli / produknya tidak ada yang terjual itu akan sangat minim menimbulkan ketertarikan konsumen untuk Checkout. \r\n\r\nKalau garansi kita tidak ada ya, karena kaka order tentu tau resikonya. \r\n\r\nTapi dari orderan sebelumnya kami belum pernah ada laporan akun yang terkena banned shopee, karena kaki tidak menggunakan aplikasi / software diluar shopee ( pihak ke tiga ) \r\n\r\nKami juga menggunakan akun real untuk proses pengerjaan produk terjual dan kami memproses setiap akunnya berbeda device serta berbeda IP adress jadi kemungkinan aman.\r\nSyarat produk terjual\r\n- min 1 terjual ( pembeli di toko kamu )\r\n- username & password\r\n- harap matikan otp login saat proses\r\n- link produk', 1000, 1000, 365000, 365000, 15000, 'Aktif', 1348, 'MEDANPEDIA', 'Sosial Media'),
(2488, 76, 'Shopee/Tokopedia/Bukalapak', 'PAKET shopee produk terjual 500 ( BACA DESKRIPSI ) Rp 240.000', 'FORMAT username | password | Link Produk\r\n\r\nProduk terjual shopee \r\nuntuk meningkatkan toko baru atau starseller yg ingin launching produk barunya, karenaÂ  kalau berjualan di shopee tidak ada yang beli / produknya tidak ada yang terjual itu akan sangat minim menimbulkan ketertarikan konsumen untuk Checkout. \r\n\r\nKalau garansi kita tidak ada ya, karena kaka order tentu tau resikonya. \r\n\r\nTapi dari orderan sebelumnya kami belum pernah ada laporan akun yang terkena banned shopee, karena kaki tidak menggunakan aplikasi / software diluar shopee ( pihak ke tiga ) \r\n\r\nKami juga menggunakan akun real untuk proses pengerjaan produk terjual dan kami memproses setiap akunnya berbeda device serta berbeda IP adress jadi kemungkinan aman.\r\nSyarat produk terjual\r\n- min 1 terjual ( pembeli di toko kamu )\r\n- username & password\r\n- harap matikan otp login saat proses\r\n- link produk', 500, 500, 495000, 495000, 15000, 'Aktif', 1349, 'MEDANPEDIA', 'Sosial Media'),
(2489, 77, 'Shopee/Tokopedia/Bukalapak', 'PAKET shopee produk terjual 200 ( BACA DESKRIPSI ) Rp 135.000 ', 'FORMAT username | password | Link Produk\r\n\r\nProduk terjual shopee \r\nuntuk meningkatkan toko baru atau starseller yg ingin launching produk barunya, karenaÂ  kalau berjualan di shopee tidak ada yang beli / produknya tidak ada yang terjual itu akan sangat minim menimbulkan ketertarikan konsumen untuk Checkout. \r\n\r\nKalau garansi kita tidak ada ya, karena kaka order tentu tau resikonya. \r\n\r\nTapi dari orderan sebelumnya kami belum pernah ada laporan akun yang terkena banned shopee, karena kaki tidak menggunakan aplikasi / software diluar shopee ( pihak ke tiga ) \r\n\r\nKami juga menggunakan akun real untuk proses pengerjaan produk terjual dan kami memproses setiap akunnya berbeda device serta berbeda IP adress jadi kemungkinan aman.\r\nSyarat produk terjual\r\n- min 1 terjual ( pembeli di toko kamu )\r\n- username & password\r\n- harap matikan otp login saat proses\r\n- link produk', 200, 200, 690000, 690000, 15000, 'Aktif', 1350, 'MEDANPEDIA', 'Sosial Media'),
(2490, 78, 'Instagram Views', 'instagram view Server 14 [ 1 Million / Hour ]', 'Kemungkinan  kecepatan 1 juta / jam\r\nKemungkinan mulai 0-10 menir', 100, 1000000, 15400, 15400, 15000, 'Aktif', 1411, 'MEDANPEDIA', 'Sosial Media'),
(2491, 79, 'Instagram Likes', ' Instagram Likes MP 2 [No Drop] Real [Max 5K]', 'Speed : 200 Likes / Hour\r\nNo Partial Issues\r\nNo Drop', 20, 5000, 22500, 22500, 15000, 'Aktif', 1421, 'MEDANPEDIA', 'Sosial Media'),
(2492, 80, 'Instagram Likes', 'Instagram Likes MP 3 [ 10k ] [ Instant - Start ]', ' Start time:\r\nFor orders under 1000 likes usually instant. If more than 1000 - may take some time, usually few hours\r\nSpeed is up to 100-200 per hour (can lower a bit when many orders)\r\nNo cancellation before 24 hours', 20, 5000, 39000, 39000, 15000, 'Aktif', 1422, 'MEDANPEDIA', 'Sosial Media'),
(2493, 81, 'Instagram Views', 'instagram view MP 1 [ 5 Million / Hour ] ( Recommended )', '5 Million / Hour', 100, 2500000, 15700, 15700, 15000, 'Aktif', 1425, 'MEDANPEDIA', 'Sosial Media'),
(2494, 82, 'Youtube Views', 'Youtube Ranking Desktop Views MP 2 [ Lifetime Guaranteed ]', ' 0-24 hour start time\r\n100k to 300k /day speed\r\nLifetime refill guarantee\r\n30-40 second watch time\r\nSafe to run with monetised videos\r\nWindows desktop watch page\r\nWorldwide viewers added in a non-stop natural pattern\r\nMust be unrestricted & open for all countries\r\nOK for VEVO\r\nIncremental Speed Based on Order Size\r\n500 Minimum order\r\n1 Million Maximum order', 500, 1000000, 39000, 39000, 15000, 'Aktif', 1430, 'MEDANPEDIA', 'Sosial Media'),
(2495, 83, 'Facebook Followers / Friends', 'Facebook Profile Follower MP 1 [ No Refill ] beta test ', ' - Speed 1k/D\r\n- Start : 0 - 24h\r\n- hanya untuk followers profil ya bukan fanspage/halaman !\r\n- No Refill .', 100, 10000, 100000, 100000, 15000, 'Aktif', 1443, 'MEDANPEDIA', 'Sosial Media'),
(2496, 84, 'Facebook Followers / Friends', 'Facebook Profile Follower MP 2 [ R30Day ] ', '- Speed 1k/D\r\n- Start : 0 - 24h\r\n- Please place Profile followers ONLY , not Fan page likes.\r\n- 30 days refill', 100, 10000, 110000, 110000, 15000, 'Aktif', 1444, 'MEDANPEDIA', 'Sosial Media'),
(2497, 85, 'Facebook Post Likes / Comments / Shares', 'Facebook Photo / Post Likes MP1[ Start Instant ][Recommended]', 'Speed 5k per day\r\nNo refill\r\nbisa untuk video live ', 25, 10000, 70000, 70000, 15000, 'Aktif', 1445, 'MEDANPEDIA', 'Sosial Media'),
(2498, 86, 'Facebook Post Likes / Comments / Shares', 'Facebook Photo / Post Likes MP3 [CHEAPEST] [10K]', 'No refill / refund\r\n0 - 48 Hours start\r\nARAB REAL', 100, 10000, 37000, 37000, 15000, 'Aktif', 1448, 'MEDANPEDIA', 'Sosial Media'),
(2499, 87, 'Youtube Likes / Dislikes / Shares / Comment', 'Youtube Like MP 3 [ 30 Days Refill - Max 5K ] [ Speed 100+/D ]', '- Start : 0 - 24 hours\r\n- Min: 50 - Max: 5K\r\n- Daily speed 50 - 200 ( Speed can slower if server overload, in this care must wait )\r\n- NON DROP so far - 30 days Refill Guarantee\r\n\r\nNOTE :\r\n- No Refund after order placed\r\n- No Refill if Old Likes Drop Below Start Count .', 50, 10000, 45000, 45000, 15000, 'Aktif', 1453, 'MEDANPEDIA', 'Sosial Media'),
(2500, 88, 'Youtube Likes / Dislikes / Shares / Comment', 'Youtube Like MP 4 [ TERMURAH ][ R30 - 10K ][ 200+/D ]â™»ï¸', '- Instant\r\n- Non drop -\r\n- Guarantee: 30 days refill if any drop\r\n- Speed 200+/D', 20, 10000, 82000, 82000, 15000, 'Aktif', 1454, 'MEDANPEDIA', 'Sosial Media'),
(2501, 89, 'Youtube Likes / Dislikes / Shares / Comment', 'Youtube Like MP 5 [ TERMURAH ][ NO REFILL- 10K ][ 10K+/D ]', '- Instant Start\r\n- Speed for now about 10K/D\r\n- No refill / No refund with any reason .', 20, 10000, 64000, 64000, 15000, 'Aktif', 1455, 'MEDANPEDIA', 'Sosial Media'),
(2502, 90, 'SoundCloud', 'Soundcloud  Followers MP 1 [ High Quality ] ~ Instant ', '[ High Quality ] ~ Instant\r\n', 20, 25000, 37000, 37000, 15000, 'Aktif', 1456, 'MEDANPEDIA', 'Sosial Media'),
(2503, 91, 'SoundCloud', 'Soundcloud Followers MP 2 [ USA ] ~ Instant ', 'Start Time: Instant - 12 hours\r\nSpeed: 1K-2K/ day\r\nRefill: 30 days\r\nSpecs: Mix Quality !', 100, 50000, 34000, 34000, 15000, 'Aktif', 1457, 'MEDANPEDIA', 'Sosial Media'),
(2504, 92, 'Telegram', 'Telegram Post Views [10K] [Last 1] ', 'Start Time: Instant - 1 hour\r\nSpeed: 10K to 20K/ day\r\nRefill: no\r\nSpecs: Latest Post\r\nSend Post Link Or channel id\r\nExample Link: https://t.me/link_example/994', 100, 200000, 16500, 16500, 15000, 'Aktif', 1459, 'MEDANPEDIA', 'Sosial Media'),
(2505, 93, 'Instagram Followers [guaranteed]', 'Instagram Followers Refill MP 3 [REFILL 30] Real dan aktif', 'Start times : 0 - 2H\r\nSpeed/day : 1k+/D\r\nRefill Guarantee : 30 Days ( Refill Button )\r\nQuality : REAL', 20, 20000, 88000, 88000, 15000, 'Aktif', 1465, 'MEDANPEDIA', 'Sosial Media'),
(2506, 94, 'Instagram Like Indonesia', 'Instagram Likes + Reels Indonesia MP 1 [ max 3k ] [ REAL AKTIF ]', 'KEMUNGKINAN selesai pada 3 - 6 jam\r\nmax 24 jam\r\nbisa order 1k 3x untuk 1 foto\r\njadi max 3000', 50, 2000, 30000, 30000, 15000, 'Aktif', 1475, 'MEDANPEDIA', 'Sosial Media'),
(2507, 95, 'Instagram Like Indonesia', 'Instagram Likes Indonesia MP 2 [ MAX 10K ] âš¡ï¸âš¡ï¸âš¡ï¸', 'KEMUNGKINAN selesai pada 3 - 6 jam\r\nmax 24 jam\r\nbisa order 1k 10x untuk 1 foto\r\njadi max 10.000 untuk 1 foto', 100, 1000, 41000, 41000, 15000, 'Aktif', 1476, 'MEDANPEDIA', 'Sosial Media'),
(2508, 96, '- PROMO - ON OFF', 'Instagram Followers PROMO 5 [ DB MEDANPEDIA ] [ TERMURAH ] fast', 'HIGH DROP\r\nINSTANT', 10, 15000, 17400, 17400, 15000, 'Aktif', 1478, 'MEDANPEDIA', 'Sosial Media'),
(2509, 97, 'Instagram Views', 'instagram view MP 2 + Impressions [RANDOM]', '+ Impressions ', 100, 2500000, 15700, 15700, 15000, 'Aktif', 1484, 'MEDANPEDIA', 'Sosial Media'),
(2510, 98, 'Instagram Followers [guaranteed]', 'Instagram Followers Refill MP 12 [ REAL ] [ STABIL ] BONUS+++', 'Speed : 500-1000 / day, Small Order Deliver Faster\r\nGuaranteed: 30 days Guaranteed\r\nwaktu mulai 0 -5 jam\r\nExtra: 20 -30% Extra Delivers', 50, 50000, 77000, 77000, 15000, 'Aktif', 1490, 'MEDANPEDIA', 'Sosial Media'),
(2511, 99, 'Instagram Followers [guaranteed]', 'Instagram Followers Refill MP 13 [ Real Mixed] [ 30 days refill ] â™»ï¸', 'kecepatan 3k/day\r\n30 days refill\r\nReal Mixed account', 50, 15000, 65000, 65000, 15000, 'Aktif', 1497, 'MEDANPEDIA', 'Sosial Media'),
(2512, 100, 'Instagram Followers [ No Refill ]', 'Instagram Followers MP 31 [ LESS DROP | DROP 10-20% ] ', '1k in 1 minutes\r\n80% real\r\nKemungkinan drop 10-20% jika anda memesan 1000+\r\n', 100, 5000, 45000, 45000, 15000, 'Aktif', 1500, 'MEDANPEDIA', 'Sosial Media'),
(2513, 101, 'Instagram Likes', 'Instagram Likes MP 9 [ Pakistan+asia+indo ] [ 40K ] ', '1k-2k/hour\r\n', 50, 40000, 25600, 25600, 15000, 'Aktif', 1501, 'MEDANPEDIA', 'Sosial Media'),
(2514, 102, 'Instagram Followers [guaranteed]', 'Instagram Followers Refill Server 4 [ Real Mixed ] [ Refill 30 days ] â™»ï¸', 'Start 0 - 1H\r\n3K/days\r\n30 Days Refill ', 50, 50000, 65000, 65000, 15000, 'Aktif', 1517, 'MEDANPEDIA', 'Sosial Media'),
(2515, 103, 'Youtube Likes / Dislikes / Shares / Comment', 'Youtube Like MP 6 [ Best Seller ][ AUTO Refill ]â™»ï¸', 'Instant\r\n30 days refill\r\nSpeed 200+/hari', 20, 10000, 55000, 55000, 15000, 'Aktif', 1518, 'MEDANPEDIA', 'Sosial Media'),
(2516, 104, 'Instagram Followers [ No Refill ]', 'Instagram Followers MP 33 [ NON DROP | BONUS 0-5% ] [ 2k/Day ]âš¡ï¸', 'waktu proses 0-1jam\r\nkecepatan 1k-3k/hari\r\nno drop\r\nkalo drop kemungkinan besar itu followers yang lain, bukan dari kami\r\nkalo drop kemungkinan sangat dikit dan no refill', 50, 100000, 135000, 135000, 15000, 'Aktif', 1531, 'MEDANPEDIA', 'Sosial Media'),
(2517, 105, 'TikTok View/share/comment', 'TIK TOK View S9 [ superfast ] [ Trending + Viral Views]', 'Layanan ini berbeda dengan view lain\r\nkarena layanan ini bisa membuat trending dan viral video', 500, 500000, 16900, 16900, 15000, 'Aktif', 1546, 'MEDANPEDIA', 'Sosial Media'),
(2518, 106, 'Twitter Views & Impressions', 'Twitter Views Server 2 [ REAL - Max 15K ]âš¡ï¸âš¡ï¸', '- Instant start and order will completed in some min\r\n- No refill\r\n- All real data , No Bo , kemungkinan drop 10%', 20, 15000, 18500, 18500, 15000, 'Aktif', 1553, 'MEDANPEDIA', 'Sosial Media'),
(2519, 107, 'Youtube Live Stream', 'Youtube Live Stream Views [ REAL ][ BETA ]', 'â€¢ Tampilan Aktif Nyata **\r\nâ€¢ MULAI INSTAN\r\nâ€¢ 100% Pemirsa Pengguna YouTube Manusia Nyata!\r\nâ€¢ Tampilan Halaman Desktop Windows & Mobile Watch\r\nâ€¢ 100% Lalu Lintas Unik dapat dimonetisasi!\r\nâ€¢ Pemirsa Seluruh Dunia\r\nâ€¢ Harus Tidak Terbatas & Terbuka untuk SEMUA negara\r\nâ€¢ Retensi Acak\r\nâ€¢ Rata-rata Bersamaan dan waktu tonton berdasarkan konten streaming langsung\r\nâ€¢ Pengiriman Lebih Dijamin\r\nâ€¢ penayangan dapat dikirim ke embed video streaming langsung yang dinonaktifkan\r\nâ€¢ Sumber Lalu Lintas: Iklan Langsung\r\n\r\nCATATAN :\r\n- Layanan Beta - itu berarti layanan yang ditawarkan apa adanya tanpa jaminan isi ulang!\r\n- Tampilan dapat mencakup keterlibatan pengguna nyata - video Anda mungkin mendapatkan suka / tidak suka setiap hari, komentar, bagikan, pelanggan ,,, semua dibuat oleh pengguna YouTube nyata yang tidak kami kontrol!', 5000, 100000, 83000, 83000, 15000, 'Aktif', 1560, 'MEDANPEDIA', 'Sosial Media'),
(2520, 108, 'Instagram Followers [guaranteed]', 'Instagram Followers Refill Server 8 [ Real,Mixed ] [ Refill 10D ][ Best Market ]âš¡ï¸â­â­â™»ï¸', '- Mulai kali 1 - 6 jam\r\n- Kecepatan 20k / days\r\n- Non drop atau sedikit drop sejauh ini\r\n- Garansi: isi ulang 10 hari jika drop', 100, 50000, 62000, 62000, 15000, 'Aktif', 1564, 'MEDANPEDIA', 'Sosial Media'),
(2521, 109, 'Instagram Likes', 'Instagram Likes MP 15 [ NO DROP ] Max terbanyak', 'No Drop Likes\r\n1-3K / Hour', 20, 50000, 24500, 24500, 15000, 'Aktif', 1565, 'MEDANPEDIA', 'Sosial Media'),
(2522, 110, 'Twitch', 'Twitch Followers 100K/days', 'No Drop & 30 Day Refill Guarantee!\r\nStarts in 5 minute\r\nStarting very fast!', 100, 500000, 24200, 24200, 15000, 'Aktif', 1570, 'MEDANPEDIA', 'Sosial Media'),
(2523, 111, 'Instagram Like Komentar [ top koment ]', 'Instagram Like Komentar Indonesia Real [ BACA Deskripsi ]', 'Diusahakan post baru\r\nkadang gk dapat id komen\r\nProses max 5 jam an\r\nformat = link post + username\r\ncontoh https://www.instagram.com/p/BiWlAgnlE5e/ + filmy_gyan33 ( ingat dipisah  link post spasi + spasi username)\r\nseperti https://gyazo.com/9ab9235b653312259e325eb1a80ce1df\r\n\r\nTOLONG DIPERHATIKAN!!!', 20, 5000, 62000, 62000, 15000, 'Aktif', 1572, 'MEDANPEDIA', 'Sosial Media'),
(2524, 112, 'Instagram Followers Indonesia', 'Instagram Followers Indonesia Server 17 max 2Kâš¡ï¸  Real Aktif', 'Proses fast\r\nmax proses 3 hari baru kompline\r\nReal indo\r\nper akun max 2k', 50, 2000, 49500, 49500, 15000, 'Aktif', 1574, 'MEDANPEDIA', 'Sosial Media'),
(2525, 113, 'Instagram Followers Indonesia', 'Instagram Followers Indonesia Server 18 [ max 500 ] [ REAL AKTIF ]', 'bisa order 2x di 1 akun\r\nreal proses max 3 hari\r\n', 100, 500, 42000, 42000, 15000, 'Aktif', 1576, 'MEDANPEDIA', 'Sosial Media'),
(2526, 114, 'Instagram Followers Indonesia', 'Instagram Followers Indonesia Server 19 [ mix bule + indo ] DB FRESH', 'bule + indo real\r\nproses cepat', 200, 4500, 37000, 37000, 15000, 'Aktif', 1580, 'MEDANPEDIA', 'Sosial Media'),
(2527, 115, 'Instagram Followers Indonesia', 'Instagram Followers Indonesia Server 20 [ mix bule + indo ] DB FRESH MAX2.5k', 'mix indo bule\r\n', 200, 2500, 40000, 40000, 15000, 'Aktif', 1581, 'MEDANPEDIA', 'Sosial Media'),
(2528, 116, 'Youtube Views', 'Youtube Views server 10 [ LIFETIME ][ FASTEST IN THE MARKET ]', 'Speed 1 Million Per Day\r\nInstant Start\r\nNON-Drop\r\nLife Time Guarantee', 1000, 100000000, 39000, 39000, 15000, 'Aktif', 1584, 'MEDANPEDIA', 'Sosial Media'),
(2529, 117, 'TikTok Followers', 'TIK TOK FOLLOWERS Server 2 [ no refill ] [baca deskripsi]', '1k-5k/day\r\nGunakan link video untuk followers jangan link profil\r\nNo Guarantee', 100, 10000, 62000, 62000, 15000, 'Aktif', 1593, 'MEDANPEDIA', 'Sosial Media'),
(2530, 118, 'Likee app', 'Likee App Post Likes [Speed : 1k-2k/day]', 'contoh target :https://likee.com/@********/video/*********\r\nNo refill', 20, 10000, 68000, 68000, 15000, 'Aktif', 1606, 'MEDANPEDIA', 'Sosial Media'),
(2531, 119, 'Likee app', 'Likee App Followers  [ 500-1k/day ]', 'contoh target https://likee.com/@********\r\nno refill', 20, 10000, 130000, 130000, 15000, 'Aktif', 1607, 'MEDANPEDIA', 'Sosial Media'),
(2532, 120, 'Shopee/Tokopedia/Bukalapak', 'Shopee Followers Server 1 [1K] Promo', 'username\r\nmasalah tidak masuk full terkadang = order terima \r\ndan masukfollowers saat status udah sukses, jangn langsung kompline ke tiket', 50, 1000, 29000, 29000, 15000, 'Aktif', 1612, 'MEDANPEDIA', 'Sosial Media'),
(2533, 121, 'TikTok View/share/comment', 'TIK TOK Custom Comments Server 1 [ 1K/ Day ]', '[Speed 1K/ Day]', 10, 1000, 245000, 245000, 15000, 'Aktif', 1619, 'MEDANPEDIA', 'Sosial Media'),
(2534, 122, 'Youtube Likes / Dislikes / Shares / Comment', 'Youtube Like MP 7 [ No Refill and Cheapest ] ', 'Real Youtube Likes\r\nInstant Start', 30, 50000, 33000, 33000, 15000, 'Aktif', 1621, 'MEDANPEDIA', 'Sosial Media'),
(2535, 123, 'TikTok Followers', 'TIK TOK FOLLOWERS Server 3 [ 30 days refill ] [ Recommended  ] ', '30 days guarantee\r\nspeed - 10k/day', 100, 10000, 62000, 62000, 15000, 'Aktif', 1623, 'MEDANPEDIA', 'Sosial Media'),
(2536, 124, 'Instagram Followers Indonesia', 'Instagram Followers Indonesia Server 22 [ TERMURAH ] [ REAL AKTIF ] Max 350', 'max 3 hari \r\nlewat bisa req canceled\r\n\r\nReal Aktif bosq', 100, 350, 38000, 38000, 15000, 'Aktif', 1629, 'MEDANPEDIA', 'Sosial Media'),
(2537, 125, 'Youtube Likes / Dislikes / Shares / Comment', 'Youtube Like MP 8 [ Lifetime Guaranted and Cheapest ] ', 'Speed 200 -1k/D\r\ninstan', 50, 20000, 35500, 35500, 15000, 'Aktif', 1634, 'MEDANPEDIA', 'Sosial Media'),
(2538, 126, 'Instagram Followers [ No Refill ]', 'Instagram Followers Server 7 [ fastest - BOT ] ', 'Bot Quality', 10, 30000, 20500, 20500, 15000, 'Aktif', 1645, 'MEDANPEDIA', 'Sosial Media'),
(2539, 127, 'Telegram', 'Telegram Channnel Members [ Max 5k ] INSTANT-3HRS ', 'Speed:5k/day', 1000, 5000, 41000, 41000, 15000, 'Aktif', 1647, 'MEDANPEDIA', 'Sosial Media'),
(2540, 128, 'Instagram Likes', 'Instagram Likes MP 19 [ NON DROP ] [5k-10k Per Day] ', 'instan', 100, 300000, 27000, 27000, 15000, 'Aktif', 1651, 'MEDANPEDIA', 'Sosial Media'),
(2541, 129, 'Instagram Followers [guaranteed]', 'Instagram Followers Refill S2 [ Refill 30Day ] LessDropâ™»ï¸', 'kemungkinan drop 5-15% ( tapi gk jami 100% )\r\nHigh-Quality\r\n', 50, 100000, 48000, 48000, 15000, 'Aktif', 1666, 'MEDANPEDIA', 'Sosial Media'),
(2542, 130, 'Instagram Followers [guaranteed]', 'Instagram Followers Refill S3 [ Refill 30Day ] [ Real Recommended  ]', 'sekitar 70%-80% real user\r\nSpeed 1k Per Day\r\n', 100, 85000, 29000, 29000, 15000, 'Aktif', 1667, 'MEDANPEDIA', 'Sosial Media'),
(2543, 131, 'Facebook Followers / Friends', 'Facebook Profile Follower MP 4 [ R30Day ] [Non Drop]', 'Refill 30Day\r\nkami sudah uji selama 2 bulan dan tidak ada penurunan\r\njadi kami tidak bisa mastikan ini nondrop 100% jika ada update dll', 50, 20000, 77000, 77000, 15000, 'Aktif', 1672, 'MEDANPEDIA', 'Sosial Media'),
(2544, 132, 'Instagram Like Indonesia', 'Instagram Likes Indonesia MP 3 [ Langsung Input MAX 4K ]  [ real aktif ] INSTANâš¡ï¸âš¡ï¸âš¡ï¸ ', 'FAST', 100, 2000, 41500, 41500, 15000, 'Aktif', 1674, 'MEDANPEDIA', 'Sosial Media'),
(2545, 133, 'Youtube Views', 'Youtube Views server 22 [ 5K - 20k/day ] [ Lifetime Guarantee ] ', '- Instant\r\n- Retention: 30 Second +\r\n- Speed 5K - 20k/day For NOW', 500, 10000, 44500, 44500, 15000, 'Aktif', 1676, 'MEDANPEDIA', 'Sosial Media'),
(2546, 134, 'Youtube View Jam Tayang', 'Youtube Views [ Jam Tayang 4000 jam ] [ Durasi Video 1 -2  jam+ ] [ cek Deskripsi ][ Best ]', 'jumlah pesan = jam tayang yang kamu dapat\r\n\r\n- Eksklusif di MEDANPEDIA\r\n- Silakan memesan 4000 tampilan untuk mendapatkan waktu menonton 4kh\r\n- Mulai 0 - 12 Jam - Real Views\r\n- 7 - 12 hari selesai \r\n- Garansi: refill 30 Hari\r\n\r\n\r\nCATATAN :\r\n- Harap cantumkan video yang memiliki panjang hanya 1 jam + (Jika tidak sampai 1 jam , pesanan masih berjalan tetapi waktu menonton tidak akan cukup, kami tidak dapat melakukan refill / reffund Dana / canceled / Partial dalam kasus ini)\r\n- Jika view lama Anda turun di bawah jumlah mulai, maka kami tidak dapat Refill / Refund / Cancel / Membatalkan sebagian. ( lebih baik menggunakan video dengan view yg sedikit/baru )\r\n- Silakan gunakan video tanpa tampilan interleaving alami (Jika Anda secara bersamaan mendapatkan view alami (ditonton orang lain selain sistem kami ) saat menjalankan waktu jam tayang, Anda tidak akan mendapatkan cukup waktu jam tayang.)\r\n- Jika video Dihapus, Ditolak, Privat sementara kami menambahkan waktu tontonan atau setelah selesai, maka kami tidak dapat refill / reffund Dana / canceled / Partial.', 100, 4000, 129000, 129000, 15000, 'Aktif', 1684, 'MEDANPEDIA', 'Sosial Media'),
(2547, 135, 'Youtube Views', 'Youtube Views server 27 [ 5 - 20 Min Retention ] [ Lifetime Guarantee ] ', '- Instant Start ( 0 - 30 mins )\r\n- Youtube Views High Retention\r\n- Retention : 5 - 20 Minutes watch time\r\n- Guarantee: 30 days refill\r\n- Speed about 1k - 5k/day for now\r\n\r\nNOTE :\r\n- Embed : disabled will not work\r\n- Premiere videos will not work\r\n- Live streamed videos will not work\r\n- Copy right content will not work', 100, 1000000, 50000, 50000, 15000, 'Aktif', 1687, 'MEDANPEDIA', 'Sosial Media'),
(2548, 136, 'Facebook Followers / Friends', 'Facebook Follower  Profile indonesia MP 5 ', 'No refill', 50, 100, 72000, 72000, 15000, 'Aktif', 1688, 'MEDANPEDIA', 'Sosial Media'),
(2549, 137, 'Youtube Views', 'Youtube Views MP 1 [ 50k-100k/day ] [ 20 Days Refill ] INSTANT', 'Instant Start\r\n1-3mins Retention\r\n50k-100k/day\r\n20 Days Refill', 100, 100000000, 47000, 47000, 15000, 'Aktif', 1705, 'MEDANPEDIA', 'Sosial Media'),
(2550, 138, 'Instagram Followers Indonesia', 'Instagram Followers Indonesia Server 25 MAX 50K PERSUBMIT', 'Instan', 100, 50000, 40000, 40000, 15000, 'Aktif', 1709, 'MEDANPEDIA', 'Sosial Media'),
(2551, 139, 'Youtube Views', 'Youtube Views MP 4 [ 10k/day ] [ Lifetime Refill ] â™»ï¸', '- Instant', 500, 1000000, 39000, 39000, 15000, 'Aktif', 1710, 'MEDANPEDIA', 'Sosial Media'),
(2552, 140, 'Youtube Views', 'Youtube Views MP 6 [ 20-50k/days ] [ 30 Days Refil ] ', 'fast', 100, 100000000, 46000, 46000, 15000, 'Aktif', 1716, 'MEDANPEDIA', 'Sosial Media'),
(2553, 141, 'Youtube Views', 'Youtube Views MP 7 [ Best Service ] [ Life Time Guaranteed ] ', 'INSTANT START\r\nGood For Ranking\r\nLife Time Guaranteed\r\nFast', 1000, 100000000, 37500, 37500, 15000, 'Aktif', 1717, 'MEDANPEDIA', 'Sosial Media'),
(2554, 142, 'Instagram Likes', 'Instagram Likes MP 20 [ Real Account ] [ Best Seller ] ', 'No garansi apaun yg terjadi\r\nKualitas bagus\r\ntidak drop paling kalo drop sekitar 10% ( kami tidak menjamin ini selamanya karna ig kadang update gk jelas )', 10, 20000, 22100, 22100, 15000, 'Aktif', 1719, 'MEDANPEDIA', 'Sosial Media'),
(2555, 143, 'Twitter Indonesia', 'Twitter Retweet REAL INDONESIA Fast', 'No garansi\r\nno kompline\r\nReal indo', 5, 100, 185000, 185000, 15000, 'Aktif', 1723, 'MEDANPEDIA', 'Sosial Media'),
(2556, 144, 'Twitter Indonesia', 'Twitter Favorite/Likes REAL INDONESIA Fast', 'No garansi\r\nno kompline\r\nReal indo', 5, 100, 50000, 50000, 15000, 'Aktif', 1724, 'MEDANPEDIA', 'Sosial Media'),
(2557, 145, 'Instagram Like Indonesia', 'Instagram Likes Indonesia MP 4 [ Langsung Input MAX 100K ] âš¡ï¸âš¡ï¸âš¡ï¸', 'Fast\r\nBot\r\nper order 25k', 100, 25000, 32000, 32000, 15000, 'Aktif', 1726, 'MEDANPEDIA', 'Sosial Media'),
(2558, 146, 'Instagram Followers [guaranteed]', 'Instagram Followers Refill S7 [ Refill 30 days ] [ baca Deskripsi ] Real Accounts Active', 'Hanya menerima followers di bawah 100\r\ndi atas 100 tidak kami terima\r\nmohon di pahami\r\nInstan', 10, 50000, 66000, 66000, 15000, 'Aktif', 1729, 'MEDANPEDIA', 'Sosial Media'),
(2559, 147, 'Youtube Subscribers', 'Youtube Subscribe BETA 4 [ No refill ] [ Cheap ] ', 'Tidak ada garansi \r\nAnda beli = berani ambil resiko\r\ntidak ada reffund untuk alasan apapun\r\nwaktu mulai 12 jam an', 20, 10000, 37000, 37000, 15000, 'Aktif', 1740, 'MEDANPEDIA', 'Sosial Media'),
(2560, 148, 'Instagram Followers Indonesia', 'Instagram Followers Indonesia Server 28  [ real aktif ] BONUS+++ ', 'Instan\r\nADA BONUS sekitar 5-10%', 100, 3000, 61000, 61000, 15000, 'Aktif', 1744, 'MEDANPEDIA', 'Sosial Media'),
(2561, 149, 'Instagram Like Indonesia', 'Instagram Likes Indonesia MP 6 [ MAX 5K ] FAST BONUS+++ âš¡ï¸âš¡ï¸âš¡ï¸', 'Real\r\nfast\r\ndapat bonus jika hoki', 100, 5000, 38000, 38000, 15000, 'Aktif', 1746, 'MEDANPEDIA', 'Sosial Media'),
(2562, 150, 'Instagram Followers Indonesia', 'Instagram Followers Indonesia Server 29  [ max 3k ]', 'proses fast\r\nmax 2 hari', 100, 3000, 57000, 57000, 15000, 'Aktif', 1747, 'MEDANPEDIA', 'Sosial Media'),
(2563, 151, 'Youtube View Jam Tayang', 'Youtube Views [ Jam Tayang 1000 jam ] [ Durasi Video 2 jam+ ] [ cek Deskripsi ] ', 'pesanan selesai dalam 3-10 hari\r\nwajib durasi 2jam lebih\r\n30 days Refill\r\nmNEDAPAT 1000 JAM', 1000, 1000, 240000, 240000, 15000, 'Aktif', 1748, 'MEDANPEDIA', 'Sosial Media'),
(2564, 152, 'Youtube Views', 'Youtube Views MP 8 [ 5 - 20 Min Retention ] [ Lifetime Guarantee ] ', 'HR', 100, 100000, 49000, 49000, 15000, 'Aktif', 1749, 'MEDANPEDIA', 'Sosial Media'),
(2565, 153, 'TikTok View/share/comment', 'TIK TOK View S10 [ super cheap ]', 'proses 0-10menit\r\n', 100, 5000000, 15020, 15020, 15000, 'Aktif', 1750, 'MEDANPEDIA', 'Sosial Media'),
(2566, 154, 'Instagram Likes', 'Instagram Likes MP 23 [ no Drop ]', 'sudah di tes dalam waktu 3 bulan tidak ada penurunan\r\njika ada update tiba tiba dan like turun\r\ntidak ada garansi', 10, 5000, 24000, 24000, 15000, 'Aktif', 1751, 'MEDANPEDIA', 'Sosial Media'),
(2567, 155, 'Facebook Post Likes / Comments / Shares', 'Facebook Photo / Post Likes Indonesia', 'Instan', 50, 100, 47000, 47000, 15000, 'Aktif', 1752, 'MEDANPEDIA', 'Sosial Media'),
(2568, 156, 'Facebook Post Likes / Comments / Shares', 'Facebook Photo / Post Likes MP5 [ 30 days Refill ] [max1k]', 'Murah', 20, 1000, 27600, 27600, 15000, 'Aktif', 1753, 'MEDANPEDIA', 'Sosial Media'),
(2569, 157, 'Instagram Likes', 'Instagram Likes MP 24 [ Less Drop ] ', 'Drop hanya 5-10%\r\nNo Garansi', 50, 10000, 20000, 20000, 15000, 'Aktif', 1754, 'MEDANPEDIA', 'Sosial Media'),
(2570, 158, 'Instagram Like Indonesia', 'Instagram Likes + Reels  Indonesia MP 7 [ MAX 5K ] [ real aktif ] FAST MURAH', 'Drop kecil banget\r\nreal \r\n\r\nmax db 5000', 50, 1000, 34000, 34000, 15000, 'Aktif', 1760, 'MEDANPEDIA', 'Sosial Media'),
(2571, 159, 'Instagram Followers [guaranteed]', 'Instagram Followers Refill S8 [ Refill 30D] [ max 5k ] HQ FAST', 'DROP KEMUNGKINAN Hanya 10%\r\nmulai 0-1jam', 100, 5000, 39500, 39500, 15000, 'Aktif', 1761, 'MEDANPEDIA', 'Sosial Media'),
(2572, 160, 'TikTok INDONESIA', 'TikTok Views Indonesia no drop', 'Pasif indo\r\ninstan\r\nlambat jika antrian panjang', 100, 10000, 19000, 19000, 15000, 'Aktif', 1764, 'MEDANPEDIA', 'Sosial Media'),
(2573, 161, 'TikTok INDONESIA', 'TikTok Share Indonesia no drop [ VIRAL ]', 'instan\r\nlambat jika antrian panjang\r\ninsyaallah fyp', 50, 20000, 45000, 45000, 15000, 'Aktif', 1766, 'MEDANPEDIA', 'Sosial Media'),
(2574, 162, 'Youtube Views', 'Youtube Views MP 9  [ 30 Days Refill] [ Recommended ] âš¡', 'instan \r\n10-30K per day', 100, 20000000, 37000, 37000, 15000, 'Aktif', 1767, 'MEDANPEDIA', 'Sosial Media'),
(2575, 163, 'Instagram Followers Indonesia', 'Instagram Followers Indonesia S2 [ REAL AKTIF ] [ cheapest ] [ Fast ] ', 'Proses sebelum 1 hari clear\r\nmax 2 hari jika bener bener padat', 100, 500, 26200, 26200, 15000, 'Aktif', 1772, 'MEDANPEDIA', 'Sosial Media'),
(2576, 164, 'Instagram TV', 'Instagram TV Like Server 4 [ BOT ] [ HQ ]', ' Instant\r\nno garansi ', 10, 10000, 26000, 26000, 15000, 'Aktif', 1773, 'MEDANPEDIA', 'Sosial Media'),
(2577, 165, 'Instagram TV', 'Instagram TV Like Server 5 [ Instan ]', 'instan', 100, 15000, 21300, 21300, 15000, 'Aktif', 1774, 'MEDANPEDIA', 'Sosial Media'),
(2578, 166, 'Instagram Followers Indonesia Guaranted/Refill', 'Instagram Followers Indonesia Guaranted 1 [ FEMALE ] [ Refill 7 days 1x ]', 'Dilarang private/ganti username/batas usia maka langsung sukses tanpa reffund\r\nklaim garansi jika drop 15%\r\nhanya bisa sekali refill', 100, 5000, 51000, 51000, 15000, 'Aktif', 1775, 'MEDANPEDIA', 'Sosial Media'),
(2579, 167, 'Instagram Followers Indonesia Guaranted/Refill', 'Instagram Followers Indonesia Guaranted 2 [ REAL ] [ Refill 7 days 1x ] ', 'Dilarang private/ganti username/batas usia maka langsung sukses tanpa reffund\r\nklaim garansi jika drop 15%\r\nhanya bisa sekali refill', 100, 5000, 60000, 60000, 15000, 'Aktif', 1776, 'MEDANPEDIA', 'Sosial Media'),
(2580, 168, 'Instagram Followers Indonesia Guaranted/Refill', 'Instagram Followers Indonesia Guaranted 3 [ FEMALE ] [ Refill 30 days ] ', 'bisa berkali2 refill setiap memenuhi ketentuan refill bisa terus sampe 30 hari\r\nDilarang private/ganti username/batas usia maka refill Hangus\r\n- Garansi 30 hari apabila drop lebih dari 15%\r\n- Waktu Proses Refill 1-2hari kerja', 100, 5000, 90000, 90000, 15000, 'Aktif', 1777, 'MEDANPEDIA', 'Sosial Media'),
(2581, 169, 'Instagram Followers Indonesia Guaranted/Refill', 'Instagram Followers Indonesia Guaranted 4 [ REAL ] [ Refill 30 days ] ', 'bisa berkali2 refill setiap memenuhi ketentuan refill bisa terus sampe 30 hari\r\nDilarang private/ganti username/batas usia maka refill Hangus\r\n- Garansi 30 hari apabila drop lebih dari 15%\r\n- Waktu Proses Refill 1-2hari kerja', 100, 5000, 95000, 95000, 15000, 'Aktif', 1778, 'MEDANPEDIA', 'Sosial Media'),
(2582, 170, 'Instagram Followers Indonesia Guaranted/Refill', 'KHUSUS REFILL LAYANAN Instagram Followers Indonesia Guaranted', 'bukan untuk di order\r\nmasukin username yg mau di refill jangan id order!!', 1, 1, 15000, 15000, 15000, 'Aktif', 1779, 'MEDANPEDIA', 'Sosial Media'),
(2583, 171, 'Instagram Views', 'instagram view MP 4 [ Recommended ]âš¡', ' 1 Million/ Hour\r\nStart 0-10 minutes', 100, 10000000, 15300, 15300, 15000, 'Aktif', 1785, 'MEDANPEDIA', 'Sosial Media'),
(2584, 172, 'Instagram Likes', 'Instagram Likes MP 25 [ Lifetime refill ] Real', 'jika delay bisa request canceled\r\nproses mulai  0-2jam', 50, 10000, 41000, 41000, 15000, 'Aktif', 1788, 'MEDANPEDIA', 'Sosial Media'),
(2585, 173, 'Instagram Likes', 'Instagram Likes MP 26 [ No drop ] Real ', 'High Quality\r\nNo drop, jika drop mungkin hanya 10% buat sekarang', 100, 15000, 27000, 27000, 15000, 'Aktif', 1789, 'MEDANPEDIA', 'Sosial Media'),
(2586, 174, 'Facebook Page / Website - Likes / Stars', 'Facebook Page Followers Server 1 [Low Quality] NO REFILL', ' INSTANT START\r\n1K PER DAY\r\nNO REFILL', 20, 2000, 42000, 42000, 15000, 'Aktif', 1792, 'MEDANPEDIA', 'Sosial Media'),
(2587, 175, 'Facebook Page / Website - Likes / Stars', 'Facebook Page Followers Server 2 [30 days refill] ', 'waktu mulai 0-6 jam\r\n ', 100, 25000, 84000, 84000, 15000, 'Aktif', 1793, 'MEDANPEDIA', 'Sosial Media'),
(2588, 176, 'Instagram Followers [ No Refill ]', 'Instagram Followers S7 [REAL ] INSTAN', 'real 70%\r\nDrop kemungkinan 20%\r\njika drop banyak tetap no refill\r\nbisa aja drop kapan aja tetap no kompline', 20, 5000, 27500, 27500, 15000, 'Aktif', 1796, 'MEDANPEDIA', 'Sosial Media'),
(2589, 177, 'Instagram Followers [ No Refill ]', 'Instagram Followers S9 [ Cheapest Market ] ', 'no kompline\r\nhigh drop', 10, 15000, 17500, 17500, 15000, 'Aktif', 1798, 'MEDANPEDIA', 'Sosial Media'),
(2590, 178, 'Youtube Likes / Dislikes / Shares / Comment', 'Youtube Dislikes MP 3 Cheapest [ 30days refill ]', 'waktu mulai 0-6 jam\r\ninstan', 100, 20000, 105000, 105000, 15000, 'Aktif', 1799, 'MEDANPEDIA', 'Sosial Media'),
(2591, 179, '- PROMO - ON OFF', 'Instagram Likes PROMO 1 [ Fast ][ REAL Likes ]', 'KAPAN AJA BISA OFF JANGAN KOMPLINE\r\nMURAHHHHH', 50, 5000, 22500, 22500, 15000, 'Aktif', 1800, 'MEDANPEDIA', 'Sosial Media'),
(2592, 180, '- PROMO - ON OFF', 'Instagram Likes PROMO 2 [ Cheap ] [ no refill ]', 'KAPAN AJA BISA OFF JANGAN KOMPLINE \r\nMURAHHHHH\r\n', 10, 15000, 15530, 15530, 15000, 'Aktif', 1801, 'MEDANPEDIA', 'Sosial Media'),
(2593, 181, 'Youtube Views', 'Youtube Views MP 13 Good for Ranking [ lifetime refill ]', 'Instant\r\n50k-100k/day\r\nRetention - 0-2 minute\r\nJika status partial reffill tidak berfungsi jika drop\r\n', 1000, 10000000, 49000, 49000, 15000, 'Aktif', 1806, 'MEDANPEDIA', 'Sosial Media');
INSERT INTO `layanan_sosmed` (`id`, `service_id`, `kategori`, `layanan`, `catatan`, `min`, `max`, `harga`, `harga_api`, `profit`, `status`, `provider_id`, `provider`, `tipe`) VALUES
(2594, 182, 'Instagram Likes', 'Instagram Likes MP 27 [10K] cheap', 'no garansi\r\nspeed 300/jam\r\nmulai 0-2 jam', 10, 10000, 20200, 20200, 15000, 'Aktif', 1807, 'MEDANPEDIA', 'Sosial Media'),
(2595, 183, '- PROMO - ON OFF', 'Instagram Followers PROMO 3 [ BIG DROPS - BOT ] ', 'KAPAN AJA BISA OFF \r\nJANGAN KOMPLINE \r\nMURAHHHHH\r\nNO REFILL', 50, 10000, 17700, 17700, 15000, 'Aktif', 1808, 'MEDANPEDIA', 'Sosial Media'),
(2596, 184, 'Instagram Likes', 'Instagram Likes MP 28 [ real ] cheapeast', 'No refill\r\n', 10, 5000, 18700, 18700, 15000, 'Aktif', 1810, 'MEDANPEDIA', 'Sosial Media'),
(2597, 185, 'Twitter Views & Impressions', 'Twitter Views Server 3 [ FAST - Max 1M ] ', '100k-200k/hour', 100, 10000000, 17000, 17000, 15000, 'Aktif', 1811, 'MEDANPEDIA', 'Sosial Media'),
(2598, 186, 'Instagram Followers [guaranteed]', 'Instagram Followers Refill S14 [ Auto Refill  30 Days ]', 'Auto Refill 30 days\r\n', 20, 10000, 38000, 38000, 15000, 'Aktif', 1814, 'MEDANPEDIA', 'Sosial Media'),
(2599, 187, 'Youtube View Jam Tayang', 'Youtube Views [ Jam Tayang CLEAR 7 HARI ] [ Durasi Video 1 jam+ ] [ cek Deskripsi ] ', 'jumlah pesan = jam tayang yang kamu dapat\r\n\r\n- Eksklusif di MEDANPEDIA\r\n- Silakan memesan 4000 tampilan untuk mendapatkan waktu menonton 4kh\r\n- SIAP DALAM 7 HARI 4 JAM\r\n- Garansi: refill 30 Hari\r\n\r\n\r\nCATATAN :\r\n- Harap cantumkan video yang memiliki panjang hanya 1 jam + (Jika tidak sampai 1 jam , pesanan masih berjalan tetapi waktu menonton tidak akan cukup, kami tidak dapat melakukan refill / reffund Dana / canceled / Partial dalam kasus ini)\r\n- Jika view lama Anda turun di bawah jumlah mulai, maka kami tidak dapat Refill / Refund / Cancel / Membatalkan sebagian. ( lebih baik menggunakan video dengan view yg sedikit/baru )\r\n- Silakan gunakan video tanpa tampilan interleaving alami (Jika Anda secara bersamaan mendapatkan view alami (ditonton orang lain selain sistem kami ) saat menjalankan waktu jam tayang, Anda tidak akan mendapatkan cukup waktu jam tayang.)\r\n- Jika video Dihapus, Ditolak, Privat sementara kami menambahkan waktu tontonan atau setelah selesai, maka kami tidak dapat refill / reffund Dana / canceled / Partial.', 100, 4000, 135000, 135000, 15000, 'Aktif', 1815, 'MEDANPEDIA', 'Sosial Media'),
(2600, 188, 'Instagram Views', 'instagram view MP 5 [ CHEAPEST ] FAST  ', 'INSTANT', 200, 10000000, 15200, 15200, 15000, 'Aktif', 1823, 'MEDANPEDIA', 'Sosial Media'),
(2601, 189, 'Instagram Likes', 'Instagram Likes MP 29 [ Real ] cheapeast ', 'No garansi\r\nfast', 100, 5000, 22000, 22000, 15000, 'Aktif', 1825, 'MEDANPEDIA', 'Sosial Media'),
(2602, 190, 'Instagram Story Views', 'Instagram - Story Views S5 [ All Story Views ] WORK', 'fast', 100, 50000, 16150, 16150, 15000, 'Aktif', 1826, 'MEDANPEDIA', 'Sosial Media'),
(2603, 191, 'Instagram Story / Impressions / Saves / Profile Vi', 'Instagram POST Impressions S1', 'Cheapest\r\nINSTANT', 100, 10000000, 15300, 15300, 15000, 'Aktif', 1827, 'MEDANPEDIA', 'Sosial Media'),
(2604, 192, 'Instagram Story / Impressions / Saves / Profile Vi', 'Instagram Impressions  + Reach Max 25k INSTAN', 'Real HQ\r\nfast', 50, 250000, 15350, 15350, 15000, 'Aktif', 1828, 'MEDANPEDIA', 'Sosial Media'),
(2605, 193, 'Youtube Likes / Dislikes / Shares / Comment', 'Youtube Video Custom Comments MP 1 Cheapest ', 'Cheapest\r\nmulai proses 0-24jam', 10, 5000, 134000, 134000, 15000, 'Aktif', 1832, 'MEDANPEDIA', 'Sosial Media'),
(2606, 194, 'Instagram Followers [guaranteed]', 'Instagram Followers Refill S15 [ Refill 99 Days ] â™»ï¸', '1K-2K/day', 10, 500000, 41800, 41800, 15000, 'Aktif', 1833, 'MEDANPEDIA', 'Sosial Media'),
(2607, 195, 'Youtube View Target Negara', 'Youtube  GEO  views Lifetime Refill [ Indonesia ][ Recommended ]', '- Mulai Instan atau 0-12H\r\n- Kecepatan 10K - 200k / D\r\n- Sangat Direkomendasikan - Tidak Pernah Stuck Order!\r\n- Cocok untuk Semua Panjang Video\r\n- Tampilan Peringkat Bertarget Murni Terbaik !!! Ditambah Tampilan REAL Aktif Sumber Stabil !!\r\n- Tampilan dapat mencakup Keterlibatan Pengguna NYATA!\r\n\r\nâ­â­â­ CATATAN:\r\n- Izinkan penyematan dan pastikan video Anda benar-benar tidak dibatasi, untuk menggunakan layanan ini. Untuk mendapatkan isi ulang, harap tetap nonaktifkan monetisasi sampai pesanan Anda benar-benar diisi ulang dan jangan gabungkan pandangan kami dengan penyedia lain (sebenarnya, jangan mendorong penayangan pihak ketiga ke video 7 hari sebelum dan setelah menerima penayangan kami ).\r\n- Tidak Dapat Membatalkan Atau Mengembalikan Dana Setelah Memulai Pesanan Dengan Alasan Apa Pun !!', 1000, 1000000000, 56000, 56000, 15000, 'Aktif', 1834, 'MEDANPEDIA', 'Sosial Media'),
(2608, 196, 'Youtube View Target Negara', 'Youtube views No Refill [ Indonesia ] ', 'mulai 0-24jam\r\n10K/Day', 1000, 1000000, 37500, 37500, 15000, 'Aktif', 1835, 'MEDANPEDIA', 'Sosial Media'),
(2609, 197, 'Instagram Comments', 'Instagram 5 Comments random [ dari Akun dengan followers 10k + ]', 'instan\r\nmendapat 5 komentar', 1000, 1000, 47000, 47000, 15000, 'Aktif', 1836, 'MEDANPEDIA', 'Sosial Media'),
(2610, 198, 'Instagram Comments', 'Instagram Comments Costum [ dari Akun dengan followers 15k + ] [ Rp2.800 ]', 'Proses slow\r\nwaktu mulai 0-48jam', 1, 30, 2815000, 2815000, 15000, 'Aktif', 1837, 'MEDANPEDIA', 'Sosial Media'),
(2611, 199, 'Instagram Comments', 'Instagram Comments Costum [ dari Akun dengan followers 10k + ] [ Rp11.000 ] ', 'lebih fast dari id layanan 1837\r\n', 1, 10, 11015000, 11015000, 15000, 'Aktif', 1838, 'MEDANPEDIA', 'Sosial Media'),
(2612, 200, 'Instagram Comments', 'Instagram  Comments Custom [Account Verif/centang biru] [ Rp25.500 ] ', 'lambat\r\nnon drop', 1, 10, 25515000, 25515000, 15000, 'Aktif', 1839, 'MEDANPEDIA', 'Sosial Media'),
(2613, 201, 'Instagram Comments', 'Instagram  Comments Random [Account Verif/centang biru] [ Rp21.000 ] ', 'lambat\r\nnon drop', 1, 10, 21015000, 21015000, 15000, 'Aktif', 1840, 'MEDANPEDIA', 'Sosial Media'),
(2614, 202, 'Instagram Comments', 'Instagram 5 Comments random [ dari Akun dengan followers 1juta+ ] ', 'waktu mulai 0-24 jam\r\ndapat 5 komentar', 1000, 1000, 68000, 68000, 15000, 'Aktif', 1841, 'MEDANPEDIA', 'Sosial Media'),
(2615, 203, 'Instagram Followers [ No Refill ]', 'Instagram Followers S14 [ LESS DROP ] [ CHEAP ]', 'no garansi followers turun\r\nreal', 100, 1000, 23600, 23600, 15000, 'Aktif', 1842, 'MEDANPEDIA', 'Sosial Media'),
(2616, 204, 'Instagram Followers [ No Refill ]', 'Instagram Followers S15 [ LESS DROP ] [ REAL ] ', '75% real\r\nno garansi', 20, 5000, 26500, 26500, 15000, 'Aktif', 1843, 'MEDANPEDIA', 'Sosial Media'),
(2617, 205, 'Instagram Followers [ No Refill ]', 'Instagram Followers S16 [ 10K ] [ REAL ] ', 'START TIME 0-1H\r\n5K/DAY\r\nno garansi', 20, 10000, 31000, 31000, 15000, 'Aktif', 1844, 'MEDANPEDIA', 'Sosial Media'),
(2618, 206, '- PROMO - ON OFF', 'Instagram Followers PROMO 4 [ Cheap - BOT ] ', 'HIGH DROP\r\nFAST', 50, 10000, 18300, 18300, 15000, 'Aktif', 1845, 'MEDANPEDIA', 'Sosial Media'),
(2619, 207, 'Instagram Followers [ No Refill ]', 'Instagram Followers Server 2 [ No Refill] [ BOT ] ', 'INSTANT\r\nkadang sukses pesanan tidak full masuj\r\nno kompline', 10, 10000, 19500, 19500, 15000, 'Aktif', 1848, 'MEDANPEDIA', 'Sosial Media'),
(2620, 208, 'Youtube Subscribers', 'Youtube Subscribe SERVER 6 [ 30 days guarantee ] 20-100/day', 'Speed - 20-100/day\r\n30 Days guarantee\r\nrefill only no reffund', 5, 2000, 351000, 351000, 15000, 'Aktif', 1850, 'MEDANPEDIA', 'Sosial Media'),
(2621, 209, 'Instagram Followers [guaranteed]', 'Instagram Followers Refill S16 [ Refill 30Days ] [ REAL HQ ]', 'instan\r\nwaktu mulai 0-6 jam\r\n', 10, 20000, 43000, 43000, 15000, 'Aktif', 1851, 'MEDANPEDIA', 'Sosial Media'),
(2622, 210, 'Instagram Followers [ No Refill ]', 'Instagram Followers Server 3 [ No Refill] [ REAL HQ ]', 'Instant \r\n High Quality\r\n3k-5k/hari', 50, 5000, 22700, 22700, 15000, 'Aktif', 1852, 'MEDANPEDIA', 'Sosial Media'),
(2623, 211, 'Twitter Views & Impressions', 'Twitter Impressions Server 2 [5M] ', 'fast', 100, 10000000, 19300, 19300, 15000, 'Aktif', 1857, 'MEDANPEDIA', 'Sosial Media'),
(2624, 212, 'Instagram Story Views', 'Instagram - Story Views S6 [ All Story Views ] INSTANT', 'Bot HQ\r\nINSTANT', 250, 5000, 15900, 15900, 15000, 'Aktif', 1862, 'MEDANPEDIA', 'Sosial Media'),
(2625, 213, 'Youtube Views', 'Youtube Views MP 16 [  Lifetime Guarantee ] NonDrop', 'Instant Start\r\nSpeed 20k-30k\r\nInstant Start\r\nVery Fast Views\r\nLifetime Guarantee', 1000, 30000, 38200, 38200, 15000, 'Aktif', 1867, 'MEDANPEDIA', 'Sosial Media'),
(2626, 214, 'Twitter Views & Impressions', 'Twitter Views Server 4 [ SUPERFAST - Max 100M ] ', '1Juta/hour', 100, 100000000, 16300, 16300, 15000, 'Aktif', 1870, 'MEDANPEDIA', 'Sosial Media'),
(2627, 215, 'TikTok Followers', 'TIK TOK FOLLOWERS Server 13 [ 30 Days Refil ] ', 'waktu mulai  0-12 jam\r\nbahkan bisa lebih karna tiktok blm stabil\r\n30 Days Refill ', 100, 50000, 53000, 53000, 15000, 'Aktif', 1873, 'MEDANPEDIA', 'Sosial Media'),
(2628, 216, 'TikTok Likes', 'TIK TOK Likes MP 2 [ HQ ] [ NON-DROP] ', 'NON-DROP & HQ\r\n1000/hours\r\nwaktu mulai 0-12 jam', 20, 5000, 63000, 63000, 15000, 'Aktif', 1876, 'MEDANPEDIA', 'Sosial Media'),
(2629, 217, 'TikTok View/share/comment', 'TIK TOK Share MP 1 [ Real Share ] INSTANT', 'THE CHEAPEST SERVICE\r\nREAL PROFILE', 10, 100000, 40000, 40000, 15000, 'Aktif', 1877, 'MEDANPEDIA', 'Sosial Media'),
(2630, 218, 'Twitter Favorites/Like', 'Twitter Likes Server 1 [ No refill ] REAL', 'kapan aja bisa drop\r\nAnda membeli dengan risiko Anda sendiri\r\n', 10, 1000, 27800, 27800, 15000, 'Aktif', 1881, 'MEDANPEDIA', 'Sosial Media'),
(2631, 219, 'Twitter Favorites/Like', 'Twitter Likes Server 2 [ No Drop ] INSTANT ', 'Start Time: Instant\r\nSpeed: Complete 1K in 10 minutes\r\nRefill: Real Likes - Non Drop', 50, 500, 120000, 120000, 15000, 'Aktif', 1882, 'MEDANPEDIA', 'Sosial Media'),
(2632, 220, 'Twitter Favorites/Like', 'Twitter Likes Server 3 [ No Drop ] Max 10K', '30 days refill\r\nREAL', 50, 15000, 95000, 95000, 15000, 'Aktif', 1883, 'MEDANPEDIA', 'Sosial Media'),
(2633, 221, 'Twitter Favorites/Like', 'Twitter Likes Server 4 [ No Refill ] Max 10K ', 'no refill\r\nspeed 300/day', 10, 800, 30000, 30000, 15000, 'Aktif', 1884, 'MEDANPEDIA', 'Sosial Media'),
(2634, 222, 'Instagram Followers [guaranteed]', 'Instagram Followers Refill S17 [ auto Refill 30Day ]â™»ï¸', 'auto refill dalam 2 minggu\r\nlebih dari itu req refill ke tiket\r\n', 100, 20000, 77000, 77000, 15000, 'Aktif', 1885, 'MEDANPEDIA', 'Sosial Media'),
(2635, 223, 'Instagram Followers [guaranteed]', 'Instagram Followers Refill S18 [ Refill 60Day ] INSTANT ', 'High Quality\r\n1k - 2k Per Day Speed\r\nLow Drop\r\n', 20, 100000, 47000, 47000, 15000, 'Aktif', 1886, 'MEDANPEDIA', 'Sosial Media'),
(2636, 224, 'Instagram Followers [guaranteed]', 'Instagram Followers Refill S19 [ Real,active ] [ Refill 30D ][ Best Market ]âš¡ï¸â­â­â™»ï¸', '- waktu mulai : 0 - 20 mins \r\n- Kecepatan 2 -3k/hari \r\n- 100% Real Bule \r\n- Less DROP', 20, 100000, 62200, 62200, 15000, 'Aktif', 1887, 'MEDANPEDIA', 'Sosial Media'),
(2637, 225, 'Instagram Followers [guaranteed]', 'Instagram Followers Refill S20 [ Refill 1 TAHUN ][ NODROP ][ BETA ]âš¡ï¸â­â­ ', 'speed berubah ubah karna masih tahap BETA\r\nBisa request refill selama setahun jika ada drop\r\n5K/Day', 151, 300000, 109000, 109000, 15000, 'Aktif', 1888, 'MEDANPEDIA', 'Sosial Media'),
(2638, 226, 'Youtube Subscribers', 'Youtube Subscribe SERVER 7 Real USA', '30-50days \r\nguaranted 30days', 5, 1500, 345000, 345000, 15000, 'Aktif', 1889, 'MEDANPEDIA', 'Sosial Media'),
(2639, 227, 'Instagram Followers [guaranteed]', 'Instagram Followers Refill S21 [ AUTO refill 30D ][ HQ ]', '- High-Quality Instagram followers\r\n- Followers Unfollow 5%\r\n- Speed 2k-5k per days', 10, 5000, 32000, 32000, 15000, 'Aktif', 1892, 'MEDANPEDIA', 'Sosial Media'),
(2640, 228, 'TikTok View/share/comment', 'TIK TOK View S13 [ WORK AFTER UPDATE ] INSTAN', 'Fast\r\nmin 500', 1000, 2000000, 16500, 16500, 15000, 'Aktif', 1896, 'MEDANPEDIA', 'Sosial Media'),
(2641, 229, 'Youtube View Jam Tayang', 'Youtube Views Jam  Tayang [ NO REFILL ]  [ Durasi Video 1 jam+ ] [ cek Deskripsi ] ', 'Start time- 0-1 hour\r\nFinish within 48 Hours[ Sometimes take time upto 4 days]\r\nNo Refill, No Refund\r\nViews and hours may drop next day or next month\r\n[ No complains accepted like views did not come etc]', 100, 8000, 85000, 85000, 15000, 'Aktif', 1898, 'MEDANPEDIA', 'Sosial Media'),
(2642, 230, 'Instagram Comments', 'Instagram Custom Comments Indonesia Instan', 'Proses fast', 3, 1000, 415000, 415000, 15000, 'Aktif', 1901, 'MEDANPEDIA', 'Sosial Media'),
(2643, 231, '- PROMO - ON OFF', 'Instagram Likes PROMO 3 [ Real ] [Cheap ] ', 'High Quality\r\nwaktu mulai 0-3 jam', 10, 5000, 17850, 17850, 15000, 'Aktif', 1902, 'MEDANPEDIA', 'Sosial Media'),
(2644, 232, 'Instagram Followers Indonesia', 'Instagram Followers Indonesia S3  [ MAX 100k ] âš¡ï¸âš¡ï¸âš¡ï¸', 'proses input 10k hanya 2 harian\r\nmax 100k', 100, 25000, 42000, 42000, 15000, 'Aktif', 1903, 'MEDANPEDIA', 'Sosial Media'),
(2645, 233, 'Shopee/Tokopedia/Bukalapak', 'Shopee Followers Indonesia  [ Max 25K] NON DROP', 'Masukkan username shopee', 100, 25000, 40000, 40000, 15000, 'Aktif', 1904, 'MEDANPEDIA', 'Sosial Media'),
(2646, 234, 'Shopee/Tokopedia/Bukalapak', 'Shopee Likes Produk Indonesia [ Max 20K] NON DROP ', 'link produk', 50, 20000, 27000, 27000, 15000, 'Aktif', 1905, 'MEDANPEDIA', 'Sosial Media'),
(2647, 235, 'Facebook Page / Website - Likes / Stars', 'Facebook Page Followers Server 3 [60 days refill] ', 'waktu mulai 1 - 2 hours\r\n3000 - 5000 / day\r\nHigh quality\r\n60 days Guaranteed', 20, 120000, 76000, 76000, 15000, 'Aktif', 1906, 'MEDANPEDIA', 'Sosial Media'),
(2648, 236, 'TikTok View/share/comment', 'TIKTOK View Server 1 EMERGENCY', 'EMERGENCY\r\nfast', 100, 5000000, 19900, 19900, 15000, 'Aktif', 1907, 'MEDANPEDIA', 'Sosial Media'),
(2649, 237, 'Instagram Followers [guaranteed]', 'Instagram Followers Refill S22 [ AUTO refill 30D ]â­â­', '30 days guarantee\r\nauto refill\r\n1-3k/day', 10, 100000, 28000, 28000, 15000, 'Aktif', 1914, 'MEDANPEDIA', 'Sosial Media'),
(2650, 238, 'Instagram Followers Indonesia', 'Instagram Followers Indonesia S4 [ REAL AKTIF ] [ FAST ] [ MAX4K ] âš¡ï¸', 'proses max 1x24 jam\r\nfast', 100, 4000, 45000, 45000, 15000, 'Aktif', 1916, 'MEDANPEDIA', 'Sosial Media'),
(2651, 239, 'Instagram Likes', 'Instagram Likes MP 31  cheapeast ', 'NO REFILL\r\n', 10, 10000, 17000, 17000, 15000, 'Aktif', 1918, 'MEDANPEDIA', 'Sosial Media'),
(2652, 240, 'Instagram Followers [guaranteed]', 'Instagram Followers Refill S23 [ refill button 99D ]â™»ï¸', 'proses refill 2-3hari\r\n', 10, 500000, 28500, 28500, 15000, 'Aktif', 1923, 'MEDANPEDIA', 'Sosial Media'),
(2653, 241, 'Instagram Followers [guaranteed]', 'Instagram Followers Refill S24 [ Refill 1 TAHUN ][ NODROP ]âš¡ï¸â­â­â™»ï¸ ', 'refill button\r\nwaktu mulai 0-12 jam', 100, 100000, 125000, 125000, 15000, 'Aktif', 1924, 'MEDANPEDIA', 'Sosial Media'),
(2654, 242, 'Instagram Followers [ No Refill ]', 'Instagram Followers Server 5 [ No Refill ] Real', 'waktu mulai 40 menitan', 100, 1000, 24900, 24900, 15000, 'Aktif', 1925, 'MEDANPEDIA', 'Sosial Media'),
(2655, 243, 'Instagram Followers [guaranteed]', 'Instagram Followers Refill S25 [ NON DROP ] real followers â™»ï¸', 'NON-drop\r\n30 days refill\r\nKualitas Bagus', 1000, 100000, 40000, 40000, 15000, 'Aktif', 1927, 'MEDANPEDIA', 'Sosial Media'),
(2656, 244, 'Instagram Followers [guaranteed]', 'Instagram Followers Refill S25 [ AUTO REFILL 30Days ] HQ', 'hanya bisa refill jika yg turun dari kami\r\nAuto Refill\r\nInstan', 20, 100000, 27500, 27500, 15000, 'Aktif', 1928, 'MEDANPEDIA', 'Sosial Media'),
(2657, 245, 'Youtube Views', 'Youtube Views MP 24 [ Non Drop ] Lifetime ', '- Instant Start\r\n- Speed 10k - 20k /day For NOW\r\n- NON DROP', 100, 100000000, 42900, 42900, 15000, 'Aktif', 1929, 'MEDANPEDIA', 'Sosial Media'),
(2658, 246, 'Instagram Followers [guaranteed]', 'Instagram Followers Refill S26 [ AUTO REFILL 30Days ] MAX 30k', 'Instant\r\n30 days auto refill\r\nSpeed 5k/D', 50, 30000, 25500, 25500, 15000, 'Aktif', 1933, 'MEDANPEDIA', 'Sosial Media'),
(2659, 247, 'Youtube View Jam Tayang', 'Youtube Views Jam Tayang [ NO REFILL ] [ Durasi Video 1 jam+ ] [ cek Deskripsi ] CHEAP', 'No garansi\r\nkecepatan 500jam/hari\r\nTidak ada kompline', 100, 5000, 70000, 70000, 15000, 'Aktif', 1934, 'MEDANPEDIA', 'Sosial Media'),
(2660, 248, 'TikTok Followers', 'TIK TOK FOLLOWERS Server 16 [ Real ] [ NON DROP ]', 'Instant\r\nfast\r\nNon Drop', 100, 5000, 24000, 24000, 15000, 'Aktif', 1936, 'MEDANPEDIA', 'Sosial Media'),
(2661, 249, 'Youtube Views', 'Youtube Views MP 24 [ Lifetime ] Cheap WORLD', 'Recommended Service\r\nworking Fast', 500, 1000000, 33000, 33000, 15000, 'Aktif', 1937, 'MEDANPEDIA', 'Sosial Media'),
(2662, 250, '- PROMO - ON OFF', 'Instagram View PROMO 1 [ Cheapeast World ] ', 'waktu mulai 0-6 jam\r\nfast', 1000, 1000000, 15094, 15094, 15000, 'Aktif', 1940, 'MEDANPEDIA', 'Sosial Media'),
(2663, 251, 'Telegram', 'Telegram Post Views [500K] [Last 1 Post] ', 'Last 1 Post', 100, 500000, 15550, 15550, 15000, 'Aktif', 1942, 'MEDANPEDIA', 'Sosial Media'),
(2664, 252, 'Telegram', 'Telegram Post Views [500K] [Last 10 Post] ', 'Last 10 Post', 50, 500000, 16800, 16800, 15000, 'Aktif', 1943, 'MEDANPEDIA', 'Sosial Media'),
(2665, 253, 'Telegram', 'Telegram Post Views [500K] [Last 50 Post] ', 'Last 50 Post ', 50, 500000, 18500, 18500, 15000, 'Aktif', 1944, 'MEDANPEDIA', 'Sosial Media'),
(2666, 254, 'Telegram', 'Telegram Post Views [500K] [Last 100 Post] ', 'Last 100 Post', 50, 500000, 20300, 20300, 15000, 'Aktif', 1945, 'MEDANPEDIA', 'Sosial Media'),
(2667, 255, 'TikTok Followers', 'TIK TOK FOLLOWERS Server 19 [ 30 Days Refil ] max20k', 'proses lambat', 100, 20000, 21000, 21000, 15000, 'Aktif', 1949, 'MEDANPEDIA', 'Sosial Media'),
(2668, 256, 'Facebook Post Likes / Comments / Shares', 'Facebook Photo / Post Likes MP6 [ 30 days Refill ] [max10k] Instant ', 'Instant ', 10, 10000, 32400, 32400, 15000, 'Aktif', 1950, 'MEDANPEDIA', 'Sosial Media'),
(2669, 257, 'TikTok Followers', 'TIKTOK FOLLOWERS Server 20 [ No Refill ] SUPER FAST', '1k/Hrs', 10, 20000, 22000, 22000, 15000, 'Aktif', 1951, 'MEDANPEDIA', 'Sosial Media'),
(2670, 258, 'TikTok View/share/comment', 'TIKTOK View Server 4 SuperFast NEW', '1m/hour', 100, 1000000, 15400, 15400, 15000, 'Aktif', 1952, 'MEDANPEDIA', 'Sosial Media'),
(2671, 259, 'Instagram Likes', 'Instagram Likes MP 32 cheapeast world [ NO DROP ] FAST', 'Instant\r\nno drop\r\nTapi Jika drop gk ada reffund', 100, 50000, 16600, 16600, 15000, 'Aktif', 1953, 'MEDANPEDIA', 'Sosial Media'),
(2672, 260, 'Instagram Likes', 'Instagram Likes MP 33 cheapeast Real', 'Real\r\nNo garansi', 50, 5000, 16850, 16850, 15000, 'Aktif', 1954, 'MEDANPEDIA', 'Sosial Media'),
(2673, 261, 'Twitter Views & Impressions', 'Twitter Views Server 5 [ Cheap - Max 100k ] ', 'No REFILL\r\n100k/hour', 100, 150000, 16000, 16000, 15000, 'Aktif', 1960, 'MEDANPEDIA', 'Sosial Media'),
(2674, 262, 'Instagram Followers Indonesia', 'Instagram Followers Indonesia S5 [ REAL AKTIF ] [ Best Seller ] [ MAX2K ] ', 'Max 2 hari biasanya\r\nreal aktif\r\nfast\r\nbiasanya gk sampai sehari done', 100, 2000, 38000, 38000, 15000, 'Aktif', 1962, 'MEDANPEDIA', 'Sosial Media'),
(2675, 263, 'TikTok Likes', 'TikTok Likes MP 4 [ REAL ] [ NON DROP ] ', 'Instant\r\nsuper fast\r\nNon Drop', 100, 1000, 26500, 26500, 15000, 'Aktif', 1963, 'MEDANPEDIA', 'Sosial Media'),
(2676, 264, 'Telegram', 'Telegram - Channnel Members/Group Server 4 [ Max 15K] R10Days', 'Mulai: 1-30 Menit\r\nKecepatan: 5000 / hari\r\nRefill 10Days\r\nKualitas: Real\r\nJika Anda Telah Mengubah Tautan Atau Saluran Dihapus Tidak Akan Ada Pembatalan Pesanan', 500, 15000, 40000, 40000, 15000, 'Aktif', 1967, 'MEDANPEDIA', 'Sosial Media'),
(2677, 265, '- PROMO - ON OFF', 'Tiktok Views GRATIS', 'No kompline', 100, 1000, 15000, 15000, 15000, 'Aktif', 1968, 'MEDANPEDIA', 'Sosial Media'),
(2678, 266, 'Youtube Views', 'Youtube Views MP 25 [ Lifetime ] No Drop [ Best ]', 'working service\r\n10k-20k/day speed\r\n100% non-drop', 500, 10000000, 42000, 42000, 15000, 'Aktif', 1969, 'MEDANPEDIA', 'Sosial Media'),
(2679, 267, 'Instagram Followers [ No Refill ]', 'Instagram Followers Server 7 [ Real ] [ Fast ] Recomended', 'Real\r\nInstan', 50, 20000, 22700, 22700, 15000, 'Aktif', 1970, 'MEDANPEDIA', 'Sosial Media'),
(2680, 268, 'Clubhouse', 'Clubhouse Followers 100 User', 'mulai : 0-6 jam\r\nKecepatan : 0-12 jam\r\norder 1000 mendapatkan 100 user', 1000, 1000, 125000, 125000, 15000, 'Aktif', 1972, 'MEDANPEDIA', 'Sosial Media'),
(2681, 269, 'Clubhouse', 'Clubhouse Followers 250 User', 'mulai : 0-6 jam\r\nKecepatan : 0-12 jam\r\norder 1000 mendapatkan 250 user', 1000, 1000, 285000, 285000, 15000, 'Aktif', 1973, 'MEDANPEDIA', 'Sosial Media'),
(2682, 270, 'Clubhouse', 'Clubhouse Followers 500 User ', 'mulai : 0-6 jam\r\nKecepatan : 0-12 jam\r\norder 1000 mendapatkan 500 user', 1000, 1000, 495000, 495000, 15000, 'Aktif', 1974, 'MEDANPEDIA', 'Sosial Media'),
(2683, 271, 'Clubhouse', 'Clubhouse Room Visitor 100 User ', 'mulai : 0-6 jam\r\nKecepatan : 0-12 jam\r\norder 1000 mendapatkan 100 user', 1000, 1000, 190000, 190000, 15000, 'Aktif', 1975, 'MEDANPEDIA', 'Sosial Media'),
(2684, 272, 'Youtube View Jam Tayang', 'Youtube Views [ Jam Tayang CLEAR 15 HARI ] [ Durasi Video 1 jam+ ] [ cek Deskripsi ] ', 'jumlah pesan = jam tayang yang kamu dapat\r\n\r\n- Eksklusif di MEDANPEDIA\r\n- Silakan memesan 4000 tampilan untuk mendapatkan waktu menonton 4kh\r\n- SIAP DALAM 12-15 hari ( sebelum waktu 15 hari dilarang kompline )\r\n- Garansi: refill 30 Hari\r\n\r\n\r\nCATATAN :\r\n- Harap cantumkan video yang memiliki panjang hanya 1 jam + (Jika tidak sampai 1 jam , pesanan masih berjalan tetapi waktu menonton tidak akan cukup, kami tidak dapat melakukan refill / reffund Dana / canceled / Partial dalam kasus ini)\r\n- Jika view lama Anda turun di bawah jumlah mulai, maka kami tidak dapat Refill / Refund / Cancel / Membatalkan sebagian. ( lebih baik menggunakan video dengan view yg sedikit/baru )\r\n- Silakan gunakan video tanpa tampilan interleaving alami (Jika Anda secara bersamaan mendapatkan view alami (ditonton orang lain selain sistem kami ) saat menjalankan waktu jam tayang, Anda tidak akan mendapatkan cukup waktu jam tayang.)\r\n- Jika video Dihapus, Ditolak, Privat       kami menambahkan waktu tontonan atau setelah selesai, maka kami tidak dapat refill / reffund Dana / canceled / Partial.', 1000, 4000, 167000, 167000, 15000, 'Aktif', 1977, 'MEDANPEDIA', 'Sosial Media'),
(2685, 273, 'Youtube View Jam Tayang', 'Youtube Views [ Jam Tayang CLEAR 48 jam ] [ Durasi Video 1 jam+ ] [ cek Deskripsi ] ', 'jumlah pesan = jam tayang yang kamu dapat\r\n\r\n- Eksklusif di MEDANPEDIA\r\n- Silakan memesan 4000 tampilan untuk mendapatkan waktu menonton 4kh\r\n- SIAP DALAM 2 hari \r\n- Garansi: refill 30 Hari\r\n\r\n\r\nCATATAN :\r\n- Harap cantumkan video yang memiliki panjang hanya 1 jam + (Jika tidak sampai 1 jam , pesanan masih berjalan tetapi waktu menonton tidak akan cukup, kami tidak dapat melakukan refill / reffund Dana / canceled / Partial dalam kasus ini)\r\n- Jika view lama Anda turun di bawah jumlah mulai, maka kami tidak dapat Refill / Refund / Cancel / Membatalkan sebagian. ( lebih baik menggunakan video dengan view yg sedikit/baru )\r\n- Silakan gunakan video tanpa tampilan interleaving alami (Jika Anda secara bersamaan mendapatkan view alami (ditonton orang lain selain sistem kami ) saat menjalankan waktu jam tayang, Anda tidak akan mendapatkan cukup waktu jam tayang.)\r\n- Jika video Dihapus, Ditolak, Privat       kami menambahkan waktu tontonan atau setelah selesai, maka kami tidak dapat refill / reffund Dana / canceled / Partial.', 1000, 4000, 185000, 185000, 15000, 'Aktif', 1978, 'MEDANPEDIA', 'Sosial Media'),
(2686, 274, 'Instagram Followers [guaranteed]', 'Instagram Followers Refill S27 [ REFILL 90Days ] Nodropâ™»ï¸', 'High quality followers\r\nDrop Sekitar: 2-3%\r\nKecepatan 20-30K/day ', 100, 500000, 39000, 39000, 15000, 'Aktif', 1979, 'MEDANPEDIA', 'Sosial Media'),
(2687, 275, 'TikTok Likes', 'TikTok Likes MP 5 [ REAL ] [ Cheap ] ', 'mulai  0-7HRS\r\n100-1k Per Day\r\nHigh Quality\r\nNO Refill', 100, 1000, 23900, 23900, 15000, 'Aktif', 1982, 'MEDANPEDIA', 'Sosial Media'),
(2688, 276, 'Shopee/Tokopedia/Bukalapak', 'Tokopedia Feeds likes', '1', 50, 2000, 23000, 23000, 15000, 'Aktif', 1983, 'MEDANPEDIA', 'Sosial Media'),
(2689, 277, 'TikTok INDONESIA', 'TikTok Like komentar Indonesia ', 'masukin username yg mau di like komenannya di Target \r\nmasukin link video di Link Post ', 5, 250, 55000, 55000, 15000, 'Aktif', 1984, 'MEDANPEDIA', 'Sosial Media'),
(2690, 278, 'TikTok INDONESIA', 'TikTok Followers Indonesia BONUS++', 'indo pasif\r\nproses 24 jam\r\ndapat bonus jika beruntung\r\nmax db 3k', 100, 1000, 75000, 75000, 15000, 'Aktif', 1986, 'MEDANPEDIA', 'Sosial Media'),
(2691, 279, 'Clubhouse', 'Clubhouse Followers Server 1  [20k] [ No Refill ]', 'No Refill', 50, 20000, 35000, 35000, 15000, 'Aktif', 1988, 'MEDANPEDIA', 'Sosial Media'),
(2692, 280, 'Youtube Views', 'Youtube Views MP 26 [ Lifetime ] Cheap', 'Start time : 0 - 12h', 500, 250000, 31500, 31500, 15000, 'Aktif', 1990, 'MEDANPEDIA', 'Sosial Media'),
(2693, 281, 'Instagram Followers Indonesia', 'Instagram Followers Indonesia S6 [ REAL AKTIF ] [ Cheap ] [ MAX1.5K ] ', 'proses max 3 hari\r\nbisa lebih cepat', 100, 1500, 31000, 31000, 15000, 'Aktif', 1994, 'MEDANPEDIA', 'Sosial Media'),
(2694, 282, 'Instagram Likes', 'Instagram Likes Server 5 [ REAL ] [ INSTANT ] 5k', 'High Quality\r\n500 Per Hrs', 20, 5000, 17800, 17800, 15000, 'Aktif', 1996, 'MEDANPEDIA', 'Sosial Media'),
(2695, 283, 'Instagram Live Video', 'Instagram Live Video Views Likes and Comments also', 'refund related issue.\r\n1286 Instagram Live Video Views - Max 30k - Likes and Comments also 12.00 20 30000 3 hours and 27 minutes Starts within 1-2 mints after ordering\r\nstart live and then order\r\nHas random likes and comments\r\nmin 20 max 20k\r\nif have problem please share screenshot with time and no. of viewers.\r\n\r\nAvg Retention of viewers - 30 to 60 minutes', 20, 30000, 235000, 235000, 15000, 'Aktif', 1998, 'MEDANPEDIA', 'Sosial Media'),
(2696, 284, 'Instagram Views', 'instagram view MP 9 [ TERMURAH FAST ] [ 100k/hour] ', '[ 100k/hour] ', 100, 500000, 15130, 15130, 15000, 'Aktif', 1999, 'MEDANPEDIA', 'Sosial Media'),
(2697, 285, 'TikTok View/share/comment', 'TIKTOK View Server 6 Super Fast', 'waktu mulai sekitar 0-2jam', 100, 100000000, 15365, 15365, 15000, 'Aktif', 2000, 'MEDANPEDIA', 'Sosial Media'),
(2698, 286, 'Youtube Views', 'Youtube Views MP 27 [ Lifetime ] Speed 5K', 'waktu mulai  : 0 - 1h', 100, 10000000, 29700, 29700, 15000, 'Aktif', 2001, 'MEDANPEDIA', 'Sosial Media'),
(2699, 287, 'Instagram Views', 'instagram Reel  view MP 1 [ TERMURAH FAST ] [ 100k/hour] ', '1M/hour\r\nReel Views', 100, 10000000, 15168, 15168, 15000, 'Aktif', 2002, 'MEDANPEDIA', 'Sosial Media'),
(2700, 288, 'Twitch', 'Twitch Followers 500K/days [R30]', ' â€¢ Start Time: 0-10 minutes\r\nâ€¢ Hours Speed: 500k/hour\r\nâ€¢ 30 Days Refill.\r\nDo not overlap orders', 100, 1000000, 21800, 21800, 15000, 'Aktif', 2004, 'MEDANPEDIA', 'Sosial Media'),
(2701, 289, 'Twitter Followers', 'Twitter Followers Server 8 [ 30 days refill ] ', '30 Days Refill\r\nSpeed;Upto 500-1k/day\r\nUSA accounts [ Mix Quality]', 10, 10000, 170000, 170000, 15000, 'Aktif', 2006, 'MEDANPEDIA', 'Sosial Media'),
(2702, 290, 'Instagram Followers [guaranteed]', 'Instagram Followers Refill S28 [ auto Refill 30 Days ] cheap', '- Kualitas Bagus\r\n- Kecepatan: 3-5k / hari\r\n- Garansi: Auto Refill 30 hari\r\n- Kami hanya akan mengisi ulang jika milik kami turun, Kami dapat memberi Anda daftar pengikut jika Anda mengklaim itu tetes kami.', 10, 500000, 24700, 24700, 15000, 'Aktif', 2009, 'MEDANPEDIA', 'Sosial Media'),
(2703, 291, 'Youtube Subscribers', 'Youtube Subscribe SERVER 7 [ 30 days refill ][ 100% real ] ', 'Speed: 500-1000/day\r\nNO stuck\r\nNO drop\r\nStart Time: 0-1hrs', 100, 55000, 405000, 405000, 15000, 'Aktif', 2011, 'MEDANPEDIA', 'Sosial Media'),
(2704, 292, 'Telegram', 'Telegram - Channnel Members/Group Server 5 [ Max 5M] REAL FAST', 'NO REFILL\r\nLink: Https://T.Me/Username\r\nStart: 1-30 menit\r\nSpeed: 50000/Hari\r\nJika Anda Telah Mengubah Tautan Atau Saluran Dihapus Tidak Akan Ada canceled Pesanan', 50, 5000000, 35300, 35300, 15000, 'Aktif', 2012, 'MEDANPEDIA', 'Sosial Media'),
(2705, 293, 'Telegram', 'Telegram - Channnel Members/Group Server 6 [ Max 10K] REAL INDIA ', 'Link: Https://T.Me/Username\r\nStart: 0-360 Min\r\nSpeed: 5000/D\r\nRefill: 30 Days\r\nQuality: INDIAN\r\nJika Anda Telah Mengubah Tautan Atau Saluran Dihapus Tidak Akan Ada canceled Pesanan', 10, 10000, 39000, 39000, 15000, 'Aktif', 2013, 'MEDANPEDIA', 'Sosial Media'),
(2706, 294, 'TikTok Followers', 'TIKTOK FOLLOWERS Server 22 [ No Refill ] LQ Quality', 'LQ Quality', 100, 10000, 28000, 28000, 15000, 'Aktif', 2014, 'MEDANPEDIA', 'Sosial Media'),
(2707, 295, 'Youtube Live Stream', 'Youtube Live Stream Views [ Min 1K ]', 'â€¢ Tampilan Aktif Nyata **\r\nâ€¢ MULAI INSTAN\r\nâ€¢ 100% Pemirsa Pengguna YouTube Manusia Nyata!\r\nâ€¢ Tampilan Halaman Desktop Windows & Mobile Watch\r\nâ€¢ 100% Lalu Lintas Unik dapat dimonetisasi!\r\nâ€¢ Pemirsa Seluruh Dunia\r\nâ€¢ Harus Tidak Terbatas & Terbuka untuk SEMUA negara\r\nâ€¢ Retensi Acak\r\nâ€¢ Rata-rata Bersamaan dan waktu tonton berdasarkan konten streaming langsung\r\nâ€¢ Pengiriman Lebih Dijamin\r\nâ€¢ penayangan dapat dikirim ke embed video streaming langsung yang dinonaktifkan\r\nâ€¢ Sumber Lalu Lintas: Iklan Langsung\r\n\r\nCATATAN :\r\n- Penayangan tersebar dari waktu ke waktu untuk menayangkan pemirsa langsung sehingga mereka tinggal selama setidaknya 30 menit - 2 jam\r\nMis: Anda memesan 1000 view, kemudian, 1000 view langsung akan tersebar sepanjang waktu dalam 30 menit - 2H +.', 1000, 1000000, 72000, 72000, 15000, 'Aktif', 2015, 'MEDANPEDIA', 'Sosial Media'),
(2708, 297, 'Facebook Page / Website - Likes / Stars', 'Facebook Page Likes Server 1 [ Refill 45 Days  ] [ REAL ] 1k/day', 'Waktu Mulai 1-3 Jam\r\nTipe akun real\r\nKecepatan 1K/Hari\r\nRasio Drop: Tidak ada penurunan atau 1-2%\r\nGaransi Isi Ulang 45 Hari\r\nAnda dapat memesan untuk tautan yang sama beberapa kali setelah menyelesaikan pesanan sebelumnya.', 100, 10000, 41500, 41500, 15000, 'Aktif', 2017, 'MEDANPEDIA', 'Sosial Media'),
(2709, 298, 'Facebook Page / Website - Likes / Stars', 'Facebook Page Likes + Followers Server 2 [ Refill 30 Days ] [ REAL ] 2K/day ', 'Waktu Mulai 1-3 Jam\r\nTipe Acc Nyata\r\nKecepatan 2K/Hari\r\nRasio Drop: Tidak ada penurunan atau 1-2%\r\nGaransi Isi Ulang 30 Hari\r\nAnda dapat memesan untuk tautan yang sama beberapa kali setelah menyelesaikan pesanan sebelumnya.', 50, 40000, 53000, 53000, 15000, 'Aktif', 2018, 'MEDANPEDIA', 'Sosial Media'),
(2710, 299, 'Instagram Followers [guaranteed]', 'Instagram Followers Refill S29 [ Refill 30 Days ] cheapeast ', '- Guarantee: 30 days Refill\r\n- Instant Start\r\n- Refill button', 50, 10000, 23500, 23500, 15000, 'Aktif', 2019, 'MEDANPEDIA', 'Sosial Media'),
(2711, 300, 'Instagram Followers [ No Refill ]', 'Instagram Followers Server 9 [ No Refill ] [ Fast ] BOT ', 'waktu mulai 0-6 jam', 50, 10000, 18600, 18600, 15000, 'Aktif', 2021, 'MEDANPEDIA', 'Sosial Media'),
(2712, 301, 'Instagram Likes', 'Instagram Likes Server 6 [ No refill ] [ TERMURAH ]', 'drop 30-45%\r\ninstan', 25, 5000, 16250, 16250, 15000, 'Aktif', 2022, 'MEDANPEDIA', 'Sosial Media'),
(2713, 303, 'Instagram Followers [guaranteed]', 'Instagram Followers Refill S30 [ Refill 45D ] less - no drop', 'fast\r\nkemungkinan 0-10% drop\r\nkualitas bagus', 20, 200000, 34000, 34000, 15000, 'Aktif', 2025, 'MEDANPEDIA', 'Sosial Media'),
(2714, 304, 'Instagram Reels', 'Instagram Reels Likes S1 [ Max - 20k ] INSTANT', 'INSTANT', 20, 20000, 20500, 20500, 15000, 'Aktif', 2026, 'MEDANPEDIA', 'Sosial Media'),
(2715, 305, 'Instagram Reels', 'Instagram Reels Likes S2 [ Max - 10k ] FAST', 'fast', 10, 10000, 27000, 27000, 15000, 'Aktif', 2027, 'MEDANPEDIA', 'Sosial Media'),
(2716, 306, 'Instagram Reels', 'Instagram Reels Views S1 [ Max - Unlimited ] SUPERFAST', 'fast', 1000, 100000000, 15135, 15135, 15000, 'Aktif', 2028, 'MEDANPEDIA', 'Sosial Media'),
(2717, 307, 'Instagram Reels', 'Instagram Reels Views S2 [ Max - Unlimited ] SUPER INSTAN', '1M/hour\r\n', 100, 10000000, 15175, 15175, 15000, 'Aktif', 2029, 'MEDANPEDIA', 'Sosial Media'),
(2718, 308, 'Instagram Likes', 'Instagram Likes Server 7 [ No refill ] [ TERMURAH DI INDONESIA ] ', 'TERMURAH FAST', 10, 200000, 15570, 15570, 15000, 'Aktif', 2030, 'MEDANPEDIA', 'Sosial Media'),
(2719, 309, 'Instagram Likes', 'Instagram Likes Server 8 [ max 20k ] [ TERMURAH DI INDONESIA ] ', 'TERMURAH  FAST', 10, 20000, 15470, 15470, 15000, 'Aktif', 2031, 'MEDANPEDIA', 'Sosial Media'),
(2720, 310, 'Facebook Video Views / Live Stream', 'Facebook Video Views S1 Nondrop - NO Refill', 'REAL\r\nwaktu mulai 0-6 jam\r\nkualitas bagus', 200, 100000000, 17900, 17900, 15000, 'Aktif', 2033, 'MEDANPEDIA', 'Sosial Media'),
(2721, 311, 'Instagram Followers Indonesia', 'Instagram Followers Indonesia S7 [ REAL AKTIF ] [ cheap ] [ MAX4K ] ', 'Waktu Proses 2 hari\r\nwaktu selesai 3-4 hari\r\nREAL AKTIF', 100, 4000, 31000, 31000, 15000, 'Aktif', 2034, 'MEDANPEDIA', 'Sosial Media'),
(2722, 312, 'Instagram Followers Indonesia', 'Instagram Followers Indonesia S8 [ REAL AKTIF ] [ cheap ] [ MAX6K ] ', 'Waktu Proses 2 hari\r\nwaktu selesai 3-4 hari\r\nREAL AKTIF', 100, 6000, 34500, 34500, 15000, 'Aktif', 2035, 'MEDANPEDIA', 'Sosial Media'),
(2723, 313, 'TikTok INDONESIA', 'TikTok Share Indonesia MAX [ 200K ] ', 'fast\r\nmasukin link video', 100, 200000, 30000, 30000, 15000, 'Aktif', 2036, 'MEDANPEDIA', 'Sosial Media'),
(2724, 314, 'TikTok Likes', 'TikTok Likes MP 6 [ REAL ] [ No refill ] 1k/days', 'waktu mulai 0-4 jam\r\n1k per day\r\nKualitas bagus', 100, 1000, 23150, 23150, 15000, 'Aktif', 2037, 'MEDANPEDIA', 'Sosial Media'),
(2725, 315, 'Instagram Likes', 'Instagram Likes Server 9 [ Real ] [ NO DROP ] ', 'No drop ( kemungkinan, drop hanya 10% )\r\ntetap no garansi', 100, 5000, 17350, 17350, 15000, 'Aktif', 2039, 'MEDANPEDIA', 'Sosial Media'),
(2726, 316, 'Instagram Story / Impressions / Saves / Profile Vi', 'Instagram Impressions + Reach Max 500k INSTAN ', 'INSTAN', 100, 500000, 15206, 15206, 15000, 'Aktif', 2041, 'MEDANPEDIA', 'Sosial Media'),
(2727, 317, 'Instagram Story / Impressions / Saves / Profile Vi', 'Instagram Impressions + Reach [ Good for Ranking! ]', 'INSTANT \r\nGunakan link', 100, 1000000, 15900, 15900, 15000, 'Aktif', 2042, 'MEDANPEDIA', 'Sosial Media'),
(2728, 318, 'Instagram Followers [ No Refill ]', 'Instagram Followers Server 10 [ No Refill ] [ Fast ] BOT CHEAP', 'fast', 10, 10000, 18190, 18190, 15000, 'Aktif', 2043, 'MEDANPEDIA', 'Sosial Media'),
(2729, 319, 'TikTok Followers', 'TIKTOK FOLLOWERS Server 23 [ No Refill ] LQ CHEAPEAST', 'tergat : https://tiktok.com/@username\r\n500/DAY\r\nINSTANT', 10, 100000, 19450, 19450, 15000, 'Aktif', 2044, 'MEDANPEDIA', 'Sosial Media'),
(2730, 320, 'Facebook Post Likes / Comments / Shares', 'Facebook Photo / Post Likes MP7 [ No Refill ] [max100k] Instant ', 'Instant ', 100, 100000, 21550, 21550, 15000, 'Aktif', 2045, 'MEDANPEDIA', 'Sosial Media'),
(2731, 321, 'Facebook Page / Website - Likes / Stars', 'Facebook Page Likes  Server 3  [ No Refill ] cheap 250K', 'cheap 250K\r\nNo Refill', 100, 100000, 27000, 27000, 15000, 'Aktif', 2046, 'MEDANPEDIA', 'Sosial Media'),
(2732, 322, 'Facebook Video Views / Live Stream', 'Facebook Live Video Stream Views [ untuk durasi 30 menit ] NEW', 'Waktu mulai: 0-5 menit\r\nFormat Tautan diterima:\r\nhttps://m.facebook.com/story.php?\r\nhttps://www.facebook.com/watch/?v=\r\nhttps://www.facebook.com/qaderkill/videos/4217814588239606/\r\nLayanan tidak memiliki hitungan mulai, Status mungkin menunjukkan tertunda tetapi pesanan akan berjalan\r\nKetat Kami tidak menerima fb.me [ tautan singkat] , Tidak ada pengembalian uang jika Anda menambahkan tautan tersebut\r\nTidak Ada Pengembalian Dana Jika Anda menambahkan video fb normal\r\nTidak Ada Pengembalian Dana untuk tautan format yang salah atau jika pesanan turun dari pembaruan fb', 50, 500, 83500, 83500, 15000, 'Aktif', 2047, 'MEDANPEDIA', 'Sosial Media'),
(2733, 323, 'TikTok Likes', 'TikTok Likes MP 8 [ REAL ] [ R30D ] INSTAN', 'fast\r\nreal\r\ngoodserver', 100, 1000, 26200, 26200, 15000, 'Aktif', 2048, 'MEDANPEDIA', 'Sosial Media'),
(2734, 324, 'Youtube Subscribers', 'Youtube Subscribe SERVER 9 [ 30 days refill ][ USA and European ] No drop', '- Waktu mulai: 0 -24 jam\r\n- Garansi: 30 hari\r\n- Drop rate: 0 - 5%\r\n- Untuk pesanan yang lebih kecil, kecepatannya adalah ~ 30 -100 sub per hari (semakin besar pesanan, semakin cepat kecepatannya)', 100, 5000, 570000, 570000, 15000, 'Aktif', 2049, 'MEDANPEDIA', 'Sosial Media'),
(2735, 325, 'Youtube Subscribers', 'Youtube Subscribe SERVER 10 [ 30 days refill ][ Super Fast ] No drop ', 'Subscribe Super Cepat dengan stabilitas 100%\r\n\r\nKecepatan: 4k-10k/hari\r\nDrop: NONdrop\r\nReal Subs', 5000, 30000, 755000, 755000, 15000, 'Aktif', 2050, 'MEDANPEDIA', 'Sosial Media'),
(2736, 326, 'Facebook Video Views / Live Stream', 'Facebook Video Views S2 Speed 50k ( Instant Start )', 'hanya bisa link dari Desktop ( PC/Komputer/Laptop )\r\ncontoh link yang benar https://www.facebook.com/admintakin/posts/1753721378117064\r\ntidak support untuk view story', 500, 10000000, 16700, 16700, 15000, 'Aktif', 2052, 'MEDANPEDIA', 'Sosial Media'),
(2737, 327, 'Twitter Followers', 'Twitter Followers Server 9 [ 7 days auto refill ] ', '7 Days AUTO Refill\r\n1K -5K/Day\r\nInstan\r\njangan komplin jika slow masuknya\r\ncoba order kecil dulu untuk tes layanan\r\njika cocok buat anda baru order besar', 20, 5000, 83000, 83000, 15000, 'Aktif', 2053, 'MEDANPEDIA', 'Sosial Media'),
(2738, 328, 'Instagram Followers [guaranteed]', 'Instagram Followers Refill S31 [ Refill button 10D ] 3k/days â™»ï¸', 'HQ\r\nwaktu mulai 0-24 jam', 50, 10000, 21600, 21600, 15000, 'Aktif', 2054, 'MEDANPEDIA', 'Sosial Media'),
(2739, 329, 'Instagram Followers [guaranteed]', 'Instagram Followers Refill S32 [ Refill 99days ] FAST HQ', 'Drop sekitar 0-10%\r\nkualitas bagus\r\nNON DROP\r\nrefill 99 days', 20, 200000, 34000, 34000, 15000, 'Aktif', 2055, 'MEDANPEDIA', 'Sosial Media'),
(2740, 330, 'Twitter Followers', 'Twitter Followers Server 10 [ No refill ] ', '- Start 0-1 hour\r\n- Speed up to 400 / day (The average speed per day is 50-400!)', 50, 1000, 133000, 133000, 15000, 'Aktif', 2056, 'MEDANPEDIA', 'Sosial Media'),
(2741, 331, 'TikTok Likes', 'TikTok Likes MP 9 [ REAL ] [ R30D ] [ BONUS VIEW ]', 'Start Time: Instant - 1H\r\nSpeed: 2K - 4K/Day\r\nRefill: 30 Days\r\nREAL', 100, 1000, 27700, 27700, 15000, 'Aktif', 2057, 'MEDANPEDIA', 'Sosial Media'),
(2742, 332, 'TikTok Followers', 'TIKTOK FOLLOWERS Server 24 [ Refill 30days ] REAL ', 'Link: https://tiktok.com/@username\r\nInstan\r\ncoba 100 dlu untuk tes layanan\r\njika cocok baru ke yang lebih besar pesanan', 100, 10000, 61000, 61000, 15000, 'Aktif', 2060, 'MEDANPEDIA', 'Sosial Media'),
(2743, 333, 'TikTok Followers', 'TIKTOK FOLLOWERS Server 25 [ REAL ] [ CHEAPEST ] INSTAN', 'Instan\r\n200-500/days', 10, 90000, 20500, 20500, 15000, 'Aktif', 2062, 'MEDANPEDIA', 'Sosial Media'),
(2744, 334, 'Instagram Views', 'instagram view MP 10 [ TERMURAH ] [ 100k/hour] INSTANT', 'Real Views', 100, 5000000, 15073, 15073, 15000, 'Aktif', 2063, 'MEDANPEDIA', 'Sosial Media'),
(2745, 335, 'Instagram Views', 'instagram view MP 11 [ TERMURAH ] [ Works on Reel, TV also] ', 'Instan', 100, 100000, 15017, 15017, 15000, 'Aktif', 2064, 'MEDANPEDIA', 'Sosial Media'),
(2746, 336, 'Facebook Video Views / Live Stream', 'Facebook Live Video Stream Views fast [ untuk durasi 30 menit ] ', 'Waktu mulai: 0-5 menit\r\nFormat Tautan diterima:\r\nhttps://m.facebook.com/story.php?\r\nhttps://www.facebook.com/watch/?v=\r\nhttps://www.facebook.com/qaderkill/videos/4217814588239606/\r\nLayanan tidak memiliki hitungan mulai, Status mungkin menunjukkan tertunda tetapi pesanan akan berjalan\r\nKetat Kami tidak menerima fb.me [ tautan singkat] , Tidak ada pengembalian uang jika Anda menambahkan tautan tersebut\r\nTidak Ada Pengembalian Dana Jika Anda menambahkan video fb normal\r\nTidak Ada Pengembalian Dana untuk tautan format yang salah atau jika pesanan turun dari pembaruan fb', 50, 2000, 87000, 87000, 15000, 'Aktif', 2067, 'MEDANPEDIA', 'Sosial Media'),
(2747, 337, 'Facebook Video Views / Live Stream', 'Facebook Live Video Stream Views fast [ untuk durasi 60 menit ]', 'Waktu mulai: 0-5 menit\r\nFormat Tautan diterima:\r\nhttps://m.facebook.com/story.php?\r\nhttps://www.facebook.com/watch/?v=\r\nhttps://www.facebook.com/qaderkill/videos/4217814588239606/\r\nLayanan tidak memiliki hitungan mulai, Status mungkin menunjukkan tertunda tetapi pesanan akan berjalan\r\nKetat Kami tidak menerima fb.me [ tautan singkat] , Tidak ada pengembalian uang jika Anda menambahkan tautan tersebut\r\nTidak Ada Pengembalian Dana Jika Anda menambahkan video fb normal\r\nTidak Ada Pengembalian Dana untuk tautan format yang salah atau jika pesanan turun dari pembaruan fb', 50, 2000, 159000, 159000, 15000, 'Aktif', 2068, 'MEDANPEDIA', 'Sosial Media'),
(2748, 338, 'Facebook Video Views / Live Stream', 'Facebook Live Video Stream Views fast [ untuk durasi 90 menit ] ', 'Waktu mulai: 0-5 menit\r\nFormat Tautan diterima:\r\nhttps://m.facebook.com/story.php?\r\nhttps://www.facebook.com/watch/?v=\r\nhttps://www.facebook.com/qaderkill/videos/4217814588239606/\r\nLayanan tidak memiliki hitungan mulai, Status mungkin menunjukkan tertunda tetapi pesanan akan berjalan\r\nKetat Kami tidak menerima fb.me [ tautan singkat] , Tidak ada pengembalian uang jika Anda menambahkan tautan tersebut\r\nTidak Ada Pengembalian Dana Jika Anda menambahkan video fb normal\r\nTidak Ada Pengembalian Dana untuk tautan format yang salah atau jika pesanan turun dari pembaruan fb', 50, 2000, 230000, 230000, 15000, 'Aktif', 2069, 'MEDANPEDIA', 'Sosial Media'),
(2749, 339, 'Facebook Video Views / Live Stream', 'Facebook Live Video Stream Views fast [ untuk durasi 120 menit ]', 'Waktu mulai: 0-5 menit\r\nFormat Tautan diterima:\r\nhttps://m.facebook.com/story.php?\r\nhttps://www.facebook.com/watch/?v=\r\nhttps://www.facebook.com/qaderkill/videos/4217814588239606/\r\nLayanan tidak memiliki hitungan mulai, Status mungkin menunjukkan tertunda tetapi pesanan akan berjalan\r\nKetat Kami tidak menerima fb.me [ tautan singkat] , Tidak ada pengembalian uang jika Anda menambahkan tautan tersebut\r\nTidak Ada Pengembalian Dana Jika Anda menambahkan video fb normal\r\nTidak Ada Pengembalian Dana untuk tautan format yang salah atau jika pesanan turun dari pembaruan fb', 50, 2000, 305000, 305000, 15000, 'Aktif', 2070, 'MEDANPEDIA', 'Sosial Media'),
(2750, 340, 'Facebook Video Views / Live Stream', 'Facebook Live Video Stream Views fast [ untuk durasi 150 menit ] ', 'Waktu mulai: 0-5 menit\r\nFormat Tautan diterima:\r\nhttps://m.facebook.com/story.php?\r\nhttps://www.facebook.com/watch/?v=\r\nhttps://www.facebook.com/qaderkill/videos/4217814588239606/\r\nLayanan tidak memiliki hitungan mulai, Status mungkin menunjukkan tertunda tetapi pesanan akan berjalan\r\nKetat Kami tidak menerima fb.me [ tautan singkat] , Tidak ada pengembalian uang jika Anda menambahkan tautan tersebut\r\nTidak Ada Pengembalian Dana Jika Anda menambahkan video fb normal\r\nTidak Ada Pengembalian Dana untuk tautan format yang salah atau jika pesanan turun dari pembaruan fb', 50, 2000, 380000, 380000, 15000, 'Aktif', 2071, 'MEDANPEDIA', 'Sosial Media'),
(2751, 341, '- PROMO - ON OFF', 'Instagram Likes GRATIS', 'No kompline', 10, 50, 15000, 15000, 15000, 'Aktif', 2072, 'MEDANPEDIA', 'Sosial Media'),
(2752, 342, '- PROMO - ON OFF', 'Instagram Views GRATIS ', 'no kompline\r\nwaktu mulai 0-6 jam', 100, 300, 15000, 15000, 15000, 'Aktif', 2073, 'MEDANPEDIA', 'Sosial Media'),
(2753, 343, '- PROMO - ON OFF', 'Twitch Followers GRATIS ', 'no kompline\r\ninstan', 100, 100, 15000, 15000, 15000, 'Aktif', 2074, 'MEDANPEDIA', 'Sosial Media'),
(2754, 344, 'Instagram Followers [ No Refill ]', 'Instagram Followers Server 11 [ No Refill ] [ mix real ] ', 'drop kemungkinan 10-20% untuk saat ini\r\nprofil ada story nya', 20, 10000, 19750, 19750, 15000, 'Aktif', 2075, 'MEDANPEDIA', 'Sosial Media'),
(2755, 345, 'Youtube Likes / Dislikes / Shares / Comment', 'Youtube Short Likes Server 1 [ refill 30 days ]', 'fast\r\nno drop\r\ngaransi refill 30 days', 10, 100000, 40500, 40500, 15000, 'Aktif', 2076, 'MEDANPEDIA', 'Sosial Media'),
(2756, 346, 'Youtube Live Stream', 'Youtube Live Stream Views [ Min 1K - Max 500k ] ', 'Waktu mulai Instan\r\njika Anda memesan 1000, Anda akan mendapatkan 1000 view dalam kelipatan 200-300 [ Live akan menampilkan 200-300 seperti ini dan menyelesaikan 1000 tampilan]\r\ntidak ada reffund dan kompline  [Pengguna real]', 1000, 500000, 51000, 51000, 15000, 'Aktif', 2077, 'MEDANPEDIA', 'Sosial Media'),
(2757, 347, 'Instagram Followers Indonesia', 'Instagram Followers Indonesia S9 [ REAL AKTIF ] [ SUPERFAST ] [ MAX5K ] BONUS+++', 'proses 1-3 jam\r\ninstan\r\nmungkin kalo delay sehari\r\nreal indo\r\nBONUS 5-10%', 100, 5000, 46000, 46000, 15000, 'Aktif', 2078, 'MEDANPEDIA', 'Sosial Media');

-- --------------------------------------------------------

--
-- Table structure for table `log`
--

CREATE TABLE `log` (
  `id` int(4) NOT NULL,
  `username` varchar(50) DEFAULT NULL,
  `aksi` enum('Login','Logout') NOT NULL,
  `ip` varchar(100) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `time` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `log`
--

INSERT INTO `log` (`id`, `username`, `aksi`, `ip`, `date`, `time`) VALUES
(592, 'demo', 'Login', '110.138.82.56', '2021-09-04', '10:19:19'),
(593, 'demo', 'Login', '182.1.122.183', '2021-09-04', '15:38:08'),
(594, 'demo', 'Login', '110.138.82.56', '2021-09-04', '18:49:23'),
(595, 'demo', 'Login', '112.215.237.243', '2021-09-05', '12:38:02'),
(596, 'demo', 'Login', '110.138.82.56', '2021-09-05', '12:54:15'),
(597, 'demo', 'Login', '110.138.82.56', '2021-09-05', '13:36:16'),
(598, 'demo', 'Login', '114.122.165.13', '2021-09-05', '16:51:38'),
(599, 'demo', 'Login', '110.138.82.56', '2021-09-05', '19:04:40'),
(600, 'demo', 'Login', '120.188.39.134', '2021-09-06', '00:25:17'),
(601, 'demo', 'Login', '36.90.97.86', '2021-09-06', '00:30:18'),
(602, 'demo', 'Login', '120.188.39.134', '2021-09-06', '14:24:12'),
(603, 'demo', 'Login', '110.138.82.56', '2021-09-07', '07:24:47'),
(604, 'demo', 'Login', '139.195.71.158', '2021-09-07', '07:41:13'),
(605, 'Demo', 'Login', '180.254.235.92', '2021-09-07', '15:22:41'),
(606, 'Demo', 'Login', '36.85.222.124', '2021-09-07', '17:27:36'),
(607, 'Demo', 'Login', '36.85.222.124', '2021-09-07', '17:36:21'),
(608, 'Demo', 'Login', '36.85.222.124', '2021-09-07', '17:37:43'),
(609, 'Demo', 'Login', '36.85.222.124', '2021-09-07', '18:25:07'),
(610, 'demo', 'Login', '110.138.82.56', '2021-09-07', '18:47:52'),
(611, 'demo', 'Logout', '110.138.82.56', '2021-09-07', '18:50:58'),
(612, 'demo', 'Login', '110.138.82.56', '2021-09-07', '18:51:11'),
(613, 'demo', 'Login', '114.122.10.160', '2021-09-07', '20:15:50'),
(614, 'demo', 'Logout', '114.122.10.160', '2021-09-07', '20:17:57'),
(615, 'tesajasyah', 'Login', '114.122.10.160', '2021-09-07', '20:18:24'),
(616, 'demo', 'Login', '110.138.93.45', '2021-09-08', '07:21:51'),
(617, 'demo', 'Login', '120.188.39.134', '2021-09-08', '07:26:14'),
(618, 'demo', 'Login', '110.138.93.45', '2021-09-08', '18:48:41'),
(619, 'demo', 'Login', '110.138.85.240', '2021-09-09', '15:41:57'),
(620, 'demo', 'Login', '114.142.168.57', '2021-09-09', '15:43:04'),
(621, 'demo', 'Login', '36.65.85.132', '2021-09-10', '08:37:48'),
(622, 'demo', 'Login', '114.4.82.193', '2021-09-11', '18:59:28'),
(623, 'demo', 'Login', '103.105.33.87', '2021-09-12', '00:22:15'),
(624, 'demo', 'Login', '110.138.85.240', '2021-09-12', '00:29:59');

-- --------------------------------------------------------

--
-- Table structure for table `metode_depo`
--

CREATE TABLE `metode_depo` (
  `id` int(255) NOT NULL,
  `tipe` enum('Pulsa Transfer','Bank','EMoney') NOT NULL,
  `provider` varchar(255) NOT NULL,
  `jalur` enum('Auto','Manual') NOT NULL,
  `nama` varchar(255) NOT NULL,
  `rate` varchar(255) NOT NULL,
  `keterangan` varchar(250) NOT NULL,
  `tujuan` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `metode_depo`
--

INSERT INTO `metode_depo` (`id`, `tipe`, `provider`, `jalur`, `nama`, `rate`, `keterangan`, `tujuan`) VALUES
(32, 'EMoney', 'OVO', 'Auto', 'OVO Konfirmasi Otomatis', '1', 'ON', '082288808400'),
(33, 'EMoney', 'DANA', 'Auto', 'DANA Konfirmasi Otomatis', '1', 'ON', '082288808400'),
(34, 'Pulsa Transfer', 'TSEL', 'Auto', 'TSEL Konfirmasi Otomatis', '1', 'ON', '082283555508'),
(35, 'Pulsa Transfer', 'AXIS', 'Auto', 'AXIS Konfirmasi Otomatis', '1', 'ON', '082283555508'),
(36, 'Pulsa Transfer', 'TSEL', 'Auto', 'TSEL Konfirmasi Otomatis', '1', 'ON', '082288808400'),
(37, 'Pulsa Transfer', 'XL', 'Auto', 'XL Konfirmasi Otomatis', '1', 'ON', '085876550051'),
(38, 'EMoney', 'GOPAY', 'Manual', 'GOPAY Konfirmasi Manual', '1', 'ON', '085876550051');

-- --------------------------------------------------------

--
-- Table structure for table `pembelian_game`
--

CREATE TABLE `pembelian_game` (
  `id` int(10) NOT NULL,
  `oid` varchar(50) COLLATE utf8_swedish_ci NOT NULL,
  `provider_oid` varchar(50) COLLATE utf8_swedish_ci NOT NULL,
  `user` varchar(50) COLLATE utf8_swedish_ci NOT NULL,
  `layanan` varchar(100) COLLATE utf8_swedish_ci NOT NULL,
  `harga` double NOT NULL,
  `profit` varchar(50) COLLATE utf8_swedish_ci NOT NULL,
  `target` varchar(50) COLLATE utf8_swedish_ci NOT NULL,
  `no_meter` varchar(50) COLLATE utf8_swedish_ci NOT NULL,
  `keterangan` varchar(100) COLLATE utf8_swedish_ci NOT NULL,
  `status` enum('Pending','Processing','Error','Partial','Success') COLLATE utf8_swedish_ci NOT NULL,
  `date` date NOT NULL,
  `time` time NOT NULL,
  `place_from` varchar(50) COLLATE utf8_swedish_ci NOT NULL DEFAULT 'Website',
  `provider` varchar(100) COLLATE utf8_swedish_ci NOT NULL,
  `refund` enum('0','1') COLLATE utf8_swedish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_swedish_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pembelian_pulsa`
--

CREATE TABLE `pembelian_pulsa` (
  `id` int(10) NOT NULL,
  `oid` varchar(50) COLLATE utf8_swedish_ci NOT NULL,
  `provider_oid` varchar(50) COLLATE utf8_swedish_ci NOT NULL,
  `user` varchar(50) COLLATE utf8_swedish_ci NOT NULL,
  `layanan` varchar(100) COLLATE utf8_swedish_ci NOT NULL,
  `harga` double NOT NULL,
  `profit` varchar(50) COLLATE utf8_swedish_ci NOT NULL,
  `target` varchar(50) COLLATE utf8_swedish_ci NOT NULL,
  `no_meter` varchar(50) COLLATE utf8_swedish_ci NOT NULL,
  `keterangan` varchar(100) COLLATE utf8_swedish_ci NOT NULL,
  `status` enum('Pending','Processing','Error','Partial','Success') COLLATE utf8_swedish_ci NOT NULL,
  `date` date NOT NULL,
  `time` time NOT NULL,
  `place_from` varchar(50) COLLATE utf8_swedish_ci NOT NULL DEFAULT 'Website',
  `provider` varchar(100) COLLATE utf8_swedish_ci NOT NULL,
  `refund` enum('0','1') COLLATE utf8_swedish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_swedish_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pembelian_sms`
--

CREATE TABLE `pembelian_sms` (
  `id` int(10) NOT NULL,
  `oid` varchar(50) COLLATE utf8_swedish_ci NOT NULL,
  `provider_oid` varchar(50) COLLATE utf8_swedish_ci NOT NULL,
  `user` varchar(50) COLLATE utf8_swedish_ci NOT NULL,
  `layanan` varchar(100) COLLATE utf8_swedish_ci NOT NULL,
  `harga` double NOT NULL,
  `profit` varchar(50) COLLATE utf8_swedish_ci NOT NULL,
  `target` varchar(50) COLLATE utf8_swedish_ci NOT NULL,
  `isi_sms` text COLLATE utf8_swedish_ci NOT NULL,
  `keterangan` varchar(100) COLLATE utf8_swedish_ci NOT NULL,
  `status` enum('Pending','Processing','Error','Success') COLLATE utf8_swedish_ci NOT NULL,
  `date` date NOT NULL,
  `time` time NOT NULL,
  `place_from` varchar(50) COLLATE utf8_swedish_ci NOT NULL DEFAULT 'WEB',
  `refund` enum('0','1') COLLATE utf8_swedish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_swedish_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pembelian_sosmed`
--

CREATE TABLE `pembelian_sosmed` (
  `id` int(10) NOT NULL,
  `oid` varchar(50) COLLATE utf8_swedish_ci NOT NULL,
  `provider_oid` varchar(50) COLLATE utf8_swedish_ci NOT NULL,
  `user` varchar(100) COLLATE utf8_swedish_ci NOT NULL,
  `layanan` varchar(100) COLLATE utf8_swedish_ci NOT NULL,
  `target` text COLLATE utf8_swedish_ci NOT NULL,
  `jumlah` int(10) NOT NULL,
  `remains` varchar(10) COLLATE utf8_swedish_ci NOT NULL,
  `start_count` varchar(10) COLLATE utf8_swedish_ci NOT NULL,
  `harga` double NOT NULL,
  `profit` double NOT NULL,
  `status` enum('Pending','Processing','Error','Partial','Success') COLLATE utf8_swedish_ci NOT NULL,
  `date` date NOT NULL,
  `time` time NOT NULL,
  `provider` varchar(100) COLLATE utf8_swedish_ci NOT NULL,
  `place_from` enum('Website','API') COLLATE utf8_swedish_ci NOT NULL,
  `refund` int(2) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_swedish_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pesan_tiket`
--

CREATE TABLE `pesan_tiket` (
  `id` int(10) NOT NULL,
  `id_tiket` int(10) NOT NULL,
  `pengirim` enum('Member','Admin') COLLATE utf8_swedish_ci NOT NULL,
  `user` varchar(50) COLLATE utf8_swedish_ci NOT NULL,
  `pesan` text COLLATE utf8_swedish_ci NOT NULL,
  `date` date NOT NULL,
  `time` time NOT NULL,
  `update_terakhir` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_swedish_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pesan_tsel`
--

CREATE TABLE `pesan_tsel` (
  `id` int(11) NOT NULL,
  `isi` varchar(255) CHARACTER SET utf8 COLLATE utf8_swedish_ci NOT NULL,
  `status` enum('UNREAD','READ') NOT NULL,
  `date` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `provider`
--

CREATE TABLE `provider` (
  `id` int(10) NOT NULL,
  `code` varchar(50) COLLATE utf8_swedish_ci NOT NULL,
  `link` varchar(100) COLLATE utf8_swedish_ci NOT NULL,
  `api_key` varchar(100) COLLATE utf8_swedish_ci NOT NULL,
  `api_id` varchar(50) COLLATE utf8_swedish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_swedish_ci;

-- --------------------------------------------------------

--
-- Table structure for table `provider_pulsa`
--

CREATE TABLE `provider_pulsa` (
  `id` int(10) NOT NULL,
  `code` varchar(50) COLLATE utf8_swedish_ci NOT NULL,
  `link` varchar(100) COLLATE utf8_swedish_ci NOT NULL,
  `key` varchar(100) COLLATE utf8_swedish_ci NOT NULL,
  `userid` varchar(100) COLLATE utf8_swedish_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8_swedish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_swedish_ci;

--
-- Dumping data for table `provider_pulsa`
--

INSERT INTO `provider_pulsa` (`id`, `code`, `link`, `key`, `userid`, `secret`) VALUES
(1, 'DPEDIA', 'https://serverh2h.id/order/pulsa', 'ac4nKE8YSWUQRS9zPVvA', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `riwayat_transfer`
--

CREATE TABLE `riwayat_transfer` (
  `id` int(10) NOT NULL,
  `pengirim` varchar(50) COLLATE utf8_swedish_ci NOT NULL,
  `penerima` varchar(50) COLLATE utf8_swedish_ci NOT NULL,
  `jumlah` double NOT NULL,
  `date` date NOT NULL,
  `time` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_swedish_ci;

-- --------------------------------------------------------

--
-- Table structure for table `setting_web`
--

CREATE TABLE `setting_web` (
  `id` int(11) NOT NULL,
  `short_title` varchar(50) NOT NULL,
  `title` varchar(150) NOT NULL,
  `deskripsi_web` text NOT NULL,
  `kontak_utama` text NOT NULL,
  `date` date NOT NULL,
  `time` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `setting_web`
--

INSERT INTO `setting_web` (`id`, `short_title`, `title`, `deskripsi_web`, `kontak_utama`, `date`, `time`) VALUES
(1, 'Pluto Pedia', 'Pluto Pedia- SerrveerTermurah & Distributor Pulsa Terbaik', 'Distributor pulsa operator H2H termurah. Provider PPOB terbaik dan terlengkap. Pusat server panel auto follower sosial media dan subscribe youtube tercepat. Jadilah member, agen atau reseller jasa sosial media, toko online, hingga PPOB online bersama Azfa Panel', '', '2019-01-03', '16:06:10');

-- --------------------------------------------------------

--
-- Table structure for table `tiket`
--

CREATE TABLE `tiket` (
  `id` int(10) NOT NULL,
  `user` varchar(50) NOT NULL,
  `subjek` varchar(250) NOT NULL,
  `pesan` text NOT NULL,
  `date` date NOT NULL,
  `time` time NOT NULL,
  `update_terakhir` datetime NOT NULL,
  `status` enum('Pending','Responded','Waiting','Closed') NOT NULL,
  `this_user` int(1) NOT NULL,
  `this_admin` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tiket`
--

INSERT INTO `tiket` (`id`, `user`, `subjek`, `pesan`, `date`, `time`, `update_terakhir`, `status`, `this_user`, `this_admin`) VALUES
(2, 'nikocintadina', 'UMUM', 'Kak nomer pelanggan itu apa yah', '2021-04-07', '22:55:30', '2021-04-07 22:58:52', 'Closed', 0, 1),
(3, 'demo', 'ORDERAN', 'saya ingin membuat sesuatu', '2021-09-03', '08:14:12', '2021-09-03 08:24:59', 'Closed', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) NOT NULL,
  `nama` varchar(50) COLLATE utf8_swedish_ci NOT NULL,
  `email` varchar(50) COLLATE utf8_swedish_ci NOT NULL,
  `username` varchar(100) COLLATE utf8_swedish_ci NOT NULL,
  `password` varchar(100) COLLATE utf8_swedish_ci NOT NULL,
  `saldo` int(10) NOT NULL,
  `pemakaian_saldo` double NOT NULL,
  `level` enum('Member','Agen','Admin','Developers','Reseller') COLLATE utf8_swedish_ci NOT NULL,
  `status` enum('Aktif','Tidak Aktif') COLLATE utf8_swedish_ci NOT NULL,
  `api_key` varchar(100) COLLATE utf8_swedish_ci NOT NULL,
  `uplink` varchar(100) COLLATE utf8_swedish_ci NOT NULL,
  `terdaftar` datetime NOT NULL,
  `update_nama` int(1) NOT NULL,
  `random_kode` varchar(20) COLLATE utf8_swedish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_swedish_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `nama`, `email`, `username`, `password`, `saldo`, `pemakaian_saldo`, `level`, `status`, `api_key`, `uplink`, `terdaftar`, `update_nama`, `random_kode`) VALUES
(257, 'PEDIA', 'ijunproject@gmail.com', 'demo', '$2y$10$9pEidkm9i5Yh5Rk2gmXio.SHc.Hd/D4u7pyCGqf9.5Xsrzf8uvgT6', 412911, 2147597735.6, 'Developers', 'Aktif', 'RPBmSaLyRvXztLpP3akc1vk2rGTLgbdx', 'Pendaftaran Gratis', '2021-03-29 09:57:11', 0, 'hJovzSrbKT4368171260');

-- --------------------------------------------------------

--
-- Table structure for table `voucher`
--

CREATE TABLE `voucher` (
  `id` int(10) NOT NULL,
  `voucher` varchar(50) NOT NULL,
  `saldo` varchar(250) NOT NULL,
  `status` enum('active','sudah di redeem') NOT NULL,
  `user` varchar(50) NOT NULL,
  `date` date NOT NULL,
  `time` time NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `voucher`
--

INSERT INTO `voucher` (`id`, `voucher`, `saldo`, `status`, `user`, `date`, `time`) VALUES
(13, 'VC-0499001711272', '10000', 'sudah di redeem', 'demo', '2021-06-23', '21:20:35'),
(14, 'VC-2619056807597', '1000000000000', 'sudah di redeem', 'demo', '2021-06-21', '00:19:48'),
(15, 'VC-5320491374243', '3000000', 'sudah di redeem', 'demo', '2021-07-13', '16:10:06'),
(16, 'VC-0207835808640', '500000000000', 'sudah di redeem', 'demo', '2021-07-13', '16:11:38'),
(17, 'VC-1543812141477', '500000', 'sudah di redeem', 'demo', '2021-08-20', '12:31:28'),
(18, 'VC-7477370626879', '10000', 'active', '-', '2021-09-02', '10:33:10');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `berita`
--
ALTER TABLE `berita`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `deposit`
--
ALTER TABLE `deposit`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `deposit_bank`
--
ALTER TABLE `deposit_bank`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `deposit_dana`
--
ALTER TABLE `deposit_dana`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `deposit_emoney`
--
ALTER TABLE `deposit_emoney`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `deposit_gopay`
--
ALTER TABLE `deposit_gopay`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `deposit_ovo`
--
ALTER TABLE `deposit_ovo`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `deposit_voucher`
--
ALTER TABLE `deposit_voucher`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `halaman`
--
ALTER TABLE `halaman`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `harga_pendaftaran`
--
ALTER TABLE `harga_pendaftaran`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `history_saldo`
--
ALTER TABLE `history_saldo`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kategori_layanan`
--
ALTER TABLE `kategori_layanan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `layanan_pulsa`
--
ALTER TABLE `layanan_pulsa`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `layanan_sosmed`
--
ALTER TABLE `layanan_sosmed`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `log`
--
ALTER TABLE `log`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `metode_depo`
--
ALTER TABLE `metode_depo`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pembelian_game`
--
ALTER TABLE `pembelian_game`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `pembelian_pulsa`
--
ALTER TABLE `pembelian_pulsa`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pembelian_sms`
--
ALTER TABLE `pembelian_sms`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pembelian_sosmed`
--
ALTER TABLE `pembelian_sosmed`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pesan_tiket`
--
ALTER TABLE `pesan_tiket`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pesan_tsel`
--
ALTER TABLE `pesan_tsel`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `provider`
--
ALTER TABLE `provider`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `provider_pulsa`
--
ALTER TABLE `provider_pulsa`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `riwayat_transfer`
--
ALTER TABLE `riwayat_transfer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `setting_web`
--
ALTER TABLE `setting_web`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tiket`
--
ALTER TABLE `tiket`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `voucher`
--
ALTER TABLE `voucher`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `berita`
--
ALTER TABLE `berita`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `deposit`
--
ALTER TABLE `deposit`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=66;

--
-- AUTO_INCREMENT for table `deposit_bank`
--
ALTER TABLE `deposit_bank`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `deposit_dana`
--
ALTER TABLE `deposit_dana`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `deposit_emoney`
--
ALTER TABLE `deposit_emoney`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `deposit_gopay`
--
ALTER TABLE `deposit_gopay`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `deposit_ovo`
--
ALTER TABLE `deposit_ovo`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `deposit_voucher`
--
ALTER TABLE `deposit_voucher`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=398;

--
-- AUTO_INCREMENT for table `halaman`
--
ALTER TABLE `halaman`
  MODIFY `id` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `harga_pendaftaran`
--
ALTER TABLE `harga_pendaftaran`
  MODIFY `id` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `history_saldo`
--
ALTER TABLE `history_saldo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;

--
-- AUTO_INCREMENT for table `kategori_layanan`
--
ALTER TABLE `kategori_layanan`
  MODIFY `id` int(30) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=932;

--
-- AUTO_INCREMENT for table `layanan_pulsa`
--
ALTER TABLE `layanan_pulsa`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3833;

--
-- AUTO_INCREMENT for table `layanan_sosmed`
--
ALTER TABLE `layanan_sosmed`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2758;

--
-- AUTO_INCREMENT for table `log`
--
ALTER TABLE `log`
  MODIFY `id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=625;

--
-- AUTO_INCREMENT for table `metode_depo`
--
ALTER TABLE `metode_depo`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT for table `pembelian_game`
--
ALTER TABLE `pembelian_game`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pembelian_pulsa`
--
ALTER TABLE `pembelian_pulsa`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pembelian_sms`
--
ALTER TABLE `pembelian_sms`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pembelian_sosmed`
--
ALTER TABLE `pembelian_sosmed`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `pesan_tiket`
--
ALTER TABLE `pesan_tiket`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `pesan_tsel`
--
ALTER TABLE `pesan_tsel`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `provider`
--
ALTER TABLE `provider`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `provider_pulsa`
--
ALTER TABLE `provider_pulsa`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `riwayat_transfer`
--
ALTER TABLE `riwayat_transfer`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `setting_web`
--
ALTER TABLE `setting_web`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tiket`
--
ALTER TABLE `tiket`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=408;

--
-- AUTO_INCREMENT for table `voucher`
--
ALTER TABLE `voucher`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
