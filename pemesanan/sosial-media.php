<?php
session_start();
require '../config.php';
require '../lib/session_user.php';
if (isset($_POST['pesan'])) {
    require '../lib/session_login.php';
    $post_kategori = $conn->real_escape_string(trim(filter($_POST['kategori'])));
    $post_layanan = $conn->real_escape_string(trim(filter($_POST['layanan'])));
    $post_jumlah = $conn->real_escape_string(trim(filter($_POST['jumlah'])));
    $post_target = $conn->real_escape_string(trim(filter($_POST['target'])));
    $post_comments = $_POST['comments'];

    $cek_layanan = $conn->query("SELECT * FROM layanan_sosmed WHERE service_id = '$post_layanan' AND status = 'Aktif'");
    $data_layanan = mysqli_fetch_assoc($cek_layanan);

    $cek_pesanan = $conn->query("SELECT * FROM pembelian_sosmed WHERE target = '$post_target' AND status = 'Pending'");
    $data_pesanan = mysqli_fetch_assoc($cek_pesanan);

    $cek_harga = $data_layanan['harga'] / 1000;
    $cek_profit = $data_layanan['profit'] / 2000;
    $hitung = count(explode(PHP_EOL, $post_comments));
    $replace = str_replace("\r\n", '\r\n', $post_comments);
    if (!empty($post_comments)) {
        $post_jumlah = $hitung;
    } else {
        $post_jumlah = $post_jumlah;
    }
    // $price = $rate*$post_quantity;
    if (!empty($post_comments)) {
        $harga = $cek_harga * $hitung;
        $profit = $cek_profit * $hitung;
    } else {
        $harga = $cek_harga * $post_jumlah;
        $profit = $cek_profit * $post_jumlah;
    }
    $order_id = acak_nomor(3) . acak_nomor(4);
    $provider = $data_layanan['provider'];

    $cek_provider = $conn->query("SELECT * FROM provider WHERE code = '$provider'");
    $data_provider = mysqli_fetch_assoc($cek_provider);

    //Get Start Count
    if ($data_layanan['kategori'] == "Instagram Likes" and "Instagram Likes Indonesia" and "Instagram Likes [Targeted Negara]" and "Instagram Likes/Followers Per Minute") {
        $start_count = likes_count($post_target);
    } else if ($data_layanan['kategori'] == "Instagram Followers No Refill/Not Guaranteed" and "Instagram Followers Indonesia" and "Instagram Followers [Negara]" and "Instagram Followers [Refill] [Guaranteed] [NonDrop]") {
        $start_count = followers_count($post_target);
    } else if ($data_layanan['kategori'] == "Instagram Views") {
        $start_count = views_count($post_target);
    } else {
        $start_count = 0;
    }

    if (!$post_target || !$post_layanan || !$post_kategori) {
        $_SESSION['hasil'] = array('alert' => 'danger', 'judul' => 'Order gagal', 'pesan' => 'Silahkan lengkapi bidang berikut :<br/> - Kategori <br /> - Layanan <br /> - Target');
    } else if (mysqli_num_rows($cek_layanan) == 0) {
        $_SESSION['hasil'] = array('alert' => 'danger', 'judul' => 'Order gagal', 'pesan' => 'Layanan tidak tersedia');
    } else if (mysqli_num_rows($cek_provider) == 0) {
        $_SESSION['hasil'] = array('alert' => 'danger', 'judul' => 'Order gagal', 'pesan' => 'Server sedang maintenance');
    } else if ($post_jumlah < $data_layanan['min']) {
        $_SESSION['hasil'] = array('alert' => 'danger', 'judul' => 'Order gagal', 'pesan' => 'Jumlah minimal order ' . number_format($data_layanan['min'], 0, ',', '.') . '.');
    } else if ($post_jumlah > $data_layanan['max']) {
        $_SESSION['hasil'] = array('alert' => 'danger', 'judul' => 'Order gagal', 'pesan' => 'Jumlah maksimal order ' . number_format($data_layanan['max'], 0, ',', '.') . '.');
    } else if ($data_user['saldo'] < $harga) {
        $_SESSION['hasil'] = array('alert' => 'danger', 'judul' => 'Order gagal', 'pesan' => 'Saldo tidak mencukupi untuk melakukan order');
    } else if (mysqli_num_rows($cek_pesanan) == 1) {
        $_SESSION['hasil'] = array('alert' => 'danger', 'judul' => 'Order gagal', 'pesan' => 'Dilarang order layanan & target yang sama, sebelum order sebelumnya sukses');
    } else {

        if ($provider == "MANUAL") {
            $api_postdata = "";
        } else if ($provider == "BOOSTNAA") {
            $dataLayananServiceLayanan = $data_layanan['layanan'];

            if ($post_comments == false) {
                $postdata = "api_key=" . $data_provider['api_key'] . "&action=pemesanan&layanan=$dataLayananServiceLayanan&target=$post_target&jumlah=$post_jumlah";
            } else if ($post_comments == true) {
                $postdata = "api_key=" . $data_provider['api_key'] . "&action=pemesanan&layanan=$dataLayananServiceLayanan&target=$post_target&jumlah=$post_jumlah&custom_comments=$post_comments";
            }

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, "https://boostnaa.com/api/sosial-media.php");
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            $chresult = curl_exec($ch);
            curl_close($ch);
            $json_result = json_decode($chresult, true);
        } else {
            die("System Error!");
        }

        if ($provider == "BOOSTNAA" and $json_result['status'] == false) {
            $_SESSION['hasil'] = array('alert' => 'danger', 'judul' => 'Order gagal', 'pesan' => isset($json_result['data']['pesan']) ? $json_result['data']['pesan'] : 'Layanan sedang maintenance');
        } else {
            if ($provider == "BOOSTNAA") {
                $provider_oid = $json_result['data']['id'];
            }

            if ($conn->query("INSERT INTO pembelian_sosmed VALUES (null,'$order_id', '$provider_oid', '$sess_username', '" . $data_layanan['layanan'] . "', '$post_target', '$post_jumlah', '0', '$start_count', '$harga', '$profit', 'Pending', '$date', '$time', '$provider', 'Website', '0')") == true) {
                $conn->query("UPDATE users SET saldo = saldo-$harga, pemakaian_saldo = pemakaian_saldo+$harga WHERE username = '$sess_username'");
                $conn->query("INSERT INTO history_saldo VALUES (null, '$sess_username', 'Pengurangan saldo', '$harga', 'Pemesanan dengan Order ID $order_id', '$date', '$time')");
                $jumlah = number_format($post_jumlah, 0, ',', '.');
                $harga2 = number_format($harga, 0, ',', '.');
                $_SESSION['hasil'] = array(
                    'alert' => 'success',
                    'judul' => 'Order berhasil',
                    'pesan' => '<b>Order ID : </b> ' . $order_id . '<br />
					- <b>Layanan : </b> ' . $data_layanan['layanan'] . '<br />
					- <b>Tujuan : </b> ' . $post_target . '<br />
					- <b>Start Count : </b> ' . $start_count . '<br />
					- <b>Jumlah Pesan : </b> ' . $jumlah . '<br />
					- <b>Total Harga : </b> Rp ' . $harga2 . ''
                );
            } else {
                $_SESSION['hasil'] = array('alert' => 'danger', 'judul' => 'Order gagal', 'pesan' => 'Layanan sedang sinkronisasi. Lakukan order baru');
            }
        }
    }
}

require("../lib/header.php");
?>

<br>
<br>
<br>
<br>
<br>
<!--Title-->
<title>SOSIAL MEDIA</title>
<meta name="description"
    content="Platform Layanan Digital All in One, Berkualitas, Cepat & Aman. Menyediakan Produk & Layanan Pemasaran Sosial Media, Payment Point Online Bank, Layanan Pembayaran Elektronik, Optimalisasi Toko Online, Voucher Game dan Produk Digital." />

<div class="row">
    <div class="col-md-7">
        <div class="card">
            <div class="card-body">
                <h4 class="m-t-0 text-uppercase text-center header-title"><i
                        class="mdi mdi-instagram mr1 text-primary"></i><b> Penambahan SOSIAL MEDIA</b></h4>
                <hr>
                <form class="form-horizontal" method="POST">
                    <input type="hidden" name="csrf_token" value="<?php echo $config['csrf_token'] ?>">
                    <div class="form-group">
                        <label class="col-md-12 control-label">Kategori *</label>
                        <div class="col-md-12">
                            <select class="form-control" id="kategori" name="kategori">
                                <option value="">Pilih Salah Satu</option>
                                <?php
                                $cek_kategori = $conn->query("SELECT * FROM kategori_layanan WHERE tipe = 'Sosial Media' ORDER BY nama ASC");
                                while ($data_kategori = $cek_kategori->fetch_assoc()) {
                                ?>
                                <option value="<?php echo $data_kategori['kode']; ?>">
                                    <?php echo $data_kategori['nama']; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-12 control-label">Layanan *</label>
                        <div class="col-md-12">
                            <select class="form-control" name="layanan" id="layanan">
                                <option value="0">Pilih Kategori</option>
                            </select>
                        </div>
                    </div>

                    <div id="catatan">
                    </div>

                    <div class="form-group">
                        <label class="col-md-12 control-label">Target *</label>
                        <div class="col-md-12">
                            <input type="text" name="target" class="form-control" placeholder="Username / Link">
                        </div>
                    </div>

                    <div id="show1">
                        <div class="form-row">

                            <div class="form-group col-md-6">
                                <label class="col-md-12 col-form-label">Jumlah *</label>
                                <div class="col-md-12">
                                    <input type="number" name="jumlah" class="form-control" placeholder="Jumlah"
                                        onkeyup="get_total(this.value).value;">
                                </div>
                            </div>
                            <input type="hidden" id="rate" value="0">
                            <div class="form-group col-md-6">
                                <label class="col-md-12 col-form-label">Total Harga</label>
                                <div class="col-md-12">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">Rp. </span>
                                        </div>
                                        <input type="number" class="form-control" id="total" readonly>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div id="show2" style="display: none;">
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label class="col-md-12 col-form-label">Komentar</label>
                                <div class="col-md-12">
                                    <textarea class="form-control" name="comments" id="comments"
                                        placeholder="Pisahkan setiap baris komentar dengan enter"></textarea>
                                </div>
                            </div>
                            <input type="hidden" id="rate" value="0">
                            <div class="form-group col-md-6">
                                <label class="col-form-label">Total Harga</label>
                                <div class="col-md-12">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">Rp. </span>
                                        </div>
                                        <input type="number" class="form-control" id="totalxx" readonly>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12"> <button type="submit"
                            class="pull-right btn btn-block btn--md btn-primary waves-effect w-md waves-light"
                            name="pesan"><i class="mdi mdi-cart"></i>Order</button>

                </form>
            </div>
        </div>
    </div>
</div>
<!-- end col -->

<!-- INFORMASI ORDER -->
<div class="col-md-5">
    <div class="card">
        <div class="card-body">

            <center>
                <h4 class="m-t-0 text-uppercase header-title"><i class="fa fa-info-circle"></i><b> Informasi Order</h4>
                </b>
                Gunakan koneksi internet yang stabil agar daftar layanan sinkron dengan kategori yang dipilih.
                <hr>
            </center>

            <!--CARA-->
            <div class="table-responsive">
                <center><i class="fa fa-check-circle"></i><b> Langkah Penambahan</b></center>
                <ol class="list-p">
                    <li>Pilih salah satu kategori & layanan.</li>
                    <li>Masukkan <u>username</u> / <u>link</u> pada target (sesuai layanan)</li>
                    <li>Pastikan target & format valid, lihat contoh dibawah</li>
                    <li>Contoh username IG: <b>boostnaa</b> (tanpa @).</li>
                    <li>Contoh link postingan IG: <b>https://www.instagram.com/p/asasaasa/</b></li>
                    <li>Jika target bukan platform instagram, ikuti diskripsi yang tertera dibawah kolom layanan.</li>
                    <li>Klik <span class="badge badge-primary"><b>Order</b></span> untuk membuat pesanan.</li>
                </ol>
            </div>

            <!--KETENTUAN-->
            <div class="table-responsive">
                <center><i class="fa fa-check-circle"></i><b> Syarat & Ketentuan Order</b></center>
                <ol class="list-p">
                    <li>Mengikuti langkah-langkah diatas.</li>
                    <li>Jika ingin <u>order baru</u> untuk layanan & target yang sama, tunggu <u>order sebelumnya</u>
                        <span class="badge badge-success"><b>Success</b></span>.
                    </li>
                    <li>Detail status order tampil setelah membuat pesanan.</li>
                    <li>Jika kurang jelas, tanyakan ke CS via <span class="badge badge-info"><a href="../tiket"
                                alt="Tiket Bantuan" title="Tiket Bantuan" target="_blank" style="color:#ffffff;"><b>Live
                                    Chat</b></a></span>.</li>
                </ol>
            </div>

        </div>
    </div>
</div>
<!-- INFORMASI ORDER -->

</div>
</div>
</div>
</div>

<script type="text/javascript">
$(document).ready(function() {
    $("#kategori").change(function() {
        var kategori = $("#kategori").val();
        $.ajax({
            url: '<?php echo $config['web']['url']; ?>ajax/layanan_sosmed.php',
            data: 'kategori=' + kategori,
            type: 'POST',
            dataType: 'html',
            success: function(msg) {
                $("#layanan").html(msg);
            }
        });
    });
    $("#layanan").change(function() {
        var layanan = $("#layanan").val();
        $.ajax({
            url: '<?php echo $config['web']['url']; ?>ajax/catatan_sosmed.php',
            data: 'layanan=' + layanan,
            type: 'POST',
            dataType: 'html',
            success: function(msg) {
                $("#catatan").html(msg);
            }
        });
        $.ajax({
            url: '<?php echo $config['web']['url']; ?>ajax/rate_sosmed.php',
            data: 'layanan=' + layanan,
            type: 'POST',
            dataType: 'html',
            success: function(msg) {
                $("#rate").val(msg);
            }
        });
    });
});
document.getElementById("show1").style.display = "none";
$("#layanan").change(function() {
    var selectedCountry = $("#layanan option:selected").text();
    if (selectedCountry.indexOf('Komen') !== -1 || selectedCountry.indexOf('komen') !== -1 || selectedCountry
        .indexOf('comment') !== -1 || selectedCountry.indexOf('Comment') !== -1) {
        document.getElementById("show1").style.display = "none";
        document.getElementById("show2").style.display = "block";
    } else {
        document.getElementById("show1").style.display = "block";
        document.getElementById("show2").style.display = "none";
    }
});
$(document).ready(function() {
    $("#comments").on("keypress", function(a) {
        if (a.which == 13) {
            var baris = $("#comments").val().split(/\r|\r\n|\n/).length;
            var rates = $("#rate").val();
            var calc = eval(baris) * rates;
            console.log(calc)
            $('#totalxx').val(calc);
        }
    });

});

function get_total(quantity) {
    var rate = $("#rate").val();
    var result = eval(quantity) * rate;
    $('#total').val(result);
}
</script>
<?php
require("../lib/footer.php");
?>