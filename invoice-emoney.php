<?php
session_start();
require 'config.php';
require 'lib/session_user.php';
require 'lib/session_login.php';

$cek_depo = $conn->query("SELECT * FROM deposit_emoney WHERE username = '$sess_username' AND status = 'Pending'");
$data_depo = $cek_depo->fetch_assoc();
$depo = $cek_depo->num_rows;

$get_id = $data_depo['kode_deposit'];
$saldo = $data_depo['get_saldo'];
$username = $data_depo['username'];

if ($depo == 0) {  
	$_SESSION['hasil'] = array('alert' => 'danger', 'judul' => 'Deposit Pending Tidak Ditemukan', 'pesan' => 'Pilih Metode Deposit Untuk Mengisi Saldo');
	exit(header("Location: pay"));
} else {


	if ($data_depo['status'] == "Pending") {
		$label = "warning";
	} else if ($data_depo['status'] == "Error") {
		$label = "danger";     
	}
	if ($data_depo['status'] == "Pending") {
		$message = "Menunggu pembayaran";
	} else if ($data_depo['status'] == "Error") {
		$message = "Permintaan dibatalkan";
	} 

	if (isset($_POST['cancel'])) {

		if ($conn->query("UPDATE deposit_emoney SET status = 'Error'  WHERE kode_deposit = '$get_id'") == true){

			$_SESSION['hasil'] = array('alert' => 'success', 'judul' => 'Deposit Dibatalkan', 'pesan' => 'Anda Telah Membatalkan Deposit');
			exit(header("location: pay"));
			}
		}
	}

require 'lib/header.php';
?>

<div class="app-content content ">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper">
            <div class="content-header row">
            </div>
            <div class="content-body">
<!--Title-->
<title>Invoice Deposit</title>
<meta name="description" content="Platform Layanan Digital All in One, Berkualitas, Cepat & Aman. Menyediakan Produk & Layanan Pemasaran Sosial Media, Payment Point Online Bank, Layanan Pembayaran Elektronik, Optimalisasi Toko Online, Voucher Game dan Produk Digital."/>

<div class="row">
	<div class="col-md-12">
		<div class="card">
			<div class="card-body">
				<div class="clearfix">
					<div class="text-center">
						<h4 class="m-0">Invoice #<?php echo $data_depo['kode_deposit']; ?></h4>
						<img src="/assets/images/pembayaran/kincaimedia-deposit-emoney.png" alt="Invoice" height="50">
					</div>
				</div>


				<div class="row">
					<div class="col-md-6">
						<div class="float-left mt-3">
							<p><b>Hallo <?php echo $data_user['nama']; ?> (<?php echo $sess_username; ?>)</b></p>
							<p class="text-muted">Silahkan lakukan pembayaran dengan detail faktur sebagai berikut: </p>
						</div>

					</div><!-- end col -->
					<div class="col-md-6">
						<div class="mt-3 text-md-right">
							<p><strong>Tanggal: </strong> <?php echo $data_depo['date']; ?></p>
							<p><span class="badge bg-<?php echo $label; ?>"><?php echo $message; ?></span></p>
							<p><strong>ID Deposit: </strong> #<?php echo $data_depo['kode_deposit']; ?></p>
						</div>
					</div><!-- end col -->
				</div>
				<!-- end row -->

				<div class="row">
					<div class="col-md-12">
						<div class="table-responsive">
							<table class="table table-centered mt-3">
								<thead>
									<th>#</th>
									<th>Metode</th>
									<th>Tujuan</th>
									<th>Jumlah Transfer</th>
									<th>Saldo Diterima</th>
								</tr></thead>
								<tbody>
									<tr>
										<td>1</td>
										<td><?php echo $data_depo['payment']; ?></td>
										<td><font color='#0d8bf2'><b><?php echo $data_depo['tujuan']; ?></b></font></td>
										<td class="text-rigth">Rp. <b><?php echo number_format($data_depo['jumlah_transfer'],0,',','.'); ?></b></td>
										<td class="text-rigth">Rp. <b><?php echo number_format($data_depo['get_saldo'],0,',','.'); ?></b></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-6">
						<div class="clearfix pt-3">
							<h5 class="text-muted">Catatan:</h5>
							<p class="text-muted">Lakukan pembayaran selambat-lambatnya 3 jam setelah faktur ini dibuat.<br/><br/>Silahkan transfer sejumlah Rp. <?php echo number_format($data_depo['jumlah_transfer'],0,',','.'); ?>.</p>
							<p class="text-muted"><b>Jangan <span class="badge badge-danger">BATALKAN</span> jika transfer sukses</b>. Tunggu hingga saldo masuk <span class="badge badge-success">OTOMATIS</span>.</p>
							<p class="text-muted"><b>Deposit belum masuk hingga 30 menit?</b> Silahkan konfirmasi via <a href="/tiket" target="_blank"><b> Tiket</b></a> atau <a href="https://api.whatsapp.com/send?phone=6281384248407&text=Konfirmasi%20deposit%20ID%20<?php echo $data_depo['kode_deposit']; ?>" target="_blank"><b> WA</b></a>.</p>
						</div>
					</div>
					<div class="col-sm-6">
						<div class="float-right pt-3">
							<h3>Total <?php echo number_format($data_depo['jumlah_transfer'],0,',','.'); ?> IDR</h3><hr>
						</div>
						<div class="clearfix"></div>
					</div>
				</div>


				<div class="d-print-none m-t-30 m-b-30">
					<div class="text-right">
						<form method="POST">
							<input type="hidden" name="csrf_token" value="<?php echo $config['csrf_token'] ?>">
							<button class="btn btn-danger" name="cancel"> BATALKAN </button>
						</form>
					</div>
				</div>
			</div>
		</div>

	</div>

</div>
<!-- end row -->

</div> <!-- end container-fluid -->
</div>
<!-- end wrapper -->

<?php
require 'lib/footer.php';
?>           