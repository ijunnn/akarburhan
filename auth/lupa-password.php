<?php
session_start();
require '../config.php';

if ($_POST) {

	if(isset($_POST['g-recaptcha-response'])){
		$captcha=$_POST['g-recaptcha-response'];
	}
	if(!$captcha){
		$_SESSION['hasil'] = array('alert' => 'danger', 'judul' => 'Pendaftaran Gagal', 'pesan' => 'Verifikasi bahwa anda bukan <font color="red">robot!</font>');
		exit(header("Location: lupa-password"));
	}
	$secret_key = '6LeuVJ0cAAAAAHTYhrfGpazEqvt4Pqh_Qke64VzM';
	$ip = $_SERVER['REMOTE_ADDR'];
	$url = 'https://www.google.com/recaptcha/api/siteverify?secret=' . urlencode($secret_key) . '&response=' . $captcha;
	$response = file_get_contents($url);
	$responseKeys = json_decode($response,true);

//Call email form validation
	$PostEmail = $conn->real_escape_string(filter(trim($_POST['email'])));
	$cek_email = $conn->query("SELECT * FROM users WHERE email = '$PostEmail'");
	$email = $cek_email->fetch_assoc();

//Call username form validation
	$PostUsername = $conn->real_escape_string(filter(trim($_POST['username'])));
	$cek_username = $conn->query("SELECT * FROM users WHERE username = '$PostUsername'");
	$user = $cek_username->fetch_assoc();

//Send new pass to email by username or email method
	$CallPostEmailUsername1 = $conn->real_escape_string(filter(trim($_POST['email'])));
	$CallPostEmailUsername2 = $conn->real_escape_string(filter(trim($_POST['username'])));
	$cek_emailusername = $conn->query("SELECT * FROM users WHERE username = '$CallPostEmailUsername2' OR email = '$CallPostEmailUsername1'");
	$useremail = $cek_emailusername->fetch_assoc();

//Validation form
	if (!$PostEmail && !$PostUsername) {
		$_SESSION['hasil'] = array('alert' => 'danger', 'judul' => 'Permintaan Gagal', 'pesan' => 'Silahkan Lengkapi Bidang Berikut :<br /> - Email<br/> <b>Atau</b><br/> - Username');
	} else if ($cek_email->num_rows == 0 && !$PostUsername) {
		$_SESSION['hasil'] = array('alert' => 'danger', 'judul' => 'Permintaan Gagal', 'pesan' => 'Email <strong>'.$PostEmail.' </strong> tidak terdaftar'); 
	} else if ($cek_username->num_rows == 0 && !$PostEmail) {
		$_SESSION['hasil'] = array('alert' => 'danger', 'judul' => 'Permintaan Gagal', 'pesan' => 'Username <strong>'.$PostUsername.' </strong> tidak terdaftar');
	} else if ($cek_email->num_rows == 0 && $cek_username->num_rows == 0) {
		$_SESSION['hasil'] = array('alert' => 'danger', 'judul' => 'Permintaan Gagal', 'pesan' => 'Email <strong>'.$PostEmail.' </strong> atau Username <strong>'.$PostUsername.' </strong> tidak terdaftar');
	} else {

		$acakin_password = acak(10).acak_nomor(10);
		$hash_pass = password_hash($acakin_password, PASSWORD_DEFAULT);
		$tujuan = $useremail['email'];
		$pesannya = "
		<p>Hai ".$useremail['username']."</p>
		<p>Anda telah membuat permintaan <b>Reset Password</b> untuk akun ".$useremail['email'].".</p>
		<p>Gunakan <b>Kode Unik</b> dibawah ini sebagai password baru, kemudian login dan mengatur ulang kata sandi di menu pengaturan.</p>
		<p>
		Email: ".$useremail['email']."<br/>
		Username: ".$useremail['username']."<br/>
		Password: $acakin_password<br/>
		Halaman Login: ".'<a href="https://boostnaa.com/auth/login">'." Member</a>
		</p>
		<hr>
		";
		$subjek = "Password akun Anda diubah";
		$header = "From:Administrator info@boostnaa.com\r\n";
		$header .= "Cc:info@boostnaa.com\r\n";
		$header .= "MIME-Version:1.0\r\n";
		$header .= "Content-type:text/html\r\n";
		$send = mail ($tujuan, $subjek, $pesannya, $header);
		if ($conn->query("UPDATE users SET password = '$hash_pass', random_kode = '$acakin_password' WHERE email = '".$useremail['email']."' ") == true) {
			$_SESSION['hasil'] = array('alert' => 'success', 'judul' => 'Ganti password berhasil!', 'pesan' => 'Cek email untuk mengetahui password baru');
			exit(header("Location: login"));
		} else {
			$_SESSION['hasil'] = array('alert' => 'danger', 'judul' => 'Ganti password gagal!', 'pesan' => 'Permintaan ganti password gagal');
			exit(header("Location: lupa-password"));
		}
	}
}

require '../lib/header_login.php';
?>
<!--Title-->
<title>Reset Password <?php echo $data['short_title']; ?></title>
<meta name="description" content="Platform Layanan Digital All in One, Berkualitas, Cepat & Aman. Menyediakan Produk & Layanan Pemasaran Sosial Media, Payment Point Online Bank, Layanan Pembayaran Elektronik, Optimalisasi Toko Online, Voucher Game dan Produk Digital."/>

<!--OG2-->
<meta content="Lupa Password <?php echo $data['short_title']; ?>" property="og:title"/>
<meta content="Platform Layanan Digital All in One, Berkualitas, Cepat & Aman. Menyediakan Produk & Layanan Pemasaran Sosial Media, Payment Point Online Bank, Layanan Pembayaran Elektronik, Optimalisasi Toko Online, Voucher Game dan Produk Digital." property="og:description"/>
<meta content="Lupa Password <?php echo $data['short_title']; ?>" property="og:headline"/>
<meta content="<?php echo $config['web']['url'];?>assets/images/halaman/tentang-kami.png" property="og:image"/>
<meta content="Lupa Password <?php echo $data['short_title']; ?>" property="twitter:title"/>
<meta content="Platform Layanan Digital All in One, Berkualitas, Cepat & Aman. Menyediakan Produk & Layanan Pemasaran Sosial Media, Payment Point Online Bank, Layanan Pembayaran Elektronik, Optimalisasi Toko Online, Voucher Game dan Produk Digital." property="twitter:description"/>
<meta content="<?php echo $config['web']['url'];?>assets/images/halaman/tentang-kami.png" property="twitter:image"/>

<body class="vertical-layout vertical-menu-modern blank-page navbar-floating footer-static  " data-open="click" data-menu="vertical-menu-modern" data-col="blank-page">
    <!-- BEGIN: Content-->
    <div class="app-content content ">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper">
            <div class="content-header row">
            </div>
            <div class="content-body">
                <div class="auth-wrapper auth-v1 px-2">
                    <div class="auth-inner py-2">
                                               <div class="card mb-0">
                            <div class="card-body">
                                <a href="javascript:void(0);" class="brand-logo">
                                    <svg viewbox="0 0 139 95" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" height="28">
                                        <defs>
                                            <lineargradient id="linearGradient-1" x1="100%" y1="10.5120544%" x2="50%" y2="89.4879456%">
                                                <stop stop-color="#000000" offset="0%"></stop>
                                                <stop stop-color="#FFFFFF" offset="100%"></stop>
                                            </lineargradient>
                                            <lineargradient id="linearGradient-2" x1="64.0437835%" y1="46.3276743%" x2="37.373316%" y2="100%">
                                                <stop stop-color="#EEEEEE" stop-opacity="0" offset="0%"></stop>
                                                <stop stop-color="#FFFFFF" offset="100%"></stop>
                                            </lineargradient>
                                        </defs>
                                        <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                            <g id="Artboard" transform="translate(-400.000000, -178.000000)">
                                                <g id="Group" transform="translate(400.000000, 178.000000)">
                                                    <path class="text-primary" id="Path" d="M-5.68434189e-14,2.84217094e-14 L39.1816085,2.84217094e-14 L69.3453773,32.2519224 L101.428699,2.84217094e-14 L138.784583,2.84217094e-14 L138.784199,29.8015838 C137.958931,37.3510206 135.784352,42.5567762 132.260463,45.4188507 C128.736573,48.2809251 112.33867,64.5239941 83.0667527,94.1480575 L56.2750821,94.1480575 L6.71554594,44.4188507 C2.46876683,39.9813776 0.345377275,35.1089553 0.345377275,29.8015838 C0.345377275,24.4942122 0.230251516,14.560351 -5.68434189e-14,2.84217094e-14 Z" style="fill: currentColor"></path>
                                                    <path id="Path1" d="M69.3453773,32.2519224 L101.428699,1.42108547e-14 L138.784583,1.42108547e-14 L138.784199,29.8015838 C137.958931,37.3510206 135.784352,42.5567762 132.260463,45.4188507 C128.736573,48.2809251 112.33867,64.5239941 83.0667527,94.1480575 L56.2750821,94.1480575 L32.8435758,70.5039241 L69.3453773,32.2519224 Z" fill="url(#linearGradient-1)" opacity="0.2"></path>
                                                    <polygon id="Path-2" fill="#000000" opacity="0.049999997" points="69.3922914 32.4202615 32.8435758 70.5039241 54.0490008 16.1851325"></polygon>
                                                    <polygon id="Path-21" fill="#000000" opacity="0.099999994" points="69.3922914 32.4202615 32.8435758 70.5039241 58.3683556 20.7402338"></polygon>
                                                    <polygon id="Path-3" fill="url(#linearGradient-2)" opacity="0.099999994" points="101.428699 0 83.0667527 94.1480575 130.378721 47.0740288"></polygon>
                                                </g>
                                            </g>
                                        </g>
                                    </svg>
                                    <h2 class="brand-text text-primary ml-1"><?php echo $data['short_title'];?></h2>
                                </a>

			<form class="form-horizontal" role="form" method="POST">

			<div class="demo-spacing-0">
                                        <div class="alert alert-primary" role="alert">
                                            <h4 class="alert-heading">PERHATIAN!</h4>
                                            <div class="alert-body">
                                               	Masukkan email / username (boleh salah satu / keduanya).
                                            </div>
                                        </div>

				<div class="form-group">
					<label>Email</label>
					<form class="form-horizontal" method="POST">
						<input type="hidden" name="csrf_token" value="<?php echo $config['csrf_token'] ?>">
						<div class="form-group">
							<div class="col-md-12">
								<input type="text" name="email" class="form-control" placeholder="Email Terdaftar">
							</div>
						</div>
					</form>
					<center><b>ATAU</b></center>
					<label>Username</label>
					<div class="form-group">
						<div class="col-md-12">
							<input type="text" name="username" class="form-control" placeholder="Username Akun">
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-12">
							<!--sitekey google apikey-->
							<small>Memastikan bahwa anda bukan robot.</small>
							<div class="g-recaptcha" data-sitekey="6LeuVJ0cAAAAAATb4TWN7NgYa03ju_XdjqbJ26dk"></div>
							<button type="submit" class="btn btn-danger btn-block" name="login"> Reset</button>
							<hr>
							<div style="text-align: center;">
								<i class="fa fa-user"></i> Sudah ingat password?
								<a href="../auth/login" class="btn btn-success btn-block">Login</a>
							</div>
						</div>                               
					</div>

				</form>
			</div>         
		</div> <!-- end col -->
	</div>
	<!-- end row -->   
	<?php
	require '../lib/footer_login.php';
	?>