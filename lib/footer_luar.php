<!-- Footer -->
</div>
</div>

<footer class="footer">
    <div class="container-fluid" style="text-align: center; text-transform: capitalize;">
        <div class="row">
            <div class="col-md-6">
                Copyright &copy;2021-<?php echo date("Y")?> <a href="http://www.izun-net.com" alt="DDGROUP" title="DDGROUP"> <font color="#0d8bf2">Praspay</font></a>.
            </div>
            <div class="col-md-6">
                <a href="<?php echo $config['web']['url'];?>halaman/tentang-kami" alt="Tentang Kami" title="Tentang Kami"> About </a>
                •
                <a href="<?php echo $config['web']['url'];?>halaman/dukungan-teknologi" alt="Dukungan Teknologi" title="Dukungan Teknologi"> Technology </a>
                •
                <a href="<?php echo $config['web']['url'];?>halaman/dukungan-pembayaran" alt="Dukungan Pembayaran" title="Dukungan Pembayaran"> Payment </a>
                •
                <a href="<?php echo $config['web']['url'];?>halaman/platform-aplikasi" alt="Platform Aplikasi" title="Platform Aplikasi"> Platform </a>
                •
                <a href="<?php echo $config['web']['url'];?>halaman/mitra-dan-jaringan" alt="Mitra dan Jaringan" title="Mitra dan Jaringan"> Partner </a>
                •
                <a href="<?php echo $config['web']['url'];?>halaman/ketentuan-layanan" alt="Ketentuan Layanan" title="Ketentuan Layanan"> Terms </a>
                •
                <a href="<?php echo $config['web']['url'];?>halaman/pertanyaan-umum" alt="Pertanyaan Umum" title="Pertanyaan Umum"> FAQs </a>
            </div>
        </div>
    </div>
</footer>
<!-- End Footer -->

</div>
<!-- end wrapper -->

<!-- App js -->
<script src="/assets/v1/vendor.min.js"></script>
<script src="/assets/v1/jquery.dataTables.min.js"></script>
<script src="/assets/v1/dataTables.bootstrap4.js"></script>
<script src="/assets/v1/dataTables.responsive.min.js"></script>
<script src="/assets/v1/responsive.bootstrap4.min.js"></script>
<script src="/assets/v1/dataTables.buttons.min.js"></script>
<script src="/assets/v1/buttons.bootstrap4.min.js"></script>
<script src="/assets/v1/buttons.html5.min.js"></script>
<script src="/assets/v1/buttons.flash.min.js"></script>
<script src="/assets/v1/buttons.print.min.js"></script>
<script src="/assets/v1/dataTables.keyTable.min.js"></script>
<script src="/assets/v1/dataTables.select.min.js"></script>
<script src="/assets/v1/app.min.js"></script>

<?php
include_once "SEOSecretIDN-schema-all.php";
include_once "SEOSecretIDN-javascript-all.php";
?>

</body>
</html>