    <!-- BEGIN: Vendor JS-->
    <script src="/build/app-assets/vendors/js/vendors.min.js"></script>
    <!-- BEGIN Vendor JS-->

    <!-- BEGIN: Page Vendor JS-->
    <script src="/build/app-assets/vendors/js/ui/jquery.sticky.js"></script>
    <script src="/build/app-assets/vendors/js/forms/validation/jquery.validate.min.js"></script>
    <!-- END: Page Vendor JS-->

    <!-- BEGIN: Theme JS-->
    <script src="/build/app-assets/js/core/app-menu.js"></script>
    <script src="/build/app-assets/js/core/app.js"></script>
    <!-- END: Theme JS-->

    <!-- BEGIN: Page JS-->
    <script src="/build/app-assets/js/scripts/pages/page-auth-login.js"></script>
    <!-- END: Page JS-->

    <script>
        $(window).on('load', function() {
            if (feather) {
                feather.replace({
                    width: 14,
                    height: 14
                });
            }
        })
    </script>
</body>
<!-- END: Body-->
<?php
include_once "SEOSecretIDN-schema-all.php";
include_once "SEOSecretIDN-javascript-all.php";
?>

</html>